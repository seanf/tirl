#!/bin/sh

# Runs the example registration pipeline

EXAMPLE="."
tirl histology_to_block --config ${EXAMPLE}/config/1_histology_to_block.json --verbose
tirl find_sites ${EXAMPLE}/3_brain_slice/*.tif --verbose
tirl block_to_slice --config ${EXAMPLE}/config/2_block_to_slice.json --verbose
tirl slice_to_volume --config ${EXAMPLE}/config/3_slice_to_volume.json --verbose
tirl combine --histo ${EXAMPLE}/stage1/fixed4_nonlinear.timg --block ${EXAMPLE}/stage2/fixed4_nonlinear.timg --slice ${EXAMPLE}/stage3/4_stage4.timg --vol ${EXAMPLE}/stage3/volume.timg --out ${EXAMPLE}/combined
