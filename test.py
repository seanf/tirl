#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


"""
By default this script will test the files in the downloaded source code of
TIRL, not the installed version. Some modules will require the C modules of
TIRL, which therefore need to be compiled for these tests to succeed.

To compile the C modules, run:
python setup.py build_ext --inplace

Then you can run either a single test or all tests:

./test.py
runs all tests of the Tensor Image Registration Library and generates a code
coverage report in the test directory. To run this script you must have the
'coverage' library installed in your conda environment.

./test.py test_chain
runs the tests pertinent to the module chain.py.
No coverage is reported in this case.

"""

import os
import sys

tirlsrc = os.path.join(os.path.dirname(__file__), "src")
testdir = os.path.join(os.path.dirname(__file__), "tests")
sys.path = [tirlsrc, testdir] + sys.path

# Either run specific tests...
tests = sys.argv[1:]
if tests:
    import unittest
    for test in tests:
        testmodule = __import__(test)
        suite = unittest.TestLoader().loadTestsFromModule(testmodule)
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)

# ... or run all tests with coverage
else:
    testsuite = __import__("test_full")
    testsuite.main()
