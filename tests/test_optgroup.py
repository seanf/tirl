import unittest
import numpy as np
from math import radians

from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.rotation import TxRotation2D
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.linear.affine import TxAffine
from tirl.optimisation.optgroup import OptimisationGroup


class TestOptGroupConstruction(unittest.TestCase):

    def test_construction_lineartxs(self):
        # Create transformations
        tx_scale = TxScale(0.5, 0.6)
        tx_rot = TxRotation2D(20, mode="deg")
        tx_trans = TxTranslation(-10, -2.5)
        tx_affine = TxAffine(np.random.rand(2, 3))
        txs = (tx_scale, tx_rot, tx_trans, tx_affine)
        # Create optimisation group
        opt = OptimisationGroup(*txs)
        # Assertions
        self.assertIsInstance(opt, OptimisationGroup,
                              "OptimisationGroup must be constructed.")
        self.assertTupleEqual(opt.elements, txs,
                              "Transformations must not be changed.")
        self.assertIsInstance(opt.metaparameters, dict,
                              "Metaparamters dict must exist.")


class TestOptGroupCopyConstruction(unittest.TestCase):

    def setUp(self):
        tx_scale = TxScale(0.5, 0.6)
        tx_rot = TxRotation2D(20, mode="deg")
        tx_trans = TxTranslation(-10, -2.5)
        tx_affine = TxAffine(np.random.rand(2, 3))
        self.txs = (tx_scale, tx_rot, tx_trans, tx_affine)
        self.opt = OptimisationGroup(*self.txs)

    def test_copy(self):
        optcpy = self.opt.copy()
        self.assertEqual(self.opt, optcpy,
                         "OptimisationGroups must be equal.")
        self.assertIsNot(self.opt, optcpy,
                         "OptimisationGroups must not be identical.")
        for otx, ctx in zip(self.opt.elements, optcpy.elements):
            self.assertIs(otx, ctx, "Transformations must be identical.")


class TestOptGroupParameterChange(unittest.TestCase):

    def setUp(self):
        tx_scale = TxScale(0.5, 0.6)
        tx_rot = TxRotation2D(20, mode="deg")
        tx_trans = TxTranslation(-10, -2.5)
        np.random.seed(42)
        tx_affine = TxAffine(np.random.rand(2, 3))
        self.txs = (tx_scale, tx_rot, tx_trans, tx_affine)
        self.rigid = OptimisationGroup(*self.txs[:-1])
        self.affine = OptimisationGroup(*self.txs)

    def test_parameter_get(self):
        # Get current parameters (without lock)
        expected = np.asarray([0.5, 0.6, radians(20), -10.0, -2.5])
        self.assertTrue(np.allclose(self.rigid.get() - expected, 0, atol=1e-5),
                        "Get() must return the parameters accurately.")

    def test_parameter_set(self):

        orig_affine = self.txs[3].parameters[:]

        # Set new parameters (no lock)
        np.random.seed(22)
        newparams = np.random.rand(5)
        self.rigid.set(newparams)

        self.assertTrue(np.allclose(
            self.rigid.get() - newparams, 0, atol=1e-5),
            "Set() must change the group parameters.")
        # TxRotation2D
        self.assertTrue(np.allclose(
            self.txs[0].parameters[:] - newparams[:2], 0, atol=1e-5),
            "Transformation parameters must change in place.")
        # TxTranslation
        self.assertTrue(np.allclose(
            self.txs[1].parameters[0] - newparams[2], 0, atol=1e-5),
            "Transformation parameters must change in place.")
        # TxAffine
        self.assertTrue(np.allclose(
            self.txs[2].parameters[:] - newparams[3:5], 0, atol=1e-5),
            "Transformation parameters must change in place.")
        # TxAffine
        self.assertTrue(np.allclose(
            self.txs[3].parameters[:] - orig_affine, 0, atol=1e-5),
            "Unchanged parameters must not change.")

        # Verify the changes in the auxiliary optimisation group
        self.assertTrue(np.allclose(
            self.affine.get()[:5] - self.rigid.get(), 0, atol=1e-5),
            "Auxiliary optimisation group must also change.")
        self.assertTrue(np.allclose(
            self.affine.get()[5:] - orig_affine, 0, atol=1e-5),
            "Auxuliary optimisation group must also change.")

    def test_parameter_update(self):

        orig_rigid = self.rigid.get()
        orig_affine = self.txs[3].parameters[:]

        # Set new parameters (no lock)
        np.random.seed(22)
        delta = np.random.rand(5)
        self.rigid.update(delta)
        expected_rigid = orig_rigid + delta

        self.assertTrue(np.allclose(
            self.rigid.get() - delta - orig_rigid, 0, atol=1e-5),
            "Set() must change the group parameters.")
        # TxRotation2D
        self.assertTrue(np.allclose(
            self.txs[0].parameters[:] - delta[:2] - orig_rigid[:2], 0,
            atol=1e-5), "Transformation parameters must change in place.")
        # TxTranslation
        self.assertTrue(np.allclose(
            self.txs[1].parameters[0] - delta[2] - orig_rigid[2], 0,
            atol=1e-5), "Transformation parameters must change in place.")
        # TxAffine
        self.assertTrue(np.allclose(
            self.txs[2].parameters[:] - delta[3:5] - orig_rigid[3:5], 0,
            atol=1e-5), "Transformation parameters must change in place.")
        # TxAffine
        self.assertTrue(np.allclose(
            self.txs[3].parameters[:] - orig_affine, 0, atol=1e-5),
            "Unchanged parameters must not change.")

        # Verify the changes in the auxiliary optimisation group
        self.assertTrue(np.allclose(
            self.affine.get()[:5] - self.rigid.get(), 0, atol=1e-5),
            "Auxiliary optimisation group must also change.")
        self.assertTrue(np.allclose(
            self.affine.get()[5:] - orig_affine, 0, atol=1e-5),
            "Auxuliary optimisation group must also change.")


class TestOptGroupBoundsChange(unittest.TestCase):

    def setUp(self):
        tx_scale = TxScale(0.5, 0.6)
        tx_scale.parameters.set_lower_bounds((0.3, 0.45))
        tx_scale.parameters.set_upper_bounds((1.22, 1.3))

        tx_rot = TxRotation2D(20, mode="deg")
        tx_rot.parameters.set_lower_bounds(radians(10))
        tx_rot.parameters.set_upper_bounds(radians(30))

        tx_trans = TxTranslation(-10, -2.5)
        tx_trans.parameters.set_lower_bounds((-20, -21.1))
        tx_trans.parameters.set_upper_bounds((22.4, 20))

        np.random.seed(42)
        tx_affine = TxAffine(np.random.rand(2, 3))
        tx_affine.parameters.set_lower_bounds(np.zeros(6))
        tx_affine.parameters.set_upper_bounds(np.ones(6))

        self.txs = (tx_scale, tx_rot, tx_trans, tx_affine)
        self.rigid = OptimisationGroup(*self.txs[:-1])
        self.affine = OptimisationGroup(*self.txs)

    def test_get_bounds(self):
        # No lock
        lb = self.rigid.get_lower_bounds()
        ub = self.rigid.get_upper_bounds()
        bounds = self.rigid.get_bounds()

        expected_lb = np.asarray([0.3, 0.45, radians(10), -20, -21.1])
        expected_ub = np.asarray([1.22, 1.3, radians(30), 22.4, 20])
        expected_bounds = np.stack([expected_lb, expected_ub], axis=-1)

        self.assertTrue(np.allclose(lb - expected_lb, 0, atol=1e-5),
                        "Get() must return lower bounds precisely.")
        self.assertTrue(np.allclose(ub - expected_ub, 0, atol=1e-5),
                        "Get() must return upper bounds precisely.")
        self.assertTrue(np.allclose(bounds - expected_bounds, 0, atol=1e-5),
                        "Get() must return upper bounds precisely.")

    def test_set_bounds(self):
        # No lock
        lb = self.rigid.get_lower_bounds()
        ub = self.rigid.get_upper_bounds()
        bounds = self.rigid.get_bounds()

        new_lb = np.asarray([0.25, 0.275, radians(11.1), -21, -30.1])
        new_ub = np.asarray([1.13, 1.54, radians(35), 22.5, 40])

        # Lower bounds
        self.rigid.set_lower_bounds(new_lb)
        self.assertTrue(np.allclose(
            self.rigid.get_lower_bounds() - new_lb, 0, atol=1e-5),
            "Group-level lower bounds must be udpated.")
        self.assertTrue(np.allclose(
            self.txs[0].parameters.get_lower_bounds() - new_lb[:2], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.txs[1].parameters.get_lower_bounds()[0] - new_lb[2], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.txs[2].parameters.get_lower_bounds() - new_lb[3:5], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.affine.get_lower_bounds()[:5] - self.rigid.get_lower_bounds(),
            0, atol=1e-5), "Auxiliary optimisation group must also get updated."
        )

        # Upper bounds
        self.rigid.set_upper_bounds(new_ub)
        self.assertTrue(np.allclose(
            self.rigid.get_upper_bounds() - new_ub, 0, atol=1e-5),
            "Group-level lower bounds must be udpated.")
        self.assertTrue(np.allclose(
            self.txs[0].parameters.get_upper_bounds() - new_ub[:2], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.txs[1].parameters.get_upper_bounds()[0] - new_ub[2], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.txs[2].parameters.get_upper_bounds() - new_ub[3:5], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.affine.get_upper_bounds()[:5] - self.rigid.get_upper_bounds(),
            0, atol=1e-5), "Auxiliary optimisation group must also get updated."
        )

        # Bounds (both lower and upper)
        new_bounds = np.stack([lb, ub], axis=-1)
        self.rigid.set_bounds(lb, ub)
        self.assertTrue(np.allclose(
            self.rigid.get_bounds() - new_bounds, 0, atol=1e-5),
            "Group-level bounds must be udpated.")
        self.assertTrue(np.allclose(
            self.txs[0].parameters.get_bounds() - new_bounds[:2], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.txs[1].parameters.get_bounds()[0] - new_bounds[2], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.txs[2].parameters.get_bounds() - new_bounds[3:5], 0,
            atol=1e-5), "Transformations must be modified in place.")
        self.assertTrue(np.allclose(
            self.affine.get_bounds()[:5] - self.rigid.get_bounds(),
            0, atol=1e-5), "Auxiliary optimisation group must also get updated."
        )


if __name__ == '__main__':
    unittest.main()
