#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import os
import unittest
import warnings
import numpy as np


from tirl.parameters import ParameterVector
from tirl.parameters import ParameterGroup


warnings.filterwarnings("default")


def resources(*args):
    import tirl
    return tirl.sharedir("resources", *args)


class TestParameterVector(unittest.TestCase):

    def test_construction_from_scalararray(self):
        p = ParameterVector(1, 2, 3, 4)
        self.assertIsInstance(p, ParameterVector)
        self.assertListEqual(p.parameters.tolist(), [1, 2, 3, 4])

    def test_locking_and_unlocking(self):
        p = ParameterVector(1, 2, 3, 4)
        p.lock(1, 2)
        self.assertSetEqual(p.locked, {1, 2})
        self.assertSetEqual(p.free, {0, 3})
        p.unlock(1, 2)
        self.assertSetEqual(p.locked, set())
        self.assertSetEqual(p.free, {0, 1, 2, 3})
        p.lock()
        self.assertSetEqual(p.locked, {0, 1, 2, 3})
        self.assertSetEqual(p.free, set())
        p.unlock()
        self.assertSetEqual(p.locked, set())
        self.assertSetEqual(p.free, {0, 1, 2, 3})

    def test_getting(self):
        p = ParameterVector(np.arange(20))
        p.lock(slice(None, None, 2))
        self.assertSetEqual(set(range(0, 20, 2)), p.locked)
        self.assertTrue(np.allclose(p.get(), np.arange(20)[1::2]))

    def test_updating(self):
        p = ParameterVector(np.arange(20))
        p.lock(3)
        self.assertSetEqual(p.locked, {3})
        orig_p = p.parameters.copy()

        # Update method
        p.update(10)
        self.assertFalse(np.all(p.parameters == orig_p + 10))
        self.assertTrue(np.all(p.parameters[:3] == orig_p[:3] + 10))
        self.assertTrue(np.all(p.parameters[4:] == orig_p[4:] + 10))
        p.update(-np.ones(19) * 10)
        self.assertTrue(np.all(p.parameters == orig_p))

        # Using ndarray.__setitem__()
        p.parameters[:] += 10
        self.assertTrue(np.all(p.parameters == orig_p + 10))
        p.parameters[:] -= 10
        self.assertTrue(np.all(p.parameters == orig_p))

        # Using reflective ParameterVector.ufunc
        p0 = p
        p += 10
        self.assertIs(p, p0)
        self.assertTrue(np.all(p.parameters == orig_p + 10))
        p.parameters[:] -= 10
        self.assertTrue(np.all(p.parameters == orig_p))

        # Using non-reflective ParameterVector.ufunc
        p0 = p
        orig_p = p.parameters
        q = p + 10
        self.assertIsNot(q, p0)
        self.assertTrue(np.all(q.parameters == orig_p + 10))

    def test_setting(self):
        p = ParameterVector(np.arange(20))
        p.lock(slice(None, None, 2))
        self.assertSetEqual(set(range(0, 20, 2)), p.locked)
        self.assertTrue(np.allclose(p.get(), np.arange(20)[1::2]))
        p.set(np.zeros(10))
        self.assertTrue(np.allclose(p.get(), 0))
        self.assertTrue(np.allclose(p[1::2], 0))
        p.set(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        self.assertTrue(np.allclose(p.get(), range(1, 11)))

    def test_copy(self):
        p = ParameterVector(range(20))
        p.dtype = np.float16
        self.assertEqual(p.dtype, np.float16)
        p.lock(2, 4, 6, 8, 10, 12)
        self.assertSetEqual(p.locked, {2, 4, 6, 8, 10, 12})
        name = p.name
        p.set_bounds({7: (-10, 10)})
        self.assertTrue(np.all(p.get_bounds(7) == [-10, 10]))

        q = p.copy()
        self.assertIsInstance(q, ParameterVector)
        self.assertIsNot(q, p)
        self.assertEqual(q.dtype, np.float16)
        self.assertSetEqual(q.locked, {2, 4, 6, 8, 10, 12})
        self.assertTrue(np.all(p.get_lower_bounds() == q.get_lower_bounds()))
        self.assertTrue(np.all(p.get_upper_bounds() == q.get_upper_bounds()))
        self.assertTrue(np.all(q.get_bounds(7) == [-10, 10]))
        self.assertEqual(q.name, name)

    def test_saveload(self):
        p = ParameterVector(range(10))
        path = resources("tirlfiles", "params.p")
        p.save(path, overwrite=True)
        import tirl
        q = tirl.load(path)
        self.assertTrue(np.all(p == q))  # parameters only
        self.assertEqual(p.dtype, q.dtype)
        self.assertEqual(p.name, q.name)
        self.assertSetEqual(p.locked, q.locked)
        self.assertTrue(np.all(p.lower_bounds == q.lower_bounds))
        self.assertTrue(np.all(p.upper_bounds == q.upper_bounds))

    def test_get_set_bounds(self):
        p = ParameterVector(range(10))

        # get_default_bounds()
        expected = p.get_default_bounds()
        self.assertTrue(np.allclose(p.get_default_bounds(Ellipsis), expected))
        self.assertTrue(
            np.allclose(p.get_default_bounds(slice(None)), expected))
        expected = np.stack(expected, axis=-1)
        # get_bounds()
        self.assertTrue(np.allclose(p.get_bounds(), expected))
        self.assertTrue(np.allclose(p.get_bounds(Ellipsis), expected))
        self.assertTrue(np.allclose(p.get_bounds(slice(None)), expected))

        # get_lower_bounds()
        expected = p.get_default_bounds(lower=True, upper=False)
        self.assertTrue(np.allclose(p.get_lower_bounds(), expected))
        self.assertTrue(np.allclose(p.get_lower_bounds(Ellipsis), expected))
        self.assertTrue(np.allclose(p.get_lower_bounds(slice(None)), expected))

        # get_upper_bounds()
        expected = p.get_default_bounds(lower=False, upper=True)
        self.assertTrue(np.allclose(p.get_upper_bounds(), expected))
        self.assertTrue(np.allclose(p.get_upper_bounds(Ellipsis), expected))
        self.assertTrue(np.allclose(p.get_upper_bounds(slice(None)), expected))

        # Set default parameters
        p.set_bounds()
        self.assertTrue(np.all(p.lower_bounds == -np.inf))
        self.assertTrue(np.all(p.upper_bounds == np.inf))

        # Set both lower and upper parameters
        q = ParameterVector(np.zeros(5))
        lb = [-1, -2, -1.5, -7, -0.1]
        ub = [0.8, 1.3, 2.1, 11, 0.3]
        q.set_bounds(lb, ub)
        self.assertTrue(np.allclose(q.lower_bounds, lb))
        self.assertTrue(np.allclose(q.upper_bounds, ub))
        q.set_bounds({0: (-100, 100), 4: (-50, 25)})
        self.assertTrue(np.allclose(q.get_bounds((0, 4)), [[-100, 100],
                                                           [-50, 25]]))
        expected = np.asarray([[-100, -2, -1.5, -7, -50],
                               [100, 1.3, 2.1, 11, 25]]).T
        self.assertTrue(np.allclose(q.get_bounds(Ellipsis), expected))
        self.assertTrue(np.allclose(q.get_bounds(slice(1, None, 2)),
                                    expected[1::2, :]))

        # Set lower bounds
        z = ParameterVector(np.zeros(5))
        # Global
        z.set_lower_bounds(-100)
        self.assertTrue(np.allclose(z.lower_bounds, -100))
        # Sequence: List
        z.set_lower_bounds(lb)
        self.assertTrue(np.allclose(z.lower_bounds, lb))
        # Sequence: Tuple
        z.set_lower_bounds((-10, -20, -10, -20, -10))
        self.assertTrue(np.allclose(z.get_lower_bounds(0, 1), [-10, -20]))
        # Array
        z.set_lower_bounds(np.asarray(lb) - 1)
        self.assertTrue(np.allclose(z.lower_bounds, np.asarray(lb) - 1))
        # Dict
        z.set_lower_bounds({2: -40})
        self.assertEqual(z.get_lower_bounds(2), -40)

        # Set upper bounds
        # Global scalar
        z.set_upper_bounds(100)
        self.assertTrue(np.allclose(z.upper_bounds, 100))
        # Sequence: List
        z.set_upper_bounds(ub)
        self.assertTrue(np.allclose(z.upper_bounds, ub))
        # Sequence: Tuple
        z.set_upper_bounds((10, 20, 10, 20, 10))
        self.assertTrue(np.allclose(z.get_upper_bounds(0, 1), [10, 20]))
        # Array
        z.set_upper_bounds(np.asarray(ub) + 1)
        self.assertTrue(np.allclose(z.upper_bounds, np.asarray(ub) + 1))
        # Dict
        z.set_upper_bounds({3: 40})
        self.assertEqual(z.get_upper_bounds(3), 40)

        # Lock parameter because of 0 range
        warnchk_msg = "Must warn about parameter lock."
        f = ParameterVector(np.zeros(5))
        f.set_lower_bounds([0, 0, 0, 0, 0])
        with self.assertWarns(UserWarning, msg=warnchk_msg):
            f.set_upper_bounds([0, 1, 0, 2, 10])
        self.assertSetEqual(f.locked, {0, 2})

        # Redirected lower and upper setter methods
        g = ParameterVector(np.ones(12))
        g.lower_bounds = -np.ones(12) * 10
        g.upper_bounds = np.ones(12) * 10

        # Automatic locking on closing bounds
        p = ParameterVector(np.ones(14) * 5)
        self.assertSetEqual(p.locked, set())
        with self.assertWarns(UserWarning, msg=warnchk_msg):
            p.set_bounds({6: (5, 5)})
        self.assertSetEqual(p.locked, {6})
        p.unlock()
        # lower bounds
        self.assertSetEqual(p.locked, set())
        with self.assertWarns(UserWarning, msg=warnchk_msg):
            p.set_lower_bounds({6: (5)})
        self.assertSetEqual(p.locked, {6})
        p.unlock()
        # upper bounds
        self.assertSetEqual(p.locked, set())
        with self.assertWarns(UserWarning, msg=warnchk_msg):
            p.set_upper_bounds({6: 5})
        self.assertSetEqual(p.locked, {6})
        p.unlock()

        # Global lower and upper bounds
        p = ParameterVector(np.zeros(6))
        p.set_bounds(-1, 1)

        # Direct setting
        # (this should not raise bounds error or lock parameters)
        g.lower_bounds[:] = 5
        self.assertTrue(np.allclose(g.lower_bounds, 5))
        self.assertTrue(np.all(g.lower_bounds > g.parameters))
        g.upper_bounds[:] = -1
        self.assertTrue(np.allclose(g.upper_bounds, -1))
        self.assertTrue(np.all(g.upper_bounds < g.parameters))


class TestParameterVectorOperations(unittest.TestCase):

    def test_eq(self):
        params = np.asarray([1, 2, 3, 4, 5])
        pv = ParameterVector(*params)
        pvc = pv.copy()
        self.assertIsNot(pv, pvc, "Copy must not be identical.")
        self.assertEqual(pv, pvc, "Copy must be equal.")

        # Disrupt equality by changing bounds
        pvb = pvc.copy()
        pvb.set_lower_bounds({2: 0.45})
        self.assertEqual(pv, pvc, "Copy must be equal.")
        self.assertNotEqual(pv, pvb, "Different bounds must disrupt equality.")

        # Disrupt equality by changing lock state
        pvl = pvc.copy()
        pvl.lock(2)
        self.assertEqual(pv, pvc, "Copy must be equal.")
        self.assertNotEqual(pv, pvl, "Different locks must disrupt equality.")


class TestParameterGroup(unittest.TestCase):

    def test_construction(self):
        p = ParameterVector(1, 2, 3, 4)
        q = ParameterVector(np.arange(20))
        g = ParameterGroup(p, q)
        self.assertIsInstance(g, ParameterGroup)

    def test_reassignment(self):
        # Construction
        p = ParameterVector(1, 2, 3, 4)
        q = ParameterVector(np.arange(20))
        g = ParameterGroup(p, q)

        # Change in index offset
        self.assertEqual(p.offset, 0)
        self.assertEqual(q.offset, p.size)

        # Detect changes in both members
        orig_p = p.parameters.copy()
        orig_q = q.parameters.copy()
        orig_g = g.parameters.copy()
        self.assertTrue(np.allclose(orig_g, np.concatenate((orig_p, orig_q))))
        p.update(-10)
        self.assertTrue(np.all(p.parameters == orig_p - 10))
        self.assertTrue(np.all(g.parameters[:4] == orig_p - 10))
        g.update(10)
        self.assertTrue(np.all(p.parameters == orig_p))
        self.assertTrue(np.all(g.parameters[4:] == orig_q + 10))

        # Linked changes on direct parameter assignment
        p[1] = -100
        self.assertEqual(g[1], -100)

    def test_group_in_group(self):
        # Construction
        p = ParameterVector(1, 2, 3, 4)
        q = ParameterVector(np.arange(20))
        g = ParameterGroup(p, q)
        r = ParameterVector(range(40))
        gg = ParameterGroup(r, g)

        # Changes in index offset
        self.assertEqual(gg.offset, 0)
        self.assertEqual(r.offset, 0)
        self.assertEqual(g.offset, 40)
        self.assertEqual(p.offset, 40)
        self.assertEqual(q.offset, 44)

        # Detect changes in members at multiple levels
        orig_p = p.parameters.copy()
        orig_q = q.parameters.copy()
        orig_r = r.parameters.copy()
        orig_gg = gg.parameters.copy()
        self.assertTrue(np.all(
            orig_gg == np.concatenate((orig_r, orig_p, orig_q))))

        p.update(-10)
        self.assertTrue(np.all(p.parameters == orig_p - 10))
        self.assertTrue(np.all(g.parameters[:4] == orig_p - 10))
        self.assertTrue(np.all(gg.parameters[40:44] == orig_p - 10))

        orig_g = g.parameters.copy()
        orig_gg = gg.parameters.copy()
        gg.update(10)
        self.assertTrue(np.all(gg.parameters == orig_gg + 10))
        self.assertTrue(np.all(p.parameters == orig_p))
        self.assertTrue(np.all(q.parameters == orig_q + 10))
        self.assertTrue(np.all(g.parameters[4:] == orig_q + 10))
        self.assertTrue(np.all(g.parameters == orig_g + 10))
        self.assertTrue(np.all(r.parameters == orig_r + 10))

        # Linked changes on direct parameter assignment
        p[1] = -100
        self.assertEqual(g[1], -100)

    def test_locking_and_unlocking(self):
        a = ParameterVector(1, 2)
        b = ParameterVector(3, 4)
        p = ParameterGroup(a, b)
        p.lock(1, 2)
        self.assertSetEqual(p.locked, {1, 2})
        self.assertSetEqual(p.free, {0, 3})
        self.assertEqual(p.count_locked(), 2)
        self.assertEqual(p.count_free(), 2)
        self.assertSetEqual(a.locked, {1})
        self.assertSetEqual(a.free, {0})
        self.assertSetEqual(b.locked, {0})
        self.assertSetEqual(b.free, {1})
        p.unlock(1, 2)
        self.assertSetEqual(p.locked, set())
        self.assertSetEqual(p.free, {0, 1, 2, 3})
        self.assertEqual(p.count_locked(), 0)
        self.assertEqual(p.count_free(), 4)
        self.assertSetEqual(a.locked, set())
        self.assertSetEqual(a.free, {0, 1})
        self.assertSetEqual(b.locked, set())
        self.assertSetEqual(b.free, {0, 1})

    def test_updating(self):
        p = ParameterVector(1, 2, 3, 4)
        q = ParameterVector(np.arange(20))
        g = ParameterGroup(p, q)
        g.lock(3)
        self.assertSetEqual(g.locked, {3})
        orig_g = g.parameters.copy()

        # Update method
        g.update(10)
        self.assertFalse(np.all(g.parameters == orig_g + 10))
        self.assertTrue(np.all(g.parameters[:3] == orig_g[:3] + 10))
        self.assertTrue(np.all(g.parameters[4:] == orig_g[4:] + 10))
        g.update(-10)
        self.assertTrue(np.all(g.parameters == orig_g))

        # Using ndarray.__setitem__()
        g.parameters[:] += 10
        self.assertTrue(np.all(g.parameters == orig_g + 10))
        g.parameters[:] -= 10
        self.assertTrue(np.all(g.parameters == orig_g))

        # Using ParameterGroup.ufunc
        g += 10
        self.assertTrue(np.all(g.parameters == orig_g + 10))
        g.parameters[:] -= 10
        self.assertTrue(np.all(g.parameters == orig_g))

    def test_copy(self):
        p = ParameterVector(range(20), dtype="<f16")
        q = ParameterVector(range(20, 45), dtype="<f16")
        self.assertEqual(p.dtype, np.float128)
        self.assertEqual(p.dtype, np.float128)
        g = ParameterGroup(p, q)
        self.assertIsInstance(g, ParameterGroup)
        self.assertEqual(p.offset, 0)
        self.assertEqual(q.offset, 20)
        g.dtype = np.float16
        self.assertEqual(g.dtype, "<f2")
        self.assertEqual(p.dtype, "<f2")
        self.assertEqual(q.dtype, "<f2")
        g.lock(16, 18, 20, 22, 24, 26)
        self.assertSetEqual(g.locked, {16, 18, 20, 22, 24, 26})
        self.assertSetEqual(p.locked, {16, 18})
        self.assertSetEqual(q.locked, {0, 2, 4, 6})
        name = g.name
        g.set_bounds({23: (-100, 100)})

        gc = g.copy()
        self.assertIsInstance(gc, ParameterVector)  # not ParameterGroup!
        self.assertIsNot(gc, g)
        self.assertEqual(gc.dtype, np.float16)
        self.assertSetEqual(gc.locked, {16, 18, 20, 22, 24, 26})
        self.assertTrue(np.all(gc.get_lower_bounds() == g.get_lower_bounds()))
        self.assertTrue(np.all(gc.get_upper_bounds() == g.get_upper_bounds()))
        self.assertTrue(np.all(gc.get_bounds(23) == [-100, 100]))
        self.assertEqual(gc.name, name)

    def test_saveload(self):
        p = ParameterVector(range(10))
        q = ParameterVector(range(25))
        r = ParameterVector(range(5))
        g = ParameterGroup(p, q, r)
        path = resources("tirlfiles", "params.pg")
        g.save(path, overwrite=True)
        import tirl
        f = tirl.load(path)
        self.assertTrue(np.all(f == g))  # parameters only
        self.assertEqual(f.dtype, g.dtype)
        self.assertEqual(f.name, g.name)
        self.assertSetEqual(f.locked, g.locked)
        self.assertTrue(np.all(f.lower_bounds == g.lower_bounds))
        self.assertTrue(np.all(f.upper_bounds == g.upper_bounds))

    def test_set_bounds(self):
        a = ParameterVector(range(10))
        b = ParameterVector(range(20))
        p = ParameterGroup(a, b)

        # Set default parameters
        p.set_bounds()
        self.assertTrue(np.all(p.lower_bounds == -np.inf))
        self.assertTrue(np.all(p.upper_bounds == np.inf))

        # Set both lower and upper parameters
        a = ParameterVector(np.zeros(2))
        b = ParameterVector(np.zeros(3))
        q = ParameterGroup(a, b)
        lb = [-1, -2, -1.5, -7, -0.1]
        ub = [1.1, 1.3, 2.1, 11, 1.3]
        q.set_bounds(lb, ub)
        self.assertTrue(np.allclose(q.lower_bounds, lb))
        self.assertTrue(np.allclose(q.upper_bounds, ub))

        # Set lower and upper bounds separately
        a = ParameterVector(np.ones(3))
        b = ParameterVector(np.ones(2))
        z = ParameterGroup(a, b)
        z.set_lower_bounds(lb)
        self.assertTrue(np.allclose(z.lower_bounds, lb))
        z.set_upper_bounds(ub)
        self.assertTrue(np.allclose(z.upper_bounds, ub))

        # Lock parameter because of 0 range
        warnchk_msg = "Must warn about parameter lock."
        a = ParameterVector(np.zeros(3))
        b = ParameterVector(np.zeros(2))
        f = ParameterGroup(a, b)
        f.set_lower_bounds([0, 0, 0, 0, 0])
        with self.assertWarns(UserWarning, msg=warnchk_msg):
            f.set_upper_bounds([0, 1, 0, 2, 10])
        self.assertSetEqual(f.locked, {0, 2})

        # Redirected lower and upper setter methods
        a = ParameterVector(np.ones(5))
        b = ParameterVector(np.ones(7))
        g = ParameterGroup(a, b)
        g.lower_bounds = -np.ones(12) * 10
        g.upper_bounds = np.ones(12) * 10

        # Direct setting
        # (this should not raise bounds error or lock parameters)
        g.lower_bounds[:] = 5
        self.assertTrue(np.allclose(g.lower_bounds, 5))
        self.assertTrue(np.all(g.lower_bounds > g.parameters))
        g.upper_bounds[:] = -1
        self.assertTrue(np.allclose(g.upper_bounds, -1))
        self.assertTrue(np.all(g.upper_bounds < g.parameters))


if __name__ == '__main__':  #pragma: no cover
    unittest.main()
