#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

import tirl
from tirl.beta import beta_function, BetaClassMeta
from tirl.timage import TImage
from tirl.tirlobject import TIRLObject


class TestImage(unittest.TestCase):

    def test_testimg(self):
        img = tirl.testimg()
        self.assertIsInstance(img, TImage, "Test Image must be a TImage.")
        self.assertGreaterEqual(img.vdim, 2, "Image dimensions must be >=2.")
        self.assertTrue(np.any(np.abs(img.data) > 0),
                        "Test Image must not be all-zeros.")


@beta_function
def func_that_is_beta():
    return True


class BetaClass(TIRLObject, metaclass=BetaClassMeta):
    def __init__(self):
        super(BetaClass, self).__init__()
        self.value = True


class TestBetaMarkers(unittest.TestCase):

    @beta_function
    def method_that_is_beta(self):
        return True

    def test_beta_function(self):
        warnchkmsg = "Beta function should display a warning."
        with self.assertWarns(UserWarning, msg=warnchkmsg):
            retval = func_that_is_beta()
        self.assertTrue(
            retval, "Beta function must be executed despite the warning.")

    def test_beta_method(self):
        warnchkmsg = "Beta method should display a warning."
        with self.assertWarns(UserWarning, msg=warnchkmsg):
            retval = func_that_is_beta()
        self.assertTrue(
            retval, "Beta method must be executed despite the warning.")

    def test_beta_class(self):
        warnchkmsg = "Beta class should display a warning on instantiation."
        with self.assertWarns(UserWarning, msg=warnchkmsg):
            instance = BetaClass()
        self.assertIsInstance(instance, BetaClass, "Object must be created.")
        self.assertTrue(instance.value, "Object attributes must be the same.")


if __name__ == '__main__':
    unittest.main()
