#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import unittest
import os
import coverage


# DEFINITIONS


# IMPLEMENTATION

def main():
    """Runs the unit tests with coverage."""
    # Source: https://www.programcreek.com/python/example/153/unittest
    # .TextTestRunner

    testdir = os.path.dirname(__file__)
    cov = coverage.coverage(os.path.join(testdir, ".coverage"))
    cov.start()
    unit_tests = unittest.TestLoader().discover(testdir, "test_*.py")
    result = unittest.TextTestRunner(verbosity=2).run(unit_tests)
    if result.wasSuccessful():
        cov.stop()
        cov.save()
        print('Coverage Summary:')
        cov.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'report/coverage')
        cov.html_report(directory=covdir)
        print('HTML version: file://%s/index.html' % covdir)
        cov.erase()
        return 0
    return 1


if __name__ == "__main__":
    main()
