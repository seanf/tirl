#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

import tirl
from tirl.timage import TImage


class TestResample(unittest.TestCase):

    def test_resample(self):
        img = tirl.testimg()
        self.assertIsInstance(img, TImage)
        vc0 = img.domain.get_voxel_coordinates()
        ic0 = img.domain.get_intermediate_coordinates()
        pc0 = img.domain.get_physical_coordinates()
        img.resample(0.25, copy=False)
        vc = img.domain.get_voxel_coordinates()
        ic = img.domain.get_intermediate_coordinates()
        pc = img.domain.get_physical_coordinates()
        self.assertFalse(np.allclose(vc0[[0, -1], :], vc[[0, -1], :], atol=4))
        self.assertTrue(np.allclose(ic0[[0, -1], :], ic[[0, -1], :], atol=4))
        self.assertTrue(np.allclose(pc0[[0, -1], :], pc[[0, -1], :], atol=4))


if __name__ == '__main__':
    unittest.main()
