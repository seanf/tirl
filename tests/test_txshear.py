#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

from tirl.transformations.linear.shear import TxShear


class MyTestCase(unittest.TestCase):

    def test_mat2indexedparams(self):
        # Identity matrix
        identity = np.asarray([[1, 0, 0],
                               [0, 1, 0],
                               [0, 0, 1]])
        params, indices = TxShear._mat2params(identity)
        self.assertTrue(np.allclose(params, np.asarray([0, 0, 0])))
        self.assertTupleEqual(tuple(indices), (1, 2, 5))

        # Example matrix
        example = np.asarray([[1, -0.15, 0],
                              [0, 1, 0.23],
                              [0.001, 0, 1]])
        params, indices = TxShear._mat2params(example)
        self.assertTrue(np.allclose(params, np.asarray([-0.15, 0.001, 0.23])))
        self.assertTupleEqual(tuple(indices), (1, 6, 5))

    def test2d_construction_upper_tri_params(self):
        tx = TxShear(2, -0.5)
        self.assertIsInstance(tx, TxShear)
        self.assertTupleEqual(tuple(tx.parameters), (-0.5,))

        self.assertEqual(tx.dim, 2)
        indices = tx.metaparameters.get("indices")
        self.assertEqual(indices, 1)
        # parammask = tx.metaparameters.get("parameter_mask")
        # expected = np.zeros((2, 2), dtype=np.bool)
        # expected.flat[indices] = True
        # self.assertTrue(np.all(parammask == expected))

    def test2d_construction_lower_tri_params(self):
        tx = TxShear(2, lower=(-0.5,))
        self.assertIsInstance(tx, TxShear)
        self.assertTupleEqual(tuple(tx.parameters), (-0.5,))

        self.assertEqual(tx.dim, 2)
        indices = tx.metaparameters.get("indices")
        self.assertEqual(indices, 2)
        # parammask = tx.metaparameters.get("parameter_mask")
        # expected = np.zeros((2, 2), dtype=np.bool)
        # expected.flat[indices] = True
        # self.assertTrue(np.all(parammask == expected))

    def test3d_construction_upper_tri_params(self):
        tx = TxShear(3, -0.1, 0.2, 0.0051)
        self.assertIsInstance(tx, TxShear)
        self.assertTrue(np.allclose(tx.parameters, [-0.1, 0.2, 0.0051]))

        self.assertEqual(tx.dim, 3)
        indices = tx.metaparameters.get("indices")
        self.assertTupleEqual(tuple(indices), (1, 2, 5))
        # parammask = tx.metaparameters.get("parameter_mask")
        # expected = np.zeros((3, 3), dtype=np.bool)
        # expected.flat[indices] = True
        # self.assertTrue(np.all(parammask == expected))

    def test3d_construction_lower_tri_params(self):
        tx = TxShear(3, lower=(-0.1, 0.2, 0.0051))
        self.assertIsInstance(tx, TxShear)
        self.assertTrue(np.allclose(tx.parameters, (-0.1, 0.2, 0.0051)))
        self.assertEqual(tx.dim, 3)
        indices = tx.metaparameters.get("indices")
        self.assertTupleEqual(tuple(indices), (3, 6, 7))
        # parammask = tx.metaparameters.get("parameter_mask")
        # expected = np.zeros((3, 3), dtype=np.bool)
        # expected.flat[indices] = True
        # self.assertTrue(np.all(parammask == expected))


if __name__ == '__main__':
    unittest.main()
