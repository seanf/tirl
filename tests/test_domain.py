#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

from tirl.domain import Domain
from tirl import settings as ts
from tirl.transformations.basic.direct import TxDirect
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.translation import TxTranslation


# DEFINITIONS

from tirl.constants import *

ENABLE_HDD_TESTS = True
EPS = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps


# IMPLEMENTATION

class TestConstruction(unittest.TestCase):

    def test_construction_compact_voxeldomains(self):
        d = Domain((200, 300))
        self.assertIsInstance(d, Domain)
        self.assertTupleEqual(d.shape, (200, 300))
        self.assertTrue(d.iscompact)

    def test_construction_noncompact_voxeldomains(self):
        np.random.seed(42)
        d = Domain(np.random.rand(16, 2))
        self.assertIsInstance(d, Domain)
        self.assertFalse(d.iscompact)
        self.assertTupleEqual(d.shape, (16, 1))
        self.assertIsInstance(d.offset[0], TxDirect)


class TestVoxelCoordinates(unittest.TestCase):

    def setUp(self) -> None:
        self.compact = Domain((200, 300))
        self.rand = np.random.rand(16, 2)
        self.noncompact = Domain(self.rand)

    def test_get_all_voxel_coordinates(self):
        # Compact
        voxels = self.compact.get_voxel_coordinates()
        self.assertTupleEqual(voxels.shape, (200 * 300, 2))
        self.assertTrue(np.allclose(voxels[0] - [0, 0], 0))
        self.assertTrue(np.allclose(voxels[-1] - [199, 299], 0))
        self.assertEqual(voxels.dtype, np.int16)

        # Non-compact
        voxels = self.noncompact.get_voxel_coordinates(dtype=np.int16)
        self.assertTupleEqual(voxels.shape, (16, 2))
        self.assertTrue(np.allclose(voxels[0] - self.rand[0], 0, atol=EPS))
        self.assertTrue(np.allclose(voxels[-1] - self.rand[15], 0, atol=EPS))
        self.assertEqual(voxels.dtype, ts.DEFAULT_FLOAT_TYPE)

    def test_get_linear_chunk_voxel_coordinates(self):
        # Compact
        voxels = self.compact.get_voxel_coordinates(
            chunk=(200, 500), dtype=np.uint32)
        self.assertTupleEqual(voxels.shape, (300, 2))
        self.assertTrue(np.allclose(voxels[0] - [0, 200], 0))
        self.assertTrue(np.allclose(voxels[-1] - [1, 199], 0))
        self.assertEqual(voxels.dtype, np.uint32)

        # Non-compact
        voxels = self.noncompact.get_voxel_coordinates(chunk=(8, 12))
        self.assertTupleEqual(voxels.shape, (4, 2))
        self.assertTrue(np.allclose(voxels[0] - self.rand[8], 0, atol=EPS))
        self.assertTrue(np.allclose(voxels[-1] - self.rand[11], 0, atol=EPS))
        self.assertEqual(voxels.dtype, ts.DEFAULT_FLOAT_TYPE)

    def test_get_multiindex_voxel_coordinates(self):
        # Compact
        voxels = self.compact.get_voxel_coordinates(
            chunk=(slice(20, 40), slice(100, 150)), dtype=np.uint32)
        self.assertTupleEqual(voxels.shape, (1000, 2))
        self.assertTrue(np.allclose(voxels[0] - [20, 100], 0))
        self.assertTrue(np.allclose(voxels[-1] - [39, 149], 0))
        self.assertEqual(voxels.dtype, np.uint32)

        # Non-compact
        voxels = self.noncompact.get_voxel_coordinates(chunk=slice(8, 12))
        self.assertTupleEqual(voxels.shape, (4, 2))
        self.assertTrue(np.allclose(voxels[0] - self.rand[8], 0, atol=EPS))
        self.assertTrue(np.allclose(voxels[-1] - self.rand[11], 0, atol=EPS))
        self.assertEqual(voxels.dtype, ts.DEFAULT_FLOAT_TYPE)


class TestSubchunkVoxelCoordinates(unittest.TestCase):

    def setUp(self) -> None:
        memlimit = 1 * 1024 ** 3   # 1 GB
        self.domain = Domain((18000, 18000), storage=HDD,
                             instance_mem_limit=memlimit)
        self.dtype = np.uint16

    @unittest.skipUnless(ENABLE_HDD_TESTS, "HDD tests disabled")
    def test_all_coordinates(self):
        vcoords = self.domain.get_voxel_coordinates(dtype=self.dtype)
        self.assertTupleEqual(vcoords.shape, (18e3 ** 2, 2))
        self.assertEqual(vcoords.dtype, self.dtype)

    def test_linear_chunk_coordinates_mem(self):
        vcoords = self.domain._calculate_voxel_coordinates_mem(
            dtype="u2", chunk=(17000, 19000))
        self.assertTupleEqual(vcoords.shape, (2000, 2))
        self.assertTrue(np.allclose(vcoords[0] - [0, 17000], 0))
        self.assertTrue(np.allclose(vcoords[-1] - [1, 999], 0))
        self.assertEqual(vcoords.dtype, "u2")

    @unittest.skipUnless(ENABLE_HDD_TESTS, "HDD tests disabled")
    def test_linear_chunk_coordinates_hdd(self):
        memlimit = 1 * 1024 ** 2   # 1 MB
        vcoords = self.domain._calculate_voxel_coordinates_hdd(
            dtype="u8", chunk=(17000, 90000), memlimit=memlimit)
        self.assertTupleEqual(vcoords.shape, (73000, 2))
        self.assertTrue(np.allclose(vcoords[0] - [0, 17000], 0))
        self.assertTrue(np.allclose(vcoords[-1] - [4, 17999], 0))
        self.assertEqual(vcoords.dtype, "u8")

    def test_multiindex_chunk_coordinates_mem(self):
        vcoords = self.domain._calculate_voxel_coordinates_mem(
            dtype="u2", chunk=(slice(5000, 6000, 2), slice(7000, 10000, 3)))
        self.assertTupleEqual(vcoords.shape, (5e5, 2))
        self.assertTrue(np.allclose(vcoords[0] - [5000, 7000], 0))
        self.assertTrue(np.allclose(vcoords[999] - [5000, 9997], 0))
        self.assertTrue(np.allclose(vcoords[1000] - [5002, 7000], 0))
        self.assertTrue(np.allclose(vcoords[1001] - [5002, 7003], 0))
        self.assertTrue(np.allclose(vcoords[-1] - [5998, 9997], 0))
        self.assertEqual(vcoords.dtype, "u2")

    @unittest.skipUnless(ENABLE_HDD_TESTS, "HDD tests disabled")
    def test_multiindex_chunk_coordinates_hdd(self):
        memlimit = 1 * 1024 ** 2  # 1 MB
        vcoords = self.domain._calculate_voxel_coordinates_hdd(
            dtype="u2", chunk=(slice(5000, 6000, 2), slice(7000, 10000, 3)),
            memlimit=memlimit)
        self.assertTupleEqual(vcoords.shape, (5e5, 2))
        self.assertTrue(np.allclose(vcoords[0] - [5000, 7000], 0))
        self.assertTrue(np.allclose(vcoords[999] - [5000, 9997], 0))
        self.assertTrue(np.allclose(vcoords[1000] - [5002, 7000], 0))
        self.assertTrue(np.allclose(vcoords[1001] - [5002, 7003], 0))
        self.assertTrue(np.allclose(vcoords[-1] - [5998, 9997], 0))
        self.assertEqual(vcoords.dtype, "u2")


class TestPhysicalCoordinates(unittest.TestCase):

    def setUp(self) -> None:
        tx_scale = TxScale(2, 2)
        tx_trans = TxTranslation(10.3, -7.76)
        np.random.seed(42)
        self.points = np.random.rand(16, 2)
        self.compact = Domain((200, 300), tx_scale, tx_trans)
        self.noncompact = Domain(self.points, tx_scale, tx_trans)

    def test_all_physical_coordinates(self):
        dtype = "<f4"
        eps = np.finfo(dtype).eps

        # Compact domain
        vc = self.compact.get_voxel_coordinates()
        pc = self.compact.get_physical_coordinates(dtype=dtype)
        self.assertTupleEqual(pc.shape, (200 * 300, 2))
        self.assertEqual(pc.dtype, dtype)
        self.assertTrue(np.allclose(2 * vc + [10.3, -7.76], pc, atol=eps))

        # Non-compact domain
        pc = self.noncompact.get_physical_coordinates(dtype=dtype)
        self.assertTupleEqual(pc.shape, (16, 2))
        self.assertEqual(pc.dtype, dtype)
        self.assertTrue(
            np.allclose(2 * self.points + [10.3, -7.76], pc, atol=eps))

    def test_linear_chunk_physical_coordinates(self):
        dtype = "<f4"
        eps = np.finfo(dtype).eps

        # Compact domain
        chunk = (250, 370)
        vc = self.compact.get_voxel_coordinates(chunk=chunk)
        pc = self.compact.get_physical_coordinates(dtype=dtype, chunk=chunk)
        self.assertTupleEqual(pc.shape, (120, 2))
        self.assertEqual(pc.dtype, dtype)
        self.assertTrue(np.allclose(2 * vc + [10.3, -7.76], pc, atol=eps))

        # Non-compact domain
        chunk = (8, 13)
        pc = self.noncompact.get_physical_coordinates(dtype=dtype, chunk=chunk)
        self.assertTupleEqual(pc.shape, (5, 2))
        self.assertEqual(pc.dtype, dtype)
        self.assertTrue(
            np.allclose(2 * self.points[8:13] + [10.3, -7.76], pc, atol=eps))

    def test_multiindex_physical_coordinates(self):
        dtype = "<f4"
        eps = np.finfo(dtype).eps

        # Compact domain
        chunk = (slice(100, 150, 2), slice(50, 200, 3))
        vc = self.compact.get_voxel_coordinates(chunk=chunk)
        self.assertTrue(np.allclose(vc[0], [100, 50]))
        self.assertTrue(np.allclose(vc[1], [100, 53]))
        self.assertTrue(np.allclose(vc[49], [100, 197]))
        self.assertTrue(np.allclose(vc[50], [102, 50]))
        self.assertTrue(np.allclose(vc[51], [102, 53]))
        pc = self.compact.get_physical_coordinates(dtype=dtype, chunk=chunk)
        self.assertTupleEqual(pc.shape, (25 * 50, 2))
        self.assertEqual(pc.dtype, dtype)
        self.assertTrue(np.allclose(2 * vc + [10.3, -7.76], pc, atol=eps))

    @unittest.skipUnless(ENABLE_HDD_TESTS, "HDD tests disabled")
    def test_all_physical_coordinates_hdd(self):
        dtype = "<f4"
        eps = np.finfo(dtype).eps
        tx_scale = TxScale(2, 2)
        tx_trans = TxTranslation(10.3, -7.76)
        memlimit = 1 * 1024 ** 3  # 1 GB
        domain = Domain((18000, 18000), tx_scale, tx_trans,
                        instance_mem_limit=memlimit, storage=HDD)
        self.assertFalse(domain._cache.cache)  # no cache yet
        vc = domain.get_voxel_coordinates()
        self.assertTrue(domain._cache.cache)  # cache should exist
        pc = domain.get_physical_coordinates(dtype=dtype)
        self.assertTupleEqual(pc.shape, (18e3 * 18e3, 2))
        self.assertEqual(pc.dtype, dtype)
        np.random.seed(42)
        for ix in np.random.randint(0, 18e3 * 18e3, (50,)):
            # Note: type casting is necessary to prevent overflow
            left = 2 * vc[[ix]].astype(np.float) + [10.3, -7.76]
            right = pc[[ix]]
            self.assertTrue(np.allclose(left, right, atol=eps))


if __name__ == '__main__':
    unittest.main()
