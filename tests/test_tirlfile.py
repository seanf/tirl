#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import os
import unittest
import numpy as np

import tirl
from tirl.timage import TImage
from tirl.transformations.linear.affine import TxAffine
from tirl.transformations.linear.translation import TxTranslation


def resources(*args):
    return tirl.sharedir("resources", *args)


class TestLoadTImage(unittest.TestCase):

    def setUp(self) -> None:
        img = tirl.testimg()
        tx_translation = TxTranslation(90, 72, name="translation")
        self.affine = np.random.rand(2, 3)
        tx_affine = TxAffine(self.affine, name="translation")
        img.domain.chain += [tx_translation, tx_affine]
        out = resources("tirlfiles", "test.timg")
        if os.path.isfile(out):
            os.remove(out)
        self.assertFalse(os.path.isfile(out))
        img.save(out, overwrite=True)
        self.assertTrue(os.path.isfile(out))

    def test_load(self):
        imfile = resources("tirlfiles", "test.timg")
        assert os.path.isfile(imfile)
        img = tirl.load(imfile)
        self.assertIsInstance(img, TImage)
        self.assertTrue(np.allclose(
            img.domain.chain[0].parameters[:], [90., 72.], atol=1e-3))
        self.assertTrue(np.allclose(
            img.domain.chain[1].parameters[:], self.affine.ravel(), atol=1e-3))


class TestLoadTransformationGroup(unittest.TestCase):

    def test_load_transformationgroup(self):
        tgfile = resources("tirlfiles", "chain0.tg")
        tg = tirl.load(tgfile)
        y = tg.map([[0, 0]])


class TestLoadTImageWithChain(unittest.TestCase):

    def test_load_timage_with_chain(self):
        imfile = resources("tirlfiles", "1_stage1.timg")
        im = tirl.load(imfile)
        self.assertIsInstance(im, TImage)


if __name__ == '__main__':
    unittest.main()
