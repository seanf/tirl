#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

from tirl.constants import *
from tirl.tfield import TField


class MyTestCase(unittest.TestCase):

    def test_tensor_reordering(self):
        # Define a 3D warp field
        f = TField((200, 300, 64), (3,), order=TENSOR_MAJOR)
        f.tensors[0] = 0; f.tensors[1] = 1; f.tensors[2] = 2

        # Reorder the tensors
        vectorder = (2, 0, 1)
        self.assertTrue(np.allclose(f.voxels[150, 150, 32], [[0], [1], [2]]))
        r = f.tensors[np.asarray(vectorder)]
        self.assertIsInstance(r, TField)
        self.assertTrue(np.allclose(r.voxels[150, 150, 32], [[2], [0], [1]]))
        self.assertTrue(np.allclose(f.voxels[150, 150, 32], [[0], [1], [2]]))

        # Use only the z axis
        vectorder = (2,)
        self.assertTrue(np.allclose(f.voxels[150, 150, 32], [[0], [1], [2]]))
        r = f.tensors[np.asarray(vectorder)]
        self.assertIsInstance(r, TField)
        self.assertTrue(np.allclose(r.voxels[150, 150, 32], [2]))


if __name__ == '__main__':
    unittest.main()
