#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np
from numbers import Real

import tirl
from tirl.costs.msd import CostMSD
from tirl.transformations.linear.rotation import TxRotation2D


class TestConstruction(unittest.TestCase):

    def setUp(self) -> None:
        self.orig = tirl.testimg()
        self.orig.centralise()
        self.rot = TxRotation2D(20).apply(self.orig.copy())
        # self.orig.preview()
        # self.rot.preview()

    def test_construction(self):
        cost = CostMSD(self.rot, self.orig)
        self.assertIsInstance(cost, CostMSD)


class TestScalarCost(unittest.TestCase):

    def setUp(self) -> None:
        self.orig = tirl.testimg()
        self.orig.centralise()
        self.rot = TxRotation2D(20).apply(self.orig.copy())

    def test_zero_cost(self):
        cost = CostMSD(self.rot, self.rot)
        costval = cost()
        self.assertIsInstance(costval, Real)
        self.assertTrue(np.isclose(costval, 0.0))

    def test_nonzero_cost(self):
        cost = CostMSD(self.rot, self.orig)
        costval = cost()
        self.assertFalse(np.isclose(costval, 0.0))


class TestJacobian(unittest.TestCase):

    def setUp(self) -> None:
        self.orig = tirl.testimg()
        self.orig.centralise()
        self.orig.dtype = "<f4"
        self.orig.smooth(5)
        self.rot = TxRotation2D(20).apply(self.orig.copy())

    def test_jacobian(self):
        cost = CostMSD(self.rot, self.orig)
        jac = cost.dx()
        # TImage.fromarray(jac.reshape((347, 347, 2)), tensor_axes=-1).preview()
        expectation = (self.rot.domain.numel, *self.rot.tshape, 2)
        self.assertTupleEqual(jac.shape, expectation)

    def test_jacobian_single_direction_x(self):
        cost = CostMSD(self.rot, self.orig)
        jac = cost.dx(0)
        # TImage.fromarray(jac.reshape((347, 347)), tensor_axes=()).preview()
        expectation = (self.rot.domain.numel, *self.rot.tshape)
        self.assertTupleEqual(jac.shape, expectation)

    def test_jacobian_single_direction_y(self):
        cost = CostMSD(self.rot, self.orig)
        jac = cost.dx(1)
        # TImage.fromarray(jac.reshape((347, 347)), tensor_axes=()).preview()
        expectation = (self.rot.domain.numel, *self.rot.tshape)
        self.assertTupleEqual(jac.shape, expectation)


if __name__ == '__main__':
    unittest.main()
