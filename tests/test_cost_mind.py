#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

import tirl
from tirl.constants import *
from tirl.timage import TImage
from tirl.costs.mind import CostMIND
from tirl.transformations.linear.rotation import TxRotation2D


def resources(*args):
    return tirl.sharedir("resources", *args)


class TestConstruction(unittest.TestCase):

    def setUp(self) -> None:
        self.orig = tirl.testimg()
        self.orig.dtype = "<f4"
        self.orig.centralise()
        self.rot = TxRotation2D(20).apply(self.orig.copy())

    def test_construction(self):
        cost = CostMIND(self.rot, self.orig)
        # cost.mind(self.orig).preview()
        self.assertIsInstance(cost, CostMIND)


class TestScalar(unittest.TestCase):

    def setUp(self) -> None:
        self.orig = tirl.testimg()
        self.orig.dtype = "<f4"
        self.orig.centralise()
        self.rot = TxRotation2D(20).apply(self.orig.copy())

    def test_scalar_cost(self):
        cost = CostMIND(self.rot, self.orig)
        # cost.mind(self.orig).preview()
        self.assertIsInstance(cost, CostMIND)
        self.assertGreater(cost(), 0)

    def test_scalar_cost_masked(self):
        cost = CostMIND(self.rot, self.orig)
        fullcost = cost()
        self.orig.mask = TImage(
            resources("testimage", "testimg_edge_mask.tif"))
        cost = CostMIND(self.rot, self.orig)
        self.assertNotEqual(fullcost, cost())


class TestGradient(unittest.TestCase):

    def setUp(self) -> None:
        self.orig = tirl.testimg()
        self.orig.dtype = "<f4"
        self.orig.centralise()
        self.rot = TxRotation2D(20).apply(self.orig.copy())

    def test_cost_gradient(self):
        cost = CostMIND(self.rot, self.orig, kernel=MK_STAR)
        jac = cost.dx()
        n_voxels = self.orig.vsize
        n_neighbours = 4       # 4-point STAR stencil
        n_layers = 3           # RGB
        ndim = self.orig.vdim  # number of partial first derivatives
        tshape = (n_neighbours, n_layers)
        self.assertTupleEqual(jac.shape, (n_voxels, *tshape, ndim))
        self.assertTrue(np.all(np.isfinite(jac)))

    def test_cost_gradient_masked(self):
        cost = CostMIND(self.rot, self.orig)
        fulljac = cost.dx()
        self.assertTrue(np.all(np.isfinite(fulljac)))
        self.orig.mask = TImage(
            resources("testimage", "testimg_edge_mask.tif"))
        cost = CostMIND(self.rot, self.orig)
        jac = cost.dx()
        self.assertFalse(np.allclose(jac, fulljac))
        self.assertTrue(np.all(np.isfinite(jac)))


if __name__ == '__main__':
    unittest.main()
