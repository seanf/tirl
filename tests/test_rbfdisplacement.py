#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest

import numpy as np
from pydoc import locate
# import matplotlib.pyplot as plt
# from scipy.interpolate import Rbf
from scipy.ndimage.interpolation import map_coordinates

import tirl
from tirl.constants import *
import tirl.settings as ts
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.interpolators.scipyinterpolator import ScipyInterpolator
from tirl.transformations.nonlinear.displacement import TxDisplacementField
from tirl.transformations.nonlinear.rbf_displacement import \
    TxRbfDisplacementField


def resources(*args):
    return tirl.sharedir("resources", *args)


@unittest.skip
class TestConstruction2D(unittest.TestCase):

    def setUp(self) -> None:

        # Create a decent deformation field
        h, w = shape = (128, 128)
        xx, yy = np.stack(np.mgrid[0:h, 0:w], axis=0)
        u = (h / 10.) * np.sin(xx / h * 2 * np.pi)
        v = (w / 10.) * np.sin(xx / w * 2 * np.pi)
        # plt.imshow(u, cmap="gray")
        # plt.suptitle("Original")
        # plt.show()

        # Define support points
        np.random.seed(42)
        self.points = np.random.randint(0, 128, (128, 2))

        # Obtain the values at the chosen support points
        u_sparse = map_coordinates(u, self.points.T)
        v_sparse = map_coordinates(v, self.points.T)
        self.dx = np.stack((u_sparse, v_sparse), axis=0)

        # Show RBf-interpolated version of the field
        # interpolator = Rbf(*self.points.T, u_sparse)
        # ipol_field = interpolator(xx.ravel(), yy.ravel())
        # plt.imshow(ipol_field.reshape(shape), cmap="gray")
        # plt.suptitle("Interpolated")
        # plt.show()

        # Create non-compact domain
        self.domain = Domain(self.points)

        # Define field
        self.field = TField.fromarray(
            self.dx, tensor_axes=(0,), domain=self.domain)
        self.assertFalse(self.field.domain.iscompact)
        self.assertIsInstance(self.field.interpolator,
                              locate(ts.DEFAULT_NONCOMPACT_INTERPOLATOR))

    def test_construction_noncompact_array(self):
        tx = TxRbfDisplacementField(self.dx, domain=self.domain)
        self.assertIsInstance(tx, TxRbfDisplacementField)

    def test_construction_noncompact_field(self):
        tx = TxRbfDisplacementField(self.field)
        self.assertIsInstance(tx, TxRbfDisplacementField)


def create_field():

    # Create a decent deformation field
    h, w = (128, 128)
    xx, yy = np.stack(np.mgrid[0:h, 0:w], axis=0)
    u = (h / 10.) * np.sin(xx / h * 2 * np.pi)
    v = (w / 10.) * np.sin(yy / w * 2 * np.pi)
    # Define support points
    np.random.seed(42)
    points = np.random.randint(0, 128, (64, 2))
    # Obtain the values at the chosen support points
    u_sparse = map_coordinates(u, points.T)
    v_sparse = map_coordinates(v, points.T)
    dx = np.stack((u_sparse, v_sparse), axis=0)
    # Create non-compact domain
    domain = Domain(points)
    # Define field
    field = TField.fromarray(dx, tensor_axes=(0,), domain=domain)
    return field, domain, points


@unittest.skip
class TestMapping2D(unittest.TestCase):

    def setUp(self) -> None:
        self.field, self.domain, self.points = create_field()
        # Create the sparse displacement field
        self.tx = TxRbfDisplacementField(self.field, domain=self.domain)
        self.assertIsInstance(self.tx, TxRbfDisplacementField)
        self.assertIs(self.tx.domain, self.domain)

        # Create the equivalent dense displacement field
        dense_domain = Domain((128, 128))
        field = self.tx.field(dense_domain)
        self.txd = TxDisplacementField(field, interpolator=ScipyInterpolator)
        self.assertIsInstance(self.txd, TxDisplacementField)
        self.assertIs(self.txd.domain, dense_domain)

    def test_map(self):
        self.tx = TxRbfDisplacementField(self.field)

        d = Domain((128, 128))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        y0 = self.txd.map(x)
        self.assertTrue(np.allclose(y, y0, 1e-3))


@unittest.skip
class TestInverse2D(unittest.TestCase):

    def setUp(self) -> None:
        self.field, self.domain, self.points = create_field()
        # Create the sparse displacement field
        self.tx = TxRbfDisplacementField(self.field, domain=self.domain)
        self.assertIsInstance(self.tx, TxRbfDisplacementField)
        self.assertIs(self.tx.domain, self.domain)

    def test_inverse_dense(self):
        dense = Domain((128, 128))
        inv = self.tx.inverse(domain=dense)
        self.assertIsInstance(inv, TxDisplacementField)
        self.assertIs(inv.domain, dense)
        # self.tx.field(dense).preview()
        # inv.field(dense).preview()
        x = dense.get_physical_coordinates()
        y = self.tx.map(x)
        xp = inv.map(y)
        self.assertTrue(np.isclose(np.median(x - xp), 0, atol=1e-03))

    def test_inverse_sparse(self):
        inv = self.tx.inverse(domain=self.tx.domain)
        self.assertIsInstance(inv, TxRbfDisplacementField)
        self.assertIs(inv.domain, self.domain)
        dense = Domain((128, 128))
        # self.tx.field(dense).preview()
        # inv.field(dense).preview()
        box = Domain((125, 128))
        x = box.get_physical_coordinates()
        y = self.tx.map(x)
        xp = inv.map(y)
        # Note: this mapping is not very accurate, because it would need to be
        # defined at the forward-mapped coordinates of the support points.
        self.assertTrue(np.isclose(np.median(x - xp), 0, atol=1e-1))
        # Test alternative domain with forward-mapped support points
        adapted = Domain(self.tx.map(self.points))
        inv = self.tx.inverse(domain=adapted)
        self.assertIsInstance(inv, TxRbfDisplacementField)
        self.assertIs(inv.domain, adapted)
        x = adapted.get_physical_coordinates()
        y = self.tx.map(x)
        xp = inv.map(y)
        # inv.field(dense).preview()
        self.assertTrue(np.isclose(np.median(x - xp), 0, atol=1e-1))


@unittest.skip
class TestUnidirectionalInverse2D3D(unittest.TestCase):

    def setUp(self) -> None:
        h, w = shape = (128, 128)
        from tirl.transformations.basic.embedding import TxEmbed
        self.dense = Domain((h, w), TxEmbed(1))
        xx, yy = np.mgrid[0:h, 0:w]
        zwarp_dense = TField.fromarray(
            h / 10. * np.sin(xx / h * 2 * np.pi), domain=self.dense)
        points = np.random.randint(0, 128, (128, 2))
        self.sparse = Domain(points, TxEmbed(1))
        zwarp_sparse = zwarp_dense.evaluate(self.sparse)
        self.tx = TxRbfDisplacementField(zwarp_sparse, vectorder=(2,))
        self.assertIsInstance(self.tx, TxRbfDisplacementField)
        self.assertIs(self.tx.domain, self.sparse)
        self.assertTrue(np.allclose(
            self.tx.parameters, zwarp_sparse.data.ravel()))
        # self.tx.field(self.dense).preview()

    def test_map(self):
        x = self.dense.get_physical_coordinates()
        y = self.tx.map(x)
        self.assertTupleEqual(y.shape, (128 * 128, 3))
        self.assertTrue(np.allclose(y[:, 0], np.repeat(np.arange(128), 128)))
        self.assertTrue(np.allclose(y[:, 1], np.tile(np.arange(128), 128)))
        zdisp = self.tx.field(self.dense)
        # zdisp.preview()
        self.assertTrue(np.allclose(y[:, 2], zdisp.data.ravel()))
        # print(np.min(y, axis=0), np.max(y, axis=0))

    @unittest.expectedFailure
    def test_inverse(self):
        """
        The 3D->2D inverse is ill-defined. As of TIRL 1.1, no conventions were
        put in place to define it exactly, but recognisably there is scope for
        an implementation. While vertices have an exact inverse, intermediate
        surface points (obtained e.g. by surface projection of/from a nearby
        structure) do have an inverse mapping in flat 2D space.

        """
        x = self.dense.get_physical_coordinates()
        y = self.tx.map(x)
        inv = self.tx.inverse(self.dense)
        xp = inv.map(y)
        print(x - xp)


class TestMapVector(unittest.TestCase):

    def setUp(self):
        warpfile = resources("tirlfiles", "rbfwarp.tx")
        self.warp = tirl.load(warpfile)

    def test_map_vector_rule_finite_strain(self):
        # Define a horizontal voxel vector at the middle of the warp domain,
        # which should coincide with the middle of the image being warped.
        voxvect = [0, 1]
        c = self.warp.domain.pcentre()
        # To compute the mapped vector, the vector should be first mapped by the
        # preceding transformations of the domain that the warp is defined on.
        vect = self.warp.domain.map_voxel_vectors(voxvect, rule=RULE_FS)
        self.assertEqual(vect.size, 3)
        # Using RULE_FS, the size of the vectors should remain the same
        self.assertAlmostEqual(np.linalg.norm(voxvect), np.linalg.norm(vect), 4)
        warpedvect = self.warp.map_vector(vect, c, rule=RULE_FS)
        self.assertAlmostEqual(
            np.linalg.norm(vect), np.linalg.norm(warpedvect), 4)
        # Using RULE_FS, the size of the vectors should remain the same
        self.assertEqual(warpedvect.size, 3)

    def test_map_vector_rule_ssr(self):
        # Define a horizontal voxel vector at the middle of the warp domain,
        # which should coincide with the middle of the image being warped.
        voxvect = [0, 1]
        c = self.warp.domain.pcentre()
        # To compute the mapped vector, the vector should be first mapped by the
        # preceding transformations of the domain that the warp is defined on.
        vect = self.warp.domain.map_voxel_vectors(voxvect, rule=RULE_SSR)
        self.assertEqual(vect.size, 3)
        # Using RULE_SSR, the size of the vectors change will have changed
        self.assertNotAlmostEqual(
            np.linalg.norm(voxvect), np.linalg.norm(vect), 4)
        warpedvect = self.warp.map_vector(vect, c, rule=RULE_SSR)
        self.assertNotAlmostEqual(
            np.linalg.norm(vect), np.linalg.norm(warpedvect), 4)
        # Using RULE_SSR, the size of the vectors change will have changed
        self.assertEqual(warpedvect.size, 3)


if __name__ == '__main__':
    unittest.main()
