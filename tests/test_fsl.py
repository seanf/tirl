#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import os
import unittest
import numpy as np

import tirl.fsl
from tirl.timage import TImage

# DEFINITIONS

FSL_FNIRT_DISPLACEMENT_FIELD       = 2006
FSL_CUBIC_SPLINE_COEFFICIENTS      = 2007
FSL_DCT_COEFFICIENTS               = 2008
FSL_QUADRATIC_SPLINE_COEFFICIENTS  = 2009

def resources(*args):
    return tirl.sharedir("resources", *args)


class TestFLIRTMapping(unittest.TestCase):

    def test_mapping(self):
        # Load images
        hrfile = resources("fsl", "T1_biascorr.nii.gz")
        highres = TImage(hrfile, sform=False, dtype="<f4")
        stdfile = resources("fsl", "MNI152_T1_2mm.nii.gz")
        standard = TImage(stdfile, sform=False, dtype="<f4")
        expfile = os.path.join(
            resources("fsl", "T1_to_MNI_lin_noresampblur.nii.gz"))
        expectation = TImage(expfile, dtype="<f4")

        # Load and set up transformation
        matfile = resources("fsl", "T1_to_MNI_lin.mat")
        flirttx = tirl.fsl.load_mat(matfile, input=hrfile, reference=stdfile)
        standard.domain.chain = flirttx.inverse()
        registered = highres.evaluate(standard.domain)
        registered.domain.chain = registered.domain.chain[:-len(flirttx)]
        # (registered - expectation).preview()
        self.assertTrue(np.isclose(
            np.median(registered.data - expectation.data), 0, atol=1e-3))
        # registered.domain.chain = expectation.domain.chain
        # registered.snapshot("reg_lin.nii.gz")


class TestFNIRTMapping(unittest.TestCase):

    def test_mapping(self):
        # Load images
        hrfile = resources("fsl", "T1_biascorr.nii.gz")
        highres = TImage(hrfile, sform=False, dtype="<f4")
        stdfile = resources("fsl", "MNI152_T1_2mm.nii.gz")
        standard = TImage(stdfile, sform=False, dtype="<f4")
        expfile = resources("fsl", "T1_to_MNI_nonlin.nii.gz")
        expectation = TImage(expfile, dtype="<f4")

        # Load and set up transformation
        warp = resources("fsl", "T1_to_MNI_nonlin_field.nii.gz")
        warp = tirl.fsl.load_warp(warp, moving=hrfile)
        standard.domain.chain = warp
        registered = highres.evaluate(standard.domain)
        registered.domain.chain = registered.domain.chain[:-len(warp)]
        # (registered - expectation).preview()
        self.assertTrue(np.isclose(
            np.median(registered.data - expectation.data), 0, atol=1e-3))
        # registered.domain.chain = expectation.domain.chain
        # registered.snapshot("reg.nii.gz")


class TestFNIRTOtherFormats(unittest.TestCase):

    @unittest.expectedFailure
    def test_cubic_spline_coefficient_field(self):
        self.fail()

    @unittest.expectedFailure
    def test_dct_coefficient_field(self):
        self.fail()

    @unittest.expectedFailure
    def test_quadratic_spline_coefficient_field(self):
        self.fail()

    @unittest.expectedFailure
    def test_unknown_field(self):
        self.fail()

    @unittest.expectedFailure
    def test_invalid_field(self):
        self.fail()


if __name__ == '__main__':
    unittest.main()
