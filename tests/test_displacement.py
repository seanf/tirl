#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np
import scipy.sparse as sp

import tirl
import tirl.settings as ts
from tirl.chain import Chain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.domain import Domain
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.linear import TxLinear
from tirl.transformations.linear.rotation import TxRotation2D
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.nonlinear.displacement import TxDisplacementField


# DEFINITIONS

from tirl.constants import *
EPS = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps
SKIP_INVERSE = False


class TestConstruction(unittest.TestCase):

    def test_tfield_tensor_major_abs(self):
        field = TField((200, 300), tensor_shape=(2,), order=TENSOR_MAJOR,
                       dtype="<f8")
        field[...] = 1
        tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertEqual(tx.parameters.dtype, ts.DEFAULT_FLOAT_TYPE)  # despite input
        self.assertEqual(tx.mode, NL_ABS)
        self.assertTrue(np.allclose(tx.parameters, 1, EPS))  # from input

        field = tx.field()
        self.assertTrue(np.allclose(field.data, 1, EPS))

        # Check link between field and parameters
        self.assertNotEqual(tx.field().data.flat[0], -10)
        tx.parameters[0] = -10
        self.assertEqual(tx.field().data.flat[0], -10)

    def test_tfield_tensor_major_rel(self):
        d = Domain((200, 300), TxRotation2D(30))
        field = TField(d, tensor_shape=(2,), order=TENSOR_MAJOR, dtype="<f8")
        field[...] = 1
        tx = TxDisplacementField(field, mode=NL_REL)
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertEqual(tx.parameters.dtype, ts.DEFAULT_FLOAT_TYPE)  # despite input
        self.assertEqual(tx.mode, NL_REL)
        self.assertTrue(np.all(tx.parameters != field.data.ravel()))
        self.assertTrue(np.allclose(tx.parameters, 1, EPS))  # from input

        sine = np.sin(30 / 180 * np.pi)
        cosine = np.cos(30 / 180 * np.pi)
        field = tx.rel2abs(tx.field())
        self.assertTrue(np.allclose(
            field.data.flat[:200*300], cosine - sine, EPS))
        self.assertTrue(np.allclose(
            field.data.flat[200*300:], cosine + sine, EPS))

    def test_tfield_voxel_major_abs(self):
        field = TField((200, 300), tensor_shape=(2,), order=VOXEL_MAJOR,
                       dtype="<f8")
        field[...] = 0.8
        tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertEqual(tx.parameters.dtype, ts.DEFAULT_FLOAT_TYPE)  # despite input
        self.assertEqual(tx.mode, NL_ABS)
        self.assertTrue(np.allclose(tx.parameters, 0.8, EPS))  # from input

    def test_tfield_voxel_major_rel(self):
        d = Domain((200, 300), TxRotation2D(30))
        field = TField(d, tensor_shape=(2,), order=VOXEL_MAJOR, dtype="<u2")
        field[...] = 2
        tx = TxDisplacementField(field, mode=NL_REL)
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertEqual(tx.parameters.dtype, ts.DEFAULT_FLOAT_TYPE)  # despite input
        self.assertEqual(tx.mode, NL_REL)
        self.assertTrue(np.allclose(tx.parameters, 2, EPS))  # from input

    def test_construction_from_array(self):
        arr = 2 * np.ones((2, 200, 300), dtype="<f2")
        tx = TxDisplacementField(arr)
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertEqual(tx.parameters.dtype, ts.DEFAULT_FLOAT_TYPE)  # despite input
        self.assertTrue(np.allclose(tx.parameters, 2, EPS))  # from input

    def test_invalid_construction(self):
        with self.assertRaises(TypeError):
            TxDisplacementField([2, 3, 4])

    def test_copy(self):
        d = Domain((200, 300), TxRotation2D(30))
        field = TField(d, tensor_shape=(2,), order=VOXEL_MAJOR, dtype="<u2")
        field[...] = 2
        tx = TxDisplacementField(field, mode=NL_REL)
        tx2 = tx.copy()
        self.assertIsNot(tx, tx2)
        self.assertEqual(tx, tx2)


class TestMappingAbsolute(unittest.TestCase):

    def setUp(self) -> None:
        self.img = tirl.testimg()
        field = TField(self.img.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.img.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -20 * np.cos(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -20, 1e-3))

    def test_map2d_rgb_abs(self):
        warped = self.tx.apply(self.img)
        self.assertIsInstance(warped, TImage)
        # warped.preview()
        # self.tx.field().preview()
        # (warped - self.img).preview()


class TestMappingRelative(unittest.TestCase):

    def setUp(self) -> None:
        # Create absolute field
        self.img = tirl.testimg()
        self.img.centralise()
        field = TField(self.img.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.img.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = 0
        self.txabs = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.txabs.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.txabs.parameters), -20, 1e-3))

        # Create relative field for a rotated version of the same image
        self.rot = tirl.testimg()
        self.rot.centralise()
        self.rot.domain.chain.append(TxRotation2D(30))
        field = TField(self.rot.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.rot.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = 0
        self.txrel = TxDisplacementField(field, mode=NL_REL)
        self.assertTrue(np.isclose(np.max(self.txrel.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.txrel.parameters), -20, 1e-3))

    def test_vector_alignment(self):
        # By definition the vectors of the relative field are in 1:1
        # correspondence with the absolute field, and they are rotated
        # covariantly with the domain, i.e. by 30 degrees.
        absfield = self.txabs.absolute().vectors()
        relfield = self.txrel.absolute().vectors()
        self.assertTrue(np.allclose(np.linalg.norm(absfield, axis=-1),
                                    np.linalg.norm(relfield, axis=-1),
                                    atol=1e-3))
        # self.txabs.absolute().field().preview("quiver")
        # self.txrel.absolute().field().preview("quiver")
        mask = np.any(relfield != 0, axis=-1)
        self.assertTrue(np.allclose(
            np.angle(relfield[mask, 0] + 1j * relfield[mask, 1]) % np.pi,
            np.pi / 6, atol=1e-3))

    def test_mapping_identity(self):
        xabs = self.img.domain.get_physical_coordinates()
        yabs = self.txabs.map(xabs)
        xrel = self.rot.domain.get_physical_coordinates()
        yrel = self.txrel.map(xrel)
        # Verify that the two transformations provide an identical mapping
        # between voxel coordinates, i.e. the image of any single voxel is the
        # same point in the voxel domain under both transformations.
        self.assertTrue(np.allclose(
            self.rot.domain.map_physical_coordinates(yrel),
            self.img.domain.map_physical_coordinates(yabs), atol=1e-3))


class TestResampleAbsolute(unittest.TestCase):

    def setUp(self) -> None:
        self.img = tirl.testimg()
        field = TField(self.img.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.img.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -20 * np.cos(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -20, 1e-3))

    def test_map2d_rgb_resized(self):
        d = self.tx.domain.copy()
        d = d.resize(0.5)
        tx = self.tx.resample(d)

        # Warp image with full-resolution transformation
        warped1 = self.tx.apply(self.img)
        self.assertIsInstance(warped1, TImage)
        # warped1.preview()

        # Warp image with a coarser transformation (4x fewer points!)
        target = self.img.domain.copy()
        target.chain.append(tx)
        warped2 = self.img.evaluate(target)
        self.assertIsInstance(warped2, TImage)
        # warped2.preview()

        # Warp with the coarse transformation and use the transformation domain
        warped3 = tx.apply(self.img)
        self.assertIsInstance(warped3, TImage)
        # warped3.preview()


class TestLocalAffine2DAbsolute(unittest.TestCase):

    def setUp(self) -> None:
        self.img = tirl.testimg()
        field = TField(self.img.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.img.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -20 * np.cos(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -20, 1e-3))

    def test_localaffine_2d_absolute(self):

        coords = self.img.domain.get_physical_coordinates()
        affines = self.tx.matrix(coords=coords)
        hc = np.pad(coords, ((0, 0), (0, 1)), mode="constant",
                    constant_values=1)
        y = np.take(affines @ hc[..., np.newaxis], 0, axis=-1)
        mapped = self.tx.map(coords=coords)
        # We expect the coordinate mapping by local affines be accurate to
        # 1/10th of a voxel everywhere compared to the "true" results
        # obtained by interpolation. This guarantees good inverse
        # consistency.
        self.assertTrue(np.allclose(y, mapped, 1e-1))


class TestInverse2DAbsolute(unittest.TestCase):

    def setUp(self) -> None:
        self.img = tirl.testimg()
        field = TField(self.img.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.img.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -20 * np.cos(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -20, 1e-3))

    def test_inverse(self):
        invtx = self.tx.inverse()
        coords = self.img.domain.get_physical_coordinates()
        mapped = self.tx.map(coords)
        remapped = invtx.map(mapped)
        err = np.linalg.norm(coords - remapped, axis=-1)
        within_fov = np.all(mapped >= 0, axis=-1) & \
                     (mapped[:, 0] < self.img.vshape[0]) & \
                     (mapped[:, 1] < self.img.vshape[1])
        # Some pixels at the edge may be faulty, so we expect the median
        # inverse consistency error be less than 1/1000th of a voxel.
        # plt.imshow(err.reshape(self.img.vshape) > 0.1, cmap="gray")
        # plt.show()
        # self.assertTrue(np.allclose(err[within_fov], 0, 1e-1))
        self.assertTrue(np.median(err[within_fov], axis=0) < 0.001)


class TestJacobian2DAbsolute(unittest.TestCase):

    def setUp(self) -> None:
        self.img = tirl.testimg()
        field = TField(self.img.domain[:], tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        h, w = self.img.vshape
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -20 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -20 * np.cos(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 20, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -20, 1e-3))

    def test_jacobian(self):
        coords = self.img.domain.get_physical_coordinates() + 0.1
        within_fov = np.all(coords >= 0, axis=-1) & \
                     np.all((coords[:, 0] < 347), axis=-1)
        coords = coords[within_fov]
        jac = self.tx.jacobian(coords=coords)
        self.assertIsInstance(jac, sp.csr_matrix)
        n_points = coords.shape[0]
        n_parameters = self.tx.parameters.size
        self.assertTupleEqual(jac.shape, (2 * n_points, n_parameters))
        delta_p = 0.247 * np.ones(n_parameters)
        du = jac @ delta_p[:, np.newaxis]
        # We expect the Jacobians be precise enough to yield the same change
        # in displacement as the result of a homogeneous translation acting
        # directly on the input points. The precision of our expectation is
        # again 1/1000th of a voxel.
        self.assertTrue(np.allclose(du[du!=0], 0.247, 1e-3))


class TestOperations2DAbsolute(unittest.TestCase):

    def setUp(self) -> None:

        # Create scalar
        self.scalar = 3

        # Create linear transformation

        self.lin = TxScale(1.1, 1.1) + \
                   TxRotation2D(15) + \
                   TxTranslation(3.14, 2.7)
        self.assertIsInstance(self.lin, TxLinear)

        # Create displacement field
        h, w = shape = (347, 347)
        field = TField(shape, tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -10 * np.sin(yy / w * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 10, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -10, 1e-3))

    def test_field_plus_scalar(self):
        tx = self.tx + self.scalar
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertIs(tx.domain, self.tx.domain)
        x = self.tx.domain.get_physical_coordinates()
        y = self.tx.map(x)
        yp = tx.map(x)
        self.assertTrue(np.allclose(y + 3, yp, 1e-3))

    def test_scalar_plus_field(self):
        tx = self.scalar + self.tx
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertIs(tx.domain, self.tx.domain)
        x = self.tx.domain.get_physical_coordinates()
        y = self.tx.map(x)
        yp = tx.map(x)
        self.assertTrue(np.allclose(y + 3, yp, 1e-3))

    def test_field_plus_lin(self):
        tx = self.tx + self.lin
        # tx.absolute().field().preview()
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertIs(tx.domain, self.tx.domain)
        x = self.tx.domain.get_physical_coordinates()
        chain = Chain(self.tx, self.lin)

        y = chain.map(x, merge=None)
        yp = tx.map(x)
        self.assertTrue(np.allclose(y, yp, 1e-3))

    def test_field_plus_field(self):
        field = TField((400, 405), (2,), VOXEL_MAJOR, dtype="<f4")
        field.tensors[0] = -3.14
        field.tensors[1] = 6.341
        tx2 = TxDisplacementField(field, mode=NL_ABS)
        tx = self.tx + tx2
        # tx.absolute().field().preview()
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertIs(tx.domain, self.tx.domain)
        x = self.tx.domain.get_physical_coordinates()
        chain = Chain(self.tx, tx2)
        y = chain.map(x, merge=None)
        yp = tx.map(x)
        self.assertTrue(np.allclose(y, yp, 1e-3))

    def test_field_times_int(self):
        tx = self.tx * self.scalar
        # tx.absolute().field().preview()
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertIs(tx.domain, self.tx.domain)
        x = self.tx.domain.get_physical_coordinates()
        chain = Chain(self.tx, self.tx, self.tx)
        y = chain.map(x, merge=None)
        yp = tx.map(x)
        self.assertTrue(np.isclose(np.median(y - yp), 0, 1e-3))

    def test_lin_plus_field(self):
        tx = self.lin + self.tx
        self.assertIsInstance(tx, TxDisplacementField)
        self.assertIsNot(tx.domain, self.tx.domain)
        self.assertTrue(np.allclose(tx.domain.chain[-1].inverse().matrix,
                                     self.lin.matrix, 1e-3))
        d = Domain((347, 347))
        # tx.absolute().field().evaluate(d).preview()
        x = d.get_physical_coordinates()
        chain = Chain(self.lin, self.tx)
        y = chain.map(x, merge=None)
        yp = tx.map(x)
        # plt.imshow(np.all(np.isclose(y, yp, 1e-3), axis=-1).reshape(d.shape), cmap="gray")
        # plt.show()
        self.assertTrue(np.isclose(np.median(y - yp), 0, 1e-3))


class Test2DRelative(unittest.TestCase):

    def setUp(self) -> None:

        # Create linear transformation
        self.lin = TxScale(1.1, 1.1) + \
                   TxRotation2D(15) + \
                   TxTranslation(3.14, 2.7)
        self.assertIsInstance(self.lin, TxLinear)

        # Create displacement field
        h, w = shape = (347, 347)
        field = TField(shape, tensor_shape=(2,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -10 * np.sin(yy / w * 2 * np.pi)
        # field.tensors[1] = 0
        self.tx = TxDisplacementField(field, mode=NL_ABS)
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 10, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -10, 1e-3))

    def test_relative_field2d(self):
        """
        There is no quantitative test here, just a convincing visualisation.

        """
        field = self.tx.absolute().field()
        d = Domain((347, 347), self.lin)
        txr = TxDisplacementField(field, d, mode=NL_REL)
        txa = TxDisplacementField(field, d, mode=NL_ABS)
        # print("Relative field.")
        # txr.absolute().field(Domain((347, 347))).preview("quiver", xsize=35, cmap="jet")
        # txr.absolute().field(Domain((347, 347))).preview("hsv")
        # print("Absolute field.")
        # txa.field(Domain((347, 347))).preview("quiver", xsize=35, cmap="jet")
        # txa.field(Domain((347, 347))).preview("hsv")


class TestUnidirectionalField2D(unittest.TestCase):

    def setUp(self) -> None:
        h, w = shape = (347, 347)
        field = TField(shape, tensor_shape=(1,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy = np.mgrid[0:h, 0:w]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS, vectorder="x")
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 10, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -10, 1e-3))

    def test_mapx(self):
        self.tx.vectorder = "x"
        d = Domain((400, 400))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        self.assertFalse(np.allclose(x[:, 0], y[:, 0], 1e-3))
        self.assertTrue(np.allclose(x[:, 1], y[:, 1], 1e-3))

    def test_mapy(self):
        self.tx.vectorder = "y"
        d = Domain((400, 400))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        self.assertTrue(np.allclose(x[:, 0], y[:, 0], 1e-3))
        self.assertFalse(np.allclose(x[:, 1], y[:, 1], 1e-3))

    @unittest.skipIf(SKIP_INVERSE, "takes too long")
    def test_inversex(self):
        self.tx.vectorder = "x"
        d = Domain((400, 400))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        invtx = self.tx.inverse()
        self.assertIsInstance(invtx, type(self.tx))
        self.assertTrue(np.isclose(np.median(invtx.map(y) - x), 0, 1e-8))

    @unittest.skipIf(SKIP_INVERSE, "takes too long")
    def test_inversey(self):
        self.tx.vectorder = "y"
        d = Domain((400, 400))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        invtx = self.tx.inverse()
        self.assertIsInstance(invtx, type(self.tx))
        self.assertTrue(np.isclose(np.median(invtx.map(y) - x), 0, 1e-8))


class TestUnidirectionalField3D(unittest.TestCase):

    def setUp(self) -> None:
        h, w, d = shape = (128, 128, 64)
        field = TField(shape, tensor_shape=(1,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy, zz = np.mgrid[0:h, 0:w, 0:d]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        self.tx = TxDisplacementField(field, mode=NL_ABS, vectorder="y")
        self.assertTrue(np.isclose(np.max(self.tx.parameters), 10, 1e-3))
        self.assertTrue(np.isclose(np.min(self.tx.parameters), -10, 1e-3))

    def test_mapx(self):
        self.tx.vectorder = "x"
        d = Domain((50, 54, 64), TxTranslation(35., 35., 0.))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        self.assertFalse(np.allclose(x[:, 0], y[:, 0], 1e-3))
        self.assertTrue(np.allclose(x[:, 1], y[:, 1], 1e-3))
        self.assertTrue(np.allclose(x[:, 2], y[:, 2], 1e-3))

    def test_mapy(self):
        self.tx.vectorder = "y"
        d = Domain((50, 54, 64), TxTranslation(35., 35., 0.))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        self.assertTrue(np.allclose(x[:, 0], y[:, 0], 1e-3))
        self.assertFalse(np.allclose(x[:, 1], y[:, 1], 1e-3))
        self.assertTrue(np.allclose(x[:, 2], y[:, 2], 1e-3))

    def test_mapz(self):
        self.tx.vectorder = "z"
        d = Domain((50, 54, 64), TxTranslation(35., 35., 0.))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        self.assertTrue(np.allclose(x[:, 0], y[:, 0], 1e-3))
        self.assertTrue(np.allclose(x[:, 1], y[:, 1], 1e-3))
        self.assertFalse(np.allclose(x[:, 2], y[:, 2], 1e-3))

    @unittest.skipIf(SKIP_INVERSE, "takes too long")
    def test_inversex(self):
        self.tx.vectorder = "x"
        d = Domain((50, 54, 64), TxTranslation(35., 35., 0.))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        invtx = self.tx.inverse()
        self.assertIsInstance(invtx, type(self.tx))
        self.assertTrue(np.isclose(np.median(invtx.map(y) - x), 0, 1e-8))

    @unittest.skipIf(SKIP_INVERSE, "takes too long")
    def test_inversey(self):
        self.tx.vectorder = "y"
        d = Domain((50, 54, 64), TxTranslation(35., 35., 0.))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        invtx = self.tx.inverse()
        self.assertIsInstance(invtx, type(self.tx))
        self.assertTrue(np.isclose(np.median(invtx.map(y) - x), 0, 1e-8))

    @unittest.skipIf(SKIP_INVERSE, "takes too long")
    def test_inversez(self):
        self.tx.vectorder = "z"
        d = Domain((50, 54, 64), TxTranslation(35., 35., 0.))
        x = d.get_physical_coordinates()
        y = self.tx.map(x)
        invtx = self.tx.inverse()
        self.assertIsInstance(invtx, type(self.tx))
        self.assertTrue(np.isclose(np.median(invtx.map(y) - x), 0, 1e-8))

    def test_stdx(self):
        # Set a few parameters for the original transformation that can be
        # tested in the standard transformation instance.
        self.tx.vectorder = "x"
        self.tx.parameters.set_bounds(-25, 25)
        self.tx.parameters.lock(range(100, 150))
        self.tx.metaparameters.update(custom="value")
        self.tx.interpolator.kwargs.update(custom="value")
        # Get standard transformation
        std = self.tx.std()
        self.assertIsInstance(std, type(self.tx))
        # Check domain
        self.assertIs(std.domain, self.tx.domain)
        # Check parameters
        self.assertEqual(std.parameters.size, 3 * 128 * 128 * 64)
        self.assertNotEqual(self.tx.parameters.size, std.parameters.size)
        # Check bounds
        n = self.tx.domain.numel
        lb0 = self.tx.parameters.get_lower_bounds()
        ub0 = self.tx.parameters.get_upper_bounds()
        lb = std.parameters.get_lower_bounds()
        ub = std.parameters.get_upper_bounds()
        self.assertTrue(np.allclose(lb0, lb[:n], 1e-3))
        self.assertTrue(np.all(lb[n:] == -np.inf))
        self.assertTrue(np.allclose(ub0, ub[:n], 1e-3))
        self.assertTrue(np.all(ub[n:] == np.inf))
        # Check vault (locked parameters)
        l0 = self.tx.parameters.locked
        free = self.tx.parameters.free
        l = std.parameters.locked
        l0 = np.fromiter(l0, dtype=np.int, count=len(l0))
        l = np.fromiter(l, dtype=np.int, count=len(l))
        self.assertEqual(l.size, n * 2 + 50)
        self.assertTrue(np.all(np.in1d(l0, l)))
        self.assertTrue(np.all(~np.in1d(free, l)))
        # Check metaparameters
        old_meta = {k: v for k, v in self.tx.metaparameters.items()
                    if k not in ("vectorder", "interpolator")}
        new_meta = {k: v for k, v in std.metaparameters.items()
                    if k not in ("vectorder", "interpolator")}
        self.assertDictEqual(old_meta, new_meta)
        self.assertEqual(new_meta.get("custom", None), "value")
        self.assertEqual(std.interpolator.kwargs.get("custom", None), "value")


class TestArithmetics(unittest.TestCase):

    def setUp(self) -> None:
        h, w, d = shape = (128, 128, 64)
        field = TField(shape, tensor_shape=(3,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy, zz = np.mgrid[0:h, 0:w, 0:d]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -10 * (yy - yy / 2) / w
        field.tensors[2] = -10 * np.sin(zz / d * 2 * np.pi)
        self.ntx = TxDisplacementField(field, mode=NL_ABS, vectorder="xyz")
        self.ltx = TxTranslation(20, 10, 15.2)

    def test_add_scalar(self):
        ntx = self.ntx + 10
        self.assertTrue(np.allclose(self.ntx.parameters[:] + 10,
                                    ntx.parameters[:], atol=1e-3))

    def test_add_linear(self):
        chain = Chain(self.ntx, self.ltx)
        combined = self.ntx + self.ltx
        self.assertIsInstance(combined, TxDisplacementField)
        np.random.seed(0)
        x = np.random.randint(0, 128, (50, 3))
        y = chain.map(x)
        self.assertTrue(np.allclose(combined.map(x), y, atol=1e-3))

    def test_multiply_scalar(self):
        ntx = self.ntx * 2
        con = self.ntx + self.ntx
        np.random.seed(0)
        x = np.random.randint(0, 128, (50, 3))
        y = con.map(x)
        self.assertIsInstance(con, TxDisplacementField)
        self.assertTrue(np.allclose(ntx.map(x), y, atol=1e-3))


if __name__ == '__main__':
    unittest.main()
