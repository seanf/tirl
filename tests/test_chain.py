#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import unittest
import numpy as np

from tirl.constants import *
from tirl.chain import Chain
from tirl.tfield import TField
from tirl.transformations.linear.linear import TxLinear
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.linear.rotation import TxRotation2D
from tirl.transformations.linear.shear import TxShear
from tirl.transformations.basic.embedding import TxEmbed
from tirl.transformations.nonlinear.nonlinear import TxNonLinear
from tirl.transformations.nonlinear.displacement import TxDisplacementField

from tirl import settings as ts


class TestConstruction(unittest.TestCase):

    def setUp(self):
        tx_rot = TxRotation2D(20, name="rotation")
        tx_scale = TxScale(1.1, 1.1, name="scale")
        tx_shear = TxShear(2, 0.5, name="shear")
        tx_trans = TxTranslation(-5, 4.2, name="translation")
        tx_embed = TxEmbed(1, name="embed")
        self.transformations = [tx_scale, tx_rot, tx_shear, tx_trans, tx_embed]

    def test_empty_chain_construction(self):
        c = Chain()
        self.assertIsInstance(c, Chain)
        self.assertTrue(issubclass(type(c), list))

    def test_linear_chain_construction(self):
        c = Chain(*self.transformations)
        self.assertIsInstance(c, Chain)
        for i, tx in enumerate(c):
            self.assertIs(tx, self.transformations[i])

    def test_construction_by_sequence(self):
        c = Chain(self.transformations)
        self.assertIsInstance(c, Chain)
        for i, tx in enumerate(c):
            self.assertIs(tx, self.transformations[i])

    def test_construction_by_generator(self):
        c = Chain(tx for tx in self.transformations)
        self.assertIsInstance(c, Chain)
        for i, tx in enumerate(c):
            self.assertIs(tx, self.transformations[i])

    def test_faulty_construction(self):
        with self.assertRaises(TypeError):
            Chain(*self.transformations[:-1], 10)

    def test_copy(self):
        c = Chain(*self.transformations)
        cc = c.copy()
        self.assertIsInstance(c, Chain)
        for i, tx in enumerate(cc):
            self.assertIsNot(tx, self.transformations[i])
            self.assertEqual(tx, self.transformations[i])


class TestGetElement(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [
            tx_scale2d, tx_rot2d, tx_shear2d, tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(
            tx_scale2d, tx_rot2d, tx_shear2d, tx_embed, tx_scale3d, tx_trans3d)

    def test_getitem_single_element(self):
        self.assertIs(self.chain[3], self.transformations[3])
        self.assertIs(self.chain[-1], self.transformations[-1])

    def test_getitem_continuous(self):
        subchain = self.chain[1:3]
        expected = self.transformations[1:3]
        self.assertIsInstance(subchain, Chain)
        for ctx, ltx in zip(subchain, expected):
            self.assertIs(ctx, ltx)

    def test_getitem_contiguous(self):
        expected = [self.transformations[1],
                    self.transformations[3],
                    self.transformations[5]]
        subchain = self.chain[1, 3, 5]
        self.assertIsInstance(subchain, Chain)
        for ctx, ltx in zip(subchain, expected):
            self.assertIs(ctx, ltx)

    def test_getitem_contiguous_list(self):
        expected = [self.transformations[1],
                    self.transformations[3],
                    self.transformations[5]]
        subchain = self.chain[[1, 3, 5]]
        self.assertIsInstance(subchain, Chain)
        for ctx, ltx in zip(subchain, expected):
            self.assertIs(ctx, ltx)


class TestSetElement(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [
            tx_scale2d, tx_rot2d, tx_shear2d, tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(
            tx_scale2d, tx_rot2d, tx_shear2d, tx_embed, tx_scale3d, tx_trans3d)
        self.newtx = TxShear(3, 0.1, -0.4, 0.25)

    def test_setitem_single_element(self):
        c = Chain(self.chain)
        self.assertIs(c[4], self.transformations[4])
        c[4] = self.newtx
        self.assertIsInstance(c, Chain)
        self.assertIs(c[4], self.newtx)
        self.assertIs(c[-1], self.transformations[-1])
        c[-1] = self.newtx
        self.assertIsInstance(c, Chain)
        self.assertIs(c[4], self.newtx)

    def test_setitem_continuous(self):
        c = Chain(self.chain)
        c[1:3] = self.newtx
        self.assertIsInstance(c, Chain)
        for ctx in c[1:3]:
            self.assertIs(ctx, self.newtx)

    def test_setitem_contiguous_homogeneous(self):
        c = Chain(self.chain)
        c[1, 3, 5] = self.newtx
        self.assertIsInstance(c, Chain)
        for ctx in c[1, 3, 5]:
            self.assertIs(ctx, self.newtx)

    def test_setitem_contiguous_heterogeneous(self):
        c = Chain(self.chain)
        c[1, 3, 5] = self.transformations[1:6:2]
        self.assertIsInstance(c, Chain)
        for ctx, ltx in zip(c[1, 3, 5], self.transformations[1:6:2]):
            self.assertIs(ctx, ltx)


class TestAddAppendPop(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [
            tx_scale2d, tx_rot2d, tx_shear2d, tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(
            tx_scale2d, tx_rot2d, tx_shear2d, tx_embed, tx_scale3d, tx_trans3d)

    def test_append(self):
        c = Chain(self.chain)
        tx_embed = TxEmbed(1)
        c.append(tx_embed)
        self.assertIsInstance(c, Chain)
        self.assertEqual(len(c), len(self.chain) + 1)
        for ctx, ltx in zip(c, self.transformations):
            self.assertIs(ctx, ltx)
        else:
            self.assertIs(c[-1], tx_embed)

    def test_add_chains(self):
        c = Chain(self.chain)
        nc = self.chain + c
        self.assertIsInstance(nc, Chain)
        for ctx, ltx in zip(nc[:len(nc) // 2], self.chain):
            self.assertIs(ctx, ltx)
        for ctx, ltx in zip(nc[len(nc) // 2:], self.chain):
            self.assertIs(ctx, ltx)

    def test_add_transformation(self):
        tx_embed = TxEmbed(1)

        ncc = tx_embed + self.chain
        self.assertIsInstance(ncc, Chain)
        self.assertEqual(len(ncc), len(self.chain) + 1)
        for ctx, ltx in zip(ncc[1:], self.transformations):
            self.assertIs(ctx, ltx)
        else:
            self.assertIs(ncc[0], tx_embed)

        ncc = self.chain + tx_embed
        self.assertIsInstance(ncc, Chain)
        self.assertIs(ncc[-1], tx_embed)
        self.assertEqual(len(ncc), len(self.chain) + 1)
        for ctx, ltx in zip(ncc, self.transformations):
            self.assertIs(ctx, ltx)
        else:
            self.assertIs(ncc[-1], tx_embed)


class TestReduction(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                           tx_embed, tx_scale3d, tx_trans3d)

    def test_linear_reduction(self):
        c = Chain(self.chain)
        nc = c.reduce(merge="linear")
        self.assertEqual(len(nc), 4)
        self.assertIsInstance(nc[0], TxLinear)
        x0 = [1, 1]
        x1 = Chain(self.transformations[:4]).map([x0], merge=False)
        eps = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps
        # Mapping identity of the first linear group
        self.assertTrue(np.allclose(nc[0].map([x0]) - x1, 0, atol=eps))
        # Skipping non-linear transformation
        self.assertIsInstance(nc[1], TxNonLinear)
        # Mapping identity of the embedding
        self.assertIsInstance(nc[2], TxEmbed)
        x1 = self.transformations[4].map([x0])
        self.assertTrue(np.allclose(nc[2].map([x0]) - x1, 0, atol=eps))
        # Mapping identity of the second linear group
        self.assertIsInstance(nc[3], TxLinear)
        x0 = [1, 1, 1]
        x1 = Chain(self.transformations[5:]).map([x0], merge=False)
        self.assertTrue(np.allclose(nc[3].map([x0]) - x1, 0, atol=eps))

    def test_nonlinear_reduction(self):
        h, w, d = shape = (128, 128, 64)
        field = TField(shape, tensor_shape=(3,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy, zz = np.mgrid[0:h, 0:w, 0:d]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -10 * (yy - yy / 2) / w
        field.tensors[2] = -10 * np.sin(zz / d * 2 * np.pi)
        pre = TxScale(1.1, 0.95, 1.0)
        ntx = TxDisplacementField(field, mode=NL_ABS, vectorder="xyz")
        post = TxTranslation(20, 10, 15.2)
        chain = Chain(pre, ntx, post)
        np.random.seed(0)
        x = np.random.randint(0, 128, (50, 3))
        y = chain.map(x)
        rc = chain.reduce("all")
        self.assertIsInstance(rc, Chain)
        self.assertEqual(len(rc), 1)
        self.assertIsInstance(rc[0], TxDisplacementField)
        self.assertTrue(np.allclose(np.median(np.abs(rc[0].map(x) - y)), 0,
                                    atol=1e-3))

    def test_non_linear_summation(self):
        h, w, d = shape = (128, 128, 64)
        field = TField(shape, tensor_shape=(3,),
                       order=TENSOR_MAJOR, dtype="<f8")
        xx, yy, zz = np.mgrid[0:h, 0:w, 0:d]
        field.tensors[0] = -10 * np.sin(xx / h * 2 * np.pi)
        field.tensors[1] = -10 * (yy - yy / 2) / w
        field.tensors[2] = -10 * np.sin(zz / d * 2 * np.pi)
        pre = TxScale(1.1, 0.95, 1.0)
        ntx = TxDisplacementField(field, mode=NL_ABS, vectorder="xyz")
        post = TxTranslation(20, 10, 15.2)
        chain = Chain(pre, ntx, post)
        np.random.seed(0)
        x = np.random.randint(0, 128, (50, 3))
        y = chain.map(x)
        from functools import reduce
        from operator import add
        sc = reduce(add, chain)
        self.assertIsInstance(sc, TxDisplacementField)
        self.assertTrue(np.allclose(np.median(np.abs(sc.map(x) - y)), 0,
                                    atol=1e-3))


class TestRepresentation(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                           tx_embed, tx_scale3d, tx_trans3d)

    def test_repr(self):
        listing = "\n".join(tuple(str(tx) for tx in self.transformations))
        expected = f"Chain[\n{listing}\n]"
        self.assertEqual(str(self.chain), expected)


class TestSignature(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                           tx_embed, tx_scale3d, tx_trans3d)

    def test_signature(self):
        from tirl.signature import Signature
        sig = self.chain.signature()
        self.assertIsInstance(sig, Signature)
        self.assertEqual(len(sig), 4)
        chain = Chain(self.chain)
        self.assertEqual(sig, chain.signature())


class TestEquality(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                           tx_embed, tx_scale3d, tx_trans3d)

    def test_equality_by_identity(self):
        self.assertEqual(self.chain, self.chain)

    def test_equality_by_matrix_equality(self):
        c_linear = Chain(self.transformations[:3]).reduce(merge="linear")
        c_parametric = Chain(self.transformations[:3])
        self.assertEqual(len(c_parametric), 3)
        self.assertEqual(len(c_linear), 1)
        self.assertIsNot(c_parametric, c_linear)
        self.assertEqual(c_parametric, c_linear)

    def test_equality(self):
        chain = Chain(self.transformations)
        self.assertEqual(chain, self.chain)
        chain2 = chain.copy()
        self.assertEqual(chain, chain2)

    def test_inequality_by_type_mismatch(self):
        self.assertFalse(self.chain == self.transformations)

    def test_inequality(self):
        chain = Chain(self.transformations[:-1])
        self.assertNotEqual(chain, self.chain)
        chain = Chain(self.transformations)
        chain2 = chain.copy()
        chain2[2].parameters[0] = -1
        self.assertNotEqual(chain, chain2)

    def test_inequality_by_parameter_mismatch(self):
        chain1 = Chain(self.transformations[:3])
        chain2 = chain1.copy()
        chain2[0].parameters[0] = -100
        self.assertEqual(len(chain1), 3)
        self.assertEqual(len(chain2), 3)
        self.assertIsNot(chain1, chain2)
        self.assertFalse(chain1 == chain2)

    def test_inequality_by_parameter_mismatch_after_reduction(self):
        c_parametric = Chain(self.transformations[:3])
        c_linear = c_parametric.copy()
        c_linear = c_linear.reduce(merge="linear")
        self.assertEqual(len(c_parametric), 3)
        self.assertEqual(len(c_linear), 1)
        self.assertIsNot(c_parametric, c_linear)
        self.assertEqual(c_parametric, c_linear)

        # Change parameter value
        c_linear = c_parametric.copy()
        c_linear[0].parameters[0] = -100
        c_linear = c_linear.reduce(merge="linear")
        self.assertFalse(c_parametric == c_linear)

    @unittest.skip
    def test_inequality_by_metaparameter_mismatch(self):
        """
        Skipping because metaparameters are never equal: transformation names
        are stored in metaparameters, which are normally unique.
        """
        chain1 = Chain(self.transformations[:3])
        chain2 = chain1.copy()
        self.assertEqual(chain1, chain2)
        chain2[0].metaparameters["custom"] = "no longer equal"
        self.assertEqual(len(chain1), 3)
        self.assertEqual(len(chain2), 3)
        self.assertIsNot(chain1, chain2)
        self.assertFalse(chain1 == chain2)

    def test_inequality_by_kind_mismatch(self):
        chain1 = Chain(TxNonLinear(1))
        chain2 = Chain(TxLinear(np.eye(3)))
        self.assertFalse(chain1 == chain2)

    def test_inequality_by_parameter_mismatch_nonlinear(self):
        chain1 = Chain(TxNonLinear(1))
        chain2 = Chain(TxNonLinear(1))
        chain3 = Chain(TxNonLinear(2))
        self.assertEqual(chain1, chain2)
        self.assertFalse(chain2 == chain3)

    @unittest.skip
    def test_inequality_by_metaparameter_mismatch_nonlinear(self):
        """
        Skipping because metaparameters are never equal: transformation names
        are stored in metaparameters, which are normally unique.
        """
        chain1 = Chain(TxNonLinear(1))
        chain2 = Chain(TxNonLinear(1))
        chain3 = Chain(TxNonLinear(1, custom="not equal"))
        self.assertEqual(chain1, chain2)
        self.assertFalse(chain2 == chain3)

    # def test_inequality_by_parameter_mismatch_nonlinear(self):
    #     chain1 = Chain(TxLinear(np.eye(3)), TxNonLinear(1))
    #     chain2 = Chain(TxLinear(np.eye(3)), TxNonLinear(1))
    #     chain3 = Chain(TxLinear(np.eye(3)), TxNonLinear(2))
    #     self.assertEqual(chain1, chain2)
    #     self.assertFalse(chain2 == chain3)
    #
    # def test_inequality_by_metaparameter_mismatch_nonlinear(self):
    #     chain1 = Chain(TxLinear(np.eye(3)), TxNonLinear(1))
    #     chain2 = Chain(TxLinear(np.eye(3)), TxNonLinear(1))
    #     chain3 = Chain(TxLinear(np.eye(3)), TxNonLinear(1, custom="not equal"))
    #     self.assertEqual(chain1, chain2)
    #     self.assertFalse(chain2 == chain3)


class TestMapping(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_embed,
                           tx_scale3d, tx_trans3d)

    def test_merge_equivalence(self):
        chain = Chain(self.chain)
        reduced_linear = chain.reduce(merge="linear")
        self.assertEqual(len(reduced_linear), 3)
        reduced_all = chain.reduce(merge="all")
        # TODO: Open question: should generic transformations be reduced?
        # Currently they are left in the chain. Fusing them would create
        # problems with inversion (singular matrix, 2D/3D field inversion...)
        with self.assertRaises(AssertionError):
            self.assertEqual(len(reduced_all), 1)
        x0 = [1, 1]
        x1 = chain.map([x0], merge=False)
        eps = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps
        self.assertTrue(np.allclose(reduced_linear.map([x0]) - x1, 0, atol=eps))
        self.assertTrue(np.allclose(reduced_all.map([x0]) - x1, 0, atol=eps))

    def test_map_with_signature(self):
        x0 = [1, 1]
        x1 = self.chain.map([x0], merge=False)
        eps = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps
        x, sig = self.chain.map_with_signature([x0])
        self.assertTrue(np.allclose(x - x1, 0, atol=eps))


class TestIndexing(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_embed,
                           tx_scale3d, tx_trans3d)

    def test_index_by_position(self):
        self.assertIs(self.chain[2], self.transformations[2])
        multi = self.chain[2, 0]
        self.assertIs(multi[0], self.transformations[2])
        self.assertIs(multi[1], self.transformations[0])

    def test_index_by_name(self):
        self.assertIs(self.chain["shear"], self.transformations[2])
        multi = self.chain["shear", "scale"]
        self.assertIs(multi[0], self.transformations[2])
        self.assertIs(multi[1], self.transformations[0])


class TestVectorMapping(unittest.TestCase):

    def setUp(self):
        tx_rot2d = TxRotation2D(20, name="rotation")
        tx_scale2d = TxScale(1.1, 1.1, name="scale")
        tx_shear2d = TxShear(2, 0.5, name="shear")
        tx_warp = TxNonLinear(1)
        tx_embed = TxEmbed(1, name="embed")
        tx_scale3d = TxScale(1, 1, 1.1, name="scale")
        tx_trans3d = TxTranslation(-5, 4.2, 10, name="translation")
        self.transformations = [tx_scale2d, tx_rot2d, tx_shear2d, tx_warp,
                                tx_embed, tx_scale3d, tx_trans3d]
        self.chain = Chain(tx_scale2d, tx_rot2d, tx_shear2d, tx_embed,
                           tx_scale3d, tx_trans3d)

    def test_vector_mapping_inverse_consistency(self):
        np.random.seed(42)
        vectors = 5 * np.random.rand(347 * 347, 2)
        coords = np.stack(np.mgrid[0:347, 0:347], axis=-1).reshape((-1, 2))
        v = self.chain.map_vector(vectors, coords, rule=RULE_SSR)
        c = self.chain.map(coords)
        self.assertTrue(vectors.shape[-1] == 2)
        self.assertTrue(v.shape[-1] == 3)
        self.assertFalse(np.allclose(vectors, v[:, :2], atol=1e-3))
        invchain = self.chain.inverse()
        v0 = invchain.map_vector(v, c, rule=RULE_SSR)
        self.assertTrue(np.allclose(v0, vectors, atol=1e-3))


if __name__ == '__main__':
    unittest.main()
