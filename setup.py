#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT

import numpy as np
from setuptools import setup, Extension, find_packages
from Cython.Build import cythonize

#-------------------------------- C/C++ Modules -------------------------------#

# Field inversion (2D and 3D)
fieldinversion = \
    Extension("tirl.cmodules.finv",
              sources=["src/tirl/cmodules/finv2d_src.c",
                       "src/tirl/cmodules/finv3d_src.c",
                       "src/tirl/cmodules/finv.pyx"],
              include_dirs=[np.get_include()],
              extra_link_args=["-lopenblas", "-lgfortran"],
              extra_compile_args=["-std=c99"],
              language="c")

# FSLInterpolator
# Implements the linear interpolation routine of FLIRT (FSL). This is meant to
# be an example of a C++ extension to TIRL.
fslinterpolator = \
    Extension("tirl.cmodules.fslinterpolator",
              sources=["src/tirl/cmodules/fslinterpolator_src.cpp",
                       "src/tirl/cmodules/fslinterpolator.pyx"],
              extra_compile_args=["-fpermissive"],
              language="c++")

cython_modules = cythonize([fieldinversion, fslinterpolator],
    compiler_directives={'embedsignature': True, 'language_level': "3"}
)


#------------------------------ External Libraries ----------------------------#

with open("requirements.txt", "r") as fp:
    dependencies = [l.strip() for l in fp.readlines()]


#-------------------------------- TIRL Installer ------------------------------#

setup(name="tirl",
      version="2.2.1",
      description="Tensor Image Registration Library",
      author="Istvan N. Huszar",
      ext_modules=cython_modules,
      install_requires=dependencies,
      packages=find_packages("src"),
      package_dir={"tirl": "src/tirl"},
      scripts=["src/tirl/tirl"]
)
