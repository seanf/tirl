if [ -e ${FSLDIR}/etc/fslconf/requestFSLpythonLink.sh ]; then
  $FSLDIR/etc/fslconf/requestFSLpythonLink.sh tirl
fi

# Move the "share" directory if the package is installed in fslpython.
if [ ! -z ${FSLDIR} ]; then
  case "$PREFIX" in
  "${FSLDIR}"*)
    mkdir -p ${FSLDIR}/data/tirl
    sharedir=${FSLDIR}/data/tirl/share
    mv ${PREFIX}/lib/python*/site-packages/tirl/share ${sharedir}
    ;;
  *)
    sharedir=$(echo "${PREFIX}/lib/python*/site-packages/tirl/share")
    ;;
  esac
fi

testsdir=$(echo "${PREFIX}/lib/python*/site-packages/tirl/tests")

# Register the "share" and "tests" directory paths within TIRL
python -c "import tirl; tirl.set_sharedir(\"${sharedir}\")"
python -c "import tirl; tirl.set_testdir(\"${testsdir}\")"

# Set the TIRLHOME environment variable
conda env config vars set TIRLHOME="`tirl home`"
