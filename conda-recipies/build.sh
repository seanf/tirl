#!/bin/bash

# Copy share and test directories to the build directory
mkdir -p ${PREFIX}/lib/python${PY_VER}/site-packages/tirl/tests
cp -r $RECIPE_DIR/../tests ${PREFIX}/lib/python${PY_VER}/site-packages/tirl/
mkdir -p ${PREFIX}/lib/python${PY_VER}/site-packages/tirl/share
cp -r $RECIPE_DIR/../share ${PREFIX}/lib/python${PY_VER}/site-packages/tirl/

# Install the python package to the build directory
python setup.py install --single-version-externally-managed --record=record.txt

# Replace placeholders with the appropriate licensing information
python ${RECIPE_DIR}/replace.py ${PREFIX}

