#!/usr/bin/env python

import os
import re
import sys

cpattern = r"  CCOPYRIGHT  "
pypattern = r"# SHBASECOPYRIGHT"
cfiles = (".cpp", ".cxx", ".c", ".py")
pyfiles = (".py", ".pyi", ".pyx", "tirl")

# Load licence text
with open("LICENSE.txt", "r") as fp:
    for _ in range(16):
        fp.readline()
    licencetext = fp.read()

# Make the licence text a comment (only for py)
pylic = "# " + licencetext.replace("\n", "\n# ")
clic = "\n " + licencetext.replace("\n", "\n ").rstrip(" ")

for root, dirs, files in os.walk(sys.argv[1]):
    for f in files:
        src = os.path.realpath(os.path.join(root, f))
        if src == __file__:
            continue

        if f.endswith(pyfiles):
            try:
                with open(src, "r") as fp:
                    srctext = fp.read()
                licenced = re.sub(pypattern, pylic, srctext)
                with open(src, "w") as fp:
                    fp.write(licenced)
            except:
                continue

        elif f.endswith(cfiles):
            try:
                with open(src, "r") as fp:
                    srctext = fp.read()
                licenced = re.sub(cpattern, clic, srctext)
                with open(src, "w") as fp:
                    fp.write(licenced)
            except:
                continue

        else:
            continue
