#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import inspect


# IMPLEMENTATION

def create_error_message(exc, msg, default_msg, *args):
    try:
        caller = inspect.stack()[2][3]
        if msg is not None:
            # args = ("(caller: {}): ".format(caller) + msg,) + args
            args = (msg,) + args
        else:
            # args = ("(caller: {}): ".format(caller) + default_msg,) + args
            args = (default_msg,) + args
    except Exception:
        args = (default_msg,) + args
        raise
    finally:
        super(type(exc), exc).__init__(*args)


class ConstructionError(Exception):

    def __init__(self, msg, *args):
        try:
            caller = inspect.stack()[1][0].f_locals["self"].__class__.__name__
            args = ("{0}: {1}".format(caller, msg),) + args
        except:
            pass
        finally:
            super().__init__(*args)


class ArgumentError(Exception):
    """ Exception raised during the validation process, because the argument
    is required by the caller but it is invalid. """

    def __init__(self, msg=None, *args):
        default_msg = "Argument error."
        create_error_message(self, msg, default_msg, *args)


class CacheError(Exception):
    """ Exception that is raised upon unexpected result of Cache operation. """

    def __init__(self, msg=None, *args):
        default_msg = "Unexpected error while using object cache."
        create_error_message(self, msg, default_msg, *args)


class DomainError(Exception):
    """ Exception raised when an error within a Domain object would interfere
    with further execution of the program. """

    def __init__(self, msg=None, *args):
        default_msg = "Domain error."
        create_error_message(self, msg, default_msg, *args)


class TransformationError(Exception):
    """ Exception raised when an error within a Transformation object would
    interfere with further execution of the program. """

    def __init__(self, msg=None, *args):
        default_msg = "Transformation error."
        create_error_message(self, msg, default_msg, *args)


class OptionError(Exception):
    """ Exception raised when invalid or missing option in the settings file
    prevents further execution of the program. """

    def __init__(self, msg=None, *args):
        default_msg = "Option error."
        create_error_message(self, msg, default_msg, *args)


class CostError(Exception):
    """ Exception raised when invalid or missing option in the settings file
    prevents further execution of the program. """

    def __init__(self, msg=None, *args):
        default_msg = "Cost function error."
        create_error_message(self, msg, default_msg, *args)


class OptimisationError(Exception):
    """ Exception raised when invalid or missing option in the settings file
    prevents further execution of the program. """

    def __init__(self, msg=None, *args):
        default_msg = "Optimisation error."
        create_error_message(self, msg, default_msg, *args)


class BroadcastError(Exception):
    """ Exception raised when invalid or missing option in the settings file
    prevents further execution of the program. """

    def __init__(self, msg=None, *args):
        default_msg = "Broadcast error."
        create_error_message(self, msg, default_msg, *args)


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
