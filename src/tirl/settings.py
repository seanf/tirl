#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  _______   _____   _____    _                                __   _
#  |__   __| |_   _| |  __ \  | |                              / _| (_)
#     | |      | |   | |__) | | |        ___    ___    _ __   | |_   _    __ _
#     | |      | |   |  _  /  | |       / __|  / _ \  | '_ \  |  _| | |  / _` |
#     | |     _| |_  | | \ \  | |____  | (__  | (_) | | | | | | |   | | | (_| |
#     |_|    |_____| |_|  \_\ |______|  \___|  \___/  |_| |_| |_|   |_|  \__, |
#                                                                         __/ |
#                                                                        |___/
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                  Directories                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
import os

# Temporary working directory (use absolute path!)
TWD = "/tmp/temp/TWD"
try:
    if not os.path.isdir(TWD):
        os.makedirs(TWD)
except Exception:
    import warnings
    warnings.warn("Temporary working directory (TWD) could not be created at "
                  "{}".format(TWD))

# Share directory
TIRLSHARE = os.path.join(os.path.dirname(__file__), "..", "..", "share")
# Tests directory
TIRLTESTS = os.path.join(os.path.dirname(__file__), "..", "..", "tests")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                   Resources                                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Number of computing cores
CPU_CORES = 1
# Default float type
DEFAULT_FLOAT_TYPE = "f8"
DEFAULT_SHELL = "/bin/bash"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                   Behaviour                                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
import warnings
warnings.simplefilter("ignore")
TYPESAFE_MODE = False
COST_VALUE_LOG_LEVEL = 7
PARAMETER_UPDATE_LOG_LEVEL = 5

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                               TIRLObject class                               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Default file extensions (must be lower-case)
EXTENSIONS = {
    "TField"                 : "tf",
    "TImage"                 : "timg",
    "Domain"                 : "dom",
    "Transformation"         : "tx",
    "TransformationGroup"    : "txg",
    "TIRLObject"             : "tobj"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                             ParameterVector class                            #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
DEFAULT_PARAMETER_DTYPE = "f8"
# Pedantic option. This will halt the program if any transformation parameter
# gets out of bounds. If True, one must make sure that bounds are ALWAYS
# updated with the parameters. Otherwise the program may suddenly terminate
# when it sees new data.
RAISE_BOUNDS_ERROR = True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 Domain class                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
DOMAIN_INSTANCE_MEMORY_LIMIT = 2048    # in MiB   (recommended: at least 2 GiB)
DOMAIN_N_THREADS = 6

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                              Interpolator class                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
CHUNK_INTERPOLATION_IF_LARGER = 225000000  # (15000 x 15000)
FILL_VALUE = 0
IPOL_INSTANCE_MEMORY_LIMIT = 4096  # in MB

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 TField class                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Maximum amount of memory that any TImage instance may occupy
TFIELD_INSTANCE_MEMORY_LIMIT = 3072    # in MB
# Chunksize for operations on memory-mapped tensor fields
TFIELD_CHUNKSIZE = 1024   # in MiB
TFIELD_INTERPOLATION_MODE = "inv+regular"  # do not change this
TFIELD_DEFAULT_FLOAT_TYPE = "<f8"
# Data layout schemes
VOXEL_MAJOR = "V"
TENSOR_MAJOR = "T"
TFIELD_DEFAULT_LAYOUT = VOXEL_MAJOR
# Change the physical layout of data in memory on swapping tensor order
SWAPDATA = False
# Default interpolators
DEFAULT_COMPACT_INTERPOLATOR = \
    "tirl.interpolators.scipyinterpolator.ScipyInterpolator"
DEFAULT_NONCOMPACT_INTERPOLATOR = \
    "tirl.interpolators.rbfinterpolator.RbfInterpolator"
TFIELD_PRESMOOTH = True
TFIELD_PRESMOOTH_KERNELSIZE_NSIGMA = 3
# Ignore singleton dimensions when loading data from array/file
TFIELD_IGNORE_SINGLETON_DIMENSIONS = True
# Autodetect tensor axes when loading data from file
TFIELD_AUTODETECT_TENSOR_AXIS = True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 TImage class                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# # Maximum amount of memory that any TImage instance may occupy
TIMAGE_INSTANCE_MEMORY_LIMIT = 3072  # in MiB
TIMAGE_MASK_INTERPOLATOR = \
    "tirl.interpolators.scipyinterpolator.ScipyInterpolator"
# Presmoothing before resampling: kernel size defined as the number of sigmas
TIMAGE_PRESMOOTH = True
TIMAGE_PRESMOOTH_KERNELSIZE_NSIGMA = 3
TIMAGE_DEFAULT_SNAPSHOT_EXT = "png"
TIMAGE_MASK_MISSING_DATA = True  # added 13 Aug 2020

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                  Operations                                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
CHUNK_OPERATION_IF_LARGER = 225000000  # (15000 x 15000)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                Transformations                               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
TX_INSTANCE_MEMORY_LIMIT = 3072  # in MiB

# Control point placement: seed number for the pseudorandom Halton algorithm
HALTON_SEED = 1

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 Visualisation                                #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
ENABLE_VISUALISATION = True
MPL_BACKEND = "Qt5Agg"
