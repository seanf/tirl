#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import dill
import psutil
import shutil
import builtins
import warnings
import tempfile
import numpy as np
from math import ceil
from operator import mul
from pydoc import locate
import multiprocessing as mp
from functools import reduce
from scipy.ndimage.filters import gaussian_filter


# TIRL IMPORTS

import tirl
from tirl.tirlobject import TIRLObject
from tirl.domain import Domain
from tirl.buffer import Buffer
from tirl.interpolators.interpolator import Interpolator
from tirl.tfield import TField
from tirl.operations.operations import Operator
from tirl.operations.spatial import SpatialOperator
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.linear.affine import TxAffine
from tirl import exceptions as te, tirlvision, utils as tu, settings as ts
from tirl.utils import isreal


# DEFINITIONS

from tirl.constants import *

# DEVELOPMENT NOTES

# TODO: Rebuild instance memory limit
# TODO: Test whether copying/referencing is intuitive


# IMPLEMENTATION

class ResolutionManager(TIRLObject):
    """
    Plug-in container that manages the low- and high-resolution image and mask
    data of a TImage instance.

    """
    def __init__(self, source):
        """
        Initialisation of ResolutionManager. The image and mask data that is
        available from the host TImage at the time of initialisation will be
        considered as high-resolution data, from which other samplings of the
        data will be derived.

        :param source: template image or field
        :type source: Union[TImage, TField]

        """
        # Call superclass initialisation
        super(ResolutionManager, self).__init__()

        # Set high-resolution buffers
        if hasattr(source, "__timage__"):
            self._mask = source._mask  # buffer
            self._header = source.header
        elif hasattr(source, "__tfield__"):
            self._mask = None
            self._header = {}
        else:
            raise TypeError("Expected TImage or TField as source, got {}."
                            .format(source.__class__.__name__))
        self._data = source._data  # buffer; mutable but not changed
        self._dtype = source.dtype
        self._dtype = source.dtype
        self._domain = source.domain  # mutable, but not changed
        self._interpolator = source.interpolator.copy()  # mutable, so copy
        self._order = source.order
        self._storage = source.storage
        self._kwargs = source.kwargs.copy()  # mutable, so copy
        self._srcname = source.name
        self._taxes = source.taxes
        self._tshape = source.tshape
        self._vaxes = source.vaxes
        self._vshape = source.vshape

    def copy(self):
        """
        Copy constructor. Note that copying the resolution manager implies
        copying the template image, which might be large.

        :returns: new ResolutionManager instance with new template image
        :rtype: ResolutionManager

        """
        # TODO Test this implementation!
        obj = super(ResolutionManager, self).__new__(self.__class__)
        obj._header = self._header.copy
        obj._mask = self._mask.copy() if self._mask is not None else None
        obj._data = self._data.copy()  # buffer
        obj._dtype = self._dtype
        obj._domain = self._domain.copy()
        obj._interpolator = self._interpolator.copy()
        obj._kwargs = self._kwargs.copy()
        obj._order = self._order
        obj._storage = self._storage
        obj._srcname = self._srcname
        obj._taxes = self._taxes
        obj._tshape = self._tshape
        obj._vaxes = self._vaxes
        obj._vshape = self._vshape

        return obj

    @classmethod
    def _load(cls, dump):
        template = TImage._load(dump)
        return cls(template)

    def _dump(self):
        rmgr = self.template.resmgr
        self.template.resmgr = None         # avoid infinite loop
        objdump = self.template.dump()
        objdump.update(super(ResolutionManager, self)._dump())  # type, id
        self.template.resmgr = rmgr
        return objdump

    def get(self, scale, *scales, fov=None, update_chain=True,
            presmooth=ts.TIMAGE_PRESMOOTH):
        """
        Creates resampled version of the template TImage, which has the same
        physical coordinates and shares transformations with the original
        instance. Note that non-linear transformations may still be replaced if
        update_chain=True (default).

        :param scale:
            Global scaling factor (relative to the high-resolution data).
            If further values are specified, this is interpreted as the
            scaling factor for the first axis.
        :type scale: int
        :param scales:
            Scaling factors along higher dimensions.
        :type scales: tuple[int]
        :param fov:
            Field of view, defined as a 2-tuple: the voxel coordinates of the
            most proximal and the most distant point from the origin of the
            voxel grid. If None, the FOV spans the whole template image.
        :type fov: Union[tuple, NoneType]
        :param update_chain:
            If True (default), non-linear transformations that are linked with
            the TImage domain will remain linked, which implies that these
            will be resampled via their regrid() method. If False, all
            non-linear transformations will be detached and left unchanged.
            In either case the identity of all transformation objects is
            preserved (no copying will take place).
        :type update_chain: bool
        :param presmooth:
            Applies Gaussian presmoothing before downsampling (no smoothing is
            performed for upsampling). This ensures that all domain points at
            the lower resolution have proper support in the higher-resolution
            image, therefore all original information is (at least indirectly)
            represented.
        :type presmooth: bool

        :returns: resampled TImage.
        :rtype: TImage

        """
        img, mask = \
            self._get_scaled_data(scale, *scales, fov=fov, presmooth=presmooth)
        # Update linked transformations and reattach the transformation chain
        img.domain.copy_transformations(self.domain, links=update_chain)
        # Update the name of the image
        scales = (scale,) + tuple(scales) if scales else (scale,)
        name = self.imgname + "_res{}".format("x".join(str(s) for s in scales))

        # Create a new resampled TImage instance, and let the new instance
        # inherit the resolution manager.
        ret = TImage.fromTField(img, copy=False, dtype=self.dtype, mask=mask,
            name=name, domain=img.domain, order=self.order,
            interpolator=img.interpolator, storage=self.storage,
            header=self.header.copy(), **self.kwargs.copy())
        # Set the scale of the new image and delegate the ResolutionManager
        # from the parent instance.
        ret._scale = scales
        ret.resmgr = self

        return ret

    def _get_scaled_data(self, scale, *scales, fov=None,
                         presmooth=ts.TIMAGE_PRESMOOTH):
        """
        Generates a scaled version of the high-resolution image and mask data
        buffers by resampling them on a new domain. Calculates scale offset
        compensation for the new domain to preserve the physical coordinates of
        the template TImage.

        :param scale:
            Global scaling factor (relative to the high-resolution data).
            If further values are specified, this is interpreted as the
            scaling factor for the first axis.
        :type scale: int
        :param scales:
            Scaling factors along higher dimensions.
        :type scales: tuple[int]
        :param fov:
            Field of view, defined as a 2-tuple: the voxel coordinates of the
            most proximal and the most distant point from the origin of the
            voxel grid. If None, the FOV spans the whole template image.
        :type fov: Union[tuple, NoneType]
        :param presmooth:
            Applies Gaussian presmoothing before downsampling (no smoothing is
            performed for upsampling). This ensures that all domain points at
            the lower resolution have proper support in the higher-resolution
            image, therefore all original information is (at least indirectly)
            represented.
        :type presmooth: bool

        :returns: domain, resampled image data and mask buffers
        :rtype: tuple[Domain, Buffer, Union[Buffer, NoneType]]

        """
        scales = (scale,) + tuple(scales) if scales else (scale,)
        if not all(isreal(f) and f > 0 for f in scales):
            raise ValueError("TImage scaling factors must be numeric and "
                             "positive.")

        # Convert image and mask data to TField format
        imgdata = TField.fromarray(
            arr=self._data.data, tensor_axes=self.taxes, copy=False,
            domain=self.domain[:0], order=None, dtype=None,
            interpolator=self._interpolator.__class__, name=None,
            storage=self._storage, **self._kwargs)

        if self._mask is not None:
            maskip = locate(ts.TIMAGE_MASK_INTERPOLATOR)
            maskdata = TField.fromarray(
                arr=self._mask.data, tensor_axes=(), copy=False,
                domain=self.domain[:0], order=None, dtype=ts.DEFAULT_FLOAT_TYPE,
                interpolator=maskip, name=None, storage=self._storage,
                **self._kwargs)
        else:
            maskdata = None

        if np.allclose(scales, 1):
            return imgdata, maskdata

        # If the scale is not 1, resample the image and mask data
        img = imgdata.resample(*scales, fov=fov, update_chain=False,
                               presmooth=presmooth)
        if maskdata is not None:
            mask = maskdata.resample(
                *scales, fov=fov, update_chain=False, presmooth=presmooth)
            # Make sure that the mask and the image are on the same domain
            mask.domain = img.domain
        else:
            mask = maskdata

        return img, mask

    @property
    def template(self):
        """
        Returns the high-resolution image template that is used by the current
        Resolution Manager instance to generate resampled image and mask data.

        :returns: high-resolution TImage template
        :rtype: TImage

        """
        return TImage(
            source=self.data, tensor_axes=self.taxes, dtype=self.dtype,
            mask=self.mask, name=self.imgname, domain=self.domain,
            order=self.order, interpolator=self.interpolator,
            storage=self.storage, header=self.header, **self.kwargs)

    @property
    def dtype(self):
        return self._dtype

    @property
    def order(self):
        return self._order

    @property
    def storage(self):
        return self._storage

    @property
    def header(self):
        return self._header

    @property
    def kwargs(self):
        return self._kwargs

    @property
    def imgname(self):
        return self._srcname

    @property
    def vshape(self):
        return self._vshape

    @property
    def vaxes(self):
        return self._vaxes

    @property
    def tshape(self):
        return self._tshape

    @property
    def taxes(self):
        return self._taxes

    @property
    def domain(self):
        return self._domain

    @domain.setter
    def domain(self, d):
        """
        Domain setter method. For advanced users only. The input must be a
        Domain instance that matches the voxel shape of the TImage data at the
        current resolution.

        """
        if isinstance(d, Domain):
            if self.domain.shape == d.shape:
                self._domain = d
            else:
                raise AssertionError(
                    "Cannot assign domain with shape {} to a {} with voxel "
                    "shape {}.".format(d.shape, self.__class__.__name__,
                                       self.vshape))
        else:
            raise TypeError("Expected a Domain instance, got {}."
                            .format(d.__class__.__name__))

    @property
    def data(self):
        """
        High-resolution TImage data.

        :returns: image data
        :rtype: np.ndarray

        """
        return self._data.data

    @property
    def mask(self):
        """
        High-resolution TImage mask.

        :returns: image mask
        :rtype: np.ndarray

        """
        if self._mask is not None:
            return self._mask.data

        else:
            return None

    @mask.setter
    def mask(self, newmask):
        """
        High-resolution TImage mask setter method.

        """
        if newmask is not None:
            if isinstance(newmask, np.ndarray) or isinstance(newmask, Buffer):
                newmask = TField.fromarray(newmask)
            else:
                newmask = TField(newmask)
            if newmask.dtype == np.bool:
                newmask.dtype = ts.DEFAULT_FLOAT_TYPE
            if newmask.tdim > 1:
                raise TypeError(
                    "Mask values must be scalars, but the specified mask has "
                    "tensor shape {}.".format(newmask.tshape))
            elif newmask.vshape == self.vshape:
                self._mask = newmask._data
            else:
                raise te.DomainError(
                    "The specified mask {} is incompatible with a {} that has "
                    "voxel shape {}.".format(
                        newmask.vshape, self.__class__.__name__,
                        self.domain.shape))
        else:
            self._mask = None

    @property
    def interpolator(self):
        return self._interpolator

    @interpolator.setter
    def interpolator(self, ip):
        """
        Sets the interpolator that is used to resample the high-resolution
        image and mask data.

        """
        # Create interpolator based on the Domain type (compact vs. non-compact)
        if ip is None:
            # Choose default interpolator according to the domain type
            if self.domain.iscompact:
                ip = locate(ts.DEFAULT_COMPACT_INTERPOLATOR)
            else:
                ip = locate(ts.DEFAULT_NONCOMPACT_INTERPOLATOR)

        # Create interpolator instance from class specification
        if issubclass(type(ip), type) and issubclass(ip, Interpolator):
            ip = ip(source=self.data, n_threads=-1)
            ip.tensor_axes = self.taxes

        # Adapt existing interpolator instance
        elif isinstance(ip, Interpolator):
            # If interpolator is given, set the source to the TField data
            if ip.tensor_axes == self.taxes:
                ip._values = self.data
            else:
                raise ValueError(
                    "The tensor axis specification of the interpolator {} "
                    "does not match {} with tensor axis/axes {}"
                    .format(ip.tensor_axes, self.__class__.__name__,
                            self.taxes))
        else:
            raise TypeError("Expected Interpolator type, got {}"
                            .format(ip.__class__.__name__))

        # Update interpolator
        self._interpolator = ip


class TImage(TField):
    """
    The TImage class implements a versatile image format that automatically
    loads data from the most common image formats, readily supports memory
    mapping, and provides full compatibility with all transformation objects in
    TIRL.

    TImage is a subclass of TField. The TImage class additionally supports
    dynamic up- and downscaling of the image without degradation of image
    quality on repeated samplings, masking, and additional image manipulation
    methods.

    """
    # Set the array priority higher than that of TField (15.0),
    # np.matrix (10.0), and np.array (0.0), so that this class can control
    # ufunc behaviour.
    __array_priority__ = 25.0

    # Supported image types
    SUPPORTED_IMAGE_TYPES = ("bmp", "gif", "jpg", "pgm", "png", "pnm", "tif",
                             ".nii", ".nii.gz", ".mgz", ".svs")

    # Constructor arguments are reserved keyword arguments
    RESERVED_KWARGS = ("source", "t_axes", "dtype", "mask", "name", "order",
                       "interpolator", "storage", "header")

    def __new__(cls, source, tensor_axes=None, dtype=None, mask=None, name=None,
                domain=None, order=None, interpolator=None, storage=MEM,
                header=None, **kwargs):
        """
        TImage default constructor.

        :param source:
            Image source, which may be a file path, an array, a buffer, a
            TField, or a TImage. If the source is a TField instance, it will
            be converted to a TImage instance without copying the data. If the
            source is another TImage instance, the object is returned
            unchanged unless further arguments are specified.
        :type source: Union[str, np.ndarray, np.memmap, TField, TImage, Buffer]
        :param tensor_axes:
            Tensor axis indices. If None, and the source is a file name, the
            tensor axis is automatically identified for RGB(A) and TImage
            formats.
        :type tensor_axes: Union[tuple[int], int, None]
        :param dtype:
            Image data type.
        :type dtype: Union[type, np.dtype]
        :param mask:
            TImage mask. The mask is defined over the spatial domain of the
            TImage, and acts as a multiplier to all tensor components when the
            cost function is calculated.
        :type mask: Union[np.ndarray, np.memmap, TField, TImage, Buffer]
        :param name:
            Name of the TImage instance for easier identification.
        :type name: Union[str, None]
        :param domain:
            User-specified domain. The shape of the domain must match the input
            image data. If None (default), the domain is automatically created
            for the image data.
        :type domain: Union[Domain, NoneType]
        :param order:
            Data layout of TImage. Can be either TENSOR_MAJOR (default) or
            VOXEL_MAJOR.
        :type order: Union[None, str]
        :param interpolator:
            Interpolator object that is used to evaluate TImage values at
            various physical locations. If None, the default interpolator is
            used.
        :type interpolator: Union[Interpolator, None]
        :param storage:
            Storage mode. If "mem", image (and mask) data is stored in memory,
            if "hdd", image and mask data is stored in a memory-mapped file on
            the hard disk.
        :type storage: Union["mem", "hdd"]
        :param header: TImage header
        :type header: Union[dict, NoneType]
        :param kwargs:
            Additional keyword arguments for TImage.
            level: Specifies the resolution level at which the histological
                   image should be loaded.
        :type kwargs: dict

        :returns: new TImage instance
        :rtype: TImage

        """
        # Validate input arguments
        if ts.TYPESAFE_MODE:
            TImage.__validate_input(
                source=source, tensor_axes=tensor_axes, dtype=dtype, mask=mask,
                name=name, domain=domain, order=order,
                interpolator=interpolator, storage=storage, header=header)

        # Handle the polymorphism of the 'source' argument

        # source: file path
        if isinstance(source, str):
            if source.endswith("." + ts.EXTENSIONS["TImage"]):
                # If the file is a TImage archive, load the object
                timg = TImage.load(source)
                return TImage.__new__(
                    cls, source=timg, tensor_axes=tensor_axes, dtype=dtype,
                    mask=mask, name=name, domain=domain, order=order,
                    interpolator=interpolator, storage=storage, **kwargs)
            else:
                # Otherwise load the data array and the header (if exists)
                arr, hdr = TImage._load_from_file(
                    source, storage=str(storage).lower(), dtype=dtype, **kwargs)
        # source: array-like object
        elif isinstance(source, (np.ndarray, np.memmap)):
            arr, hdr = source, {}
        elif isinstance(source, Buffer):
            arr = source.data
            hdr = {"input_file": source.fname, "input_file_no": source.file_no}
        # source: TImage
        elif isinstance(source, TImage):
            tensor_axes = tensor_axes or source.taxes
            dtype = dtype or source.dtype
            mask = mask or source.mask
            name = name or source.name
            domain = domain or source.domain
            order = order or source.order
            interpolator = interpolator or source.interpolator
            storage = storage or source.storage
            kwargs.update(source.kwargs.copy())
            kwargs = {k: v for (k, v) in kwargs.items()
                      if k not in TImage.RESERVED_KWARGS}
            # Create an updated version of the instance (not a copy)
            return TImage.__new__(
                cls, source=source.data, tensor_axes=tensor_axes,
                dtype=dtype, mask=mask, name=name, domain=domain, order=order,
                interpolator=interpolator, storage=storage, **kwargs)
        # source: TField
        elif isinstance(source, TField):
            # Create a TImage from a TField (note: the new instance will share
            # the data with the original TField).
            return TImage.fromTField(
                source, copy=False, dtype=dtype, mask=mask, name=name,
                domain=domain, order=order, interpolator=interpolator,
                storage=storage, header=header, **kwargs)
        # source: any other type
        else:
            try:
                # Try converting source to a numeric array
                arr, hdr = np.asarray(source, dtype=dtype), {}
                assert np.issubdtype(arr.dtype, np.number)
            except Exception:
                raise te.ConstructionError(
                    "Expected file path, array-like object, TImage or TField "
                    "as source, got {}.".format(type(source)))

        # Prepare array and determine tensor axes
        arr, tensor_axes = cls._prepare_array(arr, tensor_axes)
        # Prepare header
        if header is not None:
            hdr.update(header)

        # Create TImage using the array constructor of the class
        obj = TImage.fromarray(
            arr, tensor_axes=tensor_axes, copy=False, domain=domain,
            dtype=dtype, mask=mask, name=name, order=order,
            interpolator=interpolator, storage=storage, header=hdr, **kwargs)

        # Apply affine transformation from header (if exists and not disabled)
        niftiheader = obj.header.get("nifti_header", None)
        initialise_with_sform = bool(kwargs.get("sform", True))
        if (niftiheader is not None) and initialise_with_sform:
            sform = niftiheader.get_best_affine()
            tx = TxAffine(sform[:3], name="sform", lock="all")
            obj.domain.chain.append(tx)

        # Enforce radiological orientation (like FSL)
        # This option ensures compatibility with FSL transformations
        elif (niftiheader is not None) and kwargs.get("fsl", False):
            xdim, ydim, zdim = niftiheader.get_zooms()[:3]
            xsize, ysize, zsize = niftiheader.get_data_shape()
            sform = niftiheader.get_best_affine()
            sm = np.diag([xdim, ydim, zdim, 1])
            if np.linalg.det(sform) < 0:
                swapmat = np.diag([-1, 1, 1, 1], dtype=ts.DEFAULT_FLOAT_TYPE)
                swapmat[0, -1] = xsize - 1
                sm = sm @ swapmat
            tx = TxAffine(sm, name="fsl", lock="all")
            obj.domain.chain.append(tx)

        return obj

    @classmethod
    def fromarray(cls, arr, tensor_axes=(), copy=True, domain=None, dtype=None,
                  mask=None, name=None, order=None, interpolator=None,
                  storage=MEM, header=None, **kwargs):
        """
        Alternative TImage constructor that creates TImage from array data.
        The input array will be used as high-resolution data and no mask will
        be assigned to the TImage. Depending on the copy parameter, the
        resultant TImage instance either shares the data with the array object,
        or a copy is made, and the TImage becomes independent from the array.

        :param arr:
            Input array. Any object that defines the __array__ interface.
        :type arr: np.ndarray
        :param tensor_axes:
            Index or indices of the tensor axis or axes.
        :type tensor_axes: tuple[int]
        :param copy:
            If False, the resultant TImage will share the data with the original
            array instance. If True (default), the array data is copied to the
            new TImage instance, making it independent from the original array.
        :type copy: bool
        :param domain:
            Domain of the new TImage instance. The shape of the domain must
            match the spatial extent of the input array. If None, a new Domain
            will be automatically created based on the shape of the input array.
        :type domain: Union[Domain, NoneType]
        :param dtype:
            Tensor data type. If None, the data type will be inferred from the
            input array.
        :type dtype: Union[np.dtype, NoneType]
        :param mask:
            TImage mask used for cost function masking. This may be specified
            with a TField or any array-like object (no copies will be made).
        :type mask: Union[np.ndarray, np.memmap, TField, TImage, Buffer]
        :param name:
            Name of the new TImage instance. If None, a name of the following
            format will be automatically generated: "TImage_$memaddress".
        :type name: Union[str, NoneType]
        :param order:
            Buffer layout (C-contiguous). 'T' for tensor major (i.e. voxels are
            stored contiguously for each tensor element), 'V' for voxel major
            (i.e. tensor values are stored contiguously for each voxel). If
            None, the axis order will be determined from the specified tensor
            axes.
        :type order: Union[str, NoneType]
        :param interpolator:
            Interpolator instance. If None, a new interpolator will be
            automatically created based on the type of the domain.
        :type interpolator: Union[Interpolator, NoneType]
        :param storage:
            If "hdd", the tensor field data will be stored on the HDD (in the
            temp folder of TIRL). If "mem" (default), the tensor field data
            will be stored in memory. If there is not enough memory available,
            "hdd" mode will be automatically selected.
        :type storage: str
        :param header: TImage header
        :type header: Union[dict, NoneType]
        :param kwargs:
            Additional keyword arguments for the new TImage instance.
        :type kwargs: dict

        :returns: new TImage instance based on the input array
        :rtype: TImage

        """
        # Note on subclassing
        #
        # In the code below, the first class reference must be abstract,
        # whereas the second one must be explicit. In case TImage is subclassed
        # in the future, this ensures that subclass construction is
        # type-consistent, and the subclass can reimplement the fromTField and
        # fromTImage constructors without breaking down the second constructor
        # call in the expression below.
        #
        # If the second class reference was abstract, the subclass'
        # fromTField constructor would be invoked upon subclass construction.
        # If it was implemented consistently with its name, it would transform
        # a TField to the subclass type, hence it would be incompatible with
        # the TImage-like subclass instance that is the result of the first
        # constructor call. (Also because it starts with an explicit type
        # check.)

        # Create TField instance from array
        obj = super().fromarray(
            arr, tensor_axes=tensor_axes, copy=copy, domain=domain, order=order,
            dtype=dtype, interpolator=interpolator, name=name, storage=storage,
            **kwargs)

        # Create TImage instance from TField instance
        obj = TImage.fromTField(
            obj, copy=copy, dtype=dtype, mask=mask, name=name, domain=domain,
            order=order, interpolator=interpolator, storage=storage,
            header=header, **kwargs)

        return obj

    @classmethod
    def fromTField(cls, tfield, copy=True, dtype=None, mask=None, name=None,
                   domain=None, order=None, interpolator=None, storage=MEM,
                   header=None, **kwargs):
        """
        Alternative TImage constructor that creates TImage from TField.

        :param tfield:
            Input TField instance
        :type tfield: TField
        :param copy:
            If True, the returned TImage instance will not share the data with
            the original instance. If False, the data will be passed on to the
            new TImage instance by reference.
        :type copy: bool
        :param copy:
            If False, the resultant TImage will share the data with the original
            array instance. If True (default), the array data is copied to the
            new TImage instance, making it independent from the original array.
        :type copy: bool
        :param dtype:
            Tensor data type. If None, the data type will be inferred from the
            input array.
        :type dtype: Union[np.dtype, NoneType]
        :param mask:
            TImage mask used for cost function masking. This may be specified
            with a TField or any array-like object (no copies will be made).
        :type mask: Union[np.ndarray, np.memmap, TField, TImage, Buffer]
        :param name:
            Name of the new TImage instance. If None, a name of the following
            format will be automatically generated: "TImage_$memaddress".
        :type name: Union[str, NoneType]
        :param domain:
            User-specified domain. The shape of the domain must match the input
            field data. If None (default), the domain is automatically created
            for the field data.
        :type domain: Union[Domain, NoneType]
        :param order:
            Buffer layout (C-contiguous). 'T' for tensor major (i.e. voxels are
            stored contiguously for each tensor element), 'V' for voxel major
            (i.e. tensor values are stored contiguously for each voxel). If
            None, the axis order will be determined from the layout of the
            input TField.
        :type order: Union[str, NoneType]
        :param interpolator:
            Interpolator instance. If None, the interpolator of the input
            TField will be used.
        :type interpolator: Union[Interpolator, NoneType]
        :param storage:
            If "hdd", the tensor field data will be stored on the HDD (in the
            temp folder of TIRL). If "mem" (default), the tensor field data
            will be stored in memory. If there is not enough memory available,
            "hdd" mode will be automatically selected. If None, the storage
            mode will be inferred from the same property of the input TField.
        :type storage: str
        :param header: TImage header
        :type header: Union[dict, NoneType]
        :param kwargs:
            Additional keyword arguments for the new TImage instance.
        :type kwargs: dict

        :returns: new TImage instance based on the input TField
        :rtype: TImage

        """
        if not isinstance(tfield, TField):
            raise TypeError("Expected TField input, got {} instead."
                            .format(tfield.__class__.__name__))

        if copy:
            source = tfield._data.copy().data
            domain = domain or tfield.domain.copy()
            # Create new interpolator of the same kind
            # ipc = tfield.interpolator.__class__
            # ip = tfield.interpolator
            # if not interpolator:
            #     interpolator = ipc(source, ip.threads, ip.verbose, **ip.kwargs)
            #     interpolator.tensor_axes = tfield.taxes
            if not interpolator:
                interpolator = tfield.interpolator.copy()
                interpolator.values = source
                interpolator.tensor_axes = tfield.taxes
            newkwargs = tfield.kwargs.copy()
            newkwargs.update(kwargs)
        else:
            source = tfield.data
            domain = domain or tfield.domain
            interpolator = interpolator or tfield.interpolator.copy()
            newkwargs = tfield.kwargs.copy()
            newkwargs.update(kwargs)

        # Set default parameter values and purge keyword arguments
        dtype = dtype or tfield.dtype
        order = order or tfield.order
        name = name or (tfield.name + "_asTImage")
        storage = storage or tfield.storage
        newkwargs = {k: v for (k, v) in newkwargs.items()
                     if k not in TImage.RESERVED_KWARGS}

        # Create TImage instance and set user-defined order on it.
        obj = super().__new__(
            cls, domain, tensor_shape=tfield.tshape, order=order, dtype=dtype,
            buffer=source, offset=0, interpolator=interpolator, name=name,
            storage=storage, **newkwargs)
        obj.order = order or tfield.order
        assert isinstance(obj, TImage)

        # Add TImage-specific attributes: mask, resolution manager, and header
        obj.mask = mask
        obj._scale = (1.0,) * obj.vdim  # avoid resample() for speed
        obj.resmgr = ResolutionManager(obj)
        obj.properties.update({"header": header or {}})

        return obj

    def __init__(self, source, tensor_axes=None, dtype=None, mask=None,
                 name=None, domain=None, order=None, interpolator=None,
                 storage=MEM, header=None, **kwargs):
        """
        Initialisation of TImage. Everything that is specific to TImage is
        set by this method.

        :param source:
            Image source, which may be a file path, an array, a buffer, a
            TField, or a TImage. If the source is a TField instance, it will
            be converted to a TImage instance without copying the data. If the
            source is another TImage instance, the object is returned
            unchanged unless further arguments are specified.
        :type source: Union[str, np.ndarray, np.memmap, TField, TImage, Buffer]
        :param tensor_axes:
            Tensor axis indices. If None, and the source is a file name, the
            tensor axis is automatically identified for RGB(A) and TImage
            formats.
        :type tensor_axes: Union[tuple[int], int, None]
        :param dtype:
            Image data type.
        :type dtype: Union[type, np.dtype]
        :param mask:
            TImage mask. The mask is defined over the spatial domain of the
            TImage, and acts as a multiplier to all tensor components when the
            cost function is calculated.
        :type mask: Union[np.ndarray, np.memmap, TField, TImage, Buffer]
        :param name:
            Name of the TImage instance for easier identification.
        :type name: Union[str, None]
        :param domain:
            User-specified domain. The shape of the domain must match the input
            image data. If None (default), the domain is automatically created
            for the image data.
        :type domain: Union[Domain, NoneType]
        :param order:
            Data layout of TImage. Can be either TENSOR_MAJOR (default) or
            VOXEL_MAJOR.
        :type order: Union[None, str]
        :param interpolator:
            Interpolator object that is used to evaluate TImage values at
            various physical locations. If None, the default interpolator is
            used.
        :type interpolator: Union[Interpolator, None]
        :param storage:
            Storage mode. If "mem", image (and mask) data is stored in memory,
            if "hdd", image and mask data is stored in a memory-mapped file on
            the hard disk.
        :type storage: Union["mem", "hdd"]
        :param header: TImage header
        :type header: Union[dict, NoneType]
        :param kwargs:
            Additional keyword arguments for TImage.
            level: Specifies the resolution level at which the histological
                   image should be loaded.
        :type kwargs: Any

        :returns: new TImage instance
        :rtype: TImage

        """
        # Note: call to the parent-class initialiser is redundant, as TField is
        # entirely constructed by its __new__() method.
        super(TImage, self).__init__(self.domain)

    def __array_wrap__(self, arr, context=None):
        """
        Finishing step after one of NumPy's ufuncs was applied to the TImage.
        Subclasses SHOULD implement this method to provide appropriate type
        casting.

        :param arr: Result of the ufunc.
        :type arr: np.ndarray
        :param context:
            Ufunc context information. Specifies the order of the input TImage.
        :type context: Any

        :returns: type cast ufunc output object
        :rtype: TImage

        """
        # Call superclass method (creates either a TField or an ndarray)
        result = super().__array_wrap__(arr, context=context)

        if isinstance(result, TField):
            if (self.mask is not None) and (result.vshape == self.vshape):
                mask = self._mask.copy()
            else:
                mask = None
            name = self.name + "_ufunc"
            result.kwargs.update(self.kwargs)
            result.kwargs = {k: v for k, v in result.kwargs.items()
                             if k not in self.RESERVED_KWARGS}
            result = TImage.fromTField(result, copy=False, mask=mask, name=name)
            result.resmgr = self.resmgr  # this is by reference for speed

        return result

    def __operation_finalise__(self, tfout, op=None, template=None):
        """
        This method is called after an operation is performed on the TImage
        instance. Since members of the Operator class use the TField interface
        of the TImage, casting the output of the operation to TImage is
        necessary to ensure that the operation is 'type consistent', i.e. the
        result of the operation is also a TImage. Subclasses SHOULD overload
        this method to become fully compatible with Operators.

        :param tfout: TField output of the operation
        :type: tfout: TField
        :param op: operator
        :type op: Operator
        :param template: input object of the operation
        :type template: Any

        :returns: result of the operation
        :rtype: Any

        """
        # Call the parent-class method
        tfout = super().__operation_finalise__(tfout)

        # Generate mask
        if self._mask is not None:
            if tfout.vshape != self.vshape:
                try:
                    mask = op(TField(self.mask, domain=self.domain))
                except Exception:
                    warnings.warn(
                        "{} could not process the mask, therefore it was "
                        "discarded.".format(op.__class__.__name__))
                    mask = None
            else:
                mask = self._mask.copy()
        else:
            mask = None
        # Purge keyword arguments
        kwargs = self.kwargs.copy()
        kwargs.update(tfout.kwargs)
        kwargs = {k: v for k, v in kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        timg = TImage.fromTField(
            tfield=tfout, copy=False, dtype=self.dtype, mask=mask,
            name=tfout.name, domain=None, order=None, interpolator=None,
            storage=self.storage, header=self.header, **kwargs)

        # Add resolution manager
        if self.resmgr is not None:
            # Passing the high-resolution buffer by reference
            timg.resmgr = self.resmgr

        return timg

    def __slice_wrap__(self, ret, context=None):
        """
        Post-hoc type casting the result of TImage slicing. This prevents
        creating TField outputs when slicing a TImage.

        :param ret: TField slice, returned by TFieldIndexer
        :type ret: TField
        :param context: ("voxels"/"tensors", item) slicing context information
        :type: Any

        :returns: TImage slice
        :rtype: TImage

        """
        # Call superclass method
        ret = super().__slice_wrap__(ret, context=context)

        # Slice mask if necessary
        if self.mask is not None:
            # TODO: Add maskinterpolator attribute to TImage
            maskip = locate(ts.TIMAGE_MASK_INTERPOLATOR)
            name = "{}_mask".format(self.name)
            mask = TField.fromarray(
                self.mask, tensor_axes=(), copy=False, domain=self.domain,
                order=None, dtype=None, interpolator=maskip, name=name,
                storage=self.storage, **self.kwargs)
            if context[0] == "voxels":
                item = context[1]
                mask = mask.voxels[item]
            ret.mask = mask
        else:
            mask = None

        # Purge keyword arguments
        kwargs = self.kwargs.copy()
        kwargs.update(ret.kwargs)
        kwargs = {k: v for k, v in kwargs.items()
                  if k not in self.RESERVED_KWARGS}

        # Create TImage result with the same resolution manager
        ret = TImage.fromTField(
            ret, copy=False, dtype=self.dtype, mask=mask, name=None,
            domain=None, order=None, interpolator=None, storage=MEM,
            header=None, **kwargs)
        ret.resmgr = self.resmgr

        return ret

    def __tfield__(self):
        """
        Converts image data into a TField to provide compatibility with
        TField-specific operations.

        :returns: TImage data as TField
        :rtype: TField

        """
        domain = self.domain
        data = self.data
        ip = self.interpolator
        storage = self.storage
        name = self.name + "_asTField"
        kwargs = {k: v for (k, v) in self.kwargs.items()
                  if k not in TField.RESERVED_KWARGS}

        return TField(extent=domain, tensor_shape=self.tshape, order=self.order,
                      dtype=self.dtype, buffer=data, offset=0, interpolator=ip,
                      name=name, storage=storage, **kwargs)

    def __timage__(self):
        """
        TImage interface. Subclasses of TImage should overload this method to
        provide backwards compatibility with TImage-specific functions and
        methods.

        """
        return self

    @staticmethod
    def __validate_input(source, tensor_axes, dtype, mask, name, domain, order,
                         interpolator, storage, header):
        """
        Validate constructor arguments.

        """
        # Source
        source_type = (TImage, TField, str, np.ndarray, np.memmap, Buffer)
        if not isinstance(source, source_type):
            raise te.ConstructionError("Unrecognised image source format.")
        # Tensor axes
        if tensor_axes is None:
            pass
        elif hasattr(tensor_axes, "__iter__"):
            if any(not isinstance(ax, (int, np.integer)) for ax in tensor_axes):
                raise te.ConstructionError(
                    "Tensor axes must be specified with integers.")
        elif isinstance(tensor_axes, (int, np.integer)):
            pass
        else:
            raise te.ConstructionError(
                "Unrecognised tensor axis specification.")
        # Data type
        if dtype is not None:
            if not isinstance(dtype, (builtins.type, np.generic, np.dtype)):
                raise te.ArgumentError(
                    "Dtype input must be a valid data type.")
        # Mask
        mask_type = (np.ndarray, np.memmap, TField, TImage)
        if mask is not None:
            if not isinstance(mask, mask_type):
                raise te.ConstructionError("Unrecognised mask format.")
        # Name
        if name is not None:
            if not isinstance(name, str):
                raise te.ConstructionError("Object name must be a string.")
        # Domain
        if domain is not None:
            if not isinstance(domain, Domain):
                raise te.ConstructionError("Domain must be specified with a "
                                           "Domain instance.")
        # Order
        if order is not None:
            if not str(order).upper() in (TENSOR_MAJOR, VOXEL_MAJOR):
                raise te.ArgumentError(
                    "Buffer layout order must be either '{}' or '{}'"
                        .format(TENSOR_MAJOR, VOXEL_MAJOR))
        # Interpolator
        if interpolator is not None:
            if not isinstance(interpolator, Interpolator):
                raise te.ConstructionError(
                    "TImage interpolator must be an instance of the "
                    "Interpolator class")
        # Storage
        if str(storage).lower() not in ts.STORAGE_OPTIONS:
            raise te.ConstructionError(
                "Unrecognised storage mode. Please choose from {}."
                    .format(ts.STORAGE_OPTIONS))
        # Header
        if header is not None and not isinstance(header, dict):
            raise TypeError("Expected dictionary type for TImage header, "
                            "got {}".format(type(header)))

    # def __del__(self):
    #     """
    #     TImage destructor. Releases all buffers.
    #
    #     """
    #     try:
    #         del self._data
    #     except:
    #         pass
    #     try:
    #         del self._mask
    #     except:
    #         pass
    #     try:
    #         del self._resmgr._data
    #     except:
    #         pass
    #     try:
    #         del self._resmgr._mask
    #     except:
    #         pass


    # def __getitem__(self, item):
    #     # The parent-class itemgetter will return an proper TField form the
    #     # sliced image data.
    #     ret = super(TImage, self).__getitem__(item)
    #     # Convert this into a TImage
    #     # (pass data by reference for faster performance)
    #     ret = self.fromTField(ret, copy=False)
    #     # Complete the slicing by adding the sliced mask data
    #     # (defer the request to the ndarray itemgetter)
    #     if self.mask:
    #         new_mask = self.mask.__getitem__(item)
    #         ret._mask = Buffer(new_mask)
    #     return ret
    #
    # def __setitem__(self, key, value):
    #     # Note: the itemsetter does not need access to the mask, so the
    #     # parent-class method is adequate.
    #     super(TImage, self).__setitem__(key, value)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    #                              IMPORT METHODS                              #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def _load_nifti(fpath, storage, memlimit):
        try:
            import nibabel as nib
        except ImportError:
            raise ImportError(
                "Missing dependency for opening MRI images: nibabel.")
        mri = nib.load(fpath)
        hdr = mri.header
        header = {"nifti_header": mri.header}
        if storage == MEM:
            shape = hdr.get_data_shape()
            dtype = hdr.get_data_dtype()
            nbytes = reduce(mul, shape) * np.dtype(dtype).itemsize
            if nbytes > memlimit:
                warnings.warn("Image size exceeds instance memory limit. "
                              "Switching to HDD mode.")
                storage = HDD
            else:
                arr = mri.get_fdata().astype(dtype)

        if storage == HDD:
            if fpath.endswith(".gz"):
                # Unzip .nii.gz file to the temporary working directory
                import gzip
                with gzip.open(fpath, mode="rb") as niigz:
                    fd, fname = tempfile.mkstemp(
                        dir=ts.TWD, prefix="mri_", suffix=".nii")
                    with open(fname, 'wb') as nii:
                        shutil.copyfileobj(niigz, nii)
            else:
                fname = fpath

            mri = nib.load(fname)
            header = {"nifti_header": mri.header}
            arr = mri.dataobj.get_unscaled()

        return arr, header

    @staticmethod
    def _load_mghimage(fpath, storage, memlimit):
        # TODO: Test this.
        return TImage._load_nifti(fpath, storage, memlimit)

    @staticmethod
    def _load_tiff(fpath, storage, memlimit):
        if storage == HDD:
            # TODO: add support for this!
            warnings.warn("Memory-mapping TIFF files is not supported in the "
                          "current version. Switching to MEM mode.")
        try:
            import tifffile as tiff
        except ImportError:
            warnings.warn("Tifffile module could not be loaded. "
                          "Falling back on Pillow.")
            return TImage._load_2Dimage(fpath, storage, memlimit)
        else:
            try:
                arr = tiff.imread(fpath)
            except Exception:
                return TImage._load_2Dimage(fpath, storage, memlimit)
            else:
                header = {}
                return arr, header

    @staticmethod
    def _load_svs(fpath, storage, memlimit, level, dtype=None, n_threads=1):
        """
        Load Aperio SVS microscopy file.

        """
        inputs = (fpath, storage, memlimit, level, dtype, n_threads)
        try:
            return TImage._load_svs_tifffile(*inputs)
        except Exception as exc:
            warnings.warn("Failed to load SVS image using tifffile and "
                          "imagecodecs. Falling back on OpenSlide. {}"
                          .format(exc.args))
            return TImage._load_svs_openslide(*inputs)

    @staticmethod
    def _load_svs_tifffile(fpath, storage, memlimit, level, dtype=None,
                           n_threads=1):
        """
        Loads SVS file using the tifffile library. This is the preferred
        method, because it provides direct access to the image data array.

        """
        try:
            import tifffile as tiff
        except ImportError:
            raise ImportError("Missing dependencies for opening SVS image: "
                              "tifffile, imagecodecs.")

        # Open the slide
        slide = tiff.TiffFile(fpath)
        try:
            s = slide.series[level]
        except IndexError:
            raise IndexError("No corresponding data was found for resolution "
                             "level {}.".format(level))

        # Create header
        header = {}

        # Decide on storage mode
        dtype = dtype or np.dtype(s.dtype)
        src_itemsize = np.dtype(s.dtype).itemsize
        trg_itemsize = np.dtype(dtype).itemsize
        if np.dtype(s.dtype) != dtype:
            mem_required = s.size * (src_itemsize + trg_itemsize)
        else:
            mem_required = s.size * src_itemsize
        if mem_required <= memlimit and storage == MEM:
            # The dtype will be matched when buffer is assigned
            arr = s.asarray()
            return arr, header
        elif storage == MEM:
            warnings.warn("Histological image at the specified resolution "
                          "level is too large for the TImage memory limit. "
                          "Switching to HDD mode.")

        # Load image data into memory mapped array
        arr = tiff.memmap(fpath, series=level)
        # fd, fname = tempfile.mkstemp(dir=ts.TWD, prefix="histo_")
        # Note: this file may persist after deleting the TImage!
        # The dtype will be matched when buffer is assigned
        # arr = s.asarray(out=fname)

        return arr, header

    @staticmethod
    def _load_svs_openslide(fpath, storage, memlimit, level, dtype=None,
                            n_threads=1):
        """
        Loads SVS file using the OpenSlide backend library. This static method
        has been superseded by the equivalent method that uses the tifffile
        library. If tifffile is unavailable, TIRL falls back on OpenSlide.

        """
        try:
            import openslide
        except ImportError as exc:
            raise ImportError("Missing dependency for opening SVS image.") \
                from exc

        # Open the slide
        slideobj = openslide.open_slide(fpath)

        # Validate resolution level
        if isinstance(level, (int, np.integer)):
            if level > slideobj.level_count:
                raise ValueError("Resolution level ({}) is out of bounds ({})."
                                 .format(level, slideobj.level_count))
            else:
                level = level % slideobj.level_count
        else:
            raise TypeError("Resolution level must be a positive integer. "
                            "Available resolution levels (from highest to "
                            "lowest): {}"
                            .format(tuple(range(slideobj.level_count))))

        # Determine the size of the image at the desired resolution level
        dtype = dtype or np.uint8
        fullshape = slideobj.level_dimensions[level][::-1]
        n_pixels = reduce(mul, fullshape)
        itemsize = np.dtype(dtype).itemsize
        array_size = n_pixels * itemsize * 4  # dtype RGBA
        if dtype != np.uint8:
            mem_required = n_pixels * (1 + itemsize) * 4  # 8-bit + dtype RGBA
        else:
            mem_required = array_size

        # Load image in one piece
        if (mem_required < memlimit) and (storage == MEM):
            arr = np.asarray(slideobj.read_region(
                [0, 0], level, fullshape[::-1]), dtype=dtype)
            header = {"level_downsamples": slideobj.level_downsamples}
            return arr, header
        elif storage == MEM:
            warnings.warn("Histological image at the specified resolution "
                          "level is too large for the TImage memory limit. "
                          "Switching to HDD mode.")

        # Load image in rectangular chunks
        fd, fname = tempfile.mkstemp(dir=ts.TWD, prefix="histo_")
        # print("Pre-allocating disk space for uncompressed SVS image ({} MiB)..."
        #       .format(array_size / (1024 ** 2)))
        with open(fname, mode="r+") as f:
            f.seek(array_size - 1)
            f.write("\n")
        chunks = TImage._subdivide(
            list(fullshape), itemsize, memlimit // n_threads,
            np.asarray([0, 0]))
        jobs = [(fpath, fname, dtype, level, *chunk) for chunk in chunks]
        if n_threads > 1:
            p = mp.Pool(processes=n_threads)
            p.map(_svs_worker, jobs)
        else:
            for job in jobs:
                _svs_worker(job)
        arr = np.memmap(fname, mode="r+", shape=(*fullshape, 4),
                        dtype=dtype, offset=0, order="C")
        header = {"level_downsamples": slideobj.level_downsamples}
        return arr, header

    @staticmethod
    def _subdivide(shape, itemsize, memlimit, offset, axis=0):
        fits = 4 * reduce(mul, shape) * (itemsize + 1) < memlimit  # RGBA
        if fits:
            yield offset, shape
        else:
            shape1 = shape.copy()
            shape2 = shape.copy()
            shape1[axis] = shape1[axis] // 2
            shape2[axis] -= shape1[axis]
            offset1 = offset.copy()
            offset2 = offset.copy()
            offset2[axis] += shape1[axis]
            axis = (axis + 1) % 2
            yield from TImage._subdivide(
                shape1, itemsize, memlimit, offset1, axis)
            yield from TImage._subdivide(
                shape2, itemsize, memlimit, offset2, axis)

    @staticmethod
    def _load_2Dimage(fpath, storage, memlimit):
        from PIL import Image
        # TODO: Fix missing storage definition in this case
        if storage == HDD:
            warnings.warn("Memory-mapping ordinary 2D image files is not "
                          "supported in the current version. Switching to "
                          "MEM mode.")
        pilimg = Image.open(fpath)
        arr = np.asarray(pilimg)
        header = {}
        return arr, header

    @staticmethod
    def _load_npy(fpath, storage, memlimit):

        arr = np.load(fpath, mmap_mode="r+")
        if (arr.nbytes < memlimit) and (storage == MEM):
            arr = np.asarray(arr)
        elif storage == MEM:
            warnings.warn("NPY file is too large for the TImage memory limit. "
                          "Switching to HDD mode.")
        header = {}
        return arr, header

    @staticmethod
    def _load_from_file(fpath, storage=MEM, dtype=None, **kwargs):

        # Get instance memory limit
        memlimit = min(psutil.virtual_memory().available,
                       ts.TIMAGE_INSTANCE_MEMORY_LIMIT * 1024 ** 2)

        # Check whether the file exists
        fpath = os.path.abspath(fpath)
        if not os.path.isfile(fpath):
            raise IOError("File was not found at {}".format(fpath))
        fp, fn = os.path.split(fpath)
        fnlc = fn.lower()

        # Open according to extension
        if fnlc.endswith(".nii") or fnlc.endswith(".nii.gz"):
            data, hdr = TImage._load_nifti(fpath, storage, memlimit)
        elif fnlc.endswith(".mgz"):
            data, hdr = TImage._load_mghimage(fpath, storage, memlimit)
        elif fnlc.endswith(".tif") or fnlc.endswith(".tiff"):
            data, hdr = TImage._load_tiff(fpath, storage, memlimit)
        elif fnlc.endswith(".svs"):
            level = -1 if storage == MEM else 0
            level = kwargs.get("level", level)
            n_threads = kwargs.get("n_threads", 1)
            data, hdr = TImage._load_svs(
                fpath, storage, memlimit, level=level, dtype=dtype,
                n_threads=n_threads)
        elif fnlc.endswith(TImage.SUPPORTED_IMAGE_TYPES):
            data, hdr = TImage._load_2Dimage(fpath, storage, memlimit)
        elif fnlc.endswith(".npy"):
            data, hdr = TImage._load_npy(fpath, storage, memlimit)
        else:
            raise te.ArgumentError("Unsupported file type.")

        # Store input file location in the header.
        hdr.update({"input_file": os.path.abspath(fpath)})

        return data, hdr

    def copy(self, **kwargs):
        """
        TImage copy constructor.

        :param kwargs:
            Constructor keyword arguments provided here will override their
            values in the original object.
        :type kwargs: Any

        :returns:
            A new instance of TImage that will be the copy of the original
            object.
        :rtype: TImage

        """
        # Set parameters of the new object before construction
        attributes = {
            "tensor_axes": self.taxes,
            "dtype": self.dtype,
            "name": self.name,
            "domain": self.domain.copy(),
            "mask": None,
            "order": self.order,
            "interpolator": self.interpolator.copy(),
            "storage": self.storage,
            "header": self.header.copy(),
            "kwargs": self.kwargs.copy()
        }

        # Create new object with updated parameters
        attributes.update(self.kwargs.copy())
        attributes.update(kwargs)
        attributes = {k: v for (k, v) in attributes.items()
                      if k not in self.RESERVED_KWARGS}
        obj = TImage.fromarray(self.data, copy=True, **attributes)

        # Set mask and resolution manager
        if self._mask is not None:
            obj.mask = self.mask.copy()
        else:
            obj.mask = None
        # If the current instance has a higher-resolution image attached,
        # pass the highres image by reference. No need to copy, because the
        # current instance is not supposed to modify the high-resolution image.
        # While there might be exceptions to this, this is a design choice to
        # make copy() calls as fast as reasonably possible, as copy calls are
        # much more common than the few exceptions mentioned before.
        if self.resmgr is not None:
            obj.resmgr = self.resmgr

        return obj

    @classmethod
    def _load(cls, dump):

        # Create TField from dump
        tfield = super(TImage, cls)._load(dump)

        # Create TImage from TField
        mask = dump.get("mask")
        hdr = dill.loads(dump.get("header"))
        obj = TImage.fromTField(tfield, copy=False, mask=mask,
                                storage=tfield.storage, header=hdr)

        # Set ResolutionManager
        resmgr = dump.get("resmgr", None)
        if resmgr:
            # obj.resmgr = ResolutionManager._load(resmgr)
            obj.resmgr = resmgr

        return obj

    def _dump(self, resmgr=True):
        objdump = super(TImage, self)._dump()  # type, id
        # Save TImage-specific properties: mask, header and ResMgr
        objdump.update(mask=self.mask)
        try:
            objdump.update(header=dill.dumps(self.header))
        except:
            objdump.update(header=dict())
        if resmgr and (self.resmgr.data.base is not self.data.base):
            objdump.update({"resmgr": self.resmgr.dump()})
        else:
            objdump.update({"resmgr": None})

        return objdump

    @property
    def asTField(self):
        return self.__tfield__()

    @property
    def header(self):
        return self.properties.get("header", {})

    @header.setter
    def header(self, hdr):
        if isinstance(hdr, dict):
            self.properties.update({"header": hdr})
        else:
            raise TypeError("Image header must be specified with a dict.")

    def highres(self, interpolate=False):
        """
        Returns the high-resolution image as a separate TImage, which shares
        the data with the original instance (no copying is implied). The
        transformation chain is referenced from the current TImage domain.

        :param interpolate:
            If True, the field of view (FOV) is accurately projected to the
            high-resolution template, and the relevant section is created by
            interpolation. If False (default), the FOV is projected to match
            the nearest voxels in the template image, and the relevant section
            is returned without interpolation, which is quicker but may be
            inaccurate on the sub-pixel scale. Default: False. Recommended if
            the template has large voxels.
        :type interpolate: bool

        :returns: high-resolution TImage with the same FOV as the current TImage
        :rtype: TImage

        """
        iscropped = \
            self.domain.offset and \
            isinstance(self.domain.offset[0], TxTranslation)
        if not iscropped:
            return self.resmgr.template

        mapper = Domain(self.vshape, *self.domain.offset)
        prox = np.zeros(self.vdim)
        dist = np.asarray(self.vshape)
        fov = mapper.map_voxel_coordinates(np.vstack((prox, dist)))\
            .tolist()

        if interpolate:
            hrshape = np.round(np.subtract(fov[1], fov[0])).astype(int)
            hrdomain = Domain(tuple(hrshape), offset=[TxTranslation(*fov[0])])
            hres = self.resmgr.template.evaluate(hrdomain)
        else:
            maxsize = self.resmgr.template.vshape
            # Avoid IndexError: check bounds when defining the slicer
            slicer = tuple(
                slice(max(0, int(round(lo))), min(int(round(hi)), maxsize[i]))
                for i, (lo, hi) in enumerate(zip(fov[0], fov[1])))
            # print(slicer)
            hres = self.resmgr.template.voxels[slicer]

        # Make sure that the high-resolution image is in the same phsyical
        # space as the current TImage.
        hres.domain.chain = self.domain.chain
        return hres

    @property
    def resmgr(self):
        """
        Access to the Resolution Manager. Automatically initialises the manager
        with the current image and mask data if it is not available.

        :returns: resolution manager
        :rtype: ResolutionManager

        """
        # If the image is at full resolution, the resolution manager should be
        # updated before use, so that all changes since the construction of the
        # TImage are reflected in the resolution manager.
        if np.allclose(self.scale, 1):
            self._resmgr = ResolutionManager(self)
        return self._resmgr

    @resmgr.setter
    def resmgr(self, r):
        if isinstance(r, ResolutionManager) or (r is None):
            self._resmgr = r
        else:
            raise TypeError("Expected ResolutionManager instance, got {}"
                            .format(r.__class__.__name__))

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, scales):
        if hasattr(scales, "__iter__"):
            if all(isreal(f) and f > 0 for f in scales):
                scales = tuple(float(f) for f in scales)
            else:
                raise TypeError("Scaling factors must be numeric and positive.")
        else:
            if isreal(scales):
                scales = (scales,) * len(self.resmgr.vaxes)
            else:
                raise TypeError("Scaling factors must be numeric and positive.")
        self.resample(*scales, copy=False)

    @property
    def mask(self):
        """
        TImage mask.

        :returns: image mask
        :rtype: np.ndarray

        """
        if self._mask is not None:
            return self._mask.data
        else:
            return None

    def tmask(self):
        """
        Returns mask as a TField instance.

        :returns: mask as TField on the current TImage domain
        :rtype: TField

        """
        if self.mask is None:
            return None

        maskip = locate(ts.TIMAGE_MASK_INTERPOLATOR)
        name = self.name + "_mask"
        maskdata = self.mask
        mask = TField.fromarray(
            maskdata, tensor_axes=(), copy=False, domain=self.domain,
            order=self.order, dtype=maskdata.dtype, interpolator=maskip,
            name=name, storage=self.storage)
        return mask

    @mask.setter
    def mask(self, newmask):
        """
        TImage mask setter method. Regardless of the input format, only voxel
        shapes are matched, no evaluation takes place. This is to avoid
        erroneous mask definitions.

        """
        if newmask is not None:
            if isinstance(newmask, np.ndarray) or isinstance(newmask, Buffer):
                newmask = TField.fromarray(newmask)
            else:
                newmask = TField(newmask)
            if newmask.dtype == np.bool:
                newmask.dtype = ts.DEFAULT_FLOAT_TYPE
            if newmask.tdim > 1:
                raise TypeError(
                    "Mask values must be scalars, but the specified mask has "
                    "tensor shape {}.".format(newmask.tshape))
            if newmask.vshape != self.vshape:
                raise te.DomainError(
                    "The specified mask {} is incompatible with the "
                    "shape of the image data {}."
                        .format(newmask.vshape, self.vshape))
            self._mask = newmask._data  # set with Buffer
        else:
            self._mask = None

        # Also update the resolution manager if the image is at its native
        # resolution. Note that the mask is initialised earlier than the
        # resolution manager when TImage is constructed. If the mask is set,
        # the ResMgr will also recognise it. This update is only necessary when
        # the mask is not set at construction time, but earlier than resampling.
        if hasattr(self, "_resmgr") and np.allclose(self.scale, 1):
            self.resmgr._mask = self._mask

    def apply_mask(self, mask, normalise=True):
        """
        Multiplies TImage by the mask. Safe for shape errors and unnormalised
        values, which are common, due to images having 8-bit (0..255)
        intensities.

        """
        orig_order = self.order
        self.order = TENSOR_MAJOR

        if normalise and np.count_nonzero(mask):
            mask = mask / np.max(mask)

        self.data[...] *= mask
        self.order = orig_order

    def _evaluate_by_inversion(self, target_domain):
        """
        Evaluate TImage at the points of the target domain (physical space) by
        inverse-transforming the physical coordinates of the target domain to
        the voxel-space of the tensor field, and interpolating at these voxel
        coordinates. All transformations of the tensor field domain must be
        invertible.

        :param target_domain: target domain
        :type target_domain: Domain

        :returns: New TField instance with interpolated field values.
        :rtype: TField

        """
        # Obtain coordinates for interpolation
        pcoords = target_domain.get_physical_coordinates()
        vcoords = self.domain.map_physical_coordinates(pcoords)
        del pcoords

        # Perform interpolation and cast output
        # (storage mode is handled implicitly by the interpolator)
        if ts.TIMAGE_MASK_MISSING_DATA:
            fill_value = np.inf
            interpolator = self.interpolator.copy(fill_value=np.inf)
        else:
            interpolator = self.interpolator
        data = interpolator(vcoords, input_array=self.data)

        if self.mask is not None:
            maskip = locate(ts.TIMAGE_MASK_INTERPOLATOR)
            maskip = maskip(source=self._mask.data, tensor_axes=(), threads=1,
                            ipkwargs={"order": 1, "prefilter": False})
            # maskip = maskip(source=self._mask.data, n_threads=1,
            #                 **self.interpolator.kwargs)
            # Mask non-overlapping image regions (black area)
            # maskip.kwargs.update({"mode": "constant", "cval": 0})
            mask = maskip(vcoords)
        else:
            mask = None
        if self.tdim > 0:
            tshape = self.tshape
        else:
            tshape = ()
        if self.order == TENSOR_MAJOR:
            data = data.reshape((*tshape, *target_domain.shape))
        else:
            data = data.reshape((*target_domain.shape, *tshape))
        if self.mask is not None:
            mask = mask.reshape(target_domain.shape)

        # Bugfix: the voxel shape might change upon evaluation (2D/3D case!)
        if self.order == TENSOR_MAJOR:
            taxes = tuple(range(self.tdim))
        else:
            taxes = tuple(range(data.ndim - self.tdim, data.ndim))

        # Mask points that are outside the voxel grid
        # Added on 13 August 2020
        if ts.TIMAGE_MASK_MISSING_DATA:
            if mask is None:
                mask = np.ones(target_domain.shape, dtype=np.float32)
            if taxes:
                indices = np.flatnonzero(np.all(data == np.inf, axis=taxes))

            else:
                indices = np.flatnonzero(data == np.inf)
            data[data == np.inf] = ts.FILL_VALUE
            mask.flat[indices] = 0
            # indices = [np.flatnonzero(np.any(vcoords < 0, axis=-1))]
            # for dim in range(self.vdim):
            #     ix = np.flatnonzero(vcoords[:, dim] >= self.vshape[dim])
            #     indices.append(ix)
            # else:
            #     # These are the linear indices of all voxels outside
            #     # the voxel grid
            #     indices = np.unique(np.concatenate(indices))
            # mask.flat[indices] = 0

        # Create new TImage
        new_name = "%s.%s" % (self.name, target_domain.name)

        # ipc = self.interpolator.__class__
        # ip = self.interpolator
        # interpolator = ipc(data, ip.threads, ip.verbose, **ip.kwargs)
        # interpolator.tensor_axes = taxes
        interpolator = self.interpolator.copy()
        interpolator.values = data
        interpolator.tensor_axes = taxes
        kwargs = self.properties.get("kwargs").copy()

        # Return result as TField if the target domain is non-compact
        if not target_domain.iscompact:
            if self.order == TENSOR_MAJOR:
                interpolator.tensor_axes = tuple(range(0, data.ndim - 1))
            else:
                interpolator.tensor_axes = tuple(range(1, data.ndim))
            name = self.name + "_eval_{}".format(target_domain.name)
            kwargs = {k: v for (k, v) in kwargs.items()
                      if k not in TField.RESERVED_KWARGS}
            return TField(target_domain, tensor_shape=self.tshape,
                          order=self.order, dtype=data.dtype, buffer=data,
                          offset=0, interpolator=interpolator, name=name,
                          **kwargs)

        # Return result as TImage otherwise
        else:
            # Release storage from kwargs (bugfix)
            kwargs = {k: v for (k, v) in kwargs.items()
                      if k not in TImage.RESERVED_KWARGS}
            ret = TImage(
                data, tensor_axes=taxes, dtype=self.dtype, mask=mask,
                name=new_name, order=self.order, interpolator=interpolator,
                storage=self.storage, domain=target_domain, **kwargs)

        # Note: image header and highres images are not copied over.
        return ret

    def resample(self, target, *scales, copy=True, update_chain=True,
                 presmooth=ts.TIMAGE_PRESMOOTH):
        """
        Changes the resolution of the TImage by resampling the original (high-
        resolution) data on a different domain. There are two ways to
        set the grid spacing:
            1. Specifying another compact domain. The shape ratio of the
               current domain and the old one determines the amount of
               down/upsampling. Domain transformations are discarded in the
               process, only the domain shapes are considered. This method has
               the advantage that the output shape is defined explicitly,
               avoiding errors due to rounding.
            2. Specifying down/upscaling factors (below and above 1,
               respectively) for each dimension of the TImage, or one scaling
               factor for all dimensions. The main advantage of this method is
               its simplicity, but may result in slightly different shapes than
               expected due to rounding. Note that scaling factors are defined
               relative to the high-resolution image. If it does not exist,
               definitions are relative to the current domain.

        :param target:
            First argument, that can either be the target Domain object,
            or the global scaling factor, or the scaling factor for the 0-th
            voxel dimension of the TImage.
        :type target: Union[Domain, int, float, np.integer, np.floating]
        :param scales:
            Additional arguments that specify the scale factor for consecutive
            voxel dimensions of the TImage. If more than one scaling factor is
            specified, the number of scaling factors must match the number of
            spatial dimensions of the TImage.
        :param copy:
            If True (default), a resampled version of the current TImage will be
            returned, which will share transformations with the original
            TImage instance. The new instance will also inherit the resolution
            manager of the current instance.
            If False, the image data and the mask buffers of the current TImage
            instance will be overwritten, and the method will return nothing.
        :type copy: bool
        :param update_chain:
            If True (default), non-linear transformations that are linked with
            the TImage domain will remain linked, which implies that these
            will be resampled via their regrid() method. If False, all
            non-linear transformations will be detached and left unchanged.
            In either case the identity of all transformation objects is
            preserved (no copying will take place).
        :type update_chain: bool
        :param presmooth:
            Applies Gaussian presmoothing before downsampling (no smoothing is
            performed for upsampling). This ensures that all domain points at
            the lower resolution have proper support in the higher-resolution
            image, therefore all original information is (at least indirectly)
            represented.
        :type presmooth: bool

        :returns:
            A resampled version of the original TImage, but only when copy=True.
        :rtype: Union[TImage, NoneType]

        """
        # TImage indexing (cropping) and resampling is not fully compatible,
        # because cropping changes the image, therefore when the user resamples
        # a previously cropped image, they will expect to get the same cropped
        # field-of-view (FOV) at a different resolution, not the original FOV.
        # Therefore the FOV of the current image must be mapped to the template
        # of the resolution manager to do the resampling.
        iscropped = self.domain.offset and \
                    isinstance(self.domain.offset[0], TxTranslation)
        if iscropped:
            mapper = Domain(self.vshape, *self.domain.offset)
            prox = np.zeros(self.vdim)
            dist = np.asarray(self.vshape)
            fov = mapper.map_voxel_coordinates(np.vstack((prox, dist)))\
                .tolist()
        else:
            vdim = len(self.resmgr.vshape)
            fov = [np.zeros(vdim), self.resmgr.vshape]

        # Determine and verify scaling factors
        if isinstance(target, Domain):
            relative_scales = self._domains2scale(self.resmgr.domain, target)
        else:
            relative_scales = (target,) + tuple(scales) if scales \
                else (target,) * len(self.resmgr.vaxes)
            if len(relative_scales) != len(self.resmgr.vaxes):
                raise te.DomainError(
                    "The scaling factor specification ({}) is incompatible "
                    "with the {} that has voxel dimension {}."
                        .format(relative_scales,
                                self.resmgr.__class__.__name__,
                                len(self.resmgr.vshape)))

        # Pass on the call to the resolution manager
        if copy:
            return self.resmgr.get(
                *relative_scales, fov=fov, update_chain=update_chain,
                presmooth=presmooth)

        # Replace image data in-place
        img, mask = self.resmgr._get_scaled_data(
            *relative_scales, fov=fov, presmooth=presmooth)
        # Note that self.domain is a potentially low-resolution image
        # domain, but it is probably more recent than the domain of the
        # high-resolution template image that is stored in the
        # resolution manager. This is why the transformations are copied
        # from the previous low-resolution domain.
        img.domain.copy_transformations(self.domain, links=update_chain)
        # self.name = self.name + "_resample({})"\
        #     .format(",".join([str(s) for s in relative_scales]))
        self._data = img._data  # Buffer
        self._mask = mask if mask is None else mask._data  # Buffer
        # Note: the scale must be set immediately after the data is changed,
        # because when the scale is 1, the resolution manager gets updated
        # whenever it is called.
        self._scale = relative_scales
        # Update other TImage properties that might have changed since the
        # resolution manager was initialised.
        self.properties.update({
            "dtype": self.resmgr.dtype,
            "domain": img.domain,
            "tensor_shape": self.resmgr.tshape,
            "interpolator": self.resmgr.interpolator.copy(),  # mutable!
            "order": self.resmgr.order,
            "storage": self.resmgr.storage,
            "kwargs": self.resmgr.kwargs.copy(),  # mutable!
            "name": self.resmgr._srcname
        })

    def reset_scale(self):
        """
        Adapt domain to the high-resolution image (and mask) data, and discard
        all data at the current sampling. Note that any changes that has been
        made to the data at the current resolution will be discarded.

        :returns:
            The same TImage instance with high-resolution image and mask data.
        :rtype: TImage

        """
        self.resample(1, copy=False)

    def reset_resmgr(self):
        """
        Releases the existing ResolutionManager instance and creates a new one
        based on the current image.

        """
        self.resmgr = ResolutionManager(self)

    def reset_transformations(self):
        """
        Discard all domain transformations and take back the TImage to the
        voxel space. Note that this operation does not change the offset
        transformation which resulted from slicing another TImage/TField.

        :returns:
            The same TImage instance without domain transformations.
        :rtype: TImage

        """
        self.domain.reset()

    def smooth(self, radius, *radii, spatial_op=None, copy=True):
        """
        Smooths the image uniformly for every tensor component. Anisotropic
        smoothing can be applied by specifying smoothing radii for each spatial
        dimension. If a spatial operator is specified, radii are passed to the
        operator as its first n arguments.

        Note: repeated smoothing of a TImage leads to the degradation of image
        quality, as the smoothing is performed on the active buffer of TImage,
        not the high-resolution buffer. This also implies that smoothing
        operations at different resolution levels can be undone by resampling
        the TImage (as long as the TImage is not smoothed at native resolution).

        :param radius:
            Global smoothing radius (or custom definition of a spatial
            operator). For the default Gaussian smoothing kernel, it is the
            full-width-half-maximum distance in voxels.
        :type radius: Union[int, float, np.integer, np.floating]
        :param radii:
            Continued specification of smoothing radii for each spatial
            dimension. If not specified, the value of 'radius' will be applied
            globally.
        :type radii: Union[int, float, np.integer, np.floating]
        :param spatial_op:
            Smoothing operation. If None, Gaussian smoothing is applied with
            4 * sigma anisotropic kernel size. The operator must not change the
            voxel shape of the TImage. The operator must accept smoothing radii
            as leading arguments.
        :type spatial_op: SpatialOperator
        :param copy:
            If True (default), the smoothing result is returned as a copy of
            the original TImage (the Domain is preserved). If False, the values
            will be changed in place.

        :returns: smoothed image
        :rtype: TImage

        """
        # Verify smoothing radii
        radii_ = radii
        if isreal(radius):
            if radius >= 0:
                radii = (radius,)
            else:
                raise ValueError("Smoothing radius must be non-negative.")
        else:
            raise TypeError("Smoothing radius must be numeric.")
        if all(isreal(r) for r in radii_):
            if all(r >= 0 for r in radii_):
                radii += radii_
            else:
                raise ValueError("Smoothing radii must be non-negative.")
        else:
            raise TypeError("Smoothing radii must be numeric.")
        # Make sure that smoothing is not applied along tensor axes.
        # (SpatialOperator uses TENSOR_MAJOR axis order.)
        if len(radii) == 1:
            maskradii = radii * self.vdim
            radii = (0,) * self.tdim + radii * self.vdim
        elif len(radii) == self.vdim:
            maskradii = radii
            radii = (0,) * self.tdim + radii
        else:
            raise te.ArgumentError(
                "Smoothing radii must be specified either globally or "
                "separately for each spatial dimension.")

        # Nothing to do
        if np.allclose(radii, 0):
            return self.copy() if copy else None

        # Prepare spatial operator
        if spatial_op is None:
            coeff = np.sqrt(8 * np.log(2))
            sigma = tuple(r / coeff for r in radii)
            kernelsize = 4  # in multiples of sigma
            neighbourhood = ceil(kernelsize * max(sigma))
            # Note: there is no asterisk before sigma, as gaussian_filter
            # expects an iterable for its 'sigma' argument.
            spatial_op = SpatialOperator(
                gaussian_filter, radius=neighbourhood, name="gaussian",
                truncate=kernelsize, sigma=sigma)
        else:
            opargs = spatial_op.opargs
            spatial_op.opargs = radii + opargs

        # Perform smoothing operation on image (and mask) data
        ret_img = spatial_op(self)
        if self.mask is not None:
            coeff = np.sqrt(8 * np.log(2))
            masksigma = tuple(r / coeff for r in maskradii)
            kernelsize = 4  # in multiples of sigma
            neighbourhood = ceil(kernelsize * max(masksigma))
            maskop = SpatialOperator(
                gaussian_filter, radius=neighbourhood, name="gaussian",
                truncate=kernelsize, sigma=masksigma)
            ret_mask = maskop(self.tmask())
            ret_img._mask = Buffer(ret_mask.data)
        if copy:
            return ret_img
        else:
            # Set the data Buffer with the data buffer of the result TImage
            self._data = ret_img._data
            # Set the mask Buffer with the data buffer of the mask-TField
            self._mask = ret_img._mask

    def centre(self, weighted=False):
        """
        Returns the centre of the image in physical space.

        :param weighted:
            If weighted is True, the centre is the centre of gravity. If False,
            it is the geometrical centre of the image.

        :return: physical coordinates of image centre
        :rtype: tuple

        """
        if weighted:
            if self.storage == MEM:
                weights = self.data.reshape(-1)
                pc = self.domain.get_physical_coordinates()
                if self.mask is not None:
                    weights *= self.mask.reshape(-1)
                factor = np.sum(weights)
                c = np.sum(weights[:, np.newaxis] * pc, axis=0) / factor
                return tuple(c.tolist())
            else:
                raise NotImplementedError()
                # TODO: This might be too slow for large arrays
                # weights = TField.fromarray(self.data, domain=self.domain)
                # weights.order = TENSOR_MAJOR
                # if self.mask is not None:
                #     weights = weights * self.mask / np.sum(self.mask)
                # vc = self.domain.get_voxel_coordinates()
                # vc = TField.fromarray(
                #     vc.reshape((*self.vshape, -1)), tensor_axes=-1)
                # vc.order = TENSOR_MAJOR
                # c = tuple(np.sum(weights * vc, axis=vc.vaxes) / np.sum(weights))
                # c = self.domain.map_voxel_coordinates(c)[0]
            return c

        else:
            txs = self.domain.chain
            nonlin = any(hasattr(tx, "field") for tx in txs)
            if nonlin:
                pc = self.domain.get_physical_coordinates()
                return tuple(np.sum(pc, axis=0) / self.domain.numel)
            else:
                c = [dim / 2 for dim in self.domain.shape]
                c = self.domain.map_voxel_coordinates([c])[0]
                return tuple(c)

    def centralise(self, weighted=False):
        """
        Appends TImage transformations with a TxTranslation object to
        centralise the image in physical space.

        :param weighted:
            If True, tensors are reduced, and this scalar field is further
            multiplied by mask values at each voxel to give voxel-wise
            weighting factors for the coordinates.
        :type weighted: bool

        """
        offset = tuple(-x for x in self.centre(weighted))
        tx = TxTranslation(*offset, name="centralise")
        self.domain.chain.append(tx)

    def normalise(self):
        """
        Global normalisation of the image values to (0, 1). The changes are
        made in place, and may include changing the data type.

        """
        # TODO: This is not compatible with HDD arrays.
        self._data.data = self._data.data / self._data.data.max()
        # self.dtype = self._data.data.dtype

    def set_resolution(self, *resolution, unit="au"):
        """
        Sets the resolution of the TImage in arbitrary units per voxel. This is
        formally equivalent to setting the first element of the transformation
        chain with a TxScale transformation object.

        If one or more TxScale transformations with the name "resolution"
        already exist, the parameters of the first of these will be overwritten
        by the input values. Otherwise the chain will be prepended with a new
        transformation object.

        Note that changing the resolution will have no effect on the image data.
        To change the _sampling_ of the TImage, use the resample() method.
        Equivalently, changing the sampling of the image data will have no
        effect on the resolution of the image, because the voxel coordinates of
        the downsampled image are mapped to the original voxel coordinates by
        the "offset" part of the transformation chain.

        :param resolution:
            Separate a.u. / voxel scaling factors along the voxel axes
            or a single value to define isotropic resolution.
        :type resolution: Union[float, tuple[float]]
        :param unit: name of the unit (default: au)
        :type unit: str

        .. note::
             1. Setting the resolution is recommended to make transformation
                parameters physically meaningful, hence more intuitive to
                understand and easier to recognise unreasonable values.
             2. The number of scaling factors may not match the number of
                voxel dimensions. This occurs when a 2D domain is embedded in a
                3D field and the coordinates are transformed perpendicularly to
                the domain.
             3. Multiple resolution setter transformations may exist in the
                transformation chain if further axes are defined by embedding
                transformations. It is recommended that resolutions are set
                manually in these cases, not by this method.

        """
        # Validate input
        r = (resolution,) if not hasattr(resolution, "__iter__") \
            else tuple(resolution)
        for x in r:
            if not isreal(x) or x <= 0:
                raise ValueError("Invalid value for resolution: {}".format(x))

        try:
            txs = self.domain.chain["resolution"]
        except IndexError:
            r = r * self.vdim if len(r) == 1 else r
            tx = TxScale(*r, name="resolution", unit=unit)
            self.domain.chain = tx + self.domain.chain
            return
        else:
            if hasattr(txs, "__iter__"):
                for tx in txs:
                    if isinstance(tx, TxScale):
                        break
                else:
                    # There are transformations with the name "resolution" but
                    # none of these are of TxScale type, so put one in place.
                    r = r * self.vdim if len(r) == 1 else r
                    tx = TxScale(*r, name="resolution", unit=unit)
                    self.domain.chain = tx + self.domain.chain
                    return
            else:
                tx = txs

        # Update the parameters of the resolution setter transformation
        tx.parameters[...] = np.asarray(r)
        if isinstance(unit, str):
            tx.metaparameters.update({"unit": unit})
        else:
            raise ValueError(
                "The number of scaling factors specified for resolution ({}) "
                "must match the number of parameters of the existing "
                "transformation object ({}).".format(
                    len(r), tx.parameters.size))

    @property
    def resolution(self):
        try:
            txs = self.domain.chain["resolution"]
        except IndexError:
            return (1,) * self.vdim
        else:
            if hasattr(txs, "__iter__"):
                for tx in txs:
                    if isinstance(tx, TxScale):
                        break
                else:
                    # There are transformations with the name "resolution" but
                    # none of these are of TxScale type, so report 1.
                    return (1,) * self.vdim
            else:
                tx = txs
        return tuple(tx.parameters)

    @resolution.setter
    def resolution(self, resolution):
        if hasattr(resolution, "__iter__"):
            self.set_resolution(*resolution)
        else:
            self.set_resolution(resolution)

    def preview(self, centre=None, normal=None, resolution=1, **kwargs):
        """
        TImage preview by Tirlvision.

        :param centre:
            Centre of the viewing plane in physical coordinates
            (for 3D TImages only).
        :type centre: Union[tuple, list, np.ndarray]
        :param normal:
            Normal vector to the viewing plane (for 3D TImages only).
        :type normal: Union[tuple, list, np.ndarray]
        :param resolution:
            Canvas resolution (for 3D TImages only). Scale is arbitrary
            (depends on the scale of the TImage), but mm/px is the recommended
            convention.
        :type resolution: Union[int, float, np.integer, np.floating]

        """

        if self.vdim == 1:
            tirlvision.call("timage", "preview1d", self, **kwargs)
        elif self.vdim == 2:
            tirlvision.call("timage", "preview2d", self, **kwargs)
        elif self.vdim == 3:
            normal = normal or (0, 0, 1)
            tirlvision.call("timage", "preview3d", self, centre, normal,
                            resolution, **kwargs)
        else:
            raise NotImplementedError(
                "Preview is not implemented for image dimensions above 3.")

    def snapshot(self, fname, overwrite=False):
        """
        Saves a 2D TImage as a regular image file. Only the image data is
        saved, excluding all transformations, high-resolution buffers and
        the mask. If the image is 3D, it is saved into NIfTI format.

        :param fname:
            Path to and name of the target image file.
        :type fname: str
        :param overwrite:
            If True, the target image will be overwritten automatically
        :return:
        """
        from PIL import Image

        # Prepare file name
        fname = tu.verify_fname(fname, overwrite=overwrite)
        fn, ext = os.path.splitext(fname)
        fn = fn.rstrip(".")
        ext = ext.lstrip(".")

        # Save 3D image as NIfTI
        # TODO: Support tensor axes, header information mismatch case
        if self.vdim == 3:
            import nibabel as nib
            orig_order = self.order
            self.order = VOXEL_MAJOR
            tx = (self.domain.offset + self.domain.chain).reduce(merge="all")
            if len(tx) == 0:
                affine = np.eye(4)
            elif len(tx) == 1 and tx[0].kind == "linear":
                affine = np.eye(4)
                affine[:3, :] = tx[0].matrix
            else:
                # TODO: Decompose, apply warp and save with affine
                raise NotImplementedError()

            try:
                hdr = self.header["nifti_header"].copy()
            except Exception:
                hdr = nib.Nifti1Header()
            hdr.set_qform(np.eye(4), code=0)
            hdr.set_sform(affine, code=2)
            hdr["descrip"] = f"TIRL v{tirl.__version__}"
            nifti = nib.Nifti1Image(self.data, affine, hdr)
            fn = fn.replace(".nii", "")
            nib.save(nifti, f"{fn}.nii.gz")
            self.order = orig_order

        # Save 2D image
        elif (self.vdim == 2) and (self.tdim in (0, 1)):
            ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT if not ext else ext
            imgdata = tu.to_img(self.data)
            if (self.order == "T") and self.taxes:
                imgdata = np.moveaxis(imgdata, 0, -1)
            pilimg = Image.fromarray(imgdata)
            pilimg.save(".".join([fn, ext]))
        else:
            raise te.DomainError(
                "Snapshots can only be taken from 3D TImages and 2D TImages "
                "with one tensor dimension.")


# AUXILIARY FUNCTIONS

def _svs_worker(job):
    """ Reads a tile from a histology image into a memory map. """

    import openslide
    fname, mfile, dtype, level, offset, shape = job
    proc = mp.current_process().pid
    # print(f"Process {proc} is working...")
    ly, lx = offset
    sy, sx = shape
    slideobj = openslide.open_slide(fname)
    fullshape = tuple(slideobj.level_dimensions[0][::-1]) + (4,)
    levelshape = tuple(slideobj.level_dimensions[level][::-1]) + (4,)
    scales = np.divide(fullshape, levelshape)
    m = np.memmap(mfile, mode="r+", shape=levelshape, dtype=dtype,
                  offset=0, order="C")
    # Note: OpenSlide requires the location be given at full-resolution
    ly0, lx0 = np.round(np.multiply((ly, lx), scales[:2])).astype(int)
    m[ly:ly+sy, lx:lx+sx] = np.asarray(
        slideobj.read_region((lx0, ly0), level, (sx, sy)), dtype=dtype)
    m.flush()
    del m
    # print(f"Process {proc}: Done: {offset} + {shape}")


if __name__ == "__main__":  # pragma: no cover
    print("This module is not indended for execution.")
    exit(1)
