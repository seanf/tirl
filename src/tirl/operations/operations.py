#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
import multiprocessing as mp


# TIRL IMPORTS

from tirl.tirlobject import TIRLObject
from tirl import exceptions as te, settings as ts


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class Operator(TIRLObject):
    """
    Operator base class. Implements a fully functional tensor field operator as
    long as it is initialised with suitable operation and chunker functions.

    Subclasses of this base class SHOULD implement default chunkers, that are
    optimised for a certain kind of operation, such as operations on the
    tensors alone and spatial filters.

    """

    def __init__(self, operation, *opargs, chunker=None, n_cpu=ts.CPU_CORES,
                 name=None, **opkwargs):
        """
        Initialisation of Operator.

        :param operation:
            Operation function that will be carried out. The operation function
            must define its first argument as the input TField.
        :type operation: function
        :param opargs:
            Additional positional arguments for the operation function.
        :type opargs: Any
        :param opkwargs:
            Additional keyword arguments for the operation function.
        :type opkwargs: Any
        :param chunker:
            Chunker function. If None, the Operator instance's default chunker
            will be used to slice the tensor field if necessary.
        :type chunker: Union[function, None]
        :param n_cpu:
            Number of CPU cores used to process chunks. Note that each CPU core
            must have enough memory to accommodate a chunk with the maximum
            allowable chunksize given by ts.TFIELD_CHUNKSIZE in MiB. If -1, all
            available logical cores will be used.
        :type n_cpu: Union[int, np.integer]
        :param name:
            Name of the Operator for easier identification.
        :type name: Union[str, None]

        """
        super(Operator, self).__init__()

        # Set operation function
        if callable(operation):
            self._operation = operation
        else:
            raise te.ArgumentError("Operation must be callable.")

        # Set chunker function
        if chunker is None:
            self._chunker = self._autoselect_chunker
        elif callable(chunker):
            self._chunker = chunker
        else:
            raise te.ArgumentError("Chunker must be callable.")

        # Set the number of CPU cores
        if isinstance(n_cpu, (int, np.integer)):
            if n_cpu > 0:
                self.ncores = int(n_cpu)
            elif n_cpu == -1:
                self.ncores = mp.cpu_count()
            else:
                raise te.ArgumentError("The number of CPU cores must be "
                                       "greater than 0.")
        else:
            raise te.ArgumentError("The number of CPU cores must be integer.")

        # Set the name of the Operator
        self.name = name

        # Save opargs and opkwargs
        self.opargs = opargs
        self.opkwargs = opkwargs

    @property
    def operation(self):
        """ Operation function. Read-only to prevent overwriting."""
        return self._operation

    @property
    def chunker(self):
        """ Chunker function. Read-only to prevent overwriting. """
        return self._chunker

    @property
    def input(self):
        """ Input instance. Read-only to prevent overwriting. """
        return self._input

    @property
    def output(self):
        """ Output instance. Read-only to prevent overwriting. """
        return self._output

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, n):
        if n is None or isinstance(n, str):
            self._name = n or super().name
        else:
            raise TypeError(
                "Expected str for {} name, got {}."
                    .format(self.__class__.__name__, n.__class__.__name__))

    def _no_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        Performs operation directly on the tensor field, without chunking and
        further consideration. This allows to save time when the arrays are
        small. Subclasses MUST implement this method.

        :param tf:
            Placeholder argument for input TField instance.
        :type tf: TField
        :param op:
            Placeholder argument for operation.
        :type op: callable
        :param out:
            Placeholder argument for the output TField. If None, the chunker
            function must redirect the output of the operation to a new TField
            instance.
        :type out: Union[TField, None]
        :param opargs:
            Additional positional arguments to the operation function. The
            chunker function must be implemented in a way that it passes this
            on to the operation function.
        :type args: Any
        :params opkwargs:
            Additional keyword arguments to the operation function. The chunker
            function must be implemented in a way that it passes these on to
            the operation function.
        :type opkwargs: Any

        """
        raise NotImplementedError()

    def _autoselect_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        Starts _default_chunker if the input tensor field is not in memory;
        otherwise calls the _no_chunker method for faster performance.

        """
        if (tf.storage == HDD) or (tf.numel > ts.CHUNK_OPERATION_IF_LARGER):
            return self._default_chunker(tf, op, out, *opargs, **opkwargs)
        else:
            return self._no_chunker(tf, op, out, *opargs, **opkwargs)

    def _default_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        Default chunker function for the operator instance. This method SHOULD
        be overloaded in subclasses to create highly specialised Operators that
        perform advanced chunking that is optimised for a certain type of
        operation.

        If the method is not implemented, the Operator must be initialised with
        a suitable chunker function.

        :param tf:
            Placeholder argument for input TField instance.
        :type tf: TField
        :param op:
            Placeholder argument for operation.
        :type op: callable
        :param out:
            Placeholder argument for the output TField. If None, the chunker
            function must redirect the output of the operation to a new TField
            instance.
        :type out: Union[TField, None]
        :param opargs:
            Additional positional arguments to the operation function. The
            chunker function must be implemented in a way that it passes this
            on to the operation function.
        :type args: Any
        :params opkwargs:
            Additional keyword arguments to the operation function. The chunker
            function must be implemented in a way that it passes these on to
            the operation function.
        :type opkwargs: Any

        """
        return NotImplementedError()

    def __call__(self, tfield, out=None, **opkwargs):
        """
        Operator call.

        :param tfield:
            Tensor field to operate on.
        :type tfield: TField
        :param out:
            Tensor field output. If specified, the output of the operation will
            be written to the given TField instance. If None, the operation
            returns a new TField instance.
        :type out: Union[TField, None]
        :param opkwargs:
            Additional keyword arguments to operation.
        :type opkwargs: Any

        :returns:
            Result of operation (only if out=None).
        :rtype: Union[TField, None]

        """
        # Set input
        if hasattr(tfield, "__tfield__"):
            self._input = tfield
        else:
            raise TypeError("Input does not have a TField interface.")

        # Set output
        if hasattr(out, "__tfield__"):
            self._output = out
        elif out is None:
            self._output = None
        else:
            raise te.ArgumentError("Output must be either None or a "
                                   "TField instance.")

        args = self.opargs
        kwargs = self.opkwargs
        kwargs.update(opkwargs)

        # Pass on call to chunker that will call the operation accordingly, and
        # export the result to the specified output. The conditional below
        # decides whether the Operator instance should return a TField instance
        # from the call or not.
        # Note that tf is a TField, whereas self.input can be any subclass of
        # TField. Type consistency is achieved by calling the subclass-specific
        # __operation__finalise__() method.
        tf = self.input.__tfield__()
        if self.output is None:
            ret = self.chunker(tf, self.operation, self.output, *args, **kwargs)
            if hasattr(self.input, "__operation_finalise__"):
                return self.input.__operation_finalise__(
                    ret, op=self, template=self.input)
            else:
                return ret
        else:
            self.chunker(tf, self.operation, self.output, *args, **kwargs)

    # TODO: Add _load/dump methods for saving and loading Operators.


if __name__ == "__main__":
    print("This module is not intended for execution.")
