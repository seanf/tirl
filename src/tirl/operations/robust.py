#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from operator import mul
from functools import reduce


# TIRL IMPORTS

from tirl import exceptions as te, settings as ts
from tirl.operations.spatial import SpatialOperator


# IMPLEMENTATION

class Robust(SpatialOperator):
    """
    Clips tensor values below and above the element-specific robust range. The
    robust range is defined by percentiles (default: 1 and 99).

    """
    def __init__(self, low=1, high=99, n_cpu=ts.CPU_CORES, name=None,
                 **opkwargs):
        """
        Initialisation of Robust.

        :param low:
        :param high:
        :param n_cpu:
        :param name:
        :param opkwargs:
        """

        # Verify input
        if isinstance(low, (int, np.integer)):
            if (low < 0) or (low > 100):
                raise ValueError("Lower percentile must be in range [0,100].")
        else:
            raise TypeError("Lower percentile must be an integer value.")
        if isinstance(high, (int, np.integer)):
            if (high < 0) or (high > 100):
                raise ValueError("Higher percentile must be in range [0,100].")
        else:
            raise TypeError("Higher percentile must be an integer value.")
        if low >= high:
            raise te.ArgumentError("Lower percentile must not be greater than "
                                   "or equal to the higher percentile.")
        self.low = low
        self.high = high
        operation = self._clip
        opargs = ()
        super(Robust, self).__init__(operation, *opargs, radius=1, n_cpu=n_cpu,
                                     name=name, **opkwargs)

    def _clip(self, chunk, *unused_args, **opkwargs):
        n_channels = reduce(mul, self.tshape)
        a_min = opkwargs.get("a_min")
        a_min = (a_min,) * n_channels if not hasattr(a_min, "__iter__") \
            else a_min
        a_max = opkwargs.get("a_max")
        a_max = (a_max,) * n_channels if not hasattr(a_max, "__iter__") \
            else a_max
        result = np.zeros_like(chunk)
        if self.taxes:
            for i in range(n_channels):
                indices = np.unravel_index(i, self.tshape)
                slicer = tuple(slice(int(ix), int(ix) + 1) for ix in indices)
                result[slicer] = \
                    np.clip(chunk[slicer], a_min=a_min[i], a_max=a_max[i])
        else:
            result = np.clip(chunk, a_min=a_min[0], a_max=a_max[0])
        return result

    def __call__(self, tfield, out=None, **opkwargs):
        # Calculate clipping values from percentiles
        low = np.percentile(tfield.data, q=self.low, axis=tfield.vaxes)
        high = np.percentile(tfield.data, q=self.high, axis=tfield.vaxes)
        # Set clipping values
        a_min = opkwargs.pop("a_min", None) or low.ravel()
        a_max = opkwargs.pop("a_max", None) or high.ravel()
        opkwargs.update({"a_min": a_min, "a_max": a_max})
        self.opkwargs.update(opkwargs)
        self.tdim = tfield.tdim
        self.tshape = tfield.tshape
        self.taxes = tfield.taxes
        # Perform clipping
        return super(Robust, self).__call__(tfield, out=out, **self.opkwargs)
