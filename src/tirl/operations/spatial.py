#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import psutil
import numpy as np
from itertools import product

# TIRL IMPORTS

from tirl import exceptions as te, settings as ts
from tirl.operations.operations import Operator
from tirl.transformations.linear.translation import TxTranslation


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class SpatialOperator(Operator):
    """
    The SpatialOperator class is a container suitable for operations that are
    carried out on the spatial domain of TField independently for each tensor
    element.

    An example operation of this kind would be the component-wise Gaussian
    smoothing or gradient filtering of a 3D displacement field.

    """
    def __init__(self, operation, *opargs, radius=1, n_cpu=ts.CPU_CORES,
                 name=None, **opkwargs):
        """
        Intialisation of SpatialOperator.

        :param operation:
            Operation that will be carried out on the spatial domain of the
            input TField, defined as a function with the input TField as its
            first argument.
        :type operation: function
        :param radius:
            Extent of the spatial neighbourhood of each voxel. This is taken
            into account when creating chunks to satisfy the neighbourhood
            dependencies of the calculation at all voxels. Default value is 1
            (Markov condition). If the operation depends on more distant
            neighbours than "radius", results at the chunk interfaces will be
            inaccurate.
        :type radius: Union[int, np.integer]
        :param n_cpu:
            Number of CPU cores used to process chunks. Note that each CPU core
            must have enough memory to accommodate a chunk with the maximum
            allowable chunksize given by ts.TFIELD_CHUNKSIZE in MiB. If -1, all
            available logical cores will be used.
        :type n_cpu: Union[int, np.integer]

        """
        # Initialise by the superclass method. Set chunker to None to use the
        # default chunker, which implements spatially convex chunking of the
        # tensor-major array, which is most suitable to support spatial
        # operations.
        super(SpatialOperator, self).__init__(
            operation, *opargs, chunker=None, n_cpu=n_cpu, name=name,
            **opkwargs)

        # Set radius
        self.radius = radius

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, r):
        if isinstance(r, (int, np.integer)):
            if r >= 0:
                self._radius = int(r)
            else:
                raise te.ArgumentError(
                    "Neighbourhood radius cannot be negative.")
        else:
            raise te.ArgumentError("Neighbourhood radius must be integer.")

    def _no_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        Performs operation with less overhead. Suitable for small in-memory
        arrays only.

        """
        orig_order = tf.order
        tf.order = TENSOR_MAJOR
        result = op(tf.data, *opargs, **opkwargs)
        taxes = tuple(range(result.ndim - tf.vdim))
        result_vshape = result.shape[len(taxes):]

        # Preserve domain with original transformations for non-trimming OP
        trimming_operation = result_vshape != tf.vshape
        if not trimming_operation:
            new_domain = tf.domain

        # Copy domain for trimming OP
        else:
            od = tf.domain.copy()
            margins = np.subtract(tf.vshape, result_vshape) // 2
            assert all(m == self.radius for m in margins)
            offset = [TxTranslation(*margins)] + od.offset
            new_name = "%s_%s" % (od.name, self.name)
            od.kwargs = {k: v for k, v in od.kwargs.items()
                         if k not in od.RESERVED_KWARGS}
            new_domain = od.__class__.__new__(
                od.__class__, result_vshape, od.transformations, offset=offset,
                name=new_name, storage=od.storage, dtype=od.dtype,
                instance_mem_limit=od.instance_mem_limit, n_threads=od.threads,
                **od.kwargs)

        # Create output TField
        name = f"{tf.name}_{self.name}"
        result = tf.__class__.fromarray(
            arr=result, tensor_axes=taxes, copy=False, domain=new_domain,
            order=None, dtype=None, interpolator=tf.interpolator.__class__,
            name=name, storage=tf.storage, **tf.kwargs.copy())
        result.order = orig_order

        if out:
            out.data[...] = result.data[...]
            tf.order = orig_order
        else:
            tf.order = orig_order
            return result

    def _default_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        The default chunker method of SpatialOperator imposes tensor-major
        ordering on the input TField, and creates spatially convex chunks with
        the largest possible extent over the contiguous array dimension
        (last dimension for C-contiguous arrays) and decreasing sizes towards
        the axis with the largest strides. The spatial extent is chosen
        along each dimension such that the neighbourhood dependency is
        satisfied for all spatial dimensions.

        For example, this could mean full-width rectangles on an A4 sheet
        paper.

        :param tf:
            Input TField.
        :type tf: TField
        :param op:
            The operation is expected to take a flattened tensor-major array
            of voxels with shape ([N,] v1, v2, ..., vm) as an input, and return
            the same number of tensor elements, with .
        :type op: callable
        :param out:
            Output TField. If None, the chunker function will redirect the
            output of the operation to a new TField instance.
        :type out: Union[TField, None]
        :params opkwargs:
            Additional keyword arguments to the operation function.
        :type kwargs: dict

        """
        # Assumptions:
        # 1. The output data type is the same as the input data type
        # 2. The operation does not change the number of spatial dimensions.
        # 3. The result of the operation cannot be trusted on the margin of the
        #    tile, within the extent that is defined by the radius.
        # 4. The memory footprint of the operation is unknown.
        # 5. The best chunk shape is what minimises read overhead and uses the
        #    maximum available memory to perform the operation.

        orig_order = tf.order
        tf.order = TENSOR_MAJOR

        # To avoid the overhead of optimising chunk sizes based on too many
        # factors, we are taking the assumption that an operation requires 3+
        # times as much memory as the size of chunk. That is: the input, the
        # output, and an intermediate storage that the operation uses
        # internally. We furhther assume that the internal container is 64-bit.
        # This still ignores a potentially inefficiently implemented operator,
        # as well as the fact that the output may have different
        # tensor and/or voxel shapes.
        chunksize = self._calculate_optimal_chunksize(tf)
        chunks = self._get_chunks(tf, chunksize)
        return_out = out is None

        # Process the chunks
        # (this is hard to parallellise without shared memory in Python 3.7)
        for i, (offset, shape) in enumerate(chunks):
            # print(f"Processing chunk {offset} + {shape}")
            ss = (slice(s, e) for s, e in zip(offset, np.add(offset, shape)))
            ss = (Ellipsis,) + tuple(ss)
            data = tf.data[ss]
            res = op(data, *opargs, **opkwargs)

            # Generate output if it does not exist yet
            if (i == 0) and (out is None):
                tshape = res.shape[:-tf.vdim]
                vshape = tf.vshape
                name = tf.name + f"_{self.name}"
                out = type(tf)(vshape, tensor_shape=tshape, order=TENSOR_MAJOR,
                               dtype=res.dtype, buffer=None, offset=0,
                               interpolator=None, name=name, storage=tf.storage)

                # If the output is on the HDD, we will reload the relevant part
                # of the output memory map to make sure that we do not
                # accumulate results from different chunks in the memory.
                mfile = getattr(out.data, "filename", "")
                if mfile:
                    mdtype = out.data.dtype.str
                    moffset = out.data.offset
                    mshape = out.data.shape
                    del out
                    out = None

            # Trim the result (only internal edges, only voxel dimensions)
            trim = data.shape[-tf.vdim:] == res.shape[-tf.vdim:]
            if trim:
                chstart = offset
                chend = np.add(offset, shape)
                outslicer = []
                resslicer = []
                for ax, (start, stop) in enumerate(zip(chstart, chend)):
                    oarg1 = start + self.radius if start > 0 else start
                    rarg1 = self.radius if start > 0 else 0
                    oarg2 = stop - self.radius if stop < tf.vshape[
                        ax] else stop
                    dim = stop - start
                    rarg2 = dim - self.radius if stop < tf.vshape[
                        ax] else None
                    outslicer.append(slice(oarg1, oarg2))
                    resslicer.append(slice(rarg1, rarg2))
                else:
                    outslicer = (Ellipsis,) + tuple(outslicer)
                    resslicer = (Ellipsis,) + tuple(resslicer)

                # Reload output if it is on HDD
                if out is None:
                    out = np.memmap(mfile, dtype=mdtype, mode="r+",
                                    offset=moffset, shape=mshape)

                # Copy current result to output
                out[outslicer] = res[resslicer]
                if mfile:
                    del out
                    out = None

            else:
                raise AssertionError(
                    "The spatial operation must preserve the number of voxels.")

        # Reload output if it is on HDD
        if out is None:
            out = np.memmap(mfile, dtype=mdtype, mode="r+",
                            offset=moffset, shape=mshape)
            out = type(tf)(vshape, tensor_shape=tshape, order=TENSOR_MAJOR,
                           dtype=res.dtype, buffer=out, offset=0,
                           interpolator=None, name=name, storage=tf.storage)

        tf.order = orig_order
        out.order = orig_order

        if return_out:
            return out

    def _calculate_optimal_chunksize(self, tf):
        """
        Calculates the maximum number of image points that are allowed to be
        operated on given the currently available memory and the instance
        memory limit.

        """
        available = psutil.virtual_memory().available
        available = min(available, ts.TFIELD_INSTANCE_MEMORY_LIMIT * 1024 ** 2)
        itemsize = np.dtype(tf.data.dtype).itemsize
        return int(available / (3 * itemsize))

    def _get_chunks(self, tf, chunksize):
        """
        Generator that defines the chunks. Chunks are defined by an offset
        vector that points to their top-left-most voxel and their shape. The
        chunk shapes are maximised along the contiguous dimensions of the
        tensor field, such that the required margin is kept along all
        dimensions.

        """
        chunkshape = np.asarray(
            tf.tshape[:tf.tdim] + (1 + 2 * self.radius,) * tf.vdim)
        if np.product(chunkshape) > chunksize:
            raise MemoryError(
                "Not enough memory to allocate the minimal chunk.")

        # Set the shape of the first tile
        for ax in range(tf.tdim + tf.vdim - 1, -1, -1):
            axfactor = np.product(chunkshape) / chunkshape[ax]
            newsize = chunksize / axfactor
            if newsize - chunkshape[ax] >= 1:
                chunkshape[ax] = min(int(newsize), tf.shape[ax])
            else:
                break

        # Parallel computation is only safe if the workers can load
        # non-overlapping portions of the memory-mapped TField, so here we
        # require that the last dimension of the chunk matches the shape of the
        # TField along that dimension.
        if (chunkshape[-1] != tf.vshape[-1]) and self.ncores > 1:
            raise AssertionError(
                "The contiguous dimension of the input field is too large for "
                "the current memory limit. Consequently, parallel computations "
                "are not safe.")

        # Generate overlapping chunks
        axissteps = [range(0, ts - 2 * self.radius, cs - 2 * self.radius)
                     for ts, cs in zip(tf.vshape, chunkshape[tf.tdim:])]
        for offset in product(*axissteps):
            shape = np.minimum(np.subtract(tf.vshape, offset),
                               chunkshape[tf.tdim:])
            yield offset, shape


def process_chunk(chunk, tf, op, *opargs, **opkwargs):
    """
    Performs operation on a single chunk defined by its offset and shape.

    """
    chunk_no, (offset, shape) = chunk
    ss = (slice(s, e) for s, e in zip(offset, np.add(offset, shape)))
    ss = (Ellipsis,) + tuple(ss)
    data = tf.data[ss]
    res = op(data, *opargs, **opkwargs)
    trimming = data.shape[-tf.vdim:] != res.shape[-tf.vdim:]
    return res, trimming
