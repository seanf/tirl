#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import warnings
import numpy as np
import multiprocessing as mp
from functools import reduce
from operator import mul
from functools import partial


# TIRL IMPORTS

from tirl import exceptions as te, settings as ts
from tirl.operations.operations import Operator


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class TensorOperator(Operator):
    """
    The TensorOperator class is a container suitable for operations that are
    confined to the tensors of the TField, i.e. calculations that are
    independent at each voxel.

    An example operation of this kind would be calculating the norm(s) of a
    vector field at every voxel.

    """
    def __init__(self, operation, *opargs, n_cpu=ts.CPU_CORES, name=None,
                 **opkwargs):
        """
        Intialisation of TensorOperator.

        :param operation:
            Operation that will be carried out. The operation function must
            define the input TField as its first argument.
        :type operation: function
        :param n_cpu:
            Number of CPU cores used to process chunks. Note that each CPU core
            must have enough memory to accommodate a chunk with the maximum
            allowable chunksize given by ts.TFIELD_CHUNKSIZE in MiB. If -1, all
            available logical cores will be used.
        :type n_cpu: Union[int, np.integer]

        """
        # Initialise by the superclass method. Set chunker to None to use the
        # default chunker, which implements voxel-major chunking, that is ideal
        # for tensor operations.
        super(TensorOperator, self).__init__(
            operation, *opargs, chunker=None, n_cpu=n_cpu, name=name,
            **opkwargs)
        self._mask = None

    @property
    def mask(self):
        return self._mask

    @mask.setter
    def mask(self, m):
        if hasattr(m, "__array__"):
            self._mask = m
        else:
            raise TypeError(f"Expected boolean array-like object for mask, "
                            f"got {m.__class__.__name__} instead.")

    def _no_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        Performs operation with less overhead. Suitable for small in-memory
        arrays only. A new instance is returned with the original Domain.

        """
        orig_order = tf.order
        tf.order = ts.VOXEL_MAJOR
        data = tf.data.reshape((-1, *tf.tshape))

        if self.mask is not None:
            # If a mask is used, an output TField with the same shape as the
            # input TField must be provided, and the operation must preserve
            # the tensor shape.
            mask = np.asarray(self.mask, dtype=np.bool_).ravel()
            result = op(data[mask, ...], *opargs, **opkwargs)
            out.order = ts.VOXEL_MAJOR
            outdata = out.data.reshape((-1, *out.tshape))
            assert outdata.base is out.data
            outdata[mask, ...] = result
            out.order = orig_order
            return

        result = op(data, *opargs, **opkwargs)
        taxes = tuple(range(tf.vdim, tf.vdim + result.ndim - 1))
        result = result.reshape((*tf.vshape, *result.shape[1:]))
        name = f"{tf.name}_{self.name}"
        result = tf.__class__.fromarray(
            arr=result, tensor_axes=taxes, copy=False, domain=tf.domain,
            order=None, dtype=None, interpolator=tf.interpolator.__class__,
            name=name, storage=tf.storage, **tf.kwargs.copy())
        result.order = orig_order
        if out:
            if hasattr(out, "__tfield__") and (out.order != tf.order):
                raise ValueError("Input and output must have the same layout!")
            out.data[...] = result.data[...]
            tf.order = orig_order
        else:
            tf.order = orig_order
            return result

    def _default_chunker(self, tf, op, out, *opargs, **opkwargs):
        """
        This default chunker sets the order of the input TField to voxel-major
        and flattens the spatial domain to a single dimension before calling
        the operation on longitudinal chunks of this array.

        :param tf:
            Input TField.
        :type tf: TField
        :param op:
            The operation is expected to take a flattened array of tensors
            with shape ([N,] t1, t2, ..., tm) as an input, and return the same
            number of tensors N, with arbitrary new tensor shape.
        :type op: callable
        :param out:
            Output TField. If None, the chunker function will redirect the
            output of the operation to a new TField instance.
        :type out: Union[TField, None]
        :params opargs:
            Additional positional arguments to the operation function.
        :type kwargs: dict
        :params opkwargs:
            Additional keyword arguments to the operation function.
        :type kwargs: dict

        """
        # Keep the original TField properties
        input_orig_order = tf.order

        # Create a reshaped view into the storage buffer of the input TField
        # Note: I do not reassign this reshaped view to self.input._data,
        # because it would break down an assumed relationship between the
        # TField storage buffer and its Domain.
        tf.order = ts.VOXEL_MAJOR
        input_buffer = tf.data.reshape((-1, *tf.tshape))
        input_dtype = input_buffer.dtype
        input_tensor_count, *input_tensor_shape = input_buffer.shape
        input_tensor_element_count = int(reduce(mul, input_tensor_shape))
        input_tensor_size = \
            input_tensor_element_count * np.dtype(input_dtype).itemsize

        # Perform operation on the first tensor
        if input_tensor_size > ts.TFIELD_CHUNKSIZE * (1024 ** 2):
            raise MemoryError(
                "The current chunksize is not enough to hold a single tensor "
                "in memory. Please increase TFIELD_CHUNKSIZE.")
        start, stop, first_tensor = 0, 1, input_buffer[[0], ...]
        first_tensor_result = op(first_tensor, *opargs, **opkwargs)

        # Set up output buffer based on the result of the operation on the
        # first tensor
        output_tensor_shape = first_tensor_result.shape[1:]
        # Note: The following 3 lines were commented out as part of a bugfix.
        # # If the operation fully reduced tensors, set output shape to (1,)
        # if len(output_tensor_shape) == 0:
        #     output_tensor_shape = (1,)
        output_tensor_element_count = int(np.prod(output_tensor_shape))
        output_dtype = first_tensor_result.dtype
        output_tensor_size = \
            output_tensor_element_count * np.dtype(output_dtype).itemsize

        # Calculate maximum chunk size and create chunks
        # Assumption: the maximum allowable chunksize holds independently for
        # all processing cores.
        # TODO: Revise this memory plan.
        chunklen = (ts.TFIELD_CHUNKSIZE * (1024 ** 2)) \
                   // (input_tensor_size + output_tensor_size) \
                   // self.ncores // 2
        starting_points = list(range(1, input_tensor_count, chunklen))
        # Are there more than 1 tensors in the input TField?
        multiple_tensors = bool(starting_points)

        # Prepare existing TField instance as output
        if out:
            if out.dtype != output_dtype:
                raise TypeError("Target dtype ({}) does not match the result "
                                "of the operation ({})."
                                .format(out.dtype, output_dtype))
            # Check the balance of element counts (ignore singleton dimensions)
            squeeze = lambda shape: tuple(ax for ax in shape if ax != 1)
            if squeeze(tf.vshape) != squeeze(out.vshape):
                raise te.BroadcastError(
                    "The shape of the source domain {} does not match the "
                    "shape of the target domain {}."
                        .format(tf.vshape, out.vshape))
            if squeeze(out.tshape) != squeeze(output_tensor_shape):
                raise te.BroadcastError(
                    "Target tensor shape ({}) does not match tensor shape "
                    "after the operation ({})."
                        .format(out.tshape, output_tensor_shape))
            # Warn the user if the counts match, but the exact shapes do not,
            # as this might imply an accidental transposition between row and
            # column vectors.
            elif out.tshape != output_tensor_shape:
                warnings.warn(
                    "Singleton dimension mismatch: possible row/column vector "
                    "inversion after operation: {} -> {}"
                        .format(output_tensor_shape, out.tshape))
            return_result = False

        # Create new TField instance for output
        else:
            name = "{}_{}".format(tf.name, self.name)
            out = tf.__class__.__new__(
                tf.__class__, extent=tf.domain,
                tensor_shape=output_tensor_shape, order=input_orig_order,
                dtype=output_dtype, buffer=None, offset=0,
                interpolator=tf.interpolator.__class__, name=name,
                storage=tf.storage, **tf.kwargs.copy())
            return_result = True

        # Temporarily reorder and reshape the storage buffer of the output
        # instance to match that of the input instance.
        # (it has to be C-contiguous, i.e. VOXEL_MAJOR)
        output_orig_order = out.order
        out.order = ts.VOXEL_MAJOR
        output_buffer = out.data.reshape((-1, *output_tensor_shape))

        # Export the result of the operation on the first tensor
        output_buffer[0, ...] = first_tensor_result
        del first_tensor_result

        # If there are more than 1 tensors in the input, process these as well
        if multiple_tensors:

            # Make a more rational choice of processing cores:
            # 1. Do not use more cores than the number of chunks.
            # 2. Do not use more than 1 core if the output is in the memory
            # Note that it follows naturally from #2 that when multiple cores
            # are used, the output is memory-mapped, i.e. processes should get
            # write access to the storage file (see later).
            n_cpu = min(len(starting_points), self.ncores)
            n_cpu = 1 if out.storage == MEM else n_cpu

            # Define job for each worker
            job_info = {
                "chunklen": chunklen,
                "input_tensor_count": input_tensor_count,
                "input_tensor_shape": input_tensor_shape,
                "operation": op,
                "opargs": opargs,
                "opkwargs": opkwargs,
                "output_tensor_shape": output_tensor_shape,
                "output_dtype": output_dtype,
                "output_tensor_size": output_tensor_size,
            }
            # Include file storage information if n_cpu > 1, because this
            # implies that the output TField is memory-mapped, and the worker
            # must have write access to this file (see above).
            if n_cpu != 1:
                job_info.update({"fname": out._data.fname})
            jobs = self._jobiter(
                input_buffer, starting_points, input_tensor_shape, chunklen,
                input_tensor_count)
            job_func = partial(tensoroperator_worker, **job_info)

            # Single-core routine
            if n_cpu == 1:
                # print("Single-core routine...")
                i = 0
                for start, stop, output_array in map(job_func, jobs):
                    i += 1
                    output_buffer[start:stop, ...] = output_array
                    # print("Job {}: {}-{} complete.".format(i, start, stop))
                    del output_array

            # Multi-core routine
            else:
                # print("Multi-core routine...")
                # TODO: Decide whether forking/spawning/multithreading is better
                # with mp.get_context("fork").Pool(n_cpu) as pool:
                import multiprocessing.dummy as mt
                pool = mt.Pool(processes=self.ncores)
                pool.map(job_func, jobs)
                pool.close()
                    # i = 0
                    # for start, stop in pool.imap_unordered(job_func, jobs):
                    #     i += 1
                    #     # print("Job {}: {}-{} complete."
                    #     # .format(i, start, stop))

        # Reset the state of the input and output tensor fields
        out.order = output_orig_order
        tf.order = input_orig_order

        # Return new TField if needed
        if return_result:
            return out

    @staticmethod
    def _jobiter(input_buffer, starting_points, input_tensor_shape, chunklen,
                input_tensor_count):

        # Create chunks
        for start in starting_points:
            stop = min(start + chunklen, input_tensor_count)
            chunk_shape = (stop - start, *input_tensor_shape)
            tensors = np.zeros(chunk_shape, input_buffer.dtype)
            tensors[:] = input_buffer[start:stop, ...]

            yield start, stop, tensors


def tensoroperator_worker(job, **job_info):
    """
    Function executed by parallel processes to perform operation on chunks
    of the input data.

    :param start:
        Index of chunk's first element.
    :type start: Union[int, np.integer]
    :param job_info:
        Job descriptor variables.
    :type job_info: dict

    """
    # Report acceptance
    worker_id = mp.current_process().name
    start, stop, tensors = job
    # print("Worker {}: Processing chunk {}-{}..."
    #       .format(worker_id, start, stop))

    # Set up output storage buffer
    operation = job_info.get("operation")
    opargs = job_info.get("opargs")
    opkwargs = job_info.get("opkwargs")
    output_dtype = job_info.get("output_dtype")
    output_tensor_size = job_info.get("output_tensor_size")
    output_tensor_shape = job_info.get("output_tensor_shape")
    fname = job_info.get("fname")

    # Write to file storage
    if fname is not None:
        output_array = \
            np.memmap(fname, dtype=output_dtype, mode="r+",
                      offset=start * output_tensor_size,
                      shape=(stop - start, *output_tensor_shape), order="C")
        output_array[...] = operation(tensors, *opargs, **opkwargs)
        del output_array
        return start, stop

    # Return the result of the operation if the target is not a file
    return start, stop, operation(tensors, *opargs, **opkwargs)
