#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import joblib
import inspect
import numpy as np
import collections


# TIRL IMPORTS

from tirl import exceptions as te

# DEFINITIONS

CACHE_MODES = {"memory", "db"}


# IMPLEMENTATION

class Cache(object):
    """ Cache object for instance-specific use. Usage: define Cache object
    as part of the initialisation, then call the three public methods (query,
    retrieve, store) in succession from the same function/method.

    Example:
        signature = ("John", "Doe", "42", "Sheffield")  # for hashing
        key, exists = self.cache.query(signature)
        if exists:
            value = self.cache.retrieve(key)
        else:
            value = "successful person"
            self.cache.store(key, value)

    Note:
        To access the same cache database from multiple functions/methods,
        specify the same caller ID (the name of the function/method) while
        using query(), retrieve(), and store().

    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, mode="memory", maxsize=2):
        """
        Initialisation of Cache object.

        :param mode: Storage mode. "memory": in RAM, "db": database.
        :type mode: Union["memory", "db"]

        """
        # Validate input for Cache Mode
        if not mode.lower() in CACHE_MODES:
            raise te.ConstructionError(
                    "Unrecognised GImage mode: '{0}'. Available Cache modes "
                    "are: {1}".format(mode, ", ".join(CACHE_MODES)))
        else:
            self.mode = mode

        # Validate input for maxsize
        if isinstance(maxsize, (int, np.integer)):
            if maxsize > 0:
                self.maxsize = maxsize
            else:
                raise te.ConstructionError(
                    "Maximum cache size must be a positive integer.")
        else:
            raise te.ConstructionError(
                "Maximum cache size must be a positive integer.")

        # Create cache and accession frequency database
        if mode == "memory":
            self.cache = dict()
            self.freqs = dict()
        else:
            # TODO
            raise NotImplementedError()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def query(self, *hashables, caller=None):
        """
        Queries Cache object for existing entry based on hashable
        inputs.

        """
        # Calculate hash for hashables
        key = joblib.hash(hashables)

        # Identify caller function/method
        if caller is None:
            # caller = str(inspect.stack()[1][0].f_code.co_name)
            caller = "default"

        try:
            fcache = self.cache[caller]
            freq = self.freqs[caller]
        except:
            if self.mode == "memory":
                fcache = self.cache[caller] = dict()
                freq = self.freqs[caller] = collections.defaultdict()
            else:
                # TODO
                raise NotImplementedError()

        # Return: key, exists
        return key, key in fcache.keys()

    def retrieve(self, key, caller=None):
        """
        Retrieve cached data based on key.

        :param key: Access key (return value of Cache.query).
        :type key: str

        :returns: Cached data.

        """
        # Identify caller function/method
        if caller is None:
            caller = str(inspect.stack()[1][0].f_code.co_name)
        fcache = self.cache[caller]
        freq = self.freqs[caller]

        # Retrieve data from cache
        try:
            result = fcache[key]
        except KeyError:
            raise te.CacheError("Retrieve was unsuccessful for '{0}' "
                                "using key '{1}'.".format(caller, key))
        else:
            # Increase accession count for the specific key
            try:
                freq[key] += 1
            except:
                freq[key] = 1

        return result

    def store(self, key, value, caller=None):
        """ Adds an expensive result to the cache under a particular key. """

        # Identify caller function/method
        if caller is None:
            caller = str(inspect.stack()[1][0].f_code.co_name)
        fcache = self.cache[caller]
        freq = self.freqs[caller]

        # Add data to cache
        try:
            # Delete from cache when full
            if len(fcache) >= self.maxsize:
                delkey = min(freq.items(), key=lambda f: f[1])[0]
                fcache.pop(delkey)
                freq.pop(delkey)

            # Store new value
            fcache[key] = value

            # Increase accession count for the new key
            try:
                freq[key] += 1
            except:
                freq[key] = 1

        except KeyError:
            raise te.CacheError("The requested data could not be stored in "
                                "the instance's cache.")


if __name__ == "__main__":
    print("""This module is not intended for execution.""")

