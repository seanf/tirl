#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import numbers
import inspect
import builtins
import warnings
import tempfile
import importlib
import numpy as np
from math import ceil
from operator import mul
from pydoc import locate
import multiprocessing as mp
from functools import reduce
from types import SimpleNamespace
from scipy.ndimage.filters import gaussian_filter


# TIRL IMPORTS

from tirl import utils as tu, settings as ts
from tirl.domain import Domain
from tirl.buffer import Buffer
from tirl.tirlobject import TIRLObject
from tirl.transformations.basic.embedding import TxEmbed
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.translation import TxTranslation
from tirl.interpolators.interpolator import Interpolator
from tirl.operations.spatial import SpatialOperator


# DEFINITIONS

from tirl.constants import *

STORAGE_OPTIONS = (MEM, HDD)
LAYOUTS = (TENSOR_MAJOR, VOXEL_MAJOR)
PREVIEW_MODES = ("rgb", "hsv", "composite", "quiver")


# DEVELOPMENT NOTES

# Note on subclassing
#
# Class references in constructor calls must be explicit to
# ensure that the behaviour of the TField class under operations (and its
# methods) is type-consistent (i.e. the resultant type is also a TField).
# Subclasses that aim to adopt a TField-like behaviour under the same
# operations must define a fromTField constructor, and overload the relevant
# methods where a TField is created, and append it with a call to the
# fromTField subclass constructor. (See TImage for an example.)
#
# Second-degree subclasses of TField should similarly inherit the
# operation-defining methods from the first-degree subclass and implement a
# from<subclass_1> constructor, respectively.


# IMPLEMENTATION

class TField(TIRLObject, np.lib.mixins.NDArrayOperatorsMixin):

    # Set the array priority higher than that of np.matrix with 10.0
    # and np.array with 0.0, so that this class can control ufunc behaviour.
    __array_priority__ = 15.0

    # TField supports arithmetic with the following classes:
    _HANDLED_TYPES = (np.ndarray, numbers.Number)

    # Constructor arguments are reserved keyword arguments
    RESERVED_KWARGS = ("extent", "tensor_shape", "order", "dtype", "buffer",
                       "offset", "interpolator", "name", "storage")

    # Initialise loader plugins
    loaders = SimpleNamespace(**dict(inspect.getmembers(
        importlib.import_module("tirl.loader"), inspect.isclass)))

    @classmethod
    def fromfile(cls, f, tensor_axes=None, dtype=None, storage=None,
                 domain=None, interpolator=None, name=None, loader=None,
                 loader_kwargs=None, **kwargs):
        """
        Alternative TField constructor: loads TField from image file.

        :param f: file path
        :type f: str
        :param dtype: data type
        :type dtype: Union[NoneType, str, type, np.generic]
        :param storage: storage mode (MEM or HDD)
        :type storage: Union[NoneType, str]

        :returns: new TField instance
        :rtype: TField

        """
        from tirl.loader import LOADER_PREFERENCES

        # Concatenate loader options
        loader_kwargs = dict() if loader_kwargs is None else loader_kwargs
        loader_kwargs.update({
            "dtype": dtype,
            "storage": storage or MEM,
        })

        # Find loader
        if loader is None:
            for key in LOADER_PREFERENCES.keys():
                if f.endswith(key):
                    loader = LOADER_PREFERENCES[key](**loader_kwargs)
                    break
            else:
                raise TypeError(
                    "None of the loaders support the file type: {}".format(f))
        elif isinstance(loader, str):
            from pydoc import locate
            try:
                loader = locate(loader)(**loader_kwargs)
            except:
                raise ValueError("Cannot find loader class: {}".format(loader))
        elif inspect.isclass(loader):
            try:
                loader = loader(**loader_kwargs)
            except:
                raise TypeError("Invalid loader type: {}".format(loader))
        else:
            raise TypeError("Unsupported loader: {}".format(loader))

        # Apply loader
        arr, hdr = loader(f)

        # Create TField from array
        name = name or os.path.split(f)[-1]
        arr, tensor_axes = cls._prepare_array(arr, tensor_axes)
        # Note: the header will end up in the kwargs of the TField
        # However, the TImage defines the header separately, so it will end up
        # as a proper header if the caller was TImage.
        obj = cls.fromarray(
            arr, tensor_axes=tensor_axes, copy=False, domain=domain, order=None,
            dtype=dtype, interpolator=interpolator, name=name, storage=storage,
            header=hdr, **kwargs)

        return obj

    @classmethod
    def _prepare_array(cls, arr, tensor_axes):
        """
        Operations to perform on the intermediate data array from which the
        TField is constructed.

        :param arr:
            Intermediate array that is read from a file or created from an
            array-like object.
        :type arr: np.ndarray
        :param tensor_axes:
            Tensor axis indices.
        :type tensor_axes: Union[int, tuple[int]]

        :returns: array with the operations applied
        :rtype: np.ndarray

        """
        # Ignore singleton dimensions (if the user has enabled this feature)
        if ts.TFIELD_IGNORE_SINGLETON_DIMENSIONS:
            # Remove singleton dimensions only when vdim > 1
            if tensor_axes is not None:
                if arr.ndim - len(tensor_axes) > 1:
                    arr = np.squeeze(arr)
            else:
                arr = np.squeeze(arr)

        # Infer tensor axis from input (if the user has given permission)
        if tensor_axes is None and ts.TFIELD_AUTODETECT_TENSOR_AXIS:
            tensor_axes = cls._autodetect_tensor_axes(arr.shape)
        else:
            tensor_axes = () if tensor_axes is None else tensor_axes
            tensor_axes = (tensor_axes,) \
                if not hasattr(tensor_axes, "__iter__") else tensor_axes
            if any(ax >= arr.ndim for ax in tensor_axes):
                raise ValueError(f"One of the specified tensor axis is "
                                 f"out of bounds ({arr.ndim - 1}).")
            tensor_axes = tuple(ax % arr.ndim for ax in tensor_axes)

        return arr, tensor_axes

    @staticmethod
    def _autodetect_tensor_axes(shape):
        """
        This function attempts to detect tensor axes automatically from the
        image shape using the following rules:

            1. If the image is 2D, a 0-size tensor axis is implied.
            2. If the image is 3D, dimensions with (0, 1, 2, 3, 4) sizes are
               detected as tensor axes.
            3. If the image is 4D, the axes with (0, 1) size, or in absence of
               these, the axis with the smallest size is detected as a tensor
               axis.
            4. If the image is >4D, dimensions with (0, 1) sizes are detected
               as tensor axes.

        :param shape: shape of the input data array
        :type shape: Union[tuple, list]

        :return: indices of logical axes
        :rtype: tuple

        """
        ndim = len(shape)
        t_axes = ()
        if ndim == 3:
            t_axes = tuple(i for i, ax in enumerate(shape)
                           if ax in (0, 1, 2, 3, 4))
        elif ndim == 4:
            t_axes = tuple(i for i, ax in enumerate(shape) if ax in (0, 1))
            if not t_axes:
                t_axes = (tuple(shape).index(min(shape)),)

        elif ndim > 4:
            t_axes = tuple(i for i, ax in enumerate(shape) if ax in (0, 1))

        # Notify about positive result
        # (as this may be unexpected after leaving "t_axes" blank
        if t_axes:
            warnings.warn("Auto-detected tensor axes: {}".format(t_axes))

        return t_axes

    @classmethod
    def fromarray(cls, arr, tensor_axes=(), copy=True, domain=None, order=None,
                  dtype=None, interpolator=None, name=None, storage=MEM,
                  **kwargs):
        """
        TField's array constructor. Creates TField from array data. Depending on
        the copy parameter, the resultant TField instance either shares the
        data with the array-like object, or a copy is made and the TField
        becomes independent from the array.

        :param arr:
            Input array. Any object that defines the __array__ interface.
        :type arr: np.ndarray
        :param tensor_axes:
            Index or indices of the tensor axis or axes.
        :type tensor_axes: Union[int, tuple[int]]
        :param copy:
            If False, the resultant TField will share the data with the original
            array instance. If True (default), the array data is copied to the
            new TField instance, making it independent from the original array.
        :type copy: bool
        :param domain:
            Domain of the new TField instance. The shape of the domain must
            match the spatial extent of the input array. If None, a new Domain
            will be automatically created based on the shape of the input array.
        :type domain: Union[Domain, NoneType]
        :param order:
            Buffer layout (C-contiguous). 'T' for tensor major (i.e. voxels are
            stored contiguously for each tensor element), 'V' for voxel major
            (i.e. tensor values are stored contiguously for each voxel). If
            None, the axis order will be determined from the specified tensor
            axes.
        :type order: Union[str, NoneType]
        :param dtype:
            Tensor data type. If None, the data type will be inferred from the
            input array.
        :type dtype: Union[str, np.dtype, NoneType]
        :param interpolator:
            Interpolator instance. If None, a new interpolator will be
            automatically created based on the type of the domain.
        :type interpolator: Union[Interpolator, NoneType]
        :param name:
            Name of the new TField instance. If None, a name of the following
            format will be automatically generated: "TField_$memaddress".
        :type name: Union[str, NoneType]
        :param storage:
            If "hdd", the tensor field data will be stored on the HDD (in the
            temp folder of TIRL). If "mem" (default), the tensor field data
            will be stored in memory. If there is not enough memory available,
            "hdd" mode will be automatically selected.
        :type storage: str
        :param kwargs:
            Additional keyword arguments for the new TField instance.
        :type kwargs: dict

        :returns: new TField instance based on the input array
        :rtype: TField

        """
        # Array
        if not hasattr(arr, "__array__"):
            raise TypeError("Input field is not array-like.")
        arr = arr if isinstance(arr, np.memmap) else getattr(arr, "__array__")()
        if copy:
            arr = arr.copy()

        # Tensor axes
        if not hasattr(tensor_axes, "__iter__"):
            tensor_axes = (tensor_axes,)
        ndim = len(arr.shape)
        tensor_axes = tuple(ax % ndim for ax in tensor_axes)

        # Determine tensor shape and order from axes
        if tensor_axes == ():
            tshape = (1,)
            vshape = arr.shape
            auto_order = TENSOR_MAJOR
        else:
            tshape = tuple(
                dim for ax, dim in enumerate(arr.shape) if ax in tensor_axes)
            # Note: if the voxel domain is reduced to a single point, set the
            # shape to (1,), not ().
            vshape = tuple(
                dim for ax, dim in enumerate(arr.shape)
                if ax not in tensor_axes) or (1,)
            auto_order = \
                TENSOR_MAJOR if tensor_axes[0] == 0 else VOXEL_MAJOR

        # Default TField parameter values
        domain = domain or vshape
        dtype = dtype or arr.dtype
        buffer = arr if dtype == arr.dtype else arr.astype(dtype)
        storage = storage or (HDD if type(arr) is np.memmap else MEM)

        # Purge kwargs dictionary
        kwargs = {k: v for k, v in kwargs.items()
                  if k not in TField.RESERVED_KWARGS}
        # Create new TField instance
        ret = TField(extent=domain, tensor_shape=tshape, order=auto_order,
                     dtype=dtype, buffer=buffer, offset=0,
                     interpolator=interpolator, name=name, storage=storage,
                     **kwargs)
        ret.order = order or auto_order
        return ret

    def __new__(cls, extent, tensor_shape=None, order=None, dtype=None,
                buffer=None, offset=0, interpolator=None, name=None,
                storage=MEM, **kwargs):
        """
        TField's default constructor.

        :param extent:
            Spatial extent of the tensor field. This can be specified by a
            sequence of integers describing the shape of the domain (for
            compact domains), an array of coordinates describing a non-compact
            domain, or a Domain instance (either compact or non-compact). In
            the former two cases a new Domain instance will be created, whereas
            in the last case no copies will be made, and the input Domain
            instance will serve as the domain of the new TField instance.
        :type extent: Union[tuple[int], list[int], Domain]
        :param tensor_shape:
            Tensor shape (not to be confused with the shape of the spatial
            domain).
        :type tensor_shape: Union[tuple[int], list[int], int, NoneType]
        :param order:
            Buffer layout (C-contiguous). 'T' for tensor major (i.e. voxels are
            stored contiguously for each tensor element), 'V' for voxel major
            (i.e. tensor values are stored contiguously for each voxel).
        :type order: Union[TENSOR_MAJOR, VOXEL_MAJOR]
        :param dtype:
            Tensor data type.
        :type dtype: Union[str, np.dtype]
        :param buffer:
            Buffer interface for array data. If None, an in-memory array is
            created with zeros.
        :type buffer: Union[memoryview, np.ndarray]
        :param offset:
            Read offset from buffer.
        :type offset: int
        :param interpolator:
            Interpolator instance. If None, a new interpolator will be
            automatically created based on the type of the domain.
        :type interpolator: Union[Interpolator, NoneType]
        :param name:
            Name of the TField instance. If None, a name of the following
            format will be automatically generated: "TField_$memaddress".
        :type name: Union[str, NoneType]
        :param storage:
            If "hdd", the tensor field data will be stored on the HDD (in the
            temp folder of TIRL). If "mem", the tensor field data will be
            stored in memory. If there is not enough memory available, "hdd"
            mode will be automatically selected.
        :type storage: str
        :param kwargs:
            Additional keyword arguments for the new TField instance.
        :type kwargs: Any

        :returns: new TField instance
        :rtype: TField

        """
        # Validate input types
        if ts.TYPESAFE_MODE:
            TField.__validate_input(
                extent=extent, tensor_shape=tensor_shape, order=order,
                dtype=dtype, buffer=buffer, offset=offset)

        # Handle the polymorphism of the first argument (extent)
        # Determine the domain and the tensor shape

        # extent: file path
        if isinstance(extent, str):
            obj = TField.fromfile(extent)
            buffer = obj.data
            tensor_shape = tensor_shape or obj.tshape
            order = order or obj.order

        # extent: subclass of TField (or otherwise has the __tfield__ method)
        elif hasattr(extent, "__tfield__"):
            obj = extent.__tfield__()
            buffer = obj.data
            tensor_shape = tensor_shape or obj.tshape
            order = order or obj.order
        # extent: not a TField (needs to be created by superclass constructor)
        else:
            # extent: Domain object
            if type(extent) is Domain:
                domain = extent
            # extent: coordinate array (non-compact domain)
            elif hasattr(extent, "__array__") and len(extent.shape) == 2:
                domain = Domain(np.asarray(extent))
            # extent: shape-like (sequence of ints) (compact domain)
            elif hasattr(extent, "__iter__") and \
                    all(type(dim) in (int, np.integer) for dim in extent):
                domain = Domain(tuple(extent))
            # extent: any other type
            else:
                error_msg = "Invalid input type for TField extent: {}." \
                    .format(type(extent))
                if hasattr(extent, "__array__"):
                    error_msg += " For array-like objects the " \
                                 "TField.fromarray constructor must be used."
                raise TypeError(error_msg)

            # Handle the polymorphism of the tensor_shape argument: tuple or int
            tensor_shape = () if tensor_shape is None else tensor_shape
            if not hasattr(tensor_shape, "__iter__"):
                tensor_shape = (tensor_shape,)
            # If tensor shape is an empty tuple, set it to (1,) (scalar field)
            else:
                if len(tensor_shape) == 0:
                    tensor_shape = (1,)

            # Call superclass constructor and set basic properties
            obj = super().__new__(cls)
            obj.properties = {"domain": domain, "tensor_shape": tensor_shape}

        # Use user-specified domain if possible
        # use case: TField(old_field, domain=new_domain)
        obj.domain = kwargs.pop("domain", obj.domain)

        # Set additional properties based on input arguments
        # Preserve source instance properties unless redefined by the caller

        # Buffers should not be confused with arrays. Arrays may serve as
        # buffers, but their data type and shape is set. Buffers are continuous
        # streams of data, hence additional information is required to convert
        # them into arrays. If these values are specified incorrectly, data
        # will be scrambled, so the use of the TField.fromarray() constructor
        # instead is highly encouraged.
        buffer_requires = (extent, tensor_shape, order)
        if buffer is not None:
            if any(arg is None for arg in buffer_requires):
                raise ValueError(
                    "TField constructor requires input for 'extent', "
                    "'tensor_shape', 'order', and 'dtype' when a buffer is "
                    "specified. Consider using the TField.fromarray() "
                    "constructor instead.")
        obj.order = \
            order or obj.properties.get("order", ts.TFIELD_DEFAULT_LAYOUT)
        obj.dtype = \
            dtype or obj.properties.get("dtype", ts.TFIELD_DEFAULT_FLOAT_TYPE)
        obj.name = obj.properties.get("name", name)
        obj.storage = obj.properties.get("storage", storage)
        objkwargs = obj.properties.get("kwargs", {})
        objkwargs.update(kwargs)
        obj.kwargs = objkwargs

        # Attach data array (respecting storage mode)
        if buffer is not None:
            obj.attach_storage(buffer, offset)
        else:
            obj.attach_default_storage()
            # obj.data[...] = 0  # memory overload, because full memmap is open

        # The interpolator depends on the data, so it is set after the buffer
        # is attached.
        obj.interpolator = interpolator

        return obj

    def __init__(self, extent, tensor_shape=None, order=None, dtype=None,
                buffer=None, offset=0, interpolator=None, name=None,
                storage=MEM, **kwargs):
        """
        Initialisation of TField.

        :param extent:
            Spatial extent of the tensor field. This can be specified by a
            sequence of integers describing the shape of the domain (for
            compact domains), an array of coordinates describing a non-compact
            domain, or a Domain instance (either compact or non-compact). In
            the former two cases a new Domain instance will be created, whereas
            in the last case no copies will be made, and the input Domain
            instance will serve as the domain of the new TField instance.
        :type extent: Union[tuple[int], list[int], Domain]
        :param tensor_shape:
            Tensor shape (not to be confused with the shape of the spatial
            domain).
        :type tensor_shape: Union[tuple[int], list[int], int]
        :param order:
            Buffer layout (C-contiguous). 'T' for tensor major (i.e. voxels are
            stored contiguously for each tensor element), 'V' for voxel major
            (i.e. tensor values are stored contiguously for each voxel).
        :type order: Union[TENSOR_MAJOR, VOXEL_MAJOR]
        :param dtype:
            Tensor data type.
        :type dtype: Union[str, np.dtype]
        :param buffer:
            Buffer interface for array data. If None, an in-memory array is
            created with zeros.
        :type buffer: Union[memoryview, np.ndarray]
        :param offset:
            Read offset from buffer.
        :type offset: int
        :param interpolator:
            Interpolator instance. If None, a new interpolator will be
            automatically created based on the type of the domain.
        :type interpolator: Union[Interpolator, NoneType]
        :param name:
            Name of the TField instance. If None, a name of the following
            format will be automatically generated: "TField_$memaddress".
        :type name: Union[str, NoneType]
        :param storage:
            If "hdd", the tensor field data will be stored on the HDD (in the
            temp folder of TIRL). If "mem", the tensor field data will be
            stored in memory. If there is not enough memory available, "hdd"
            mode will be automatically selected.
        :type storage: str
        :param kwargs:
            Additional keyword arguments for the new TField instance.
        :type kwargs: Any

        :returns: new TField instance
        :rtype: TField

        """
        super(TField, self).__init__()

    def __del__(self):
        """
        TField destructor.

        """
        # Close and delete binary storage file if it still persists
        try:
            fd = self._data.file_no
            fname = self._data.fname
        except Exception:
            pass
        else:
            self._remove_tmpfile(fd, fname)

    def __array__(self, *args):
        """
        Array interface of TField.

        """
        return self.data.__array__(*args)

    def __tfield__(self):
        """
        TField interface, which subclasses should implement for enhanced
        compatibility.

        :returns: equivalent TField object
        :rtype: TField

        """
        return self

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):

        default_order = VOXEL_MAJOR  # enables matrix-aware ufuncs!
        # Create a list of inputs converted to default-oriented ndarray
        args = []
        input_orders = []
        for input_ in inputs:
            if hasattr(input_, "__tfield__"):
                input_orders.append(input_.order)
                input_.order = default_order
                args.append(input_._data.data)
            else:
                args.append(input_)
                input_orders.append(None)

        # Create a list of outputs converted to default-oriented ndarray
        outputs = kwargs.pop("out", None)
        output_orders = []
        if outputs:
            out_args = []
            for output in outputs:
                if hasattr(inputs[-1], "__tfield__"):
                    output_orders.append(output.order)
                    output.order = default_order
                    out_args.append(output._data.data)
                else:
                    out_args.append(output)
                    output_orders.append(None)
            kwargs["out"] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        # Modify axis definition (not used to avoid NumPy users' confusion)
        # self._redirect_ufunc_axes_to_domain(kwargs)

        # If any of the inputs is a memory mapped array, redirect ufunc
        # operation to the dask implementation.
        input_arrays = tuple(arr for arr in inputs if hasattr(arr, "__array__"))
        if any(isinstance(getattr(arr, "__array__")(), np.memmap)
               for arr in input_arrays):
            raise NotImplementedError(
                "ufuncs for HDD objects are not supported in the current "
                "version of TIRL. Consider using objects from the TIRL "
                "Operator class.")
            import dask.array as da
            # TODO: Determine chunksize based on a memory limit
            # Note: the problem with this implementation is that dask does not
            # support memory mapping, i.e. the result of the operation is
            # always in memory, hence this will not protect from filling the
            # RAM.
            # At the moment this seems an efficient way of speeding up NumPy
            # ufuncs for large TFields, so the decision whether to use dask
            # should be made on whether the array is large enough to benefit
            # from parallel computation, not on the basis of where it is stored.
            # Running ufunc on HDD-arrays and restricting memory usage would
            # require an implementation of chunking here, which should also
            # take the output of the ufunc into account. This will hopefully be
            # done in a future release of TIRL. For the time being, TIRL
            # Operators are a potential substitute for these cases.
            args = tuple(da.from_array(arr)
                         if hasattr(arr, "__array__") else arr for arr in args)
            ufunc_signature = ufunc.signature or "(),()->()"
            results = da.apply_gufunc(getattr(ufunc, method), ufunc_signature,
                                      *args, **kwargs).compute()

        # If all inputs are in-memory, redirect ufunc operation to the
        # NumPy implementation (coded in C).
        else:
            try:
                results = self.data.__array_ufunc__(
                    ufunc, method, *args, **kwargs)
            finally:
                # Revert layout changes of the inputs
                for input_, input_order in zip(inputs, input_orders):
                    if hasattr(input_, "__tfield__"):
                        input_.order = input_order

        # Create a collection of the final results
        if ufunc.nout == 1:
            results = (results,)
        final_results = []
        for i, (result, output) in enumerate(zip(results, outputs)):
            if output is not None:
                if hasattr(output, "__tfield__"):
                    output._data.data[...] = result
                    output.order = output_orders[i]
                else:
                    output[...] = result
                final_results.append(output)
            else:
                context = (ufunc.__name__, method, default_order)
                tfout = self.__array_wrap__(result, context=context)
                # The order of the output should match that of the ufunc handler
                if hasattr(tfout, "__tfield__"):
                    tfout.order = self.order
                final_results.append(tfout)

        if results is NotImplemented:
            return NotImplemented

        return final_results[0] if len(final_results) == 1 else final_results

    def _redirect_ufunc_axes_to_domain(self, kwargs):
        """
        This method takes the axis argument of a ufunc that were given with
        respect to the spatial subdomain of the TField and adapts it to match
        the underlying full data array. The axis definition is modified in
        place.

        E.g. for a TField(tensor=(3,), domain=(2000, 3000)), axes specified as
        axis=(0, 1) would be converted to (1, 2), because the TField is
        tensor-major and has one tensor axis.

        :param kwargs: ufunc keyword arguments
        :type kwargs: Any

        """
        # Redirect ufunc operation to the spatial domain of the inputs by
        # adapting the axis parameter (if exists).
        axes = kwargs.get("axis", False)
        # If axes were not specified constrain ufunc to the spatial domain
        supported_axes = tuple(range(self.vdim))
        axes = supported_axes if axes is None else axes
        if axes:
            if hasattr(axes, "__iter__"):
                if any((-self.vdim > ax) or (ax >= self.vdim) for ax in axes):
                    raise ValueError("Only voxel axes {} are supported."
                                     .format(supported_axes))
                axes = tuple((ax % self.vdim) + self.tdim for ax in axes)
                if any(ax not in self.vaxes for ax in axes):
                    raise ValueError("Only voxel axes {} are supported."
                                     .format(supported_axes))
            elif isinstance(axes, (int, np.integer)):
                if (-self.vdim > axes) or (axes >= self.vdim):
                    raise ValueError("Only voxel axes {} are supported."
                                     .format(supported_axes))
                axes = (axes % self.vdim) + self.tdim
                if axes not in self.vaxes:
                    raise ValueError("Only voxel axes {} are supported."
                                     .format(supported_axes))
            else:
                raise ValueError("Expected integer for ufunc axis definintion, "
                                 "got {} instead.".format(type(axes)))
            kwargs.update({"axis": axes})

    def __repr__(self):
        if self.order == VOXEL_MAJOR:
            s = "{0}(domain={1}, tensor={2}, dtype={3}, {4})"
            arg1 = self.vshape
            arg2 = self.tshape
        else:
            s = "{0}(tensor={1}, domain={2}, dtype={3}, {4})"
            arg1 = self.tshape
            arg2 = self.vshape
        return s.format(self.__class__.__name__, arg1, arg2, self.dtype,
                        self.storage)

    def __slice_wrap__(self, ret, context=None):
        """
        Finishing step of all slicing operations on a TField. Subclasses SHOULD
        implement this method to ensure that the result of the slicing is
        properly type cast and has the correct attributes and methods.

        :param ret:
            TField object returned from the slicing operation.
        :type ret: TField
        :param context:
            Context information: (slicing context, slicing expression)
            - Slicing context: either "voxels" or "tensors", whichever
              subfield of the TField is being indexed.
            - Slicing expression: tuple/slice expression that defines the
              slicing.
        :type context: Any

        :returns: type cast slicing result object
        :rtype: TField

        """
        return ret

    def __array_wrap__(self, arr, context=None):
        """
        Finishing step after applying a NumPy ufunc to the TField.
        Subclasses SHOULD implement this method to provide appropriate type
        casting.

        :param arr:
            Result of the ufunc.
        :type arr: np.ndarray
        :param context:
            Ufunc context information: (ufunc_name, ufunc_method, input_order)
        :type context: Any

        :returns: type cast ufunc output object
        :rtype: TField

        """
        # Return ndarray if the domain shape change is evident (1st test)
        if arr.ndim < self.vdim:
            return arr

        # Has the voxel shape changed?
        vdim = self.vdim
        uname, umethod, arr_order = context

        if arr_order == VOXEL_MAJOR:
            voxel_intact = arr.shape[:vdim] == self.vshape
            new_tshape = arr.shape[vdim:]
        else:
            voxel_intact = arr.shape[-vdim:] == self.vshape
            new_tshape = arr.shape[:-vdim]

        # Return ndarray if voxel shape change has become evident (2nd test)
        if not voxel_intact:
            return arr

        # Return TField if the voxel shape has not changed
        new_extent = self.domain
        ip = self.interpolator.__class__
        name = self.name + f"_{uname}_{umethod}."
        kwargs = tu.rcopy(self.kwargs)
        kwargs = {k: v for (k, v) in kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        result = TField(extent=new_extent, tensor_shape=new_tshape,
                        order=arr_order, dtype=arr.dtype, buffer=arr,
                        offset=0, interpolator=ip, name=name,
                        storage=self.storage, **kwargs)
        result.interpolator.kwargs.update(self.interpolator.kwargs.copy())
        return result

    @classmethod
    def __operation_finalise__(cls, tfout, op=None, template=None):
        """
        This magic method is called by the Operator class when the output
        TField instance is not specified, and the Operator must therefore
        return a new instance. This method MAY be overloaded in subclasses to
        provide subclass-specific casting of the result of the operation.
        The value returned by this method will be returned to the caller of the
        operation as the result.

        :param tfout: TField output of the operation
        :type: tfout: TField
        :param op: operator
        :type op: Operator
        :param template: input object of the operation
        :type template: Any

        :returns: result of the operation
        :rtype: Any

        """
        return tfout

    def abs(self, *args, **kwargs):
        # Result is an object of identical type
        return np.abs(self, *args, **kwargs)

    # def argmin(self, *args, **kwargs):
    #     # Reducing operation, result may be a scalar, an ndarray, or type(self)
    #     ret = np.argmin(self.data, *args, **kwargs)
    #     return self.__array_wrap__(ret, context=("argmin", None, self.order))
    #
    # def argmax(self, *args, **kwargs):
    #     # Reducing operation, result may be a scalar, an ndarray, or type(self)
    #     ret = np.argmax(self.data, *args, **kwargs)
    #     return self.__array_wrap__(ret, context=("argmax", None, self.order))

    def min(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        kwargs.update({"axis": axis})
        return np.minimum.reduce(self, *args, **kwargs)

    def amin(self, *args, **kwargs):
        # Numpy congruency alias
        return self.min(*args, **kwargs)

    def max(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        kwargs.update({"axis": axis})
        return np.maximum.reduce(self, *args, **kwargs)

    def amax(self, *args, **kwargs):
        # Numpy congruency alias
        return self.max(*args, **kwargs)

    def mean(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        ret = np.core.fromnumeric._methods._mean(self, *args, **kwargs)
        return self.__array_wrap__(ret, context=("mean", None, self.order))
        #return np._methods._mean(self.data, *args, **kwargs)

    def median(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        ret = np.median(self.data, *args, **kwargs)
        return self.__array_wrap__(ret, context=("median", None, self.order))

    def sum(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or type(self)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        kwargs.update({"axis": axis})
        return np.add.reduce(self, *args, **kwargs)

    def astype(self, dtype):
        # Result is an object of identical type, with different dtype
        return self.__class__(self, dtype=dtype)

    def flat(self):
        # Result is a flatiterator object to the data array
        return self.data.flat

    def __getitem__(self, item):
        """ Indexing is deferred to the data array. """
        return self.data[item]

    def __setitem__(self, key, value):
        """ Indexing is deferred to the data array. """
        self.data[key] = value

    @classmethod
    def zeros_like(cls, tf):
        if isinstance(tf, np.ndarray):
            vshape = tf.shape
            tshape = ()
        elif isinstance(tf, TField):
            vshape = tf.vshape
            tshape = tf.tshape
        else:
            raise TypeError(f"Input type not understood: "
                            f"{tf.__class__.__name__}.")
        out = cls(extent=vshape, tensor_shape=tshape, order=tf.order,
                  dtype=tf.dtype, storage=tf.storage)
        out[...] = 0
        return out

    @classmethod
    def ones_like(cls, tf):
        if isinstance(tf, np.ndarray):
            vshape = tf.shape
            tshape = ()
        elif isinstance(tf, TField):
            vshape = tf.vshape
            tshape = tf.tshape
        else:
            raise TypeError(f"Input type not understood: "
                            f"{tf.__class__.__name__}.")
        out = cls(extent=vshape, tensor_shape=tshape, order=tf.order,
                  dtype=tf.dtype, storage=tf.storage)
        out[...] = 1
        return out

    def copy(self):
        """
        Copy constructor.

        """
        arr = self._data.copy().data    # this handles memmap implicitly
        # ipc = self.interpolator.__class__
        # ip = self.interpolator.copy()
        # interpolator = ipc(arr, ip.threads, ip.verbose, **ip.kwargs)
        # interpolator.tensor_axes = self.taxes
        interpolator = self.interpolator.copy()
        interpolator.values = arr
        interpolator.tensor_axes = self.taxes
        domain = self.domain.copy()
        kwargs = self.kwargs.copy()
        kwargs = {k: v for (k, v) in kwargs.items()
                  if k not in self.RESERVED_KWARGS}

        return TField(
            extent=domain, tensor_shape=self.tshape, order=self.order,
            dtype=self.dtype, buffer=arr, offset=0, interpolator=interpolator,
            name=self.name, storage=self.storage, **kwargs)

    @classmethod
    def _load(cls, dump):

        # Load data arrays
        data = dump.get("data")

        # Load properties
        props = dump.get("properties")
        # domain = Domain._load(props.get("extent"))
        domain = props.get("extent")
        tshape = props.get("tensor_shape")
        order = props.get("order")
        dtype = np.dtype(props.get("dtype"))
        name = props.get("name")
        storage = props.get("storage")

        # Load interpolator (preserving its type)
        interpolator = props.get("interpolator")
        # from pydoc import locate
        # ipc = locate(interpolator["type"])
        # interpolator = ipc._load(interpolator)

        # Additional keyword arguments
        kwargs = props.get("kwargs")
        kwargs = {k: v for (k, v) in kwargs.items()
                  if not k in cls.RESERVED_KWARGS}

        # Recreate TField
        obj = TField(extent=domain, tensor_shape=tshape, order=order,
                     dtype=dtype, buffer=data, offset=0,
                     interpolator=interpolator, name=name, storage=storage,
                     **kwargs)
        return obj

    def _dump(self):
        # Create TIRLObject dump
        objdump = super(TField, self)._dump()
        # Save the data
        objdump["data"] = self.data
        # Save the properties
        objdump["properties"] = {
            "extent": self.domain,#.dump(),  # calling dump, not _dump!
            "tensor_shape": self.tshape,
            "order": self.order,
            "dtype": np.dtype(self.dtype).str,
            "interpolator": self.interpolator,#.dump(),  # dump, not _dump!
            "name": self.name,
            "storage": self.storage,
            "kwargs": tu.rcopy(self.kwargs)
        }
        return objdump

    @property
    def data(self):
        """ This is a read-only proxy attribute to prevent accidental
        dereferencing and garbage collection of the instance buffer. """
        return self._data.data

    @property
    def dtype(self):
        return self.properties["dtype"]

    @dtype.setter
    def dtype(self, dt):
        try:
            dt = np.dtype(dt)
        except:
            raise TypeError(f"Unrecognised input for data type: "
                            f"{dt.__class__.__name__}")
        try:
            data = self.data
        except AttributeError:
            pass
        else:
            if self.storage == MEM:
                self._data = Buffer(data.astype(dt))
            else:
                self._data = Buffer(self._change_memmap_dtype(data, dt))
        finally:
            self.properties.update({"dtype": dt})

    @property
    def domain(self):
        """ Domain of the TField instance."""
        return self.properties["domain"]

    @domain.setter
    def domain(self, d):
        """
        The Domain attribute must be set with a Domain instance that matches
        the voxel shape of the TField instance. This way of changing the
        domain is for advanced users only. Please consider using the evaluate()
        method instead.

        """
        if isinstance(d, Domain):
            if self.domain.shape == d.shape:
                self.properties.update({"domain": d})
            else:
                raise AssertionError(
                    f"Cannot assign domain with shape {d.shape} to a "
                    f"{self.__class__.__name__} with voxel shape "
                    f"{self.vshape}. Consider using the evaluate() method "
                    f"instead.")
        else:
            raise TypeError("The domain attribute must be set with a Domain "
                            "instance.")

    @property
    def interpolator(self):
        """ Delegated interpolator object. """
        return self.properties.get("interpolator")

    @interpolator.setter
    def interpolator(self, ip):
        """
        Setter method for delegated interpolator object.

        :param ip:
            Interpolator class or instance. If None, the interpolator is chosen
            automatically to match the domain type (compact vs non-compact).
        :type ip: Union[Interpolator, NoneType]

        """
        # Create interpolator based on the Domain type (compact vs. non-compact)
        if ip is None:
            # Choose default interpolator according to the domain type
            from pydoc import locate
            if self.domain.iscompact:
                ip = locate(ts.DEFAULT_COMPACT_INTERPOLATOR)
            else:
                ip = locate(ts.DEFAULT_NONCOMPACT_INTERPOLATOR)

        # Create interpolator instance from class specification
        if issubclass(type(ip), type) and issubclass(ip, Interpolator):
            ip = ip(self, threads=self.tsize)
            ip.tensor_axes = self.taxes  # direct access for speed
            ip.threads = reduce(mul, self.tshape)  # direct access for speed
            ip.values = self.data

        # Adapt existing interpolator instance
        elif isinstance(ip, Interpolator):
            # If interpolator is given, set the source to the TField data
            ip.tensor_axes = self.taxes
            ip.values = self.data
            # if ip.tensor_axes == self.taxes:
            #     ip._values = self.data
            # else:
            #     raise ValueError(
            #         "The tensor axis specification of the interpolator {} "
            #         "does not match {} with tensor axis/axes {}"
            #             .format(ip.tensor_axes, self.__class__.__name__,
            #                     self.taxes))
        else:
            raise TypeError(f"Expected Interpolator type, "
                            f"got {ip.__class__.__name__} instead.")

        # Update interpolator
        self.properties.update({"interpolator": ip})

    @property
    def kwargs(self):
        """
        :return: further keyword arguments for TField
        :rtype: dict

        """
        return self.properties.get("kwargs", {})

    @kwargs.setter
    def kwargs(self, kw):
        if isinstance(kw, dict):
            self.properties.update({"kwargs": kw})
        else:
            raise TypeError("Kwargs must be specified with a dict.")

    @property
    def ndim(self):
        return self.data.ndim

    @property
    def nbytes(self):
        """ Total number of array elements. Read-only. """
        return self.data.nbytes

    @property
    def numel(self):
        return self.tsize * self.domain.numel

    @property
    def name(self):
        return self.properties.get("name", super().name)

    @name.setter
    def name(self, n):
        if isinstance(n, str):
            self.properties.update({"name": n})
        elif n is None:
            self.properties.update({"name": super().name})
        else:
            raise TypeError("Name must be a string or None.")

    @property
    def order(self):
        return self.properties["order"]

    @order.setter
    def order(self, o):
        o = str(o).upper()
        if o not in LAYOUTS:
            raise ValueError(f"Buffer layout order must be either "
                             f"'{TENSOR_MAJOR}' or '{VOXEL_MAJOR}'")
        # Only change data layout if it is different from the current layout
        # and there is data to work with.
        if hasattr(self, "data"):
            if o != self.order:
                # Don't delete the original Buffer (self._data), just replace
                # the ndarray pointer within the Buffer (self._data.data),
                # because a simple view casting of a memmap will refer to the
                # original memmap file, which gets deleted if the buffer is
                # replaced.
                self._data.data = self._swap_data_layout(rewrite=ts.SWAPDATA)
                self.properties.update({"order": o})
                # Update the source data of the interpolator
                self.interpolator.values = self.data
                # Update the tensor_axes property of the interpolator
                self.interpolator.tensor_axes = self.taxes
        else:
            self.properties.update({"order": o})

    @property
    def shape(self):
        if self.order == TENSOR_MAJOR:
            return tuple(self.tshape[:self.tdim]) + self.domain.shape
        elif self.order == VOXEL_MAJOR:
            return self.domain.shape + tuple(self.tshape[:self.tdim])

    @property
    def storage(self):
        return self.properties.get("storage", "")

    @storage.setter
    def storage(self, mode):
        # Don't do anything if the storage mode is not expected to change
        if mode == self.storage:
            return
        # Detect error in input
        if mode not in STORAGE_OPTIONS:
            raise ValueError(f"Unrecognised storage mode: {mode}. "
                             f"Please choose from {STORAGE_OPTIONS}")

        # If the user-set mode is MEM, check whether there is enough RAM
        # available.
        if mode == MEM:
            full_size = self.dtype.itemsize * self.numel  # byte
            if full_size >= ts.TFIELD_INSTANCE_MEMORY_LIMIT * (1024 ** 2):
                warnings.warn("Tensor field is larger than the instance memory "
                              "limit. MEM mode not supported.")
                mode = HDD

        # Change storage mode
        try:
            # Is there a storage attached to the current TField instance?
            data = self.data
        except AttributeError:
            self.properties.update({"storage": mode})
        else:
            self.properties.update({"storage": mode})
            self.attach_storage(data, 0)

    @property
    def tdim(self):
        """ Number of tensor dimensions. Read-only. """
        if self.tshape == (1,):
            return 0
        else:
            return len(self.tshape)

    @property
    def taxes(self):
        """ Tensor axes. Read-only. """
        if self.tshape == (1,):
            return ()
        else:
            if self.order == TENSOR_MAJOR:
                return tuple(range(self.tdim))
            elif self.order == VOXEL_MAJOR:
                return tuple(range(self.data.ndim - self.tdim, self.data.ndim))

    @property
    def tsize(self):
        return reduce(mul, self.tshape)

    @property
    def vdim(self):
        """ Number of voxel dimensions. Read-only. """
        return self.domain.ndim

    @property
    def vaxes(self):
        """ Voxel axes. Read-only. """
        if self.order == VOXEL_MAJOR:
            return tuple(range(0, self.vdim))
        else:
            return tuple(range(self.tdim, self.ndim))

    @property
    def vsize(self):
        return int(self.domain.numel)

    @property
    def vshape(self):
        """ Voxel shape. Read-only. """
        return self.domain.shape

    def _swap_data_layout(self, rewrite=ts.SWAPDATA):
        """
        Swap data layout from tensor-major to voxel-major and vice versa. Note
        that this method only changes the actual layout of the data for
        in-memory instances - swapping memory-mapped objects is not currently
        supported.

        """
        # Determine new axis order
        if self.order == TENSOR_MAJOR:
            tensoraxes = tuple(ax + self.vdim for ax in self.taxes)
            voxelaxes = tuple(ax - self.tdim for ax in self.vaxes)
            axisorder = tensoraxes + voxelaxes

        else:
            voxelaxes = tuple(ax + self.tdim for ax in self.vaxes)
            tensoraxes = tuple(ax - self.vdim for ax in self.taxes)
            axisorder = voxelaxes + tensoraxes

        # Swap data dimensions accordingly (respecting storage mode)
        if self.storage == MEM:
            buffer = np.moveaxis(self.data, range(self.ndim), axisorder)
            if rewrite:
                return np.ascontiguousarray(buffer)
            else:
                return buffer
        else:
            if rewrite:
                # TODO: Add support for rechunking memory-mapped data
                warnings.warn(
                    "Swapping the order of a TField instance does not change "
                    "the layout of memory-mapped data. Performance gain might "
                    "be negligible.")
            return np.moveaxis(self.data, range(self.ndim), axisorder)

    @property
    def tensors(self):
        return TFieldIndexer(self, axes=self.vaxes, context="tensors")

    @property
    def voxels(self):
        return TFieldIndexer(self, axes=self.taxes, context="voxels")

    @property
    def tshape(self):
        """ Tensor shape. Read-only. """
        return tuple(self.properties["tensor_shape"])

    @staticmethod
    def __validate_input(extent, tensor_shape, order, dtype, buffer, offset):
        try:
            # Extent (domain)
            if hasattr(extent, "__tfield__"):
                pass
            elif hasattr(extent, "__array__"):
                if len(extent.shape) != 2:
                    raise AssertionError(
                        f"Expected (n_points, n_coordinates) shape for "
                        f"coordinate array, got {extent.shape}")
            elif hasattr(extent, "__iter__"):
                if not all(isinstance(dim, (int, np.integer)) and dim > 0
                           for dim in extent):
                    raise ValueError(
                        "The shape of the TField domain must be specified with "
                        "positive integers.")
            elif not isinstance(extent, Domain):
                raise TypeError(f"Expected Domain type for domain "
                                f"specification, got {type(extent)}.")
            # Tensor shape
            if tensor_shape is None:
                pass
            elif not hasattr(tensor_shape, "__iter__"):
                if isinstance(tensor_shape, (int, np.integer)):
                    if tensor_shape <= 0:
                        raise ValueError("Vector size must be positive.")
                else:
                    raise ValueError("Vector dimension must be integer.")
            else:
                noninteger = any(not isinstance(ax, (int, np.integer))
                                 for ax in tensor_shape)
                nonpositive = any(ax <= 0 for ax in tensor_shape)
                if noninteger or nonpositive:
                    raise ValueError(
                        "Tensor dimensions must be positive integers.")
            # Order
            if order is not None and (str(order).upper() not in LAYOUTS):
                raise ValueError(f"Buffer layout order must be either "
                                 f"'{TENSOR_MAJOR}' or '{VOXEL_MAJOR}'")
            # Data type
            dtype = np.dtype(dtype) if type(dtype) is str else dtype
            data_types = builtins.type, np.generic, np.dtype
            if dtype is not None and (not isinstance(dtype, data_types)):
                raise TypeError("Dtype input must be a valid data type.")
            # Buffer
            buffer_type = (memoryview, np.memmap, np.ndarray, Buffer)
            if buffer is None:
                # Create buffer at initialisation
                pass
            elif not isinstance(buffer, buffer_type):
                raise TypeError(
                    "Buffer must be a TIRL Buffer, a NumPy array or a "
                    "memoryview object.")
            # Offset
            if not isinstance(offset, (int, np.integer)) or (offset < 0):
                raise ValueError(
                    "Buffer offset must be a non-negative integer.")
        except (AssertionError, ValueError, TypeError) as exc:
            raise AssertionError("Wrong input to TField constructor.") from exc

    def attach_storage(self, buffer, offset=0):
        # Attach existing Buffer instance
        if isinstance(buffer, Buffer):
            arr = np.ndarray(
                shape=self.shape, dtype=self.dtype, buffer=buffer.data.data,
                offset=offset, order="C")
            self._data = Buffer(arr, fname=buffer.fname, file_no=buffer.file_no)
            return
        # Create array from buffer
        if not isinstance(buffer, np.ndarray):
            buffer = np.ndarray(shape=self.shape, dtype=self.dtype,
                                buffer=buffer, offset=offset)
        # Convert to in-memory array if necessary
        if isinstance(buffer, np.memmap) and (self.storage == MEM):
            if buffer.size != self.numel:
                raise ValueError("Buffer size does not match the shape of "
                                 "the {}.".format(self.__class__.__name__))
            self._data = Buffer(np.asarray(buffer, dtype=self.dtype))
        # Convert to memory-mapped hard disk image if necessary
        elif not isinstance(buffer, np.memmap) and (self.storage == HDD):
            if reduce(mul, buffer.shape) != self.numel:
                raise ValueError("Buffer size does not match the shape of {}."
                                 .format(self.__class__.__name__))
            fd, fname = tempfile.mkstemp(dir=ts.TWD, prefix="TField_")
            m = np.memmap(fname, mode="r+", shape=buffer.shape,
                          dtype=self.dtype, order="C")
            m[...] = buffer
            m.flush()
            self._data = Buffer(m, fname=fname, file_no=fd)
        # Attach array
        else:
            if buffer.dtype != self.dtype:
                if isinstance(buffer, np.memmap):
                    buffer = self._change_memmap_dtype(buffer, self.dtype)
                else:
                    buffer = np.asarray(buffer, dtype=self.dtype)
            if buffer.size != self.numel:
                raise ValueError("Buffer size does not match the shape of the "
                                 "{}.".format(self.__class__.__name__))
            buffer = buffer.reshape(self.shape)
            self._data = Buffer(buffer, fname=getattr(buffer, "filename", None))

    def attach_default_storage(self):
        """
        Creates an empty (uninitialised) array either in memory or on the HDD
        and attaches it to the buffer of the current TField instance.

        """
        if self.storage == MEM:
            ctype = np.ctypeslib.as_ctypes_type(self.dtype)
            mp.Array(ctype, self.shape)
            buffer = np.zeros(self.shape, self.dtype)
            self._data = Buffer(buffer)
        else:
            # Create new binary storage on hard disk
            fd, fname = tempfile.mkstemp(prefix="TField_", dir=ts.TWD)
            # Open file, pre-occupy space and do not close the file
            with open(fname, "w") as f:
                nbytes = self.numel * np.dtype(self.dtype).itemsize
                f.seek(nbytes - 1)
                f.write('\n')
            m = np.memmap(fname, mode="r+", shape=self.shape,
                          dtype=self.dtype, offset=0, order="C")
            self._data = Buffer(m, fname=fname, file_no=fd)

    @staticmethod
    def _change_memmap_dtype(m, dt):
        memlimit = (ts.TFIELD_CHUNKSIZE * (1024 ** 2))
        return tu.change_memmap_dtype(m, dt, memlimit)

    @staticmethod
    def _remove_tmpfile(fd, fname):
        if (fd is not None) and os.path.isfile(fname):
            try:
                os.close(fd)
            except Exception:
                pass
            try:
                os.remove(fname)
            except Exception:
                warnings.warn("Temporary file could not be removed from {}."
                              .format(fname))

    def evaluate(self, domain, tensors=None,
                 interpolation_mode=ts.TFIELD_INTERPOLATION_MODE):
        """
        Evaluate tensor field on a specified domain. The original TField
        instance is returned if the target domain is the same as the source
        domain. If the domain instances are different but equal, a copy of the
        original TField instance is returned.

        :param domain:
            Target domain. Tensor values will be evaluated at the points of
            this domain, specified by their physical coordinates.
        :type domain: Domain
        :param tensors:
            Defines what happens to the tensors after evaluation at the target
            domain.
            1. None (default): tensor components are interpolated, and no
               futher operations take place. This is recommended for
               transformation-invariant tensor fields, such as colour space
               vectors.
            2. "ppd": Preservation of principal directions.
            3. "FS"
        :type tensors: str or None
        :param interpolation_mode:
            Interpolation mode.

            - If "inv+regular", domain transformations are inverted to
              transform physical coordinates to the regular grid that the field
              is defined on. Use this option if inverting non-linear
              transformations is faster or more accurate than scattered
              interpolation by inverse distance weighted nearest neighbours.

            - If "scattered", field estimates are calculated directly by
              interpolation of the field in physical space. Use this option if
              the interpolation of scattered data by inverse distance weighted
              nearest neighbours is faster or more accurate than inverting
              non-linear transformations.
        :type interpolation_mode: Union["inv+regular", "scattered"]

        :returns:
            New TField instance with field estimates evaluated on the specified
            domain.
        :rtype: TField

        """
        # Save time when the evaluation is trivial
        if domain is self.domain:
            return self
        elif domain == self.domain:
            other = self.copy()
            other.domain = domain
            other.domain = domain
            return other

        # Perform the calculations when the evaluation is non-trivial
        if interpolation_mode == "inv+regular":
            return self._evaluate_by_inversion(domain)
        elif interpolation_mode == "scattered":
            return self._evaluate_by_idwnn(domain)
        else:
            raise ValueError(
                "Unrecognised interpolation mode: {}. Please select either "
                "'inv+regular' or 'scattered'".format(interpolation_mode))

    def reduce_tensors(self, tensor_op=None, copy=False):
        """
        Flattens the tensors of the field, yielding a TField with scalar voxel
        values. Unless copy=True, the operation is in-place, hence cannot be
        undone (default behaviour).

        :param tensor_op:
            Reduction operation. If None, scalar values are calculated by
            taking the L2-norm of the tensor values.
        :type tensor_op: TensorOperator
        :param copy:
            If True, the result will be returned as a copy of the original
            image. If False, tensor values will be modified in place.
        :type copy: bool

        :returns:
            If copy is True, a TImage with scalar voxel values is returned.
        :rtype: Union[None, TImage]

        """
        # Do not reduce tensors if the tensor dimension is already 0.
        if not self.tdim:
            if copy:
                return self.copy()
            else:
                return

        from tirl.operations.operations import Operator

        # Reduce tensors if the tensor dimension is not 0.
        if tensor_op is None:
            from tirl.operations.tensor import TensorOperator
            taxes = tuple(range(1, self.tdim + 1))
            tensor_op = \
                TensorOperator(np.linalg.norm, name="l2-norm", axis=taxes)
        elif not isinstance(tensor_op, Operator):
            raise TypeError(
                "The reduction operation must be a TIRL Operator instance.")
        ret = tensor_op(self)
        if copy:
            return ret
        else:
            self._data = ret._data
            self.properties.update({"tensor_shape": ret.tshape})
            # Update the interpolator as well!
            self.interpolator.tensor_axes = ()
            self.interpolator.values = self.data

    def _evaluate_by_inversion(self, target_domain):
        """
        Evaluate TField at the points of the target domain (physical space) by
        inverse-transforming the physical coordinates of the target domain to
        the voxel-space of the tensor field, and interpolating at these voxel
        coordinates. All transformations of the tensor field domain must be
        invertible.

        :param target_domain: target domain
        :type target_domain: Domain

        :returns: New TField instance with interpolated field values.
        :rtype: TField

        """
        # Obtain coordinates for interpolation
        pcoords = target_domain.get_physical_coordinates()
        vcoords = self.domain.map_physical_coordinates(pcoords)
        del pcoords

        # Perform interpolation and cast output
        # (storage mode is handled implicitly by the interpolator)
        ret = self.interpolator(vcoords, input_array=self.data)
        if self.tdim > 0:
            tshape = self.tshape
        else:
            tshape = ()
        if self.order == TENSOR_MAJOR:
            ret = ret.reshape((*tshape, *target_domain.shape))
        else:
            ret = ret.reshape((*target_domain.shape, *tshape))

        # Choose interpolator based on the target domain
        if target_domain.iscompact == self.domain.iscompact:
            ip = self.interpolator.copy()
        elif target_domain.iscompact:
            ip = locate(ts.DEFAULT_COMPACT_INTERPOLATOR)
        else:
            ip = locate(ts.DEFAULT_NONCOMPACT_INTERPOLATOR)

        # Return new TField instance
        name = self.name + "_eval_{}".format(target_domain.name)
        kwargs = {k: v for (k, v) in self.kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        ret = TField(extent=target_domain, tensor_shape=self.tshape,
                     order=self.order, dtype=self.dtype, buffer=ret, offset=0,
                     interpolator=ip, name=name, **kwargs)
        return ret

    def _evaluate_by_idwnn(self, coords):
        """
        Evaluate TField at the points of the target domain (physical space)
        using direct interpolation by inverse distance weighted nearest
        neighbours.

        :param coords: coordinates of the target domain
        :type coords: Union[np.ndarray, np.memmap]

        :return:

        """
        raise NotImplementedError(
            "Direct interpolation by inverse distance weighted nearest "
            "neighbours is not supported in the current version.")

    def preview(self, mode="composite", **kwargs):
        """
        Preview tensor field.

        :param mode: Preview mode.
            "rgb": interpret vector components as red, green and blue image
                   channels
            "composite": separate grayscale image for each tensor component
            "hsv": colour-coded representation of field strength and direction
            "quiver": 2D/3D arrow representation of field strength and direction
        :type mode: Union["rgb", "composite", "quiver"]
        :param kwargs:
            Additional keyword arguments for display function.
        :type kwargs: Any

        """
        from tirl import tirlvision

        # Check the compatibility of the TField with the selected mode
        mode = str(mode).lower()
        if (mode == "rgb") and (self.tshape != (3,)):
            raise ValueError("RGB mode is not compatible with tensor shape {}."
                             .format(self.tshape))
        elif (mode in ("hsv", "quiver")) and (self.tshape not in ((2,), (3,))):
            raise ValueError("{} mode is not compatible with tensor shape {}."
                             .format(mode.upper(), self.tshape))
        elif (mode == "quiver") and (self.vdim not in (2, 3)):
            raise ValueError("QUIVER mode is not compatible with the number of "
                             "spatial dimensions of the TField domain ({})."
                             .format(self.vdim))

        # Call TIRLVision with the selected mode
        if mode == "rgb":
            tirlvision.call("tfield", "preview_rgb", self, **kwargs)
        elif mode == "hsv":
            tirlvision.call("tfield", "preview_hsv", self, **kwargs)
        elif mode == "composite":
            tirlvision.call("tfield", "preview_composite", self, **kwargs)
        elif mode == "quiver":
            tirlvision.call("tfield", "preview_quiver", self, **kwargs)
        else:
            raise ValueError("Unrecognised preview mode: {}".format(mode))

    @staticmethod
    def _domains2scale(old_domain, new_domain):
        """
        Calculate image scaling factors by dividing the shape of the new
        domain by the shape of the old domain.

        :param old_domain: old (high-resolution) domain
        :type old_domain: Domain
        :param new_domain: new (low-resolution) domain
        :type new_domain: Domain

        :returns: scaling factors for each spatial dimensions
        :rtype: tuple[float]

        """
        for domain in (old_domain, new_domain):
            if not isinstance(domain, Domain):
                raise TypeError("Expected Domain instance, got {}."
                                .format(domain.__class__.__name__))
        if old_domain.ndim != new_domain.ndim:
            raise AssertionError(
                f"The specified domains are incompatible with dimensions "
                f"{old_domain.ndim} and {new_domain.ndim}.")
        sc = tuple(n / o for n, o in zip(new_domain.shape, old_domain.shape))
        return sc

    # noinspection SpellCheckingInspection
    def resample(self, target, *scales, fov=None, update_chain=True,
                 presmooth=ts.TFIELD_PRESMOOTH):
        """
        # TODO: Update this docstring based on TImage
        Changes the resolution of the TField in place by resampling the data on
        a different domain. There are two ways to set the grid spacing:
            1. Specifying another compact domain. The shape ratio of the
               current domain and the old one determines the amount of
               down/upscaling. Domain transformations are discarded in the
               process, only the domain shapes are considered. This method has
               the advantage that the output shape is defined explicitly,
               avoiding rounding errors.
            2. Specifying down/upscaling factors (below and above 1,
               respectively) for each dimension of the TField, or one scaling
               factor for all dimensions. The main advantage of this method is
               its simplicity, but may result in slightly different shapes than
               expected due to rounding. Definitions of scale are relative to
               the current domain.

        :param target:
            First argument, that can either be the target Domain object,
            or the global scaling factor, or the scaling factor for the 0-th
            voxel dimension of the TField.
        :type target: Union[Domain, int, float, np.integer, np.floating]
        :param scales:
            Additional arguments that specify the scale factor for consecutive
            voxel dimensions of the TField. If more than one scaling factor is
            specified, the number of scaling factors must match the number of
            spatial dimensions of the TField.
        :param fov:
            Field of view, defined as a 2-tuple: the voxel coordinates of the
            most proximal and the most distant point from the origin of the
            voxel grid. If None, the FOV spans the whole field.
        :type fov: Union[tuple, NoneType]

        :notes:
            If the TField does not have high-resolution field data, the
            requested scaling is defined relative to the current domain, and
            data on the current domain will be set as the high-resolution
            reference for further resampling operations, until it is released
            by the 'reset_scale' method.

        """
        # Define field-of-view
        fovdef = fov
        if fov is None:
            fov = self.vshape
        else:
            fov = tuple(np.subtract(fov[1], fov[0]).tolist())

        # Determine and verify scaling factors
        if isinstance(target, Domain):
            if scales:
                raise AssertionError(
                    "If resampling is defined by a target domain, further "
                    "scaling factors must not be specified.")
            new_shape = target.shape
        else:
            relative_scales = (target,) + tuple(scales) if scales \
                else (target,) * len(self.vaxes)
            if len(relative_scales) != len(self.vaxes):
                raise AssertionError(
                    f"The scaling factor specification ({relative_scales}) is "
                    f"incompatible with the {self.__class__.__name__} that has "
                    f"voxel shape {self.vshape}.")
            new_shape = tuple(
                int(round(f * d)) for f, d in zip(relative_scales, fov))

        if fov == new_shape == self.vshape:
            return self
        else:
            relative_scales = np.divide(new_shape, fov)

        # Create target domain with appropriate scale and translation offset
        # transformations to compensate for the sampling difference. This will
        # ensure that the physical coordinates remain unchanged relative to the
        # original field.
        offset = self.domain.offset
        if fovdef and not np.allclose(fovdef[0], 0):
            trans_offset = TxTranslation(*np.asarray(fovdef[0]))
            offset = [trans_offset] + offset
        scale_offset = TxScale(*tuple(1. / s for s in relative_scales))
        offset = [scale_offset] + offset

        # Purge domain kwargs
        ndkwargs = {k: v for (k, v) in self.domain.kwargs.items()
                    if k not in self.domain.RESERVED_KWARGS}
        new_domain = Domain(
            new_shape, offset=offset, storage=self.domain.storage,
            instance_mem_limit=self.domain.instance_mem_limit,
            n_threads=self.domain.threads, **ndkwargs)

        # Convert input data to a TField instance without transformations
        # Transformations are removed to confine the evaluation to the voxels.
        field = TField.fromarray(
            self.data, self.taxes, copy=False, domain=self.domain[:0],
            order=self.order, dtype=self.dtype,
            interpolator=self.interpolator.__class__, storage=self.storage,
            **self.kwargs)

        # Pre-smoothing creates a proper Gaussian support for each point of the
        # new domain in the old domain, so in theory all original information is
        # represented in the downsampled data.
        if presmooth:
            coeff = 1. / (2. * np.sqrt(2 * np.log(2)))
            t_sigmas = (0,) * len(self.taxes)
            v_sigmas = \
                tuple(1 / s * coeff if s < 1 else 0 for s in relative_scales)
            sigmas = t_sigmas + v_sigmas  # operator uses TENSOR_MAJOR order
            kernelsize = ts.TFIELD_PRESMOOTH_KERNELSIZE_NSIGMA
            neighbourhood = max(ceil(kernelsize * max(sigmas)), 1)
            smop = SpatialOperator(
                gaussian_filter, radius=neighbourhood, name="smooth",
                sigma=sigmas, truncate=kernelsize)
            field = smop(field)

        # Resample field data on a new voxel domain (no transformations)
        field = field.evaluate(new_domain)
        assert isinstance(field, TField)

        # Restore transformations
        new_domain.copy_transformations(self.domain, links=update_chain)

        assert field.domain is new_domain
        return field


# noinspection SpellCheckingInspection
class TFieldIndexer(object):

    def __init__(self, tfield, axes, context=None):
        if isinstance(tfield, TField):
            self.tfield = tfield
        else:
            raise TypeError("TFieldIndexer: Invalid input for TField.")

        # Verify axes input
        if not hasattr(axes, "__iter__"):
            if not isinstance(axes, (int, np.integer)):
                raise TypeError("TFieldIndexer: Invalid axis specification.")
            else:
                if axes < self.tfield.ndim:
                    axes = (axes,)
        else:
            if not all(isinstance(ax, (int, np.integer)) for ax in axes):
                raise IndexError("TFieldIndexer: Invalid axis specification.")
            else:
                if all(ax < self.tfield.ndim for ax in axes):
                    axes = tuple(axes)
                else:
                    raise ValueError("TFieldIndexer: Axis index out of bounds.")
        # Handle negative input
        axes = tuple(ax % self.tfield.ndim for ax in axes)

        # Set visible and hidden axes
        self.visible = np.asarray(tuple(filter(lambda ax: ax not in axes,
                                    range(self.tfield.ndim))))
        self.hidden = np.asarray(axes)

        # Initialise alternative slicer that will replace the user input
        self.slicer = np.asarray([slice(None)] * self.tfield.ndim)

        # Specify slicing context
        self.context = context

    def __getitem__(self, item):
        # Return the original object if no axis can be indexed.
        if self.visible.size == 0:
            if item == 0:
                return self.tfield
            else:
                raise IndexError("Indexing scalar values is undefined for "
                                 "indices other than 0.")
        # Consider other cases

        # Multi-indexing by a tuple
        if isinstance(item, tuple):
            for i in range(self.slicer.flat[self.visible].size):
                # self.slicer.flat[self.visible] = np.asarray(item)
                if hasattr(item[i], "__iter__"):
                    self.slicer[self.visible[i]] = np.asarray(item[i])
                else:
                    self.slicer[self.visible[i]] = item[i]

        # Subarray indexing: fancy indexing using boolean subarray
        elif isinstance(item, np.ndarray) \
                and np.issubdtype(item.dtype, np.bool_):
            inshape = np.asarray(self.tfield.shape)[self.visible]
            if self.hidden.size > 0:
                outshape = np.asarray(self.tfield.shape)[self.hidden]
            else:
                outshape = (1,)
            if item.size == np.prod(inshape):
                if self.visible[0] == 0:
                    flatshape = (-1, *outshape)
                    ret = self.tfield.data.reshape(flatshape)[item.ravel()]
                else:
                    flatshape = (*outshape, -1)
                    ret = self.tfield.data.reshape(flatshape)[..., item.ravel()]
                context = (self.context, item)
                return self.__finish__(ret, item, context=context)

        # Fancy indexing by integers
        elif isinstance(item, np.ndarray) \
                and np.issubdtype(item.dtype, np.integer):
            self.slicer[self.visible[0]] = item

        elif hasattr(item, "__tfield__"):
            mask = item.__array__() != 0
            data = np.moveaxis(
                self.tfield.data, self.visible, range(len(self.visible)))
            data = data.reshape(
                (-1, *np.asarray(self.tfield.shape)[self.hidden]))
            return data[mask.ravel(), :]  # ndarray

        # Single-axis indexing
        else:
            if self.visible.size > 0:
                self.slicer[self.visible[0]] = item

        # Execute alternative slicing and return values in correct format
        ret = self.tfield.data[tuple(self.slicer)]
        context = (self.context, item)
        try:
            ret = self.__finish__(ret, item, context=context)
        except Exception:
            pass
        return ret

    def __setitem__(self, key, value):
        # Raise IndexError if scalars are being indexed.
        if self.visible.size == 0:
            if key != 0:
                raise IndexError("Indexing scalar values is undefined for "
                                 "indices other than 0.")

        # Consider other cases.

        # Multi-indexing by a tuple
        if isinstance(key, tuple):
            self.slicer.flat[self.visible] = np.asarray(key)

        # Subarray indexing: fancy indexing using boolean subarray
        elif isinstance(key, np.ndarray) \
                and np.issubdtype(key.dtype, np.bool_):
            inshape = np.asarray(self.tfield.shape)[self.visible]
            outshape = np.asarray(self.tfield.shape)[self.hidden]
            if key.size == np.prod(inshape):
                if self.visible[0] == 0:
                    flatshape = (-1, *outshape)
                    self.tfield.data.reshape(flatshape)[key.ravel()] = value
                else:
                    flatshape = (*outshape, -1)
                    self.tfield.data.reshape(flatshape)[..., key.ravel()] \
                        = value

        # Single-axis indexing
        else:
            if self.visible.size > 0:
                self.slicer[self.visible[0]] = key

        # If the value itself has tensor axes, match the order with the target
        # of the assignment operation.
        if hasattr(value, "__tfield__"):
            value_order = value.order
            value.order = self.tfield.order

        # Execute alternative slicing and assign values
        self.tfield.data[tuple(self.slicer)] = value

        # Revert the order of the value (if relevant)
        if hasattr(value, "__tfield__"):
            value.order = value_order

    def __finish__(self, arr, item, context=None):
        """
        Inspects the result of slicing, and returns the result as a specialised
        TIRL object (TField and subclasses) if possible, otherwise returns the
        result as an ndarray.

        :param arr: result of slicing
        :type arr: np.ndarray
        :param item: slicing specification
        :type item: Union[tuple, slice, int, np.integer, Ellipse]
        :param context: context information (for post-hoc type casting)
        :type context: Any

        :returns: result of slicing
        :rtype: Union[TField, np.ndarray, np.memmap]

        """
        if not isinstance(item, tuple):
            item = (item,)
        offset = []
        discontinuous = False
        inversion = False
        reducing = []
        for i, sl in enumerate(item):
            # If the current slice is along a tensor axis...
            if self.visible[i] in self.tfield.taxes:
                if isinstance(sl, (int, np.integer)):
                    reducing.append(self.visible[i])
                elif isinstance(sl, slice):
                    start, stop, step = \
                        sl.indices(self.tfield.shape[self.visible[i]])
                    if stop == start:
                        raise ValueError("0-slice is not supported.")
                    # if stop - start == 1:
                    #     reducing.append(self.visible[i])

            # If the current slice is along a voxel axis...
            elif self.visible[i] in self.tfield.vaxes:
                if isinstance(sl, (int, np.integer)):
                    offset.append(sl)
                    reducing.append(self.visible[i])
                elif isinstance(sl, slice):
                    start, stop, step = \
                        sl.indices(self.tfield.shape[self.visible[i]])
                    if step == -1:
                        inversion = True
                        break
                    elif step != 1:
                        discontinuous = True
                        break
                    else:
                        offset.append(start)
                        if stop == start:
                            raise ValueError("0-slice is not supported.")
                        # if stop - start == 1:
                        #     reducing.append(self.visible[i])
                elif isinstance(sl, type(Ellipsis)):
                    offset.append(Ellipsis)
                else:
                    discontinuous = True
                    break

        # print("Inversion:", inversion)
        # print("Discontinuous:", discontinuous)
        # print("Offset:", offset)
        # print("Reducing:", reducing)

        if inversion:
            raise IndexError("TField axis inversion by slicing is not "
                             "supported in the current version.")

        if discontinuous:
            return arr

        else:
            # Replace Ellipsis in offset
            try:
                pos = offset.index(Ellipsis)
            except ValueError:
                pos = len(offset)
                n = self.tfield.vdim - len(offset)
            else:
                offset.pop(pos)
                n = self.tfield.vdim - len(offset) + 1

            # Obtain offset for unrepresented spatial dimensions
            offset = offset[:pos] + [0] * n + offset[pos:]
            # print("Updated offset:", offset)

            # Infer remaining tensor axes and voxel axes
            new_axes = tuple(ax for ax in range(self.tfield.ndim)
                             if ax not in reducing)
            new_taxes = tuple(i for i, ax in enumerate(new_axes)
                              if ax in self.tfield.taxes)
            # print("New tensor axes:", new_taxes)
            new_tshape = tuple(arr.shape[ax] for ax in new_taxes)
            new_vaxes = tuple(i for i, ax in enumerate(new_axes)
                              if ax in self.tfield.vaxes)
            new_vshape = tuple(arr.shape[ax] for ax in new_vaxes)

            # If the spatial domain was reduced to a scalar, keep it as a
            # singleton dimension.
            if not new_vshape:
                new_vshape = (1,)
                if self.tfield.order == TENSOR_MAJOR:
                    arr = arr[..., np.newaxis]
                    new_vaxes = (self.tfield.tdim,)
                    new_taxes = tuple(range(self.tfield.tdim))
                else:
                    arr = arr[np.newaxis, ...]
                    new_vaxes = (0,)
                    new_taxes = tuple(range(1, self.tfield.tdim + 1))

            # Create compensatory translation if the offset is non-zero.
            od = self.tfield.domain
            otxs = list(od.offset)  # note: no copy iterator!
            if any(val != 0 for val in offset):
                otxs = [TxTranslation(*offset)] + otxs

            # Create compensatory embedding if the slicing was reducing.
            # Note that the TxEmbed transformation must precede the
            # translation offsets for the latter to be effective.
            dim_gap = od.ndim - len(new_vshape)
            if dim_gap:
                otxs = [TxEmbed(len(new_vshape), od.ndim)] + otxs

            # Create new Domain with the original transformations
            # (no copy iterator is used)
            name = od.name + "_sliced"
            kwargs = {k: v for (k, v) in od.kwargs.copy().items()
                      if k not in od.RESERVED_KWARGS}
            new_domain = Domain(
                new_vshape, *od.chain, offset=otxs, name=name,
                storage=od.storage, instance_mem_limit=od.instance_mem_limit,
                n_threads=od.threads, **kwargs)

            # Create new interpolator
            # ipc = self.tfield.interpolator.__class__
            # ip = self.tfield.interpolator.copy()
            # interpolator = ipc(arr, ip.threads, ip.verbose, **ip.kwargs)
            # interpolator.tensor_axes = new_taxes
            interpolator = self.tfield.interpolator.copy()
            interpolator.values = arr
            interpolator.tensor_axes = new_taxes

            # Create new TField instance
            kwargs = self.tfield.kwargs.copy()
            kwargs = {k: v for (k, v) in kwargs.items()
                      if k not in self.tfield.RESERVED_KWARGS}
            name = self.tfield.name + "_slice"
            ret = TField(extent=new_domain, tensor_shape=new_tshape,
                         order=self.tfield.order, dtype=self.tfield.dtype,
                         buffer=arr, offset=0, interpolator=interpolator,
                         name=name, **kwargs)
            # Allow post-hoc type casting by caller object
            if hasattr(self.tfield, "__slice_wrap__"):
                ret = self.tfield.__slice_wrap__(ret, context=context)
            return ret

    def _axes_prepare(self, axis):
        if axis:
            axis = (axis,) if not hasattr(axis, "__iter__") else axis
            n_axes = self.hidden.size
            offset = self.hidden[0]
            axes = []
            for ax in axis:
                assert ax < n_axes or ax >= -n_axes, \
                    "Axis index out of range (0-{}).".format(n_axes - 1)
                ax = offset + ax % n_axes if ax < 0 else offset + ax
                axes.append(ax)
            return tuple(axes)
        else:
            return tuple(self.hidden)

    def abs(self, *args, **kwargs):
        # Result is an object of identical type
        return self.tfield.abs(*args, **kwargs)

    def argmin(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        res = []
        for ax in axes:
            kwargs.update({"axis": ax})
            res.append(self.tfield.argmin(*args, **kwargs))
        context = ("argmin", self.context, self.tfield.order)
        return self.tfield.__array_wrap__(res, context=context)

    def argmax(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        res = []
        for ax in axes:
            kwargs.update({"axis": ax})
            res.append(self.tfield.argmax(*args, **kwargs))
        context = ("argmax", self.context, self.tfield.order)
        return self.tfield.__array_wrap__(res, context=context)

    def min(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray, or
        # type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.min(*args, **kwargs)

    def amin(self, *args, **kwargs):
        return self.min(*args, **kwargs)

    def max(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.max(*args, **kwargs)

    def amax(self, *args, **kwargs):
        return self.max(*args, **kwargs)

    def mean(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.mean(*args, **kwargs)

    def median(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.median(*args, **kwargs)

    def sum(self, *args, **kwargs):
        # Reducing operation, result may be a scalar, an ndarray,
        # or type(self.tfield)
        axis = args.pop(0) if args else kwargs.get("axis", None)
        axes = self._axes_prepare(axis)
        kwargs.update({"axis": axes})
        return self.tfield.sum(*args, **kwargs)

    def astype(self, dtype):
        # Result is self.tfield, with different dtype
        ret = self.tfield.astype(dtype)
        if self.context == "tensors":
            ret.order = TENSOR_MAJOR
        elif self.context == "voxels":
            ret.order = VOXEL_MAJOR
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")
        return ret

    @property
    def shape(self):
        """ Proxy to TField subdomain shape. """
        if self.context == "tensors":
            return self.tfield.tshape
        elif self.context == "voxels":
            return self.tfield.vshape
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")

    @property
    def ndim(self):
        """ Proxy to TField subdomain dimensionality. """
        if self.context == "tensors":
            return self.tfield.tdim
        elif self.context == "voxels":
            return self.tfield.vdim
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")

    @property
    def axes(self):
        """ Proxy to TField subdomain axes. """
        if self.context == "tensors":
            return self.tfield.taxes
        elif self.context == "voxels":
            return self.tfield.vaxes
        else:
            raise ValueError("Invalid context setting for TFieldIndexer.")


if __name__ == "__main__":
    print("This module was not intended for execution.")
