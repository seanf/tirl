#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import tempfile
import itertools
from threading import Lock


# TIRL IMPORTS

from tirl.settings import EXTENSIONS
from tirl import tirlfile as tf


# DEFINITIONS

# Global lock for the entire TIRL namespace
tirl_lock = Lock()


# DEVELOPMENT NOTES

# TODO: Consider adding a real-time instance logger on a specific thread.
# 6-July-2020 Istvan N Huszar:
# It is not hard to implement this: add self._logger to all TIRLObject, and
# a self.log() method that records on a specific log channel and at a specific
# log level that is defined for instance in the tirl configurations. The only
# important consideration is whether this would have a srong impact on
# performance where logging isn't a strict necessity. At the moment, logging is
# confined to tirl scripts and the above formulation was implemented for the
# Optimiser class. This class is not involved in I/O, so it's also simpler in
# this regard.


# IMPLEMENTATION

class InstanceCounterMeta(type):
    """
    Metaclass to make instance counter not share count with descendants

    """
    tirl_instance_counter = itertools.count(1)

    def __init__(cls, name, bases, attrs):
        super().__init__(name, bases, attrs)
        cls.instance_counter = itertools.count(1)


class TIRLObject(object, metaclass=InstanceCounterMeta):

    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        # Increase instance counter
        # The GIL should make this thread-safe within a single process
        obj._instance_id = next(cls.instance_counter)
        obj._tirl_id = next(cls.tirl_instance_counter)
        return obj

    def __init__(self):
        super().__init__()
        self._tmpfiles = []

    def tmpfile(self, prefix=None, dir=None, suffix=None):
        fd, fname = tempfile.mkstemp(prefix=prefix, suffix=suffix, dir=dir)
        self._tmpfiles.append((fd, fname))
        return fd, fname

    def __del__(self):
        if hasattr(self, "_tmpfiles"):
            for fd, fname in self._tmpfiles:
                try:
                    os.close(fd)
                except:
                    pass
                try:
                    os.remove(fname)
                except:
                    pass

    @property
    def file_ext(self):
        ext = None
        for c in type(self).__mro__:
            ext = EXTENSIONS.get(c.__name__, None)
            if ext:
                break
        ext = "tirl" if ext is None else ext
        return ext

    @file_ext.setter
    def file_ext(self, ext):
        # Note: changing the file extension does not overwrite the contents
        # of settings.py, therefore it is only valid during runtime, and only
        # within the same process.
        EXTENSIONS.update({type(self).__name__: str(ext).lower()})

    @property
    def name(self):
        return "{}_{}".format(self.__class__.__name__, id(self))

    @classmethod
    def _load(cls, dump):
        return NotImplementedError()

    @staticmethod
    def hload(node, objects=None):
        """
        This method assumes that every TIRLObject has a 'hload' method that
        assembles the TIRLObject from its hierarchically substituted object
        dump (i.e. every delegated TIRLObject is already converted, no need to
        parse the respective foreign object dump) by an explicit call to its
        _load method.

        """
        from pydoc import locate

        if objects is None:
            objects = dict()

        if isinstance(node, dict):
            iterator = ((key, node[key]) for key in sorted(node.keys()))
            for key, item in iterator:
                res = TIRLObject.hload(item, objects)
                if isinstance(res, TIRLObject):
                    node[key] = res
            if ("type" in node.keys()) and ("id" in node.keys()):
                obj = getattr(locate(node["type"]), "_load")(node)
                if node["id"] not in objects.keys():
                    objects.update({node["id"]: obj})
                    return objects[node["id"]]
                else:
                    return obj
        elif isinstance(node, list):
            for key, item in enumerate(node):
                res = TIRLObject.hload(item, objects)
                if isinstance(res, TIRLObject):
                    # In theory tuples get converted to lists in TIRLObject
                    # dumps, so it's not a concern that we assign items to a
                    # tuple here. -> Not true. Tuples should be actively
                    # avoided.
                    node[key] = res
        else:
            return node

    @classmethod
    def load(cls, fname):
        """
        Load TIRLObject from file. The TIRlObject base class implements the
        first stage only: rebuilding an object dump from a TIRL file.
        Subclasses must call this method and provide the second stage,
        converting an object dump into an actual object.

        :param fname: path to source file
        :type fname: str
        :returns: TIRLObject dump
        :rtype: dict

        """
        dump = tf.load(fname)
        return TIRLObject.hload(dump)

    @staticmethod
    def hdump(node):
        """
        Creates hierarchical TIRLObject dump. Guarantees that delegated
        TIRLObject instances get dumped at any depth.

        :return: TIRLObject dump
        :rtype: dict

        """
        if isinstance(node, TIRLObject):
            return node.dump()  # invokes hierarchical dump creation
        elif isinstance(node, dict):
            iterator = ((key, node[key]) for key in sorted(node.keys()))
            for key, item in iterator:
                res = TIRLObject.hdump(item)
                node[key] = res
            return node
        elif isinstance(node, list):
            # Chain doesn't support item assignment
            # unless the value is a Transformation.
            chainlist = [0] * len(node)
            if hasattr(node, "__chain__"):
                for key, item in enumerate(node):
                    res = TIRLObject.hdump(item)
                    chainlist[key] = res
                return chainlist
            else:
                for key, item in enumerate(node):
                    res = TIRLObject.hdump(item)
                    node[key] = res
                return node
        elif isinstance(node, tuple):
            tmp = list(node)
            for key, item in enumerate(tmp):
                res = TIRLObject.hdump(item)
                tmp[key] = res
            return tmp  # return list for mutability
        else:
            return node

    def _dump(self):
        """
        Creates a base-class TIRLObject dump. Subclasses SHOULD call this
        base-class method and implement their class-specific dumping protocol.

        The TIRLObject dump is a dict that contains all necessary information
        to create an identical copy of the current instance. The general rule
        of saving a TIRLObject to disk is that the object dumped is saved as a
        JSON-string in the file header, followed by a lookup-header that
        defines the offset to each of the unserialisable elements of the object
        dump (e.g. numerical arrays, memory-mapped arrays, bytestrings, tuples,
        complex numbers) that are appended to the end of the file in binary
        format.

        The type information is saved to call type-specific dump/_load methods.
        The object ID is saved to identify linking redundancies within objects.
        These are not only important to remove to save disk space, but also to
        keep dynamic links between transformations and domains.

        :return: TIRLObject dump
        :rtype: dict

        """
        objdump = {"type": ".".join([self.__class__.__module__,
                                     self.__class__.__name__]),
                   "id": str(id(self))}
        return objdump

    def dump(self):
        """
        Creates a TIRLObject dump. Subclasses MUST NOT override this method
        unless they return a hierarchical object dump.

        :return: TIRLObject dump
        :rtype: dict

        """
        return self.hdump(self._dump())

    def save(self, fname, overwrite=False, compressed=False):
        """
        Save TIRLObject to file.

        :param fname: path to target file
        :type fname: str
        :param overwrite:
            If True, any existing file that matches the target file will be
            overwritten. If False, the program asks for confirmation to
            overwrite an existing file.
        :type overwrite: bool
        :param compressed:
            If True, numerical arrays will be saved to disk in a compressed
            format. If False, numerical arrays will be saved as raw byte arrays.
        :type compressed: bool

        """
        # Guess extension
        fname = os.path.abspath(fname)
        path, fn = os.path.split(fname)
        fn, ext = os.path.splitext(fn)
        ext = self.file_ext if not ext else ext
        ext = ext.lstrip(".")
        fn = fn.rstrip(".")
        fname = os.path.join(path, ".".join([fn, ext]))

        # Get object dump
        dump = self.dump()

        # Save dump to disk
        tf.create(fname, dump, overwrite=overwrite, compressed=compressed)


if __name__ == "__main__":
    print("""This module is not intended for execution.""")
