#!/usr/bin/env python

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import dill
import psutil
import hashlib
import numpy as np
from numba import njit
from operator import mul
from warnings import warn
from functools import reduce
from numpy.lib.format import open_memmap
from numbers import Integral, Real, Number


# TIRL IMPORTS

from tirl.cache import Cache
from tirl import utils as tu, settings as ts
from tirl.tirlobject import TIRLObject
from tirl.transformations import Transformation
from tirl.transformations.basic.embedding import TxEmbed
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.basic.direct import TxDirect
from tirl.chain import Chain
from tirl.signature import Signature


# DEFINITIONS

from tirl.constants import *

INT_DTYPES = (np.int8, np.uint8, np.int16, np.uint16, np.int32, np.uint32,
              np.int64, np.uint64)
INSTANCE_MEMORY_LIMIT = ts.DOMAIN_INSTANCE_MEMORY_LIMIT * 1024 ** 2


# IMPLEMENTATION

class Domain(TIRLObject):
    """
    The Domain class provides an easy syntactic interface for vectorised
    coordinate transformations, and optimised handling of larger-than-memory
    domains.

    Domain defines a set of points (either on a rectangular grid, or
    scattered), and uses a sequence of transformations to map them into
    physical space. The voxel and physical coordinates are computed once and
    retrieved from the memory or a memory-mapped file on the hard disk for
    higher performance on repeated calls (coordinate array caching).

    Domains can be named for easier referencing.

    """

    # Constructor arguments are reserved keyword arguments
    RESERVED_KWARGS = ("extent", "transformations", "offset", "name",
                       "storage", "dtype", "instance_mem_limit", "n_threads")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __new__(cls, extent, *transformations, offset=None, name=None,
                storage=MEM, instance_mem_limit=INSTANCE_MEMORY_LIMIT,
                n_threads=ts.DOMAIN_N_THREADS, **kwargs):
        """
        Domain constructor.

        :param extent:
            There are three ways to define domain points in voxel space:
            1. Shape tuple/list: creates a dense rectangular domain with the
            given dimensions.
            2. (n_points, m_coordinates) array of integer coordinates:
            creates a discontinuous domain from N points embedded in
            M-dimensional space.
            3. Existing Domain instance: the input is returned without copying.
        :type extent: Union[tuple, list, np.ndarray]
        :param transformations:
            A list of Transformation objects that convert the voxel coordinates
            of the domain to physical coordinates.
        :type transformations: Transformation or Chain
        :param offset:
            List of offset-transformations. These may only be translation and
            scale transformations.
        :type offset:
            Union[TxTranslation, TxScale, list[TxTranslation, TxScale]]
        :param name:
            Human-readable name for domain object for easier referencing while
            scripting.
        :type name: Union[str, NoneType]
        :param storage:
            If MEM, the arrays of cached voxel and physical coordinates are
            stored in memory. If HDD, these arrays are stored in a
            memory-mapped file on the hard disk.
        :type storage: str
        :param instance_mem_limit:
            Maximum allowable memory used by the current Domain instance
            in bytes.
        :type instance_mem_limit: Union[int, np.integer]
        :param n_threads:
            Number of computing threads.
        :type n_threads: Union[int, np.integer]
        :param kwargs:
            Additional keyword arguments for Domain.
        :type kwargs: Any

        :returns: New instance of Domain.
        :rtype: Domain

        """
        # Return the input (without copying) if the input is a Domain
        if isinstance(extent, Domain):
            return extent

        # Call the parent class constructor
        return super(Domain, cls).__new__(cls)

    def __init__(self, extent, *transformations, offset=None, name=None,
                storage=MEM, instance_mem_limit=INSTANCE_MEMORY_LIMIT,
                 n_threads=ts.DOMAIN_N_THREADS, **kwargs):
        """
        Initialisation of Domain.

        :param extent:
            There are three ways to define domain points in voxel space:
            1. Shape tuple/list: creates a dense rectangular domain with the
            given dimensions.
            2. (n_points, m_coordinates) array of integer coordinates:
            creates a discontinuous domain from N points embedded in
            M-dimensional space.
            3. Existing Domain instance: the input is returned without copying.
        :type extent: Union[tuple, list, np.ndarray]
        :param transformations:
            A list of Transformation objects that convert the voxel coordinates
            of the domain to physical coordinates.
        :type transformations: Transformation
        :param offset:
            List of offset-transformations. These may only be translation and
            scale transformations.
        :type offset:
            Union[TxTranslation, TxScale, list[TxTranslation, TxScale]]
        :param name:
            Human-readable name for domain object for easier referencing while
            scripting.
        :type name: str
        :param storage:
            If MEM, the arrays of cached voxel and physical coordinates are
            stored in memory. If HDD, these arrays are stored in a
            memory-mapped file on the hard disk.
        :type storage: str
        :param instance_mem_limit:
            Maximum allowable memory used by the current Domain instance
            in bytes.
        :type instance_mem_limit: Union[int, np.integer]
        :param n_threads:
            Number of computing threads.
        :type n_threads: Union[int, np.integer]
        :param kwargs:
            Additional keyword arguments for Domain.
        :type kwargs: Any

        :returns: New instance of Domain.
        :rtype: Domain

        """
        # Do not do anything if the Domain object already exists
        # Note that __init__ may be called regardless what happens in __new__.
        if isinstance(extent, Domain):
            return

        # Call the parent-class initialisation
        super(Domain, self).__init__()
        # Initialise object properties dictionary
        self.properties = dict()

        # Set object properties
        self.name = name
        self.instance_mem_limit = instance_mem_limit
        self.threads = n_threads
        self.storage = storage
        self.kwargs = kwargs

        # Set internal and external transformations
        # Old names: "offset" (internal) and "volatile" (external)
        self.offset = offset
        self.chain = transformations
        self._cache = Cache(maxsize=1)

        # Define the extent of the Domain:

        # A) Sequence of integers -> shape of a compact grid
        if hasattr(extent, "__iter__") and \
                all(isinstance(val, Integral) for val in extent):
            self.properties.update(shape=tuple(extent), compact=True)
            maxval = max(extent) - 1

        # B) (n_points, m_dims) Coordinate table -> points of a non-compact grid
        elif hasattr(extent, "__array__"):
            extent = getattr(extent, "__array__")()
            n_points, ndim = extent.shape
            shape = (n_points,) + (1,) * (ndim - 1)  # encode dimensionality
            self.properties.update(shape=shape, compact=False)
            self.offset = TxDirect(extent, name="PointSet") + self.offset
            maxval = n_points - 1

        # Invalid specification
        else:
            raise TypeError("Invalid domain extent specification.")

        # Set minimum data type for voxel coordinates (saving RAM)
        for dtype in INT_DTYPES:
            if maxval <= np.iinfo(np.dtype(dtype)).max:
                self.properties.update(dtype=dtype)
                break
        else:
            raise ValueError("Voxel size too big for the largest integer type.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def chain(self) -> Chain:
        """ Transformation chain getter. """
        return self.properties.get("chain")

    @chain.setter
    def chain(self, c):
        """ Transformation chain setter. """
        # Convert single input to multiple input
        if hasattr(c, "__tx_priority__"):
            c = Chain(c)
        if c is None:
            c = Chain()
        # Handle multiple input
        if isinstance(c, (Chain, list, tuple)):
            self.properties.update(chain=Chain(c))
        else:
            raise TypeError(f"Expected Transformation or Chain, got "
                            f"{c.__class__.__name__} instead.")

    @property
    def instance_mem_limit(self) -> int:
        return self.properties.get("instance_mem_limit")

    @instance_mem_limit.setter
    def instance_mem_limit(self, limit):
        if isinstance(limit, Integral) and (limit > 0):
            self.properties.update(instance_mem_limit=int(limit))
        else:
            raise ValueError(f"Expected a positive integer number of bytes for "
                             f"instance memory limit, got {limit} instead.")

    @property
    def iscompact(self) -> bool:
        """ Read-only property. """
        return self.properties.get("compact")

    @property
    def kwargs(self) -> dict:
        return self.properties.get("kwargs", dict())

    @kwargs.setter
    def kwargs(self, kw):
        if isinstance(kw, dict):
            self.properties.update(kwargs=kw)
        else:
            raise TypeError(f"Expected a dict for Domain kwargs, "
                            f"got {kw} instead.")

    @property
    def name(self) -> str:
        return self.properties.get("name")

    @name.setter
    def name(self, n):
        if n is None:
            n = f"{self.__class__.__name__}_{id(self)}"
        if isinstance(n, str):
            self.properties.update(name=n)
        else:
            raise TypeError(f"Expected str for Domain name, "
                            f"got {n.__class__.__name__} instead.")

    @property
    def ndim(self) -> int:
        """ Read-only property. """
        return len(self.shape)

    @property
    def numel(self) -> int:
        """ Read-only property. """
        return reduce(mul, self.shape)

    @property
    def shape(self) -> tuple:
        """ Read-only property. """
        return self.properties.get("shape")

    @property
    def storage(self) -> str:
        """ Storage mode getter. """
        return self.properties.get("storage", None)

    @storage.setter
    def storage(self, smode):
        """
        Storage mode setter.

        :param smode: storage mode
        :type smode: Union[MEM, HDD]

        """
        if smode in (MEM, HDD):
            self.properties.update(storage=smode)
        else:
            raise ValueError(f"Unrecognised storage mode: {smode}")

    @property
    def threads(self) -> int:
        return self.properties.get("n_threads")

    @threads.setter
    def threads(self, t):
        if isinstance(t, Integral) and (t > 0):
            self.properties.update({"n_threads": t})
        else:
            raise ValueError(f"Expected a positive integer for the number of "
                             f"threads, got {t} instead.")

    @property
    def offset(self) -> Chain:
        """
        Returns the Chain of internal (offset) transformations that precede the
        external (user-set) domain transformations.

        :returns: offset transformations
        :rtype: list

        """
        return self.properties.get("offset")

    @offset.setter
    def offset(self, otx):
        """
        The Chain of offset-transformations is a list of translation and
        scale transformations that ensure that sliced TFields/TImages can
        easily preserve their location in physical space.

        """
        allowed = (TxTranslation, TxScale, TxEmbed, TxDirect)
        # Convert singular input to multiple input
        if hasattr(otx, "__tx_priority__"):
            otx = Chain(otx)
        if otx is None:
            otx = Chain()

        # Handle multiple inputs
        if isinstance(otx, (tuple, list, Chain)):
            if all(isinstance(tx, allowed) for tx in otx):
                self.properties.update(offset=Chain(otx))
            else:
                raise TypeError(f"Only the following types of Transformations "
                                f"are allowed as offsets: {allowed}")

        # Invalid input
        else:
            raise TypeError(f"Expected Transformation or Chain, got "
                            f"{otx.__class__.__name__} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __domain__(self):
        """ Domain interface. """
        return self

    def __eq__(self, other):
        # Is the other operand also a Domain?
        if not isinstance(other, type(self)):
            return False
        # Are the Domain shapes equal?
        if self.shape != other.shape:
            return False
        # Is the compactness the same?
        if self.iscompact != other.iscompact:
            return False
        # Are all offsets and transformations equal?
        all_tx = self.offset + self.chain
        other_all_tx = other.offset + other.chain
        if all_tx != other_all_tx:
            return False
        # Declare Domains equal
        return True

    def __repr__(self):
        if self.iscompact:
            extent = " x ".join(str(dim) for dim in self.shape)
        else:
            if self.shape[0] > 1:
                extent = f"{self.shape[0]} points, dim={self.ndim}"
            else:
                extent = f"scalar, dim={self.ndim}"

        return f"{type(self).__name__}({extent}, offset={len(self.offset)}, " \
               f"tx={len(self.chain)}, storage={self.storage})"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):

        # Load object properties from object dump
        name = dump.get("name")
        storage = dump.get("storage")
        instance_mem_limit = int(dump.get("instance_mem_limit"))
        n_threads = int(dump.get("n_threads"))
        kwargs = dump.get("kwargs")
        otxs = dump.get("offset")
        transformations = dump.get("chain")

        # Note that the new construction will add the TxDirect to the offset,
        # so keeping it would result in duplicating it.
        extent = dump.get("extent")
        if hasattr(extent, "__array__"):  # if the Domain is non-compact
            otxs.pop(0)

        # Resurrect Domain instance
        # Purge kwargs
        kwargs = {k: v for (k, v) in kwargs.items()
                  if k not in cls.RESERVED_KWARGS}
        ret = cls(extent, *transformations, offset=otxs, name=name,
                  storage=storage, instance_mem_limit=instance_mem_limit,
                  n_threads=n_threads, **kwargs)

        return ret

    def _dump(self):
        objdump = super(Domain, self)._dump()
        objdump.update({
            "name": self.name,
            "storage": self.storage,
            "instance_mem_limit": self.instance_mem_limit,
            "n_threads": self.threads,
            "kwargs": tu.rcopy(self.kwargs)})

        # Add domain definition to Domain descriptor dict
        if self.iscompact:
            objdump["extent"] = tuple(int(dim) for dim in self.shape)
        else:
            objdump["extent"] = self.offset[0].coordinates()

        # Add internal transformations to Domain descriptor dict
        objdump["offset"] = self.offset

        # Add external transformations to Domain descriptor dict
        # Note: calling the dump method for transformations instead of _dump,
        # because transformations may have other delegated TIRLObjects
        # (e.g. a domain) delegated to them, which also must be dumped.
        objdump["chain"] = self.chain

        return objdump

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def copy(self, **newparams):
        """
        Copy constructor of Domain.

        :param newparams:
            Any argument that is specified here overrides the parameters of the
            object being copied.
        :type newparams: Any

        :returns:
            A new Domain instance with identical points and transformations.
            The Transformations of the new instance are independent from the
            original transformations.
        :rtype: Domain

        """
        if self.iscompact:
            extent = self.shape
        else:
            assert isinstance(self.offset[0], TxDirect)
            extent = self.offset[0].coordinates()

        transformations = self.chain.copy()
        offset = self.offset.copy()
        if not self.iscompact:
            offset.pop(0)
        name = self.name + "_copy"

        obj = type(self)(
            extent, *transformations, offset=offset, name=name,
            storage=self.storage, instance_mem_limit=self.instance_mem_limit,
            n_threads=self.threads, **tu.rcopy(self.kwargs))

        return obj

    def get_voxel_coordinates(self, dtype=None, chunk=None):
        """
        Retrieve voxel coordinates from cache if available.

        """
        signature = (np.dtype(dtype).str, chunk)  # for hashing

        # In the case of non-compact domains, the voxel coordinates also depend
        # on the leading TxDirect object among the internal transformations.
        if not self.iscompact:
            signature = signature + (self.offset[0].signature(),)

        caller = "get_voxel_coordinates"
        key, exists = self._cache.query(signature, caller=caller)
        if exists:
            vcoords = self._cache.retrieve(key, caller=caller)
        else:
            vcoords = self._calculate_voxel_coordinates(dtype, chunk)
            self._cache.store(key, vcoords, caller=caller)

        return vcoords

    def _calculate_voxel_coordinates(self, dtype=None, chunk=None):
        """

        """
        # This function is called too often to perform a memory check here, so
        # the consensus is that when the storage mode of the Domain is MEM,
        # a retrieval is attempted irrespective of the available RAM. If
        # however the storage mode is HDD, the retrieval is optimised for
        # the available memory (taking into account the instance memory limit).

        # Setting default options
        if dtype is None:
            dtype = self.properties.get("dtype")
        if isinstance(chunk, slice):
            chunk = (chunk,)

        # Non-compact domains are always in memory.
        if not self.iscompact:
            if chunk is None:
                vc = self.offset[0].coordinates()
            elif isinstance(chunk, slice):
                vc = self.offset[0].coordinates()[chunk, :]
            elif isinstance(chunk, tuple) and (len(chunk) == 1) and \
                    isinstance(chunk[0], slice):
                chunk = chunk[0]
                vc = self.offset[0].coordinates()[chunk, :]
            elif isinstance(chunk, tuple) and \
                    all(isinstance(ix, Integral) for ix in chunk) and \
                    len(chunk) == 2:
                start, stop = chunk
                vc = self.offset[0].coordinates()[start:stop, :]
            elif hasattr(chunk, "__iter__") and \
                    all(isinstance(ix, Integral) for ix in chunk):
                indices = np.asarray(chunk, dtype=np.int)
                vc = self.offset[0].coordinates()[indices, :]
            else:
                raise TypeError(f"Unrecognised chunk specification: {chunk}")

            return vc.astype(ts.DEFAULT_FLOAT_TYPE)

        if self.storage == MEM:
            return self._calculate_voxel_coordinates_mem(dtype, chunk)
        elif self.storage == HDD:
            available = psutil.virtual_memory().available
            allowed = self.instance_mem_limit
            memlimit = min(available, allowed)
            return self._calculate_voxel_coordinates_hdd(dtype, chunk, memlimit)
        else:
            raise ValueError(f"Invalid storage mode: {self.storage}")

    def _calculate_voxel_coordinates_mem(self, dtype, chunk):
        """
        Returns the voxel (point) coordinates of the domain.

        :param dtype:
            Data type used to represent the voxel coordinates.
        :type dtype: np.generic or str
        :param chunk:
            None (default):
                coordinates of all points in the Domain will be returned
            tuple (l_start, l_end):
                defines a subset of points by linear indices
                (l_start inclusive, l_end exclusive)
            tuple[slice(start, end, step)]:
                defines a subset of points by multi-dimensional indexing
                (start inclusive, end exclusive)
        :type chunk: slice or tuple or None

        :returns: (n_points, m_dimensions) coordinate table
        :rtype: np.ndarray

        """
        # All coordinates
        if chunk is None:
            slicer = tuple(slice(dim) for dim in self.shape)
            coordinates = np.mgrid[slicer].reshape(self.ndim, -1).T

        # Linear chunk (C-order)
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, Integral) for ix in chunk) and \
                len(chunk) == 2:
            start, end = chunk
            indices = np.arange(start, end)
            coordinates = np.unravel_index(indices, self.shape, order="C")
            coordinates = np.stack(coordinates, axis=0).T

        # Multidimensional indexing
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, slice) for ix in chunk):
            slicer = [slice(dim) for dim in self.shape]
            slicer[:len(chunk)] = list(chunk)
            coordinates = np.mgrid[tuple(slicer)].reshape(self.ndim, -1).T

        else:
            raise ValueError(f"Unrecognised chunk specification: {chunk}")

        return coordinates.astype(dtype)

    def _count_voxels(self, chunk=None):
        """
        Returns the number of voxel selected by the chunk definition.

        """
        # All points
        if chunk is None:
            return self.numel

        # Linear chunk (C-order)
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, Integral) for ix in chunk) and \
                len(chunk) == 2:
            start, end = chunk
            return end - start

        # Multidimensional indexing
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, slice) for ix in chunk):
            slicer = [slice(dim) for dim in self.shape]
            slicer[:len(chunk)] = list(chunk)
            n_points = 1
            for sl, dim in zip(slicer, self.shape):
                start, end, step = sl.indices(dim)
                n_points = n_points * (end - start) // step
            else:
                return n_points
        else:
            raise ValueError(f"Unrecognised chunk specification: {chunk}")

    def _calculate_voxel_coordinates_subchunk(self, dtype, chunk, batchsize):
        """
        A private helper method for the 'get_voxel_coordinates' public method.
        Given a selection of voxels defined by 'chunk', this method implements
        a generator that returns the voxel coordinate table for these
        coordinates in several batches, as defined by the batchsize argument.

        This method is called when the size of the voxel coordinate table for
        the selected voxels exceeds the amount of RAM that has been allocated
        to the current Domain instance.

        """
        # All coordinates
        if chunk is None:
            for start in range(0, self.numel, batchsize):
                end = min(start + batchsize, self.numel)
                vcoords = np.unravel_index(np.arange(start, end), self.shape)
                yield np.stack(vcoords, axis=0).T.astype(dtype)

        # Linear chunk (C-order)
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, Integral) for ix in chunk) and \
                len(chunk) == 2:
            start_outer, end_outer = chunk
            for start in range(start_outer, end_outer, batchsize):
                end = min(start + batchsize, end_outer)
                vcoords = np.unravel_index(np.arange(start, end), self.shape)
                yield np.stack(vcoords, axis=0).T.astype(dtype)

        # Multidimensional (gap)slicing
        elif isinstance(chunk, tuple) and \
                all(isinstance(ix, slice) for ix in chunk):
            slicer = [slice(dim) for dim in self.shape]
            slicer[:len(chunk)] = list(chunk)
            marginals = [np.arange(*sl.indices(dim))
                         for sl, dim in zip(slicer, self.shape)]
            n_points = reduce(mul, [axis.size for axis in marginals])
            for start in range(0, n_points, batchsize):
                end = min(start + batchsize, n_points)
                vcoords = np.empty((end - start, self.ndim), dtype)
                roll = np.cumprod([1] + [axis.size for axis in marginals[::-1]])
                unravel_subchunk(vcoords, start, end, roll, *marginals)
                yield vcoords

        else:
            raise ValueError("Unrecognised chunk specification: {chunk}")

    def _calculate_voxel_coordinates_hdd(self, dtype, chunk, memlimit):
        """
        Returns the voxel (point) coordinates of the domain.

        :param dtype:
            Data type used to represent the voxel coordinates.
        :type dtype: np.generic or str
        :param chunk:
            None (default):
                coordinates of all points in the Domain will be returned
            tuple (l_start, l_end):
                defines a subset of points by linear indices
                (l_start inclusive, l_end exclusive)
            tuple[slice(start, end, step)]:
                defines a subset of points by multi-dimensional indexing
                (start inclusive, end exclusive)
        :type chunk: slice or tuple or None

        :returns: (n_points, m_dimensions) coordinate table
        :rtype: np.ndarray

        """
        n_points = self._count_voxels(chunk)
        batchsize = memlimit // (np.dtype(np.intp).itemsize * 2 * self.ndim)
        if batchsize > n_points:
            return self._calculate_voxel_coordinates_mem(dtype, chunk)

        # Create HDD array
        cls = self.__class__.__name__
        shape = (n_points, self.ndim)
        fd, fname = self.tmpfile(prefix=f"{cls}_vc_", dir=ts.TWD)
        coordinates = open_memmap(fname, mode="w+", dtype=dtype, shape=shape)

        # Retrieve batches
        generator = \
            self._calculate_voxel_coordinates_subchunk(dtype, chunk, batchsize)
        start = 0
        for i, batch in enumerate(generator):
            n_batch = batch.shape[0]
            slicer = slice(start, start + n_batch)
            coordinates[slicer] = batch
            start += n_batch
        else:
            del coordinates
            return open_memmap(fname, mode="r+", dtype=dtype, shape=shape)

    def get_intermediate_coordinates(self, dtype=None, chunk=None):
        """
        Returns the voxel coordinates transformed by the offset chain
        (internal transformations).

        This offset-free "intermediate" pseudo-voxel coordinate space often
        stands as the "original" or "real" voxel coordinate space, where the
        user-defined transformations map the voxels into "physical" space.

        E.g. 1.: the voxel array of a non-compact domain is 1-dimensional, but
        the support points of such a domain usually represent locations in 2D
        or 3D space. Consecutive elements of the 1D array are therefore mapped
        into this "intermediate" voxel-coordinate space, where the
        interpolation is defined.

        E.g. 2.: when a TImage is sliced along the voxel dimensions, the
        resultant part of the image preserves its location in physical space.
        Instead of altering the transformation chain, an offset transformation
        is introduced that maps the new voxel coordinates into this
        intermediate space, which is in 1:1 correspondence with the "original"
        (unsliced) voxel space.

        """
        vc = self.get_voxel_coordinates(dtype=dtype, chunk=chunk)
        if self.iscompact:
            return self.offset.map(vc).astype(dtype)
        else:
            return self.offset[1:].map(vc).astype(dtype)

    def get_physical_coordinates(self, dtype=None, chunk=None):
        """
        Returns the physical (real-world) coordinates for select voxels of the
        Domain, as specified by 'chunk'.

        """
        # This function is called too often to perform a memory check here, so
        # the consensus is that when the storage mode of the Domain is MEM,
        # a calculation is attempted irrespective of the available RAM. If
        # however the storage mode is HDD, the calculation is optimised for
        # the available memory (taking into account the instance memory limit).

        # Setting default options
        if dtype is None:
            dtype = ts.DEFAULT_FLOAT_TYPE
        if isinstance(chunk, slice):
            chunk = (chunk,)

        if self.storage == MEM:
            return self.get_physical_coordinates_mem(dtype, chunk)
        elif self.storage == HDD:
            available = psutil.virtual_memory().available
            allowed = self.instance_mem_limit
            memlimit = min(available, allowed)
            return self.get_physical_coordinates_hdd(dtype, chunk, memlimit)
        else:
            raise ValueError(f"Invalid storage mode: {self.storage}")

    def get_physical_coordinates_mem(self, dtype=None, chunk=None):
        """
        Computes the physical coordinates of the requested domain points
        without checking the available RAM.

        """
        coordinates = self.get_voxel_coordinates(chunk=chunk)
        signature = [self.signature()]  # signature chain starter
        return self.map_voxel_coordinates(coordinates, signature, dtype=dtype)

    def get_physical_coordinates_hdd(self, dtype=None, chunk=None,
                                     memlimit=None):
        """
        Optimises the calculation of physical coordinates for the requested
        domain points for a certain amount of RAM.
        """
        signature = [self.signature()]
        n_points = self._count_voxels(chunk)
        vcoords = self.get_voxel_coordinates(chunk=chunk)
        vbytes = 0 if isinstance(vcoords, np.memmap) else vcoords.nbytes
        pdim = self.map_voxel_coordinates([[1] * self.ndim]).shape[-1]
        batchsize = (memlimit - vbytes) // (pdim * np.dtype(dtype).itemsize * 2)
        if batchsize > n_points:
            return self.map_voxel_coordinates(vcoords, signature, dtype=dtype)

        # Create HDD array
        cls = self.__class__.__name__
        shape = (n_points, pdim)
        fd, fname = self.tmpfile(prefix=f"{cls}_pc_", dir=ts.TWD)
        coordinates = open_memmap(fname, mode="w+", dtype=dtype, shape=shape)

        # Retrieve batches
        for start in range(0, n_points, batchsize):
            end = min(start + batchsize, n_points)
            coordinates[start:end] = \
                self.map_voxel_coordinates(
                    vcoords[start:end], signature, dtype=dtype)
        else:
            del coordinates
            return open_memmap(fname, mode="r+", dtype=dtype, shape=shape)

    def map_voxel_coordinates(self, coords, signature=None, dtype=None):
        """
        Calls the transformation chain to map voxel coordinates to physical
        coordinates.

        """
        if dtype is None:
            dtype = ts.DEFAULT_FLOAT_TYPE

        signature = [] if signature is None else signature  # chain starter
        coords = self.all_tx().reduce().map(
            coords, merge="linear", signature=signature)
        # signature.append(self.chain.signature())  # exclude offset!

        return coords.astype(dtype)

    def map_physical_coordinates(self, coords, signature=None):
        """
        Inverts the transformation chain to map physical coordinates to voxel
        coordinates. The resultant voxel coordinates are floating-point type,
        subject to interpolation.

        """
        inv_tx = self.all_tx().reduce().inverse()
        signature = [] if signature is None else signature  # chain starter
        coords = inv_tx.map(coords, merge="linear", signature=signature)
        # signature.append(inv_chain.signature())  # exclude offset!

        return coords

    # TODO: The following methods should be aware of memory and/or storage mode.

    def all_tx(self):
        if self.iscompact:
            return self.offset + self.chain
        else:
            return self.offset[1:] + self.chain

    def map_voxel_vectors(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        return self.all_tx().map_vector(vects=vects, coords=coords, rule=rule)

    def map_physical_vectors(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector(vects=vects, coords=coords, rule=rule)

    def map_voxel_tensors(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        return self.all_tx().map_tensor(
            tensors=tensors, coords=coords, rule=rule)

    def map_physical_tensors(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector(tensors=tensors, coords=coords, rule=rule)

    # And the same set of methods for combined vector/tensor + coordinate xfm

    def map_voxel_vectors_and_coordinates(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        return self.all_tx().map_vector_and_coordinates(
            vects=vects, coords=coords, rule=rule)

    def map_physical_vectors_and_coordinates(
            self, vects, coords=None, rule=RULE_SSR, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector_and_coordinates(
            vects=vects, coords=coords, rule=rule)

    def map_voxel_tensors_and_coordinates(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        return self.all_tx().map_tensor_and_coordinates(
            tensors=tensors, coords=coords, rule=rule)

    def map_physical_tensors_and_coordinates(
            self, tensors, coords=None, rule=RULE_PPD, signature=None):
        all_tx = self.all_tx().inverse()
        return all_tx.map_vector_and_coordinates(
            tensors=tensors, coords=coords, rule=rule)

    def __getitem__(self, item):
        """
        Domain slicing allows the creation of domains with identical extent and
        settings, but with a selected subset of transformations. The new domain
        will share the transformations with the old domain, so any changes of
        the transformation parameters in either of the objects will be
        represented in the other.

        Note 1: offset-transformations will be shared regardless of the slice
        specification.

        Note 2: while discontinuous slices are allowed, these may not result in
        valid Domain objects if the resultant chain of transformations is not
        internally congruent.

        :param item:
            Slice term. This must be 1-dimensional, and indices must be chosen
            relative to the list of "volatile" transformations (those
            accessible via Domain.chain), excluding all "offset"
            transformations (those accessible via Domain.offset) that logically
            precede the former.
        :type item:
            Union[int, np.integer, slice, Ellipsis, list, np.ndarray]

        :returns:
            Child Domain object that shares the selected transformations with
            the input Domain instance.
        :rtype: Domain

        """
        newchain = self.chain.__getitem__(item)
        if not hasattr(newchain, "__iter__"):
            newchain = (newchain,)
        name = f"{self.name}_slice"
        kwargs = {k: v for (k, v) in self.kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        ret = type(self)(self.shape, *newchain, offset=self.offset, name=name,
                         storage=self.storage,
                         instance_mem_limit=self.instance_mem_limit,
                         n_threads=self.threads, **kwargs)
        return ret

    def __setitem__(self, key, value):
        """
        Allows to set elements in the transformation chain by directly
        indexing the Domain object.
        """
        self.chain.__setitem__(key, value)

    def signature(self):
        """
        Returns the signature of the Domain instance. The domain signature
        depends on the shape of the domain, and the individual signatures of
        the offset and regular transformations. Signatures uniquely identify
        the mapping from a set of voxel coordinates to a set of physical
        coordinates. If any of the transformations change or the domain is not
        identical to another, it is reflected in the change of the signature.
        Note that identical transformations may have different signatures.

        :returns: domain signature
        :rtype: str

        """
        # Level 0: voxel shape (compact) / point set (non-compact) signature
        def level0():
            hasher = hashlib.sha1()
            if self.iscompact:
                hasher.update(dill.dumps(self.shape))
            else:
                tx = self.offset["PointSet"]
                coordinates = tx.parameters.parameters.reshape(-1, tx.ndim)
                hasher.update(dill.dumps(coordinates))
            return hasher.hexdigest()

        # Level 1: External transformation chain ("chain") signature
        def level1():
            return self.chain.signature()

        # Level 2: Internal transformation chain ("offset") signature
        def level2():
            return self.offset.signature()

        return Signature(level0, level1, level2)

    def resize(self, shape_or_scale, *scales, copy=False):
        """
        Changes the number of voxels in a compact domain and prepends the
        intrinsic transformations (offset chain) with a scaling, such that the
        new voxel array is mapped to the same physical location and has the
        same physical extent along all dimensions.

        :notes:

            1. For non-compact domains, this option is not available, and
               raises a DomainError.
            2. Unless explicitly specified, the shape of the new Domain
               instance will be rounded to the nearest integer that is higher
               (ceil).
            3. Transformations are shared between the old and the new Domain
               instances (unless copy=True), but kwargs are copied either way.

        :param shape_or_scale:
            Target shape as a tuple, or non-negative numerical scaling factor
            for the 0-th voxel dimension.
        :type shape_or_scale:
            Union[tuple[int], float, np.floating, int, np.integer]
        :param scales:
            Scaling factors for the 1st, 2nd, etc. voxel dimensions.
        :type scales: Union[int, float, np.integer, np.floating]

        :returns:
            New resized Domain instance with identical transformations and
            metaparameters.
        :rtype: Domain

        :raises DomainError:
            If the input Domain is non-compact.

        """
        if not self.iscompact:
            raise TypeError("Only compact domains (regular voxel arrays) "
                            "can be resized.")

        # If target is an explicit shape specification
        if hasattr(shape_or_scale, "__iter__"):
            return self._resize_to_shape(shape_or_scale)

        # If the target is a global scaling factor
        elif isinstance(shape_or_scale, Number) and not scales:
            factors = (shape_or_scale,) * self.ndim
            return self._resize_by_factors(*factors, copy=copy)

        # If scaling factors are defined for (supposedly) all dimensions
        elif isinstance(shape_or_scale, Number) and scales:
            factors = (shape_or_scale,) + scales
            return self._resize_by_factors(*factors, copy=copy)

        # If the input is invalid
        else:
            args = (shape_or_scale,) + scales
            raise ValueError(f"Invalid target shape or scaling factor "
                             f"specification for resizing Domain: {args}")

    def _resize_to_shape(self, shape, copy=False):
        """
        Resizes domain to a specific shape. Preserves all transformations,
        therefore the resized domain maps to the same physical location and has
        the same physical extent.

        What changes as a result of resizing is the "sampling density" of the
        quantity that is defined on the domain. E.g. reducing the size of a
        TImage domain makes the image blurrier.

        """
        # Sanity check
        if not all(isinstance(dim, Integral) for dim in shape):
            raise ValueError(f"Domain must have integer dimensions: {shape}")
        if any(dim < 1 for dim in shape):
            raise ValueError(f"Domain must have positive dimensions: {shape}")
        if len(shape) != self.ndim:
            raise ValueError(f"Resizing a Domain must preserve the number of "
                             f"dimensions in the Domain. Old dimensions: "
                             f"{len(shape)}, new dimensions: {self.ndim}")

        # Compensatory transformation
        factors = np.divide(self.shape, shape)
        tx = TxScale(*factors, name="ResizeCompensation")

        # Create resized Domain
        name = f"{self.name}_resized"
        kwargs = {k: v for k, v in self.kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        kwargs = tu.rcopy(kwargs) if copy else kwargs
        chain = self.chain.copy() if copy else self.chain
        offset = tx + self.offset.copy() if copy else tx + self.offset
        resized = type(self)(shape, *chain, offset=offset, name=name,
                             storage=self.storage,
                             instance_mem_limit=self.instance_mem_limit,
                             n_threads=self.threads, **kwargs)
        return resized

    def _resize_by_factors(self, *factors, copy=False):
        """
        Resizes domain by dimension-specific scaling factors. The scaling
        factors must be positive real numbers, and the resultant shape
        will be rounded up to the next integer (ceil).

        """
        # Sanity check
        if not all(isinstance(f, Real) for f in factors):
            raise ValueError(f"All scaling factors must be positive real "
                             f"numbers: {factors}")
        if any(f <= 0 for f in factors):
            raise ValueError(f"Scaling factors must be positive real numbers: "
                             f"{factors}")
        if len(factors) != self.ndim:
            raise ValueError(f"The number of scaling factors must match the "
                             f"number of Domain dimensions: "
                             f"{len(factors)} vs. {self.ndim}.")

        # New shape
        shape = tuple(np.ceil(np.multiply(self.shape, factors)).astype(int))

        # Compensatory transformation
        # Note that the compensation can be slightly different from the
        # specified factors, as it also takes rounding into account.
        factors = np.divide(self.shape, shape)
        tx = TxScale(*factors, name="ResizeCompensation")

        # Create resized Domain
        name = f"{self.name}_resized"
        kwargs = {k: v for k, v in self.kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        kwargs = tu.rcopy(kwargs) if copy else kwargs
        chain = self.chain.copy() if copy else self.chain
        offset = tx + self.offset.copy() if copy else tx + self.offset
        resized = type(self)(shape, *chain, offset=offset, name=name,
                             storage=self.storage,
                             instance_mem_limit=self.instance_mem_limit,
                             n_threads=self.threads, **kwargs)
        return resized

    def reset(self, offset=False):
        """
        Resets all external transformations (chain). Internal transformations
        (offset) are preserved if 'offset' is False (default).

        """
        if offset:
            self.properties.update(offset=Chain())
        self.properties.update(chain=Chain())

    def vcentre(self):
        if self.iscompact:
            c = self.offset.map([[dim / 2 for dim in self.shape]]).tolist()
            return tuple(c)
        else:
            c = np.mean(self.get_voxel_coordinates(), axis=0).tolist()
            return tuple(c)

    def pcentre(self):
        if self.iscompact:
            c = self.all_tx().map([[dim / 2 for dim in self.shape]]).tolist()
            return tuple(c)
        else:
            vc = np.mean(self.get_voxel_coordinates(), axis=0)
            c = self.all_tx().map(vc).tolist()
            return tuple(c)

    def centralise(self):
        """
        Appends the chain of transformations with a TxTranslation object,
        which moves the geometrical centre of the domain to the origin. All
        points/voxels bear equal weights in averaging.

        """
        # Non-linear transformations change the density of points, therefore
        # the calculation of the domain centre must involve all points.
        nonlin = any(tx.kind == "nonlinear" for tx in self.chain)
        if nonlin:
            coords = self.get_physical_coordinates()
            offset = -np.add.reduce(coords, axis=0) / coords.shape[0]
        else:
            offset = [-dim / 2 for dim in self.shape]
            offset = self.map_voxel_coordinates([offset])[0]
        tx = TxTranslation(*offset, name="Centralise")

        # Warn user if centralisation is duplicated
        if np.allclose(offset, 0):
            last_tx = self.chain[-1]
            if type(last_tx, TxTranslation) and last_tx.name == "Centralise":
                warn("Domain is centralised more than once.")

        # Apply centralisation
        self.chain.append(tx)

    def copy_transformations(self, old_domain, links=True):
        """
        Copies the transformation chain from an existing domain to the current
        domain. This method is useful when a TField or a TImage is
        resampled to a new grid.

        :param old_domain: template domain
        :type old_domain: Domain
        :param links:
            If True, Non-linear transformations that were dynamically linked to
            the other domain, will be recreated so that they will be also
            linked to the current domain. If False, these transformations will
            be statically linked to the new domain.
        :type links: bool

        """
        self.chain = old_domain.chain
        if links:
            for i, tx in enumerate(old_domain.chain):
                if tx.kind == TX_NONLINEAR:
                    # If the transformation was dynamically linked to
                    # the current domain, re-grid that transformation.
                    if tx.domain == old_domain[:i]:
                        # Erase transformations on the tx domain
                        tx.metaparameters["domain"] = old_domain[:0]
                        # Resample among voxel domains
                        new_tx = tx.regrid(self[:0])
                        # Add back all new antecedent transformations
                        new_tx.metaparameters["domain"] = self[:i]
                        # Add the current transformation to the new domain
                        self.chain[i] = new_tx

    def get_transformation(self, name, case_sensitive=False, index=False):
        """
        Parses the domain transformations to find a Transformation object with
        the specified name. All transformations with the same name are returned
        as a tuple. Note that offset transformations are excluded from the
        search.

        If the transformation with a given name is not found, IndexError is
        raised.

        :param name:
            Name of the desired Transformation object.
        :type name: str
        :param case_sensitive:
            Turn this option on to make the search case-sensitive.
        :type case_sensitive: bool
        :param index:
            If True, transformations' indices are also returned
        :type index: bool

        :returns:
            Transformation object(s) with the specified name. Indices.
        :rtype: Union[Transformation, tuple[Tranformation, int],
                tuple[Transformation], tuple[tuple]]
        :raises: IndexError if the transformation is not found in the chain

        """
        group = self.chain
        return self._get_tx_from_chaingroup(group, name, case_sensitive, index)

    def get_offset_transformation(self, name, case_sensitive=False,
                                  index=False):
        """
        Parses the offset transformations of the domain to find a Transformation
        object with the specified name. All transformations with the same name
        are returned as a tuple. Note that offset transformations are excluded
        from the search.

        If the transformation with a given name is not found, IndexError is
        raised.

        :param name:
            Name of the desired Transformation object.
        :type name: str
        :param case_sensitive:
            Turn this option on to make the search case-sensitive.
        :type case_sensitive: bool
        :param index:
            If True, transformations' indices are also returned
        :type index: bool

        :returns:
            Transformation object(s) with the specified name. Indices.
        :rtype: Union[Transformation, tuple[Tranformation, int],
                tuple[Transformation], tuple[tuple]]
        :raises: IndexError if the transformation is not found in the chain

        """
        group = self.offset
        return self._get_tx_from_chaingroup(group, name, case_sensitive, index)

    def _get_tx_from_chaingroup(self, group, name, case_sensitive=False,
                                index=False):
        """
        Private method that implements the common parts of the public methods
        Domain.get_transformation() and Domain.get_offset_transformation().

        :param group:
            Either Domain.offset or Domain.chain. Defines the scope where the
            algorithm searches for matching transformations.
        :type group: list

        """
        txs = []
        indices = []
        name = str(name).lower() if case_sensitive else str(name)
        if case_sensitive:
            txnames = [getattr(tx, "name", "") for tx in group]
        else:
            txnames = [str(getattr(tx, "name", "")).lower() for tx in group]
        for i, n in enumerate(txnames):
            if name == n:
                txs.append(group[i])
                if index:
                    indices.append(i)

        # Return transformation(s)
        if len(txs) == 0:
            if case_sensitive:
                msg = "No transformation with name '{}' (case sensitive)."
            else:
                msg = "No transformation with name '{}'."
            raise IndexError(msg.format(name))
        if len(txs) == 1:
            if index:
                return txs[0], indices[0]
            else:
                return txs[0]
        else:
            if index:
                return tuple(txs), tuple(indices)
            else:
                return tuple(txs)


@njit
def unravel_subchunk(vcoords, start, end, roll_ax, *marginals):
    """
    This is a numba-compiled helper function for get_voxel_coordinates_hdd,
    that calculates the voxel coordinates for voxels that were selected using
    multi-dimensional (gap)slicing.

    """
    for point_no in range(start, end):
        vcoords[point_no - start] = \
            [axis[(point_no // roll) % len(axis)]
             for axis, roll in zip(marginals, roll_ax[1::-1])]
    else:
        return vcoords
