#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES


# TIRL IMPORTS


# IMPLEMENTATION

class Signature(list):

    """
    Signature implements the unique fingerprint of a TIRL object. The signature
    consists of multiple levels that are increasingly more expensive to
    compute. When called, the object returns the hash codes corresponding to a
    chosen level, or the hash codes for all levels if a level is not specified.

    """
    def __init__(self, *contents):
        super(Signature, self).__init__(contents)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        if len(self) != len(other):
            return False
        for slevel, olevel in zip(self, other):
            slevel = slevel() if callable(slevel) else slevel
            olevel = olevel() if callable(olevel) else olevel
            if slevel != olevel:
                return False
        else:
            return True

    def __call__(self, *levels):
        levels = list(self) if not levels else [self[ix] for ix in levels]
        for level in levels:
            if isinstance(level, str):
                return level
            elif callable(level):
                return level()
            else:   # pragma: no cover
                assert False  # this should never be reached
