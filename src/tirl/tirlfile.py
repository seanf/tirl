#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import re
import os
import json
import inspect
import numpy as np
from pydoc import locate
from collections import namedtuple


# TIRL IMPORTS

from tirl import utils as tu

# DEFINITIONS

__version__ = (2, 0)

ctype = namedtuple("ctype", ["nbytes", "byteorder", "signed"])

MAGIC = b"TIRLFile"
UINT8 = ctype(1, "little", False)  # 8-bit little-endian unsigned integer
UINT64 = ctype(8, "little", False)  # 64-bit little-endian unsigned integer
ARRAY_TAG = b"NDArray\0"
BYTES_TAG = b"Bytes\0"
MMAP_TAG = b"MemoryMap\0"
LH_TAG = b"LookupHeader"
BUFSIZE = 100 * 1024 ** 2  # 100 MB


# IMPLEMENTATION

def bytes2int(bytes, int_type):
    """
    Converts a byte array into a single or a sequence of integers based on
    the length of the byte array and the integer type.

    :param bytes: byte array
    :type bytes: bytes
    :param int_type: integer type as a tuple: (n_bytes, byteorder, signed)
    :type int_type: tuple[int, str, bool]

    :returns: a single integer or a sequence of integers
    :rtype: Union[int, tuple[int]]

    """
    itemsize, byteorder, signed = int_type
    assert len(bytes) % itemsize == 0
    result = []
    for start in range(0, len(bytes), itemsize):
        result.append(
            int.from_bytes(bytes[start:start+itemsize], byteorder,
                           signed=signed))
    return result[0] if len(result) == 1 else tuple(result)


def int2bytes(i, int_type):
    """
    Converts an integer or a sequence of integers into byte representation,
    given the integer type.

    :param i: integer or a sequence of integers
    :type i: Union[int, tuple[int]]
    :param int_type: integer type as a tuple: (n_bytes, byteorder, signed)
    :type int_type: tuple[int, str, bool]

    :returns: byte array
    :rtype: bytes

    """
    itemsize, byteorder, signed = int_type
    if isinstance(i, int):
        return int.to_bytes(i, itemsize, byteorder, signed=signed)
    result = []
    if hasattr(i, "__iter__") and all(isinstance(e, int) for e in i):
        for e in i:
            result.append(int.to_bytes(e, itemsize, byteorder, signed=signed))
        return b"".join(result)


def save_replacements(f, replacements, compressed=False):
    """
    Writes the replaced items to a file.

    :param f: target file pointer
    :type f: File
    :param replacements: dictionary of replaced non-serialisable elements
    :type replacements: dict
    :param compressed:
        Note: This feature is not yet available in TIRL 1.0, i.e. all data is
        saved to file without compression.
        If True, in-memory numerical arrays will be saved to the file after
        compression, otherwise they are save as raw byte arrays. Note that
        memory-mapped disk arrays will be saved in non-compressed format
        regardless of this setting.
    :type compressed: bool

    """
    replacements = replacements["decoded_objects"]

    # Create lookup header
    f.write(LH_TAG)

    # Store the names of the keys in alphabetical order
    sorted_keys = sorted(replacements.keys())
    js_sorted_keys = json.dumps(sorted_keys).encode()
    f.write(int2bytes(len(js_sorted_keys), UINT64))
    f.write(js_sorted_keys)

    # Create a placeholder for storing offsets to each element
    placeholder = f.tell()
    f.write(b"\x00" * UINT64.nbytes * len(sorted_keys))
    f.flush()
    os.fsync(f.fileno())

    # Store the individual elements with their own header
    offsets = []
    for key in sorted_keys:
        item = replacements[key]
        offsets.append(f.tell())
        if isinstance(item, np.memmap):
            save_memmap(f, item, compressed=compressed)
        elif isinstance(item, np.ndarray):
            save_array(f, item, compressed=compressed)
        elif isinstance(item, bytes):
            save_bytes(f, item, compressed=compressed)
        else:
            raise NotImplementedError()

    # Add offsets to the LookupHeader
    f.seek(placeholder)
    f.write(b"".join(int2bytes(val, UINT64) for val in offsets))


def save_array(f, item, compressed=False):
    """
    Writes ndarray with header to file.

    :param f: file pointer
    :type f: File
    :param item: array
    :type item: np.ndarray

    """
    f.write(ARRAY_TAG)
    array_size_field = f.tell()
    f.write(b"\x00" * UINT64.nbytes)  # placeholder
    f.flush()
    os.fsync(f.fileno())
    np.save(f, item)
    # if compressed:
    #     np.savez_compressed(f, item)
    # else:
    #     np.savez(f, item)
    f.flush()
    os.fsync(f.fileno())
    eof = f.tell()
    f.seek(array_size_field)
    f.write(int2bytes(eof - array_size_field - UINT64.nbytes, UINT64))
    f.seek(eof)


def save_bytes(f, b, compressed=False):
    """
    Writes a bytearray replacement block to file.

    :param f: file pointer
    :type f: File
    :param b: bytearray
    :type b: bytes

    """
    f.write(BYTES_TAG)
    f.write(int2bytes(len(b), UINT64))
    f.write(b)
    f.flush()
    os.fsync(f.fileno())


def save_memmap(f, memmap, compressed=False):
    """
    Writes a block of memory-mapped arrays to an open file. No data compression
    is applied.
    Block header: <MemoryMapBlock><number_of_arrays_int64><start_positions>
    Array header: <MemoryMap><hdrsize_int64><shape><dtype><order>

    :param f: file pointer where the memmap will be saved
    :type f: File
    :param memmap: memory-mapped array that will be saved
    :type memmap: np.memmap

    """

    # Memmap header
    f.write(MMAP_TAG)
    descriptor = (memmap.shape, np.dtype(memmap.dtype).str,
                  memmap.flags["C_CONTIGUOUS"])
    js_desc = json.dumps(descriptor).encode()
    f.write(int2bytes(len(js_desc), UINT64))
    f.write(js_desc)
    f.flush()
    os.fsync(f.fileno())

    # Memmap data
    buffer = memmap.ravel().data
    nbytes = memmap.nbytes
    for start in range(0, nbytes, BUFSIZE):
        stop = min(start + BUFSIZE, nbytes)
        f.write(buffer[start:stop])
        # Flush to avoid data accumulating in the system buffer
        f.flush()
    os.fsync(f.fileno())


def load_replacements(f):
    """
    Loads replacement items from TIRLFile.

    :param f: file pointer
    :type f: File

    :returns: replacements dictionary
    :rtype: dict

    """
    replacements = dict()

    # Load the LookupHeader
    if f.read(len(LH_TAG)) != LH_TAG:
        raise IndexError("Invalid LookupHeader in TIRlFile.")
    js_sorted_keys = f.read(bytes2int(f.read(UINT64.nbytes), UINT64)).decode()
    sorted_keys = json.loads(js_sorted_keys)
    offsets = bytes2int(f.read(len(sorted_keys) * UINT64.nbytes), UINT64)
    offsets = (offsets,) if not hasattr(offsets, "__iter__") else offsets

    # Load the replaced elements
    for key, offset in zip(sorted_keys, offsets):
        f.seek(offset)

        # Read replacement block label
        label = [None]
        while label[-1] != b"\0":
            label.append(f.read(1))
        else:
            label = b"".join(label[1:])

        # Read replacement block
        if label == MMAP_TAG:
            item = load_memmap(f)
        elif label == ARRAY_TAG:
            item = load_array(f)
        elif label == BYTES_TAG:
            item = load_bytes(f)
        else:
            raise NotImplementedError()

        replacements.update({key: item})
    return replacements


def load_array(f):
    """
    Load ndarray from object dump at file pointer.

    :param f: file pointer where the array should be loaded from
    :type f: File

    :returns: array handle
    :rtype: np.ndarray

    """
    import mmap
    blocksize = bytes2int(f.read(UINT64.nbytes), UINT64)
    fd = os.open(f.name, os.O_RDONLY)
    offset = f.tell() - f.tell() % mmap.ALLOCATIONGRANULARITY
    correction = f.tell() - offset
    try:
        f_seg = mmap.mmap(fd, length=correction + blocksize,
                          access=mmap.ACCESS_READ, offset=offset)
        f_seg.seek(correction, 0)
        arr = np.load(f_seg)
    except:
        raise
    finally:
        os.close(fd)
    return arr


def load_bytes(f):
    """
    Loads bytearray replacement block from file.

    :param f: file pointer
    :type f: File

    :return: bytearray
    :rtype: bytes

    """
    blocksize = bytes2int(f.read(UINT64.nbytes), UINT64)
    return f.read(blocksize)


def load_memmap(f, mode="r+"):
    """
    Returns a memory-mapped array from a file pointer.

    :param f: file pointer where the memory-mapped array is stored
    :type f: File
    :param mode:
        By default, memory maps are loaded as both readable and writable (r+).
    :type mode: str

    :returns: memory-mapped array variable
    :rtype: np.memmap

    """
    js_descr = f.read(bytes2int(f.read(UINT64.nbytes), UINT64)).decode()
    shape, dtype, order = json.loads(js_descr)
    order = "C" if order else "F"
    data = np.memmap(f.name, dtype=np.dtype(dtype), mode=mode,
                     offset=f.tell(), shape=tuple(shape), order=order)
    return data


def create(fname, dump, overwrite=False, compressed=False):
    """
    Create TIRLFile from TIRLObject dump.

    :param fname: full path to the target file
    :type fname: str
    :param dump: TIRLObject dump (decoded)
    :type dump: dict
    :param overwrite:
        If True, an existing file with the target name will be overwritten
        without notification. If False, the program asks for confirmation from
        the user.
    :type overwrite: bool
    :param compressed:
        If True, numerical arrays from the RAM will be saved in compressed
        format. If False, numerical arrays from the RAM will be saved as raw
        byte arrays. Note that no compression is applied to memory-mapped disk
        arrays regardless of this setting.
    :type compressed: bool

    """
    # Verify input
    assert isinstance(dump, dict)
    fname = tu.verify_fname(fname, overwrite)

    with open(fname, "wb", buffering=BUFSIZE) as f:

        # Write magic string and file version
        f.write(MAGIC)
        f.write(bytes(__version__))

        # Write main header
        header, replacements = encode(dump)[:2]
        try:
            js_header = json.dumps(header).encode()
        except TypeError:
            from pygments import highlight, lexers, formatters
            formatted_json = json.dumps(header, indent=4)
            colorful_json = highlight(formatted_json.encode("UTF-8"),
                                      lexers.JsonLexer(),
                                      formatters.TerminalFormatter())
            print(colorful_json)

        f.write(int2bytes(len(js_header), UINT64))  # main header size
        f.write(js_header)

        # Write replacements with LookupHeader
        save_replacements(f, replacements, compressed=compressed)


def load(fname):
    """
    Rebuilds object dump from TIRLFile.

    :param fname: input TIRLFile
    :type fname: str

    :returns: TIRLObject dump
    :rtype: dict

    """
    with open(fname, "rb") as f:

        # Check file type
        if f.read(len(MAGIC)) != MAGIC:
            raise TypeError("Invalid TIRLFile: {}".format(fname))

        # Check version number
        major, minor = bytes2int(f.read(2 * UINT8.nbytes), UINT8)
        if (major > __version__[0]) or \
                (minor > __version__[1]):
            raise TypeError("The maximum supported TIRLFile version is {}"
                .format(".".join([str(v) for v in __version__])))

        # Read main header
        hdrsize = bytes2int(f.read(UINT64.nbytes), UINT64)
        header = json.loads(f.read(hdrsize).decode())

        # Load replacements
        replacements = load_replacements(f)

        # Create decoded object dump
        object_dump = decode(header, replacements)

    return object_dump


def header(fname):
    """ Returns the header of a TIRLFile. """
    with open(fname, "rb") as fp:
        fp.seek(len(MAGIC) + 2 * UINT8.nbytes)
        hdrsize = bytes2int(fp.read(UINT64.nbytes), UINT64)
        hdr = fp.read(hdrsize)
    return json.loads(hdr.decode())


def info(fname):
    """
    Prints the formatted file header without parsing the binary part of the
    file.

    """
    from pygments import highlight, lexers, formatters
    hdr = header(fname)
    formatted_json = json.dumps(hdr, indent=4)
    colorful_json = highlight(formatted_json.encode("UTF-8"),
                              lexers.JsonLexer(),
                              formatters.TerminalFormatter())
    print(colorful_json)


def encode(node):
    """
    Recursive function that traverses an object dump and replaces
    unserialisable elements with string codes. The replaced elements are
    collated in a dictionary with their respective string codes for separate
    processing.

    :returns: serialisable dump, replaced objects
    :rtype: tuple[Union[dict, tuple, list], dict]

    """
    replacements = dict()
    if "decoded_objects" not in replacements.keys():
        replacements["decoded_objects"] = dict()
    assert hasattr(node, "__iter__")

    if isinstance(node, dict):
        collection = dict()
        # Set fixed traversing order (needed for object redundancy mapping)
        iterator = ((key, node[key]) for key in sorted(node.keys()))
    else:
        collection = [0] * len(node)
        iterator = enumerate(node)

    for key, item in iterator:
        if isinstance(item, dict):
            # Label identical if an object with the same ID has been seen before
            if "id" in item.keys():  # object = "id" key exists in a dict
                if item["id"] in replacements.keys():
                    collection[key] = "<obj>" + item["id"] + "</obj>"
                    continue
            # Otherwise process the dict recursively
            sub_coll, sub_rep = encode(item)
            collection[key] = sub_coll
            dobjs = sub_rep.pop("decoded_objects")
            replacements.update(sub_rep)
            replacements["decoded_objects"].update(dobjs)
            # Store the encoded object dump if it was seen for the first time
            if "id" in item.keys():
                replacements.update({item["id"]: sub_coll})
        elif isinstance(item, (np.memmap, np.ndarray, bytes)):
            objkey = str(id(item))
            replacements["decoded_objects"].update({objkey: item})
            collection[key] = "<obj>" + objkey + "</obj>"
        elif isinstance(item, (tuple, list)):
            sub_coll, sub_rep = encode(item)
            dobjs = sub_rep.pop("decoded_objects")
            replacements.update(sub_rep)
            replacements["decoded_objects"].update(dobjs)
            if isinstance(item, tuple):
                collection[key] = ["<tuple>"] + sub_coll + ["</tuple>"]
            else:
                collection[key] = sub_coll
        elif inspect.isclass(item):
            collection[key] = "<class>" + item.__name__ + "</class>"
        elif isinstance(item, complex):
            collection[key] = "<complex>{}</complex>".format(str(item))
        else:
            collection[key] = item

    return collection, replacements


def decode(encoded_dump, replacements):
    """
    Creates an object dump from an encoded dump by replacing string codes
    with the original unserialisable elements stored in the replacements
    dictionary (arrays and memory maps).

    :param encoded_dump: encoded object dump
    :type encoded_dump: Union[dict, list, tuple]
    :param replacements: a dictionary of encoded elements
    :type replacements: dict

    :returns: decoded object dump
    :rtype: dict

    """
    assert hasattr(encoded_dump, "__iter__")
    if "decoded_objects" not in replacements.keys():
        replacements["decoded_objects"] = dict()
    objs = replacements["decoded_objects"]

    if isinstance(encoded_dump, dict):
        collection = dict()
        iterator = \
            ((key, encoded_dump[key]) for key in sorted(encoded_dump.keys()))
    else:
        collection = [0] * len(encoded_dump)
        iterator = enumerate(encoded_dump)

    def istagged(item, tag):
        bra = "<{}>".format(tag)
        ket = "</{}>".format(tag)
        return item.startswith(bra) and item.endswith(ket)

    def getid(s):
        return re.findall(r"(?<=>).+(?=<)", s)[0]

    for key, item in iterator:
        if isinstance(item, dict):
            sub_coll = decode(item, replacements)
            collection[key] = sub_coll
        elif isinstance(item, (tuple, list)):
            sub_coll = decode(item, replacements)
            if sub_coll and \
                    sub_coll[0] == "<tuple>" and sub_coll[-1] == "</tuple>":
                collection[key] = tuple(sub_coll[1:-1])
            else:
                collection[key] = sub_coll
        elif isinstance(item, str):
            if istagged(item, "obj"):
                objid = getid(item)
                # Decode object on its first encounter
                if objid not in objs.keys():
                    if isinstance(replacements[objid], dict):
                        obj = decode(replacements[objid], replacements)
                    else:
                        obj = replacements[objid]
                    objs[objid] = obj
                    # Once decoded, discard the encoded version of the object
                    replacements.pop(objid)
                # Replace string placeholder with the object
                collection[key] = objs[objid]
            elif istagged(item, "class"):
                collection[key] = locate(getid(item))
            elif istagged(item, "complex"):
                collection[key] = complex(getid(item))
            else:
                collection[key] = item
        else:
            collection[key] = item

    return collection
