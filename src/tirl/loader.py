#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import imageio
import numpy as np
from operator import add
from functools import reduce


# TIRL IMPORTS

from tirl import utils as tu, settings as ts

# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class GenericLoader(object):

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        self.dtype = dtype
        self.storage = storage
        self.kwargs = kwargs

    def __call__(self, f):
        """
        Loads image data from file into array format.

        :param f: file path
        :type f: str

        :returns: tuple: image array, image header
        :rtype: tuple[Union[np.ndarray, np.memmap], dict]

        """
        if not os.path.isfile(f):
            raise FileNotFoundError("File not found: {}".format(f))


class ImageIOLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = \
        reduce(add, (f.extensions for f in imageio.formats._formats)) + \
        (".pnm",)

    def __init__(self, method="imread", storage=MEM, dtype=None, **kwargs):
        super(ImageIOLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)
        self.method = method

    def __call__(self, f):
        super(ImageIOLoader, self).__call__(f)
        img = getattr(imageio, self.method)(f, **self.kwargs)
        meta = dict(img.meta)
        dtype = self.dtype or img.dtype
        if self.storage == MEM:
            data = np.ascontiguousarray(img, dtype=dtype)
        elif self.storage == HDD:
            import tempfile
            fd, fname = tempfile.mkstemp(prefix="imgio_", dir=ts.TWD)
            data = np.memmap(fname, dtype=img.dtype, mode="r+", offset=0,
                          shape=img.shape, order="C")
            data[...] = np.frombuffer(img, dtype=img.dtype, count=img.size,
                                      offset=0).reshape(img.shape)
            data.flush()
            import tirl.utils as tu
            import psutil
            memlimit = psutil.virtual_memory().available
            data = tu.change_memmap_dtype(data, dtype, memlimit)
        else:
            raise ValueError("Unknown storage option: {}".format(self.storage))
        hdr = {"meta": meta,
               "input_file": os.path.abspath(f)}

        return data, hdr


class TiffLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".svs", ".tif", ".tiff")

    def __init__(self, series=0, storage=HDD, dtype="uint8", **kwargs):
        super(TiffLoader, self).__init__(storage=storage, dtype=dtype, **kwargs)
        self.pages = kwargs.get("pages", None)
        self.series = series

    def __call__(self, f):
        super(TiffLoader, self).__call__(f)
        import tifffile as tiff
        tfobj = tiff.TiffFile(f)
        from operator import mul
        t = getattr(tfobj, "series")[self.series]
        nbytes = reduce(mul, t.shape) * np.dtype(t.dtype).itemsize
        # hdr = {"meta": tfobj}
        hdr = {}
        if self.storage == MEM:
            arr = tfobj.asarray(key=self.pages, series=self.series)
            hdr["input_file"] = os.path.abspath(f)
        else:
            import tempfile
            fileno, fname = tempfile.mkstemp(prefix="tiff_", dir=ts.TWD)
            hdr["input_file"] = os.path.abspath(fname)
            # with open(tmp[1], "wb") as f:
            #     f.seek(nbytes - 1)
            #     f.write(b"\x00")
            # f = open(tmp[1], "rb+")
            # from mmap import mmap
            # m = mmap(f.fileno(), nbytes, offset=0)
            # arr = np.ndarray(t.shape, t.dtype, buffer=m,
            #                  offset=0, order="C")
            arr = np.memmap(fname, dtype=t.dtype, mode="r+",
                            offset=0, shape=t.shape, order="C")
            # print("Created file.")
            tfobj.asarray(key=self.pages, series=self.series, out=arr,
                          maxworkers=ts.CPU_CORES or None)
            # print("Loaded image.")
            if t.dtype != self.dtype:
                import psutil
                memlimit = psutil.virtual_memory().available
                arr = tu.change_memmap_dtype(arr, self.dtype, memlimit)
            arr.flush()
            # f.close()

        return arr, hdr


class OpenSlideLoader(GenericLoader):

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, level=-1,
                 **kwargs):
        super(OpenSlideLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)
        self.level = level
        self.cpu = ts.CPU_CORES

    def __call__(self, f):
        super(OpenSlideLoader, self).__call__(f)
        import openslide
        import multiprocessing as mp
        from functools import partial
        import psutil
        import tempfile
        memlimit = psutil.virtual_memory().available
        memlimit = min(memlimit, self.kwargs.get("memlimit", memlimit))
        itemsize = 4 * 2 * np.dtype(self.dtype).itemsize
        batchsize = int(np.sqrt(memlimit / itemsize / self.cpu))

        # Load the slide object
        obj = openslide.open_slide(f)
        w, h = obj.level_dimensions[self.level]

        if h * w * itemsize >= memlimit:
            import warnings
            warnings.warn(f"The current memory limit ({memlimit / 1024 ** 2}) "
                          f"is lower than needed to load the object "
                          f"({h * w * itemsize / 1024 ** 2}). Switching to "
                          f"HDD mode.")
            self.storage = HDD

        # Create storage space and populate it with the histology image data
        if self.storage == MEM:
            arr = np.empty(shape=(h, w, 4), dtype=self.dtype, order="C")
            with mp.pool.ThreadPool(processes=self.cpu) as pool:
                jobfunc = partial(openslide_worker_thread, obj=obj,
                                  level=self.level, arr=arr)
                jobs = openslide_generate_jobs(h, w, batchsize)
                _ = pool.map(jobfunc, jobs)

        else:
            fileno, fname = tempfile.mkstemp(prefix="tiff_", dir=ts.TWD)
            arr = np.memmap(fname, dtype=self.dtype, mode="r+", offset=0,
                          shape=(h, w, 4), order="C")
            m = (fname, np.dtype(self.dtype).str, (h, w, 4))
            with mp.Pool(processes=ts.CPU_CORES) as pool:
                lock = mp.Manager().RLock()
                jobfunc = partial(openslide_worker_process, slide=f,
                                  level=self.level, arr=m, lock=lock)
                jobs = openslide_generate_jobs(h, w, batchsize)
                _ = pool.map(jobfunc, jobs)

        hdr = dict(input_file=f)
        # hdr = {
        #     "input_file": f,
        #     "meta": obj
        # }
        return arr, hdr


def openslide_worker_thread(job, obj, level, arr):
    row, endrow, col, endcol = job
    x = int(col * obj.level_downsamples[level])
    y = int(row * obj.level_downsamples[level])
    region = obj.read_region(
        (x, y), level=level, size=(endcol - col, endrow - row))
    region = np.asarray(region, dtype=arr.dtype)
    arr[row:endrow, col:endcol] = region[...]


def openslide_worker_process(job, slide, level, arr, lock):
    row, endrow, col, endcol = job
    import openslide
    obj = openslide.open_slide(slide)
    x = int(col * obj.level_downsamples[level])
    y = int(row * obj.level_downsamples[level])
    region = obj.read_region(
        (x, y), level=level, size=(endcol - col, endrow - row))
    lock.acquire()
    fname, dtype, (h, w, c) = arr
    offset = row * w * c * np.dtype(dtype).itemsize
    arr = np.memmap(fname, dtype=dtype, mode="r+", offset=offset,
                    shape=(endrow - row, w, c), order="C")
    arr[:, col:endcol] = np.asarray(region, dtype=arr.dtype)
    del arr
    lock.release()


def openslide_generate_jobs(h, w, batchsize):
    for row in range(0, h, batchsize):
        endrow = min(row + batchsize, h)
        for col in range(0, w, batchsize):
            endcol = min(col + batchsize, w)
            print(f"Loading ({row}, {col}) - ({endrow}, {endcol})")
            yield row, endrow, col, endcol


class NiBabelLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".nii", ".nii.gz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(NiBabelLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)

    def __call__(self, f):
        super(NiBabelLoader, self).__call__(f)
        try:
            import nibabel as nib
        except ImportError:
            raise ImportError(
                "{} has a missing dependency: nibabel.".format(
                    self.__class__.__name__))
        mri = nib.load(f)
        hdr = mri.header

        # Comply with the memory limit
        if self.storage == MEM:
            arr = mri.get_data()

        elif self.storage == HDD:
            if f.endswith(".gz"):
                # Unzip .nii.gz file to the temporary working directory
                import gzip
                with gzip.open(f, mode="rb") as niigz:
                    import tempfile
                    fd, f = tempfile.mkstemp(
                        dir=ts.TWD, prefix="nifti_", suffix=".nii")
                    with open(f, 'wb') as nii:
                        import shutil
                        shutil.copyfileobj(niigz, nii)
            mri = nib.load(f)
            arr = mri.dataobj.get_unscaled()

        else:
            raise ValueError("Unrecognised storage option: {}"
                             .format(self.storage))
        hdr = {"meta": hdr,
               "input_file": os.path.abspath(f)}

        return arr, hdr


class NumPyLoader(GenericLoader):

    SUPPORTED_IMAGE_TYPES = (".npy", ".npz")

    def __init__(self, storage=MEM, dtype=ts.DEFAULT_FLOAT_TYPE, **kwargs):
        super(NumPyLoader, self).__init__(
            storage=storage, dtype=dtype, **kwargs)

    def __call__(self, f):
        super(NumPyLoader, self).__call__(f)
        hdr = {"meta": {}}
        if self.storage == MEM:
            arr = np.load(f, mmap_mode=None)
            hdr["input_file"] = os.path.abspath(f)
        else:
            arr = np.load(f, mmap_mode="r+")
            hdr["input_file"] = arr.filename

        return arr, hdr


LOADER_PREFERENCES = {
    '.3fr': ImageIOLoader,
    '.arw': ImageIOLoader,
    '.avi': ImageIOLoader,
    '.bay': ImageIOLoader,
    '.bmp': ImageIOLoader,
    '.bmq': ImageIOLoader,
    '.bsdf': ImageIOLoader,
    '.bufr': ImageIOLoader,
    '.bw': ImageIOLoader,
    '.cap': ImageIOLoader,
    '.cine': ImageIOLoader,
    '.cr2': ImageIOLoader,
    '.crw': ImageIOLoader,
    '.cs1': ImageIOLoader,
    '.ct': ImageIOLoader,
    '.cur': ImageIOLoader,
    '.cut': ImageIOLoader,
    '.dc2': ImageIOLoader,
    '.dcm': ImageIOLoader,
    '.dcr': ImageIOLoader,
    '.dcx': ImageIOLoader,
    '.dds': ImageIOLoader,
    '.dicom': ImageIOLoader,
    '.dng': ImageIOLoader,
    '.drf': ImageIOLoader,
    '.dsc': ImageIOLoader,
    '.ecw': ImageIOLoader,
    '.emf': ImageIOLoader,
    '.eps': ImageIOLoader,
    '.erf': ImageIOLoader,
    '.exr': ImageIOLoader,
    '.fff': ImageIOLoader,
    '.fit': ImageIOLoader,
    '.fits': ImageIOLoader,
    '.flc': ImageIOLoader,
    '.fli': ImageIOLoader,
    '.foobar': ImageIOLoader,
    '.fpx': ImageIOLoader,
    '.ftc': ImageIOLoader,
    '.fts': ImageIOLoader,
    '.ftu': ImageIOLoader,
    '.fz': ImageIOLoader,
    '.g3': ImageIOLoader,
    '.gbr': ImageIOLoader,
    '.gdcm': ImageIOLoader,
    '.gif': ImageIOLoader,
    '.gipl': ImageIOLoader,
    '.grib': ImageIOLoader,
    '.h5': ImageIOLoader,
    '.hdf': ImageIOLoader,
    '.hdf5': ImageIOLoader,
    '.hdp': ImageIOLoader,
    '.hdr': ImageIOLoader,
    '.ia': ImageIOLoader,
    '.icns': ImageIOLoader,
    '.ico': ImageIOLoader,
    '.iff': ImageIOLoader,
    '.iim': ImageIOLoader,
    '.iiq': ImageIOLoader,
    '.im': ImageIOLoader,
    '.img': ImageIOLoader,
    '.ipl': ImageIOLoader,
    '.j2c': ImageIOLoader,
    '.j2k': ImageIOLoader,
    '.jfif': ImageIOLoader,
    '.jif': ImageIOLoader,
    '.jng': ImageIOLoader,
    '.jp2': ImageIOLoader,
    '.jpc': ImageIOLoader,
    '.jpe': ImageIOLoader,
    '.jpeg': ImageIOLoader,
    '.jpf': ImageIOLoader,
    '.jpg': ImageIOLoader,
    '.jpx': ImageIOLoader,
    '.jxr': ImageIOLoader,
    '.k25': ImageIOLoader,
    '.kc2': ImageIOLoader,
    '.kdc': ImageIOLoader,
    '.koa': ImageIOLoader,
    '.lbm': ImageIOLoader,
    '.lfp': ImageIOLoader,
    '.lfr': ImageIOLoader,
    '.lsm': ImageIOLoader,
    '.mdc': ImageIOLoader,
    '.mef': ImageIOLoader,
    '.mgh': ImageIOLoader,
    '.mgz': NiBabelLoader,
    '.mha': ImageIOLoader,
    '.mhd': ImageIOLoader,
    '.mic': ImageIOLoader,
    '.mkv': ImageIOLoader,
    '.mnc': ImageIOLoader,
    '.mnc2': ImageIOLoader,
    '.mos': ImageIOLoader,
    '.mov': ImageIOLoader,
    '.mp4': ImageIOLoader,
    '.mpeg': ImageIOLoader,
    '.mpg': ImageIOLoader,
    '.mpo': ImageIOLoader,
    '.mri': ImageIOLoader,
    '.mrw': ImageIOLoader,
    '.msp': ImageIOLoader,
    '.nef': ImageIOLoader,
    '.nhdr': ImageIOLoader,
    '.nia': ImageIOLoader,
    '.nii': NiBabelLoader,
    '.nii.gz': NiBabelLoader,
    '.nonexistentext': ImageIOLoader,
    '.npy': NumPyLoader,
    '.npz': NumPyLoader,
    '.nrrd': ImageIOLoader,
    '.nrw': ImageIOLoader,
    '.orf': ImageIOLoader,
    '.pbm': ImageIOLoader,
    '.pcd': ImageIOLoader,
    '.pct': ImageIOLoader,
    '.pcx': ImageIOLoader,
    '.pef': ImageIOLoader,
    '.pfm': ImageIOLoader,
    '.pgm': ImageIOLoader,
    '.pic': ImageIOLoader,
    '.pict': ImageIOLoader,
    '.png': ImageIOLoader,
    '.pnm': ImageIOLoader,
    '.ppm': ImageIOLoader,
    '.ps': ImageIOLoader,
    '.psd': ImageIOLoader,
    '.ptx': ImageIOLoader,
    '.pxn': ImageIOLoader,
    '.pxr': ImageIOLoader,
    '.qtk': ImageIOLoader,
    '.raf': ImageIOLoader,
    '.ras': ImageIOLoader,
    '.raw': ImageIOLoader,
    '.rdc': ImageIOLoader,
    '.rgb': ImageIOLoader,
    '.rgba': ImageIOLoader,
    '.rw2': ImageIOLoader,
    '.rwl': ImageIOLoader,
    '.rwz': ImageIOLoader,
    '.sgi': ImageIOLoader,
    '.spe': ImageIOLoader,
    '.sr2': ImageIOLoader,
    '.srf': ImageIOLoader,
    '.srw': ImageIOLoader,
    '.sti': ImageIOLoader,
    '.stk': ImageIOLoader,
    '.svs': TiffLoader,
    '.swf': ImageIOLoader,
    '.targa': ImageIOLoader,
    '.tga': ImageIOLoader,
    '.tif': TiffLoader,
    '.tiff': TiffLoader,
    '.vtk': ImageIOLoader,
    '.wap': ImageIOLoader,
    '.wbm': ImageIOLoader,
    '.wbmp': ImageIOLoader,
    '.wdp': ImageIOLoader,
    '.webp': ImageIOLoader,
    '.wmf': ImageIOLoader,
    '.wmv': ImageIOLoader,
    '.xbm': ImageIOLoader,
    '.xpm': ImageIOLoader
}
