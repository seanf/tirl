#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
23-June-2020 Istvan N Huszar
This class is not currently used in scripts, but may be used later.
Marked as beta, as it is not tested.


(31-May-2020 Istvan N Huszar)
This optimiser class is primarily intended for the optimisation of 2D or 3D
dense deformation fields, and in particular with diffusion regularisation of
the parameters.

It is planned to enhance this class in the near future to support the
optimisation of linear transformations. Another improvement dwould be allowing
the simultaneous optimisation of linear and non-linear transformations, but
this requires more work.

"""


# DEPENDENCIES

import numpy as np
from time import time
import scipy.sparse as sp
from scipy.sparse.linalg import minres
from numbers import Integral, Real
from tirl.beta import BetaClassMeta

# TIRL IMPORTS

from tirl.optimisation.optimiser import Optimiser
from tirl.regularisers.txregulariser import TransformationRegulariser


# DEFINITIONS

from tirl.constants import *

NO_PARAMETERS = 0
MAXITER_REACHED = 1
MAXTIME_REACHED = 2
XTOL_REL_REACHED = 3
XTOL_ABS_REACHED = 4
FTOL_REL_REACHED = 5
FTOL_ABS_REACHED = 6
COSTVAL_REACHED = 7
ASCENT = -1


# IMPLEMENTATION


class AOSOptimiser(Optimiser, metaclass=BetaClassMeta):
    """
    GNOptimiser -
        performs unidirectional registration that aligns a moving image to a
        fixed image by optimising the parameters of one or more transformations
        of the image domains. The optimiser uses parameter derivatives of
        transformations and regularisations, and spatial derivatives of image
        cost to optimise the transformation parameters by the Gauss-Newton
        method.

    """
    RESERVED_KWARGS = ("target", "cost_items", "maxiter", "maxtime", "xtol_rel",
                       "xtol_abs", "ftol_rel", "ftol_abs", "costval", "tau",
                       "alpha", "metaparameters")

    def __init__(self, target, *cost_items, maxiter=100, maxtime=0, xtol_rel=0,
                 xtol_abs=0, ftol_rel=0, ftol_abs=0, costval=None, tau=0.25,
                 alpha=5, **metaparameters):
        """
        Initialisation of GNOptimiser.

        :param target:
            Optimisation target. A Transformation or an OptimisationGroup that
            specifies the parameters for optimisation. Note that only the free
            (not locked) parameters will be optimised.
        :param cost_items:
        :param maxiter:
        :param maxtime:
        :param xtol_rel:
        :param xtol_abs:
        :param ftol_rel:
        :param ftol_abs:
        :param costval:
        :param metaparameters:

        """
        # Call parent-class initialisation
        super(AOSOptimiser, self).__init__(
            target, *cost_items, **metaparameters)

        # Set class-specific metaparameters
        self.maxiter = maxiter
        self.maxtime = maxtime
        self.xtol_rel = xtol_rel
        self.xtol_abs = xtol_abs
        self.ftol_rel = ftol_rel
        self.ftol_abs = ftol_abs
        self.costval = costval
        self.tau = tau
        self.alpha = alpha

        # There must be at least one cost object, otherwise the Optimiser
        # cannot reach the fixed and moving images.
        if not self.costs:
            raise AssertionError(
                f"{self.__class__.__name__} requires an image cost object.")

        # Accept only transformation regularisers
        for txregobj in self.regularisers:
            if not isinstance(txregobj, TransformationRegulariser):
                raise TypeError(f"{self.__class__.__name__} only accepts "
                                f"regularisers that are based on "
                                f"transformation parameters.")

        # Identify the source and the target images
        self.moving = self.costs[0]._source  # prefiltered source image
        self.fixed = self.costs[0].target   # prefiltered target image

        # Obtain the combined voxel-to-voxel transfomation chain
        self.chain = self.fixed.domain.offset + \
                     self.fixed.domain.chain + \
                     self.moving.domain.chain.inverse() + \
                     self.moving.domain.offset.inverse()
        # Identify the transformations being optimised within the combined chain
        self.indices = self._get_transformation_indices()
        # TODO: Consider reducing constant subchains outside the transformations

        # TODO: Temporary ban on simultaneous optimisation
        # At the moment, only the optimisation of a single non-linear
        # transformation is allowed. This ban must remain until the
        # implementation of the jacobian for linear transformations is finished.
        # <BAN>
        if len(self.indices) > 1:
            assert False, "Temporarily disabled feature."
        # for tx in self.chain[self.indices]:
        #     if tx.kind == TX_LINEAR:
        #         assert False, "Temporarily disabled feature."
        # </BAN>

        # DISABLED FEATURE: multiple non-linear transformations must not be
        # optimised simultaneously in the same chain.
        # Reason: it is quite hard to compute the propagation of an incremental
        # displacement through a non-linear transformation that is also
        # changing. While it is not impossible, it is expected that the
        # smoothness assumptions of the propagation will be violated in such a
        # scenario, hence the calculation is probably not worth the
        # (computational) effort.
        # Reason 2: non-linear transformations have a high degrees of freedom,
        # so optimising multiple of them simultaneously is likely going to be
        # one against the other. All in all, this feature is better be disabled
        # to avoid any confusion.
        nonlinear = map(lambda tx: tx.kind == TX_NONLINEAR,
                           self.chain[self.indices])
        if sum(nonlinear) > 1:
            raise AssertionError(
                "Optimisation of multiple non-linear transformations in the "
                "same chain is not allowed.")

        # Initialise previous parameter update vector and previous cost
        # (these are initial values for convergence testing)
        self.dp0 = np.zeros(self.transformation.parameters.size)
        self.f0 = np.inf

    @property
    def maxiter(self):
        return self.metaparameters.get("maxiter")

    @maxiter.setter
    def maxiter(self, count):
        if isinstance(count, Integral) and (count >= 0):
            self.metaparameters.update(maxiter=int(count))
        else:
            raise ValueError(f"Expected a nonnegative integer for maximum "
                             f"iteration count, got {count} instead.")

    @property
    def maxtime(self):
        return self.metaparameters.get("maxtime")

    @maxtime.setter
    def maxtime(self, interval):
        if isinstance(interval, Real) and (interval >= 0):
            self.metaparameters.update(maxtime=float(interval))
        else:
            raise ValueError(f"Expected a nonnegative float for maximum "
                             f"optimisation time, got {interval} instead.")

    @property
    def xtol_rel(self):
        return self.metaparameters.get("xtol_rel")

    @xtol_rel.setter
    def xtol_rel(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(xtol_rel=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for relative "
                             f"parameter change, got {val} instead.")

    @property
    def xtol_abs(self):
        return self.metaparameters.get("xtol_abs")

    @xtol_abs.setter
    def xtol_abs(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(xtol_abs=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for absolute "
                             f"parameter change, got {val} instead.")

    @property
    def ftol_rel(self):
        return self.metaparameters.get("ftol_rel")

    @ftol_rel.setter
    def ftol_rel(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(ftol_rel=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for relative "
                             f"cost value change, got {val} instead.")

    @property
    def ftol_abs(self):
        return self.metaparameters.get("ftol_abs")

    @ftol_abs.setter
    def ftol_abs(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(ftol_abs=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for absolute "
                             f"cost value change, got {val} instead.")

    @property
    def costval(self):
        return self.metaparameters.get("costval")

    @costval.setter
    def costval(self, val):
        if isinstance(val, Real):
            self.metaparameters.update(costval=float(val))
        elif val is None:
            self.metaparameters.update(costval=None)
        else:
            raise ValueError(f"Expected a float for target cost function "
                             f"value, got {val} instead.")

    def _get_transformation_indices(self):
        """
        Returns the index of the transformation(s) in the combined
        target->source (fixed->moving) chain. The return value is always a
        tuple.

        """
        if hasattr(self.transformation, "__tx_priority__"):
            txs = (self.transformation,)
        elif hasattr(self.transformation, "__iter__"):
            txs = tuple(self.transformation)
        else:
            raise AssertionError("Invalid target for optimisation.")

        ids = [id(tx) for tx in txs]
        indices = []

        for i, tx in enumerate(txs):
            try:
                indices.append(ids.index(id(tx)))
            except IndexError:
                raise AssertionError(
                    f"Transformation {i} in the optimisation target is not "
                    f"a member of the transformation chain between the moving "
                    f"and the fixed image.")
        else:
            return tuple(indices)

    @property
    def alpha(self):
        return self.metaparameters.get("alpha")

    @alpha.setter
    def alpha(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(alpha=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for regularisation "
                             f"weight, got {val} instead.")

    @property
    def tau(self):
        return self.metaparameters.get("tau")

    @tau.setter
    def tau(self, val):
        if isinstance(val, Real) and (val >= 0):
            self.metaparameters.update(tau=float(val))
        else:
            raise ValueError(f"Expected a nonnegative float for time step "
                             f"got {val} instead.")

    def __call__(self, *args, **kwargs):
        """
        Solves Euler-Lagrange equation of diffusion regularised registration
        using additive operator splitting.

        """
        niter = 0
        start_time = time()
        ndim = self.transformation.domain.ndim
        numel = self.transformation.domain.numel
        tau = self.tau
        n, m = self.fixed.vshape

        Bn = np.ones((3, n), dtype=np.float32)
        Bn[1] = -2
        Bn = sp.spdiags(Bn, [-1, 0, 1], n, m)
        Bm = np.ones((3, m), dtype=np.float32)
        Bm[1] = -2
        Bm = sp.spdiags(Bm, [-1, 0, 1], n, m)
        operators = [
            self.alpha *
            (sp.kron(np.eye(n) - ndim * tau * Bn, sp.eye(m), format="csr")),
            self.alpha *
            (sp.kron(sp.eye(n), np.eye(m) - ndim * tau * Bm, format="csr"))
        ]

        vc = self.fixed.domain.get_voxel_coordinates()
        vv = self.transformation.vectors()
        v2w = self.transformation.domain.offset + \
              self.transformation.domain.chain
        w2v = v2w.inverse()

        while True:
            niter += 1
            # print(f"Iteration: {niter}")

            for j in range(ndim):
                # Compute the j-th component of the force field
                f = -self.costs[0].jacobian_aos(j)
                vv = w2v.map_vector(vects=vv, coords=vc)[0].T
                u = vv[j]  #vv[j * numel:(j + 1) * numel]
                # u = self.transformation.parameters[j * numel:(j + 1) * numel]
                # Compute the AOS iterate
                v = 0
                for l in range(ndim):
                    A_dia = operators[l % ndim]
                    b = u.ravel() + tau * f.T.ravel()
                    v += minres(A_dia, b)[0]
                else:
                    indices = np.arange(j * numel, (j + 1) * numel)
                    vv = np.zeros((numel, ndim))
                    vv[:, j] = v / ndim
                    vv = v2w.map_vector(vects=vv, coords=vc)[0]
                    self.transformation.parameters.parameters[:] += \
                        vv.T.ravel()
                    # self.moving.evaluate(self.fixed.domain).preview()
            else:
                if self._converged(niter, start_time):
                    break

        # As the transformation parameters are update in-place, there is no
        # need to return them from this function.

    def _converged(self, niter, start_time):

        # Convergence by reaching maximum interation count
        if (self.maxiter > 0) and (niter >= self.maxiter):
            return MAXITER_REACHED
        if (self.maxtime > 0) and (time() - start_time >= self.maxtime):
            return MAXTIME_REACHED
        # if (self.xtol_rel > 0) and \
        #         (np.all(np.abs(dp / self.dp0) < self.xtol_rel)):
        #     return XTOL_REL_REACHED
        # if (self.xtol_abs > 0) and \
        #         (np.all(np.abs(dp - self.dp0) < self.xtol_abs)):
        #     return XTOL_ABS_REACHED

        f = self.costs[0]()

        # Convergence by diminishing relative cost update
        if (self.ftol_rel > 0) and (np.abs((f - self.f0) / f) < self.ftol_rel):
            self.f0 = f
            return FTOL_REL_REACHED
        # Convergence by diminishing absolute cost update
        if (self.ftol_abs > 0) and ((f - self.f0) < self.ftol_abs):
            self.f0 = f
            return FTOL_ABS_REACHED
        # Convergence by reaching a specific cost value
        if self.costval and (f <= self.costval):
            return COSTVAL_REACHED

    def _get_total_cost(self):
        """
        Calculates the total scalar cost, which is the sum of all image
        similarity cost terms and all transformation regularisation terms.

        """
        all_cost = self.costs + self.regularisers
        return sum(c() for c in all_cost)

    def _cumulative_tx_jacobian(self):
        """
        Calculates the total derivative of the voxelwise displacement vector
        with respect to the parameters of the optimisation target.

        This function accomplishes accounts for the following:
            - handles the derivative from all transformations being optimised
            - propagates the derivatives through the static parts of the
              transformation chain
            - propagates the derivatives through simultaneously optimised
              linear transformations (nonlinear not allowed)

        """
        # TODO: The linear Jacobians must be implemented for this functionality.
        # At the moment, while the above BAN is in place, this method only
        # provides a pass-through propagation of one incremental displacement
        # through static transformations. No simultaneous optimisations are
        # allowed.
        vc = self.fixed.domain.get_voxel_coordinates()
        ndim, n_points = self.fixed.domain.ndim, self.fixed.domain.numel

        # Currently we only assume one transformation. This will have to be
        # expanded into a for loop when there will be more.
        ix = self.indices[0]

        changing_tx = self.chain[ix]
        dudp = changing_tx.jacobian(self.chain[:ix].map(vc))

        # Pass through the subchain that follows the tx that is being opt.
        for static_tx in self.chain[ix + 1:]:
            if static_tx.kind == TX_LINEAR:
                dudp = static_tx.adjustment_matrix() @ dudp
            else:
                # TODO: Implement these!
                # Currently not implemented for generic and non-linear
                # transformations. Reason: need to map vectors in sparse
                # matrix without conversion to array.
                raise NotImplementedError()

        return dudp

    def _cumulative_reg_jacobian(self):
        """
        Returns the total derivative of regularisation cost with respect to the
        optimised parameter vector. Regularisation does not have to take
        propagation into account, but must account for regularisations that are
        defined for a subset of the entire parameter vector.

        """
        # Identify the transformations whose parameters are used to compute
        # the regularisation term. Note that the same transformation may be
        # the basis (target) of multiple regularisation objects.
        regularised_transformation_indices = []
        chain_ids = list(map(lambda tx: id(tx), self.chain))
        ignored = []
        drdp = 0

        # Loop over all regulariser objects
        for reg_no, regobj in enumerate(self.regularisers):
            # For now, ignore regularisers without a transformation
            if not isinstance(regobj, TransformationRegulariser):
                ignored.append(regobj)
                continue

            # Find the relevant transformation(s) in chain
            regbasis = regobj.transformation
            if hasattr(regbasis, "__tx_priority__"):
                txs = (regobj.transformation,)
            elif hasattr(regbasis, "__iter__") and \
                    all(hasattr(tx, "__tx_priority__") for tx in regbasis):
                txs = regobj.transformation
            else:
                raise TypeError(f"Expected Transformation or OptimisationGroup "
                                f"as the regularisation target, found "
                                f"{regbasis} instead.")
            for tx in txs:
                try:
                    ix = chain_ids.index(id(tx))
                except IndexError:
                    raise IndexError(f"Regularisation {reg_no} is based on a "
                                     f"transformation that is not part of the "
                                     f"transformation chain.")
                else:
                    # Appending preserves multiplicity
                    regularised_transformation_indices.append(ix)

            drdp += regobj.jacobian()


        # Account for the previously ignored regularisers that appear as a cost
        # term in the cost functional, but they are not based on transformation
        # parameter values (i.e. they are not TransformationRegularisers).
        # Note: there is no such type in TIRL v1.1. Should they appear later,
        # this part of the function should be changed to also calculate the
        # contribution of these to dr/dp(x).
        if ignored:
            raise NotImplementedError()
