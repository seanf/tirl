#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import logging
import warnings
from operator import add
from functools import reduce


# TIRL IMPORTS

from tirl import utils as tu, settings as ts
from tirl.tirlobject import TIRLObject
from tirl.transformations import Transformation, TransformationGroup
from tirl.costs.cost import Cost
from tirl.regularisers.regulariser import Regulariser
from tirl.optimisation.optgroup import OptimisationGroup


# IMPLEMENTATION

class Optimiser(TIRLObject):
    """
    Optimiser - generic optimisation base class.

    """
    RESERVED_KWARGS = ("target", "cost_items", "metaparameters")

    def __init__(self, target, *cost_items, logger=None, **metaparameters):
        """
        Initialisation of Optimiser.

        :param target:
            Transformation or OptimisationGroup object whose parameters will
            be optimised. During the optimisation, the parameters of this
            object will be updated in place.
        :type target:
            Transformation or OptimisationGroup or Iterable[Transformation]
        :param cost_items:
            One or more Cost or Regulariser objects that together define the
            objective function for the optimisation.
        :type cost_items: Cost or Regulariser
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger
        :param metaparameters:
            Additional keyword argument to the Optimiser object.
        :type metaparameters: Any

        """
        # Invoke TIRLObject initialisation
        super(Optimiser, self).__init__()

        # Set transformation or optimisationgroup as target
        self.transformation = target

        # Set cost and regularisation terms
        costs = []
        regularisers = []
        for i, item in enumerate(cost_items):
            if isinstance(item, Cost) and hasattr(item, "__call__"):
                costs.append(item)
            elif isinstance(item, Regulariser) and hasattr(item, "__call__"):
                regularisers.append(item)
            elif not hasattr(item, "__call__"):
                raise AttributeError(
                    f"Cost item No. {i + 1} is not callable: {item}")
            else:
                raise TypeError(
                    f"Expected Cost or Regulariser for "
                    f"{self.__class__.__name__}, "
                    f"got {item.__class__.__name__} instead.")
        else:
            self.costs = costs
            self.regularisers = regularisers

        # Sanity check
        if len(self.costs) + len(self.regularisers) == 0:
            raise ValueError("No optimisation cost was specified.")
        if self.transformation.get().size == 0:
            raise ValueError("No parameters to optimise. Are they all locked?")

        # Set metaparameters
        self.metaparameters = metaparameters
        self.logger = logger

    @property
    def metaparameters(self):
        return self._metaparameters

    @metaparameters.setter
    def metaparameters(self, meta):
        if isinstance(meta, dict):
            self._metaparameters = meta
        else:
            raise TypeError("Metaparameters must be set with a dict.")

    @property
    def logger(self):
        return self.metaparameters.get("logger")

    @logger.setter
    def logger(self, lgr):
        if lgr is None:
            self.metaparameters.update(logger=logging.getLogger())
        elif isinstance(lgr, str):
            self.metaparameters.update(logger=logging.getLogger(lgr))
        elif isinstance(lgr, logging.Logger):
            self.metaparameters.update(logger=lgr)
        else:
            raise TypeError(f"Expected logger name of logger instance, "
                            f"got {lgr} instead.")

    @property
    def costs(self):
        return self._costs

    @costs.setter
    def costs(self, c):
        if isinstance(c, Cost):
            self._costs = (c,)
        elif hasattr(c, "__iter__"):
            costs = tuple(c)
            if all(isinstance(item, Cost) for item in costs):
                self._costs = costs
            else:
                raise TypeError("Cost function terms must be defined by "
                                "one or more Cost objects.")
        else:
            raise TypeError("Unrecognised input for cost function terms.")

    @property
    def regularisers(self):
        return self._regularisers

    @regularisers.setter
    def regularisers(self, r):
        if isinstance(r, Regulariser):
            self._regularisers = (r,)
        elif hasattr(r, "__iter__"):
            regularisers = tuple(r)
            if all(isinstance(item, Regulariser) for item in regularisers):
                self._regularisers = regularisers
            else:
                raise TypeError("Regularisation terms must be defined by "
                                "one or more Regulariser objects.")
        else:
            raise TypeError("Unrecognised input for regularisation.")

    @property
    def transformation(self):
        return self._transformation

    @transformation.setter
    def transformation(self, tx):

        if isinstance(tx, (Transformation, OptimisationGroup)):
            self._transformation = tx

        # If the target is a transformation group, do not optimise the TG
        # directly, because it is too vulnerable to state changes. Instead,
        # create a temporary optimisation group from the transformations.

        # Here is an explanation on the vulnerability:
        # if any of the members of the transformation group are reassigned to
        # another transformation group after it was set as the optimisation
        # target, the parameter vector of the respective transformation object
        # gets detached from the transformation, hence no changes in the
        # transformation group will have any effect on the parameters of the
        # transformation object.

        # Beware: this replacement only happens at the top level,
        # transformation groups within an optimisation group are not unraveled!

        elif isinstance(tx, TransformationGroup):
            self._transformation = OptimisationGroup(
                tx.elements, **tx.metaparameters)
            warnings.warn(
                "TransformationGroups should not be optimised. "
                "An equivalent OptimisationGroup was created instead.")

        elif hasattr(tx, "__iter__") and \
                all(isinstance(elem, Transformation) for elem in tx):
            og = OptimisationGroup(*tuple(tx))
            self._transformation = og
            warnings.warn(
                f"An OptimisationGroup was automatically created for the "
                f"specified transformations ({len(tx)} tx -> {og.name}).")

        else:
            raise TypeError(
                f"Expected Transformation object or OptimisationGroup for "
                f"optimisation target, got {tx.__class__.__name__} instead.")

    def copy(self):
        """
        Copy constructor of Optimiser.

        :returns:
            New Optimiser instance. Note that the associated transformation,
            cost and regulariser objects are passed by reference, not by value
            (i.e. no copies are made of these objects).
        :rtype: Optimiser

        """
        tx = self.transformation
        cs = self.costs
        rs = self.regularisers
        cost_items = tuple(cs) + tuple(rs)
        metaparameters = tu.rcopy(self.metaparameters)
        return type(self)(tx, *cost_items, **metaparameters)

    def log(self, msg, *args, level=ts.PARAMETER_UPDATE_LOG_LEVEL, **kwargs):
        """
        This method wraps Logger.log().

        Logs an event to the Logger that has been configured for the optimiser
        object. Uses the level that is specified in PARAMETER_UPDATE_LOG_LEVEL
        as the default level for the logs.

        """
        self.logger.log(level, msg, *args, **kwargs)

    def total_cost(self):
        """
        Calculates the value of the combined objective function given the
        current set of parameters.

        :returns: objective function value
        :rtype: tuple[float]

        """
        all_cost = self.costs + self.regularisers
        # TODO: Make this parallel to speed up joint registration.
        ret = tuple(float(getattr(item, "__call__")()) for item in all_cost)
        ret = float(reduce(add, ret))
        return ret

    def __call__(self, *args, **kwargs):
        """
        Run the optimisation.

        """
        raise NotImplementedError()



if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
