#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral, Number

from tirl import utils as tu
from tirl.tirlobject import TIRLObject
from tirl.parameters import ParameterVector
from tirl.transformations.transformation import Transformation


# IMPLEMENTATION

class OptimisationGroup(TIRLObject):
    """
    The OptimisationGroup class is very similar to the TransformationGroup,
    allowing to group separate Transformation objects that need to be optimised
    collectively. The major difference between OG and TG is that OG does not
    link ParameterVectors to itself, hence allowing Transformations to coexist
    across multiple OGs.

    """

    def __init__(self, *transformations, **metaparameters):
        """
        Initialisation of OptimisationGroup.

        :param transformations:
            Transformation objects to optimise.
        :type transformations: Transformation
        :param metaparameters:
            Additional keyword arguments for OptimisationGroup.
        :type metaparameters: Any

        """
        super(OptimisationGroup, self).__init__()
        self.elements = transformations
        self.metaparameters = metaparameters

    def __eq__(self, other):
        """
        Equality operator. Two OptimisationGroups are identical if and only if
        all their elements are identical.

        """
        if not isinstance(other, OptimisationGroup):
            return False

        return (stx is otx for stx, otx in zip(self.elements, other.elements))

    def copy(self):
        """
        Copy constructor. Note that transformations are not copied, instead
        they are passed by reference.

        :returns: Identical OptimsiationGroup instance.
        :rtype: OptimisationGroup

        """
        metaparameters = tu.rcopy(self.metaparameters)
        obj = OptimisationGroup(*self.elements, **metaparameters)
        return obj

    @property
    def metaparameters(self):
        return self._metaparameters

    @metaparameters.setter
    def metaparameters(self, meta):
        if isinstance(meta, dict):
            self._metaparameters = meta
        else:
            raise TypeError(
                f"Expected a dict, got {meta.__class__.__name__} instead.")

    @property
    def elements(self):
        """
        :returns: Elements of the OptimisationGroup instance.
        :rtype: tuple[Transformation]
        """
        return self._elements

    @elements.setter
    def elements(self, txs):
        if hasattr(txs, "__iter__"):
            if all(isinstance(tx, Transformation) for tx in txs):
                self._elements = tuple(txs)
            elif (len(txs) == 1) and hasattr(txs[0], "__iter__"):
                self._elements = tuple(txs[0])
            else:
                raise TypeError("OptimisationGroup may only contain "
                                "Transformation objects.")
        else:
            raise TypeError("Elements must be defined with an iterable.")

    @property
    def parameters(self):
        """
        Returns a concatenated ParameterVector from the elements. Provided for
        compatibility with Transformation objects. Note that teh returned
        ParameterVector is a copy, and therefore changing its elements will not
        have any effect on the original Transformation objects.

        This is a read-only property.

        """
        params = []
        bounds = []
        locked = set()

        offset = 0
        for tx in self.elements:
            n_params = tx.parameters.size
            params.append(tx.parameters[:])
            bounds.append(tx.parameters.get_bounds())
            locked.update({val + offset for val in tx.parameters.locked})
            offset += n_params
        else:
            params = np.concatenate(params)
            lb, ub = np.concatenate(bounds, axis=0).T

        return ParameterVector(*params, dtype=params.dtype, bounds=(lb, ub),
                               lock=locked)

    def locked(self):
        """
        Returns a set of indices that correspond to the parameters that are in
        the locked state.

        """
        lock_indices = set()
        offset = 0
        for tx in self.elements:
            txlck = tx.parameters.locked
            nlck = len(txlck)
            newlocks = offset + np.fromiter(txlck, dtype=np.intp, count=nlck)
            lock_indices.update(set(newlocks))
            offset += tx.parameters.size
        return lock_indices

    def _distribute_mask_or_indices(self, mask_or_indices):
        """
        Generator that breaks down a common boolean mask or index specification
        for the group to index/mask specifications for individual
        transformations.

        """
        offset = 0
        for tx in self.elements:
            n_params = tx.parameters.size

            # Boolean mask input
            if isinstance(mask_or_indices, np.ndarray) and \
                    np.issubdtype(mask_or_indices.dtype, np.bool_):
                result = mask_or_indices[offset:offset + n_params]
                offset += n_params

            # Sequence of indices
            elif hasattr(mask_or_indices, "__iter__") and \
                    all(isinstance(ix, Integral) for ix in mask_or_indices):
                result = np.array([
                    ix for ix in mask_or_indices
                    if (ix >= offset) and (ix < (offset + n_params))])
                result -= offset
                offset += n_params

            # None: retrieve free (non-locked) parameter indices
            elif mask_or_indices is None:
                if tx.parameters.locked:
                    free = set(range(n_params)).difference(tx.parameters.locked)
                    result = np.fromiter(free, dtype=np.intp, count=len(free))
                else:
                    result = None # np.full(n_params, True, np.bool_)

            else:  # pragma: no cover
                # This should never be reached
                raise TypeError(
                    f"Expected boolean array or index set, got "
                    f"{mask_or_indices.__class__.__name__} instead.")

            yield result

    def _distribute_single_boundspec(self, bounds):
        """
        Generator that breaks down a common lower/upper bounds specification to
        separate lower/upper bounds specifications for individual
        transformations.

        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. (lb0, lb1, ...) - sequence of lower bounds for each
               parameter. The order must correspond to the order of the
               parameters.
            2. {param_no: lb} - dictionary of lower bound values, where the
               keys represent parameter indices (starting from 0).
            3. lb_arr - flat iterable with lower bounds for all parameters
            4. "auto" - the get_default_bounds() method is called to create
               default lower bounds.
            5. None - parameter bounds are not set.
            6. lb - a single scalar value to set lower bounds globally
        :type bounds: Union[tuple, list, dict, str]

        """
        # "auto"/None -> list of "auto"/None for each transformation
        if (bounds is None):
            for _ in self.elements:
                yield bounds

        elif isinstance(bounds, str) and (bounds == "auto"):
            for _ in self.elements:
                yield bounds

        # Global bound value -> list of global bound values per transformation
        elif isinstance(bounds, Number):
            for _ in self.elements:
                yield bounds

        # Sequence of lower bounds -> list of sequences of lower bounds
        elif hasattr(bounds, "__iter__") and \
                all(isinstance(val, Number) for val in bounds):
            bounds = np.asarray(bounds)
            offset = 0
            for tx in self.elements:
                n_params = tx.parameters.size
                current_bounds = bounds[offset:offset + n_params]
                offset += n_params
                yield current_bounds

        # {index: lower bound} -> list of dicts per transformation
        elif isinstance(bounds, dict):
            assert all(isinstance(key, Integral) for key in bounds.keys())
            offset = 0
            for tx in self.elements:
                n_params = tx.parameters.size
                current_bounds = \
                    {k: v - offset for k, v in bounds.items()
                     if (k >= offset) and (k < (offset + n_params))}
                offset += n_params
                yield current_bounds

        else:  # pragma: no cover
            # This should never be reached
            raise TypeError(
                f"Invalid bound specification for {self.__class__.__name__}.")

    def _distribute_double_boundspec(self, bound, *other):
        """
        Generator that breaks down a common lower+upper bounds specification to
        separate lower+upper bounds specifications for individual
        transformations.

        :param bound:
            Parameter bounds can be defined in one of the following formats:
            1. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            2. (lb_arr, ub_arr) - flat iterable with lower and upper bounds
               for all parameters.
            3. "auto" (default) - the get_default_bounds() method is called to
               create default lower and upper bounds.
            4. None - parameter bounds are not set.
            5. lb, ub - global scalar lower and upper bounds
        :type bound: Union[tuple, list, dict, str]
        :param other:
            Auxiliary bound specification. Based on 'bound', this can be empty,
            a global upper bound specification, or a sequence of (lb_i, ub_i)
            pairs.
        :type other: Union[int, float]

        """
        # "auto"/None -> list of "auto"/None for each transformation
        if bound is None:
            for _ in self.elements:
                yield bound, other

        elif isinstance(bound, str) and (bound == "auto"):
            for _ in self.elements:
                yield bound, other

        # Global bound value pair -> list of global bound value pairs
        elif isinstance(bound, Number) and isinstance(other[0], Number):
            for _ in self.elements:
                yield bound, other

        # Sequence of lower bounds -> list of sequences of lower bounds
        elif isinstance(bound, np.ndarray) and isinstance(other[0], np.ndarray):
            offset = 0
            for tx in self.elements:
                n_params = tx.parameters.size
                lb = bound[offset:offset + n_params]
                ub = other[0][offset:offset + n_params]
                offset += n_params
                yield lb, (ub,)

        # {index: lower bound} -> list of dicts per transformation
        if isinstance(bound, dict):
            assert all(isinstance(key, Integral) for key in bound.keys())
            offset = 0
            for tx in self.elements:
                n_params = tx.parameters.size
                current_bounds = \
                    {k: (lb - offset, ub - offset)
                     for k, (lb, ub) in bound.items()
                     if (k >= offset) and (k < (offset + n_params))}
                offset += n_params
                yield current_bounds, other

        else:  # pragma: no cover
            # This should never be reached
            raise TypeError(
                f"Invalid bound specification for {self.__class__.__name__}.")

    def get(self, mask_or_indices=None):
        """
        Returns the specified subset of transformation parameters. This method
        is a wrapper around Transformation.get(), providing additional
        flexibility and syntatic ease for accessing parameters without
        explicitly having to change to the lock state of the parameters in
        advance.

        Note that the returned parameter array is a unidirectional link to the
        transformation parameters, and changing the array in place will not
        affect any of the transformation objects.

        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be returned.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        :returns: specified subset of the transformation parameters
        :rtype: np.ndarray

        :returns: concatenated flat parameter array
        :rtype: np.ndarray

        """
        group_parameters = []
        selections = self._distribute_mask_or_indices(mask_or_indices)
        for tx, selection in zip(self.elements, selections):
            group_parameters.append(tx.get(mask_or_indices=selection))
        return np.concatenate(group_parameters)

    def update(self, delta, mask_or_indices=None):
        """
        Updates the specified transformation parameters by the given amount.
        Use either a sequence of indices or a boolean parameter mask to
        select a subset of parameters to be updated. The method preserves
        dynamic linking.

        To implement a specific update rule, such as p1 = p2 + p3, subclass the
        most suitable Transformation object, and override the update method of
        the child class to implement the specific update rule. Make sure that
        the original lock state of the parameters is restored after calling the
        update() method.

        :param delta:
            Absolute parameter increments for the selected parameters. Delta
            must be either a scalar (to increment parameters globally), or a
            flat array that has one element corresponding to each selected
            (aka free, non-locked) parameter (to increment parameters
            individually).
        :type delta: Union[int, float, np.number, np.ndarray]
        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be updated.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        """
        offset = 0
        selections = self._distribute_mask_or_indices(mask_or_indices)
        for tx, selection in zip(self.elements, selections):
            n_free = tx.parameters.size if selection is None else selection.size
            current_delta = delta[offset:offset + n_free]
            tx.update(current_delta, mask_or_indices=selection)
            offset += n_free

    def set(self, parameters, mask_or_indices=None):
        """
        Replaces the values of the specified subset of transformation
        parameters with the ones provided. This method is a wrapper around
        Transformation.set(), providing additional flexibility and syntatic
        ease for modifying parameters of the OptimisationGroup without
        explicitly having to change the lock state of the parameters in
        advance. The method preserves dynamic linking.

        :param parameters: new values for the specified parameters
        :type parameters: Union[tuple[float], list[float], np.ndarray]
        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be returned.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        """
        offset = 0
        selections = self._distribute_mask_or_indices(mask_or_indices)
        for tx, selection in zip(self.elements, selections):
            n_free = tx.parameters.size if selection is None else selection.size
            current_params = parameters[offset:offset + n_free]
            tx.set(current_params, mask_or_indices=selection)
            offset += n_free

    def get_lower_bounds(self, mask_or_indices=None):
        """
        Returns the lower bounds for the specified subset of group parameters.
        This method is a wrapper around ParameterVector.get_lower_bounds().

        Note that the returned lower bound array is a unidirectional link to
        the lower bounds of the transformation objects, therefore changing the
        array in place will not have any effect on the transformation objects.

        """
        lower_bounds = []
        selections = self._distribute_mask_or_indices(mask_or_indices)
        for tx, selection in zip(self.elements, selections):
            if mask_or_indices is not None:
                mask = tu.indices2mask(mask_or_indices,
                                       maxindex=tx.parameters.size)
                lower_bounds.append(tx.parameters.get_lower_bounds(mask))
            else:
                lower_bounds.append(tx.parameters.get_lower_bounds())
        return np.concatenate(lower_bounds)

    def set_lower_bounds(self, bounds="auto"):
        """
        Sets lower bounds for all group parameters (including locked ones).
        This method is a wrapper around ParameterVector.set_lower_bounds().

        """
        boundspecs = self._distribute_single_boundspec(bounds)
        for tx, boundspec in zip(self.elements, boundspecs):
            tx.parameters.set_lower_bounds(boundspec)

    def get_upper_bounds(self, mask_or_indices=None):
        """
        Returns the upper bounds for the specified subset of group parameters.
        This method is a wrapper around ParameterVector.get_upper_bounds().

        Note that the returned upper bound array is a unidirectional link to
        the upper bounds of the transformation objects, therefore changing the
        array in place will not have any effect on the transformation objects.

        """
        upper_bounds = []
        selections = self._distribute_mask_or_indices(mask_or_indices)
        for tx, selection in zip(self.elements, selections):
            if mask_or_indices is not None:
                mask = tu.indices2mask(mask_or_indices,
                                       maxindex=tx.parameters.size)
                upper_bounds.append(tx.parameters.get_upper_bounds(mask))
            else:
                upper_bounds.append(tx.parameters.get_upper_bounds())
        return np.concatenate(upper_bounds)

    def set_upper_bounds(self, bounds="auto"):
        """
        Sets upper bounds for all group parameters (including locked ones).
        This method is a wrapper around ParameterVector.set_upper_bounds().

        """
        boundspecs = self._distribute_single_boundspec(bounds)
        for tx, boundspec in zip(self.elements, boundspecs):
            tx.parameters.set_upper_bounds(boundspec)

    def get_bounds(self, mask_or_indices=None):
        """
        Returns both the lower and upper bounds for the specified subset of
        group parameters. This method is a wrapper around
        ParameterVector.get_bounds().

        Note that the returned arrays are unidirectional links to the upper
        bounds of the transformation objects, therefore changing the arrays in
        place will not have any effect on the transformation objects.

        """
        bounds = []
        selections = self._distribute_mask_or_indices(mask_or_indices)
        for tx, selection in zip(self.elements, selections):
            if mask_or_indices is not None:
                mask = tu.indices2mask(mask_or_indices,
                                       maxindex=tx.parameters.size)
                bounds.append(tx.parameters.get_bounds(mask))
            else:
                bounds.append(tx.parameters.get_bounds())
        return np.concatenate(bounds, axis=0)

    def set_bounds(self, bound="auto", *other):
        """
        Simultaneously sets lower and upper bounds for all group parameters
        (including locked ones).

        This method is a wrapper around ParameterVector.set_bounds().

        """
        args = self._distribute_double_boundspec(bound, *other)
        for tx, (bound, other) in zip(self.elements, args):
            tx.parameters.set_bounds(bound, *other)


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
