#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Real
from scipy.ndimage.filters import gaussian_filter

# TIRL IMPORTS

from tirl.tfield import TField
from tirl import settings as ts
from tirl.costs.msd import CostMSD
from tirl.costs.cost import combine_filters
# from tirl.operations.tensor import TensorOperator
# from tirl.operations.spatial import SpatialOperator


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class CostMIND(CostMSD):
    """
    CostMIND -
        Mean Squared Difference of
        Modality-Independent Neighbourhood Descriptors

    Applies the MIND filter on both the source and the target image and
    computes the scalar cost as the mean squared difference of the
    corresponding MIND descriptors.

    For a detaild discussion on MIND descriptors see (Heinrich, 2012).

    """
    RESERVED_KWARGS = (
        "source", "target", "target_filter", "source_prefilter",
        "source_postfilter", "maskmode", "matchmode", "kernel", "sigma",
        "truncate", "logger", "metaparameters")

    def __init__(self, source, target, target_filter=None,
                 source_prefilter=None, source_postfilter=None,
                 maskmode="and", matchmode="l2_norm", normalise=True,
                 kernel=MK_FULL, sigma=1.0, truncate=1.5, logger=None,
                 **metaparameters):
        """
        Initialisation of CostMIND.

        """
        # Set class-specific metaparameters
        # The reason this must be set first is that the parent-class
        # initialiser filters the target image at construction. The
        # metaparameters are therefore required to perform this filtering.

        self._metaparameters = dict(ndim=target.vdim)
        self.kernel = kernel
        self.sigma = sigma
        self.truncate = truncate

        # Call parent-class initialisation
        metaparameters.update(**self._metaparameters)
        super(CostMIND, self).__init__(
            source, target,
            target_filter=combine_filters(target_filter, self.mind),
            source_prefilter=source_prefilter,
            source_postfilter=combine_filters(source_postfilter, self.mind),
            maskmode=maskmode, matchmode=matchmode, normalise=normalise,
            logger=logger, **metaparameters)

        self._metaparameters.update(metaparameters)

    @property
    def ndim(self):
        """ This is a read-only property. """
        return self.metaparameters.get("ndim")

    @property
    def kernel(self):
        return self.metaparameters.get("kernel")

    @kernel.setter
    def kernel(self, k):
        """
        Sets neighbourhood kernel.

        """
        ndim = self.ndim

        # Create array kernels for default inputs
        if k == MK_FULL:
            k = np.ones((3,) * ndim, dtype=np.bool_)
            k[1, 1] = 0
        elif k == MK_STAR:
            k = np.zeros((3,) * ndim)
            k[1] = True
            k[:, 1] = True
            k[1, 1] = 0

        # Store if input is array, or has been turned into an array
        if hasattr(k, "__array__"):
            k = (np.asarray(k) > 0).astype(np.bool_)
            if k.ndim == ndim:
                self.metaparameters.update(kernel=k)
            else:
                raise ValueError(
                    f"Kernel dimensions ({k.ndim}) do not match the "
                    f"dimensions of the target domain ({ndim}).")

        else:
            raise TypeError(f"Expected boolean/binary array for kernel, "
                            f"got {k} instead.")

    @property
    def sigma(self):
        return self.metaparameters.get("sigma")

    @sigma.setter
    def sigma(self, s):
        if isinstance(s, Real) and (s >= 0):
            self.metaparameters.update(sigma=s)
        else:
            raise TypeError(f"Expected nonnegative float for sigma, "
                            f"got {s} instead.")

    @property
    def truncate(self):
        return self.metaparameters.get("truncate")

    @truncate.setter
    def truncate(self, t):
        if isinstance(t, Real) and (t >= 0):
            self.metaparameters.update(truncate=t)
        else:
            raise TypeError(f"Expected nonnegative float for truncation point, "
                            f"got {t} instead.")

    @staticmethod
    def _get_shifts(kernel):
        centre = [dim // 2 for dim in kernel.shape]
        return np.subtract(np.stack(np.nonzero(kernel), axis=-1), centre)

    def mind_mem(self, img, kernel, sigma, truncate):
        """
        Implements the in-memory calculation of the Modality-Independent
        Neighbourhood Descriptor (MIND) representation of a TImage. This method
        allows fast computation, if the storage modes of the image is "MEM".

        :param img: input TImage
        :type img: TImage

        :returns: MIND representation of the input image
        :rtype: TImage

        """
        orig_order = img.order
        img.order = TENSOR_MAJOR

        # Initialise output TImage
        shifts = self._get_shifts(kernel)
        n_neighbours, ndim = shifts.shape
        mind_tshape = (n_neighbours,) + img.tshape
        ip = img.interpolator.copy()
        name = img.name + "_mind"
        args = ("extent", "tensor_shape", "order", "dtype",
                "interpolator", "name", "storage")
        kwargs = {k: v for k, v in img.kwargs.items() if k not in args}
        out = TField(img.domain, mind_tshape, order=TENSOR_MAJOR,
                     dtype=ts.DEFAULT_FLOAT_TYPE, interpolator=ip, name=name,
                     storage=MEM, **kwargs)
        out = img.fromTField(out, copy=False)
        # Restore the mask but discard the resolution manager
        out.mask = img.mask
        # Restore the axis order
        out.order = img.order

        vc = img.domain.get_voxel_coordinates()
        if img.mask is not None:
            masked = np.flatnonzero(np.isclose(img.mask, 0, atol=1e-2))
        else:
            masked = []

        # Calculate patch-distance of the central voxel from each neighbour
        for i, vector in enumerate(shifts):

            # Shift the image such that the central voxel
            # overlaps with the specific neighbour
            img_shifted = img.copy()
            for axis, vi in zip(img.vaxes, vector):
                if vi:
                    img_shifted.data[...] = np.roll(img_shifted.data, vi, axis)

            if img.mask is not None:
                ssdmask = img.mask.copy()
            else:
                ssdmask = np.ones(img.vshape, dtype=np.float32)

            # Calculate voxelwise squared differences over the entire image
            ssd = (img_shifted.data - img.data) ** 2
            del img_shifted

            # Zero the difference on the edges (Neumann)
            # This is to prevent "sticky edges"
            test_coordinates = vc - vector
            bindices = [np.flatnonzero(np.any(test_coordinates < 0, axis=-1))]
            for dim in range(img.vdim):
                ix = np.flatnonzero(test_coordinates[:, dim] >= img.vshape[dim])
                bindices.append(ix)
            else:
                bindices = np.unique(np.concatenate(bindices))
            ssd.flat[bindices] = 0
            ssdmask.flat[bindices] = 0

            # Treat mask edges the same as image borders
            mindices = []
            if self.metaparameters.get("ignore_masked_edges", False):
                tc = np.delete(test_coordinates, bindices, axis=0)
                vcc = np.delete(vc, bindices, axis=0)
                masked = np.asarray(masked)
                mindices = np.ravel_multi_index(tc.T, img.vshape)
                mindices = np.ravel_multi_index(
                    vcc[np.in1d(mindices, masked), :].T, img.vshape)
                ssd.flat[mindices] = 0
                ssdmask.flat[mindices] = 0
                ssd.flat[masked] = 0
                ssdmask.flat[masked] = 0

            # Convert distances to patch-wise distance
            # This step ensures robust estimation of the distance
            # from the neighbour. Do not smooth across tensor values.
            sigma_nd = (sigma,) * ndim + (0,) * img.tdim
            if self.metaparameters.get("ignore_masked_regions", False):
                tmp = \
                    gaussian_filter(ssd, sigma=sigma_nd, truncate=truncate) / \
                    gaussian_filter(ssdmask, sigma=sigma_nd, truncate=truncate)
            else:
                tmp = gaussian_filter(ssd, sigma=sigma_nd, truncate=truncate)
            tmp[~np.isfinite(tmp)] = 0
            tmp.flat[bindices] = 0
            if self.metaparameters.get("ignore_masked_edges", False):
                tmp.flat[mindices] = 0
            out.tensors[i] = tmp

            del ssd, tmp

        # Calculate MIND descriptor from patch distances
        # This is essentially making MIND vectors universally comparable
        tmp = out.data
        variance = np.mean(tmp, axis=0, keepdims=True)
        low, high = np.asarray([1e-3, 1e3]) * np.mean(variance)  # robustness
        variance = np.clip(variance, low, high)
        tmp = np.exp(-tmp / variance)

        del variance
        out.data[...] = tmp / np.max(tmp, 0, keepdims=True)
        # out.data[~np.isfinite(out.data)] = 0

        img.order = orig_order
        return out

    def mind_hdd(self, img, kernel, sigma, truncate):
        # TODO: Implement the HDD version in a future version.
        # Reason: implementation from previous version has become obsolete.
        return NotImplementedError()

    # @staticmethod
    # def mind_hdd(img, inner_radius=0, outer_radius=1, patch_radius=1,
    #              truncate=1.5, copy=False, n_threads=None, **kwargs):
    #     """
    #     Calculates the Modality-Independent Neighbourhood Descriptor (MIND)
    #     representation of a TImage.
    #
    #     :param img: input TImage
    #     :type img: TImage
    #     :param inner_radius: inner radius of voxel neighbourhood
    #     :type inner_radius: Union[int, np.integer]
    #     :param outer_radius: outer radius of voxel neighbourhood
    #     :type outer_radius: Union[int, np.integer]
    #     :param patch_radius: pooling radius
    #     :type patch_radius: Union[int, float, np.integer, np.floating]
    #     :param truncate: Gaussian kernel size in multiples of sigma
    #     :type truncate: Union[int, float, np.integer, np.floating]
    #     :param copy:
    #         If True, the shifted image is created as a ghost above the original
    #         image. If 'copy' is False (default), empty space is filled with
    #         zeros.
    #     :type copy: bool
    #     :param n_threads:
    #         number of computing threads (default: number of pixel neighbours)
    #     :type n_threads: Union[int, NoneType]
    #
    #     :returns: MIND representation of the input image
    #     :rtype: TImage
    #
    #     """
    #     # Validate MIND arguments
    #     if ts.TYPESAFE_MODE:
    #         CostMIND._validate_mind_args(
    #             inner_radius=inner_radius, outer_radius=outer_radius,
    #             patch_radius=patch_radius, truncate=truncate, copy=copy,
    #             n_threads=n_threads)
    #
    #     # Define neighbourhood
    #     radius = outer_radius - inner_radius
    #     vdim = img.vdim
    #     if radius > 0:
    #         st = range(-radius - inner_radius, inner_radius + radius + 1, 1)
    #         neighbourhood = set(product(*((st,) * vdim)))
    #         st = range(-inner_radius, inner_radius + 1, 1)
    #         core = set(product(*((st,) * vdim)))
    #         neighbourhood = sorted(neighbourhood.difference(core))
    #     else:
    #         ps = (inner_radius + 1,) + (0,) * (vdim - 1)
    #         ns = (-1 - inner_radius,) + (0,) * (vdim - 1)
    #         neighbourhood = set(permutations(ps)).union(set(permutations(ns)))
    #         neighbourhood = sorted(neighbourhood)
    #
    #     # Create TField container for MIND
    #     mind_tshape = (len(neighbourhood), *img.tshape[:img.tdim])
    #     name = img.name + "_mind"
    #     ip = img.interpolator.__class__
    #     mind = TField(img.domain, tensor_shape=mind_tshape, order=img.order,
    #                   dtype=ts.DEFAULT_FLOAT_TYPE, buffer=None, offset=0,
    #                   interpolator=ip, name=name, storage=img.storage,
    #                   **img.kwargs.copy())
    #     mind.interpolator.kwargs.update(img.interpolator.kwargs.copy())
    #     # mind.interpolator.kwargs.update({"mode": "nearest"})
    #
    #     # Calculate MIND (in parallel, if possible)
    #     # Bugfix: set the img order to TENSOR_MAJOR, which is enforced by the
    #     # spatial operations. This prevents threads mutating the order
    #     # simultaneously.
    #     orig_order = img.order
    #     img.order = ts.TENSOR_MAJOR
    #     job_kwargs = {
    #         "img": img,
    #         "copy": copy,
    #         "mind": mind,
    #         "patch_radius": patch_radius,
    #         "truncate": truncate
    #     }
    #     jobfunc = partial(CostMIND._calc_direction, **job_kwargs)
    #     n_neighbours = len(neighbourhood)
    #     n_threads = min(n_threads or n_neighbours, n_neighbours)
    #     # TODO: Thread lock
    #     n_threads = 1
    #     if n_threads == 1:
    #         for job in enumerate(neighbourhood):
    #             jobfunc(job)
    #     else:
    #         workers = mt.Pool(processes=n_threads)
    #         workers.map(jobfunc, enumerate(neighbourhood))
    #         workers.close()
    #     img.order = orig_order
    #
    #     # Small adjustments
    #     def discard_infinitesimals_func(x):
    #         return np.where(x < np.finfo(mind.dtype).eps, 0, x)
    #     def discard_nonfinite_func(x):
    #         return np.where(np.isfinite(x), x, 0)
    #     discard_infinitesimals = SpatialOperator(discard_infinitesimals_func)
    #     discard_nonfinite = SpatialOperator(discard_nonfinite_func)
    #
    #     # Calculate voxel-wise variance of MIND components (exclude outliers)
    #     taxes = tuple(range(1, 1 + mind.tdim))
    #     tensormean = TensorOperator(np.mean, axis=taxes)
    #     variances = tensormean(mind)
    #     # discard_outliers = Robust(low=1, high=99)
    #     # variances = discard_outliers(variances)
    #     # TODO: Replace this with operators
    #     m = np.mean(variances.data)
    #     variances.data[...] = np.clip(variances.data, 0.001 * m, 1000 * m)
    #
    #     # # Calculate normalised MIND vectors
    #     exp = SpatialOperator(np.exp)
    #     # # Note: the unary minus sign creates a new 'mind' with all the
    #     # # transformations copied over. This breaks down any links with a
    #     # # TransformationGroup.
    #     # # mind = discard_infinitesimals(mind)
    #     # # variances = discard_infinitesimals(variances)
    #     mind = exp(-mind / variances)
    #     maxop = TensorOperator(np.max, axis=taxes)
    #     mind = mind / maxop(mind)
    #     # mind = discard_nonfinite(mind)
    #
    #     # Return MIND representation as a TImage on the original image domain
    #     if hasattr(img, "__timage__"):
    #         name = img.name + "_mind"
    #         mind = img.__class__.fromTField(
    #             mind, copy=False, mask=img.mask, name=name, domain=img.domain,
    #             header=img.header)
    #         mind.resmgr = img.resmgr
    #     return mind
    #
    # # @staticmethod
    # # def _shift_roll(x, vector):
    # #     axes = tuple(range(x.ndim - len(vector), x.ndim))
    # #     return np.roll(x, vector, axis=axes)
    #
    # @staticmethod
    # def _shift(x, vector, tdim):
    #     shifted = x.copy()
    #     tslicer = (slice(None),) * tdim
    #     sslicer = tuple(slice(max(0, dx), min(dim + dx, dim)) for dx, dim
    #                     in zip(vector, x.shape[tdim:]))
    #     dslicer = tuple(slice(max(0, -dx), min(dim - dx, dim)) for dx, dim
    #                     in zip(vector, x.shape[tdim:]))
    #     shifted[tslicer + dslicer] = x[tslicer + sslicer]
    #     return shifted
    #
    # @staticmethod
    # def _calc_direction(job, **job_kwargs):
    #     worker_id = mt.current_process().getName()
    #     # print("Thread {} is working...".format(worker_id))
    #     i, direction = job
    #     img = job_kwargs.get("img")
    #     copy = job_kwargs.get("copy")
    #     mind = job_kwargs.get("mind")
    #     patch_radius = job_kwargs.get("patch_radius")
    #     truncate = job_kwargs.get("truncate")
    #
    #     # Create spatial operator for shifting
    #     # (spatial operations are carried out in tensor major layout)
    #     vector = (0,) * img.tdim + direction
    #
    #     # shiftop = SpatialOperator(shift, vector, order=1, prefilter=False)
    #     shiftop = SpatialOperator(CostMIND._shift, vector, img.tdim)
    #     # Shift image in the current direction
    #     shifted = shiftop(img)
    #     # # Shift with ghost if 'copy' is True
    #     # if copy:
    #     #     for ax in range(shifted.vdim):
    #     #         component = vector[ax]
    #     #         tslicer = (slice(None),) * shifted.tdim
    #     #         vslicer = [slice(None)] * shifted.vdim
    #     #         dim = shifted.vshape[ax]
    #     #         if component < 0:
    #     #             vslicer[ax] = slice(dim + component, dim)
    #     #         else:
    #     #             vslicer[ax] = slice(0, component)
    #     #         if shifted.order == ts.VOXEL_MAJOR:
    #     #             slicer = tuple(vslicer) + tslicer
    #     #         else:
    #     #             slicer = tslicer + tuple(vslicer)
    #     #         shifted.data[slicer] = img.data[slicer]
    #
    #     # Discard infinitesimals from the interpolation
    #     def discard_infinitesimals_func(x):
    #         x[x < np.finfo(shifted.dtype).eps] = 0
    #         return x
    #     discard_infinitesimals = SpatialOperator(discard_infinitesimals_func)
    #
    #     # Calculate Euclidean distances with the shifted image
    #     tmp = ((shifted - img) ** 2)
    #     # tmp = discard_infinitesimals()
    #     sigma = (0,) * img.tdim + (patch_radius,) * img.vdim
    #     patch_ssd = SpatialOperator(gaussian_filter, radius=2, sigma=sigma,
    #                                 truncate=truncate)
    #     tmp.order = mind.order
    #     mind.tensors[i] = patch_ssd(tmp).data
    #     # print("Thread {} finished.".format(worker_id))

    def _mind(self, img, kernel, sigma, truncate):
        """
        Low-level method for calculating the MIND descriptors of a TImage.

        """
        if img.storage == MEM:
            return self.mind_mem(
                img, kernel=kernel, sigma=sigma, truncate=truncate)
        else:
            return self._mind_hdd(
                img, kernel=kernel, sigma=sigma, truncate=truncate)

    def mind(self, img):
        """
        High-level method for calculating the MIND descriptors of a TImage.

        """
        return self._mind(
            img, kernel=self.kernel, sigma=self.sigma, truncate=self.truncate)

    # Note that the dx() and function() methods are implemented by the parent
    # class, CostMSD.
