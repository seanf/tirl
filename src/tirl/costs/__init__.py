#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# PACKAGE IMPORTS

# from tirl import expose_package_contents
#
# # Expose all cost function objects at the module level
# # Note: this routine imports the Cost base class, and every
# # subclass thereof from all modules within the "costs" package.
#
# expose_package_contents(
#     baseclass=Cost, pkg="tirl.costs", path=__path__, globals=globals())
