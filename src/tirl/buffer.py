#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import shutil
import warnings
import tempfile
import numpy as np


# TIRL IMPORTS

from tirl import settings as ts


class Buffer(object):
    """
    Buffer class. Uniform interface for memory-mapped and in-memory arrays.
    Automatically deletes linked files on the hard disk when discarded. This
    class is a successor of the Buffer namedtuple that was used in the original
    implementation of Domain.

    """
    def __init__(self, arr=None, fname=None, file_no=None, signature=None):

        # Signature
        self.signature = signature

        # File ID and file name
        if isinstance(file_no, int):
            self.file_no = file_no
        else:
            self.file_no = None
        if isinstance(fname, str):
            self.fname = fname
        else:
            self.fname = None

        # Infer file name from memory map
        if isinstance(arr, np.memmap) and (fname is None):
            self.fname = arr.filename

        # Data (even if None)
        self.data = arr

    def __del__(self):
        if (self.file_no is not None) and os.path.isfile(self.fname):
            try:
                os.close(self.file_no)
            except Exception:
                pass
            try:
                os.remove(self.fname)
            except Exception:
                warnings.warn(
                    "Temporary file could not be removed from {}."
                        .format(self.fname))

    def __bool__(self):
        """ The implicit bool value of the Buffer object is False, when no data
        is attached to it. """
        return self.data is not None

    def __array__(self):
        """ Array interface of the Buffer class. """
        return self.data

    def copy(self):
        if self.data is None:
            data = None
        elif isinstance(self.data, np.memmap):
            old_file = self.data.filename
            fd, new_file = tempfile.mkstemp(dir=ts.TWD, prefix="buffer_")
            with open(old_file, mode="rb+") as of:
                with open(new_file, mode="rb+") as nf:
                    shutil.copyfileobj(of, nf)
            data = np.memmap(new_file, mode="r+", dtype=self.data.dtype,
                             offset=0, shape=self.data.shape, order="C")
        else:
            data = self.data.copy()
        return Buffer(data, self.fname, self.file_no)

    @classmethod
    def _load(cls, dump):
        file_no = dump.get("file_no")
        fname = dump.get("fname")
        data = dump.get("data")
        ret = Buffer(arr=data, fname=fname, file_no=file_no)
        return ret

    def _dump(self):
        dbuffer = {
            "type": ".".join([self.__class__.__module__,
                              self.__class__.__name__]),
            "id": str(id(self)),
            "file_no": self.file_no,
            "fname": self.fname,
            "data": self.data,
        }
        return dbuffer
