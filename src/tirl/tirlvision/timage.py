#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _       __      __  _         _
#  |__   __| |_   _| |  __ \  | |      \ \    / / (_)       (_)
#     | |      | |   | |__) | | |       \ \  / /   _   ___   _    ___    _ __
#     | |      | |   |  _  /  | |        \ \/ /   | | / __| | |  / _ \  | '_ \
#     | |     _| |_  | | \ \  | |____     \  /    | | \__ \ | | | (_) | | | | |
#     |_|    |_____| |_|  \_\ |______|     \/     |_| |___/ |_|  \___/  |_| |_|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from math import ceil
from operator import mul
from functools import reduce
from skimage.exposure import rescale_intensity


# TIRL IMPORTS

from tirl.domain import Domain
from tirl.timage import TImage
from tirl import settings as ts
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.basic.embedding import TxEmbed
from tirl.transformations.linear.axisangle import TxAxisAngle
from tirl.transformations.linear.translation import TxTranslation

# Safe import Matplotlib
if ts.ENABLE_VISUALISATION:
    import os
    import matplotlib
    if os.getenv("DISPLAY"):
        matplotlib.use(ts.MPL_BACKEND)
    else:
        matplotlib.use("agg")
    import matplotlib.pyplot as plt
else:
    import warnings
    warnings.warn("Visualisations are disabled in tirlconfig. "
                  "Matplotlib was not loaded.", ImportWarning)


# IMPLEMENTATION

def preview1d(obj, **unused_kwargs):
    """
    Preview of a 1D TImage. Displays a line plot over the spatial extent of the
    input TImage.

    :param obj:
        TImage instance (the caller)
    :type obj: TImage
    :param unused_kwargs:
        Syntactic buffer for extra arguments. None of these are used in the
        plotting.
    :type unused_kwargs: Any

    """
    n_channels = obj.tdim or 1
    n_points = obj.vshape[0]
    orig_order = obj.order
    obj.order = ts.TENSOR_MAJOR
    if obj.tdim == 0:
        input_buffer = obj.data.reshape((-1,))
    else:
        input_buffer = obj.data.reshape((n_channels, n_points))

    fig, axes = plt.subplots(n_channels, 1)
    axes = axes.ravel()

    x = np.arange(n_points)
    for i in range(n_channels):
        axes[i].plot(x, input_buffer[i, :].ravel())

    obj.order = orig_order
    fig.canvas.draw()
    plt.show()


def preview2d(obj, composite=True, **kwargs):
    """
    Preview of a 2D TImage. Recognises RGB(A) images automatically. For
    real-valued images, tensor components are displayes as separate grayscale
    images.

    :param obj:
        TImage instance (the caller)
    :type obj: TImage
    :param kwargs:
        Additional keyword arguments to the preview function.
    :type kwargs: Any

    """
    if composite:
        if obj.dtype == np.uint8:
            n_channels = reduce(mul, obj.tshape)
            if n_channels in (3, 4):
                fig, ax = plt.subplots(1, 1)
                ax.set_aspect("equal")
                if obj.order == ts.VOXEL_MAJOR:
                    ax.imshow(obj.data)
                else:
                    ax.imshow(np.moveaxis(obj.data, 0, -1))
                if n_channels == 3:
                    ax.set_title("RGB image")
                elif n_channels == 4:
                    ax.set_title("RGBA image")
                fig.canvas.draw()
                plt.show()
                return
            else:
                composite = False

        elif np.issubdtype(obj.dtype, np.integer) or \
                np.issubdtype(obj.dtype, np.floating):
            n_channels = reduce(mul, obj.tshape)
            if n_channels in (3, 4):
                orig_order = obj.order
                obj.order = ts.TENSOR_MAJOR
                input_buffer = obj.data.reshape((-1, *obj.vshape))
                composite = []
                for i in range(n_channels):
                    channel = input_buffer[i]
                    channel = rescale_intensity(channel, out_range=(0, 1))
                    composite.append(channel.astype(np.float32))
                composite = np.stack(composite, axis=-1)
                fig = plt.figure()
                ax = fig.add_subplot(111)
                ax.set_aspect("equal")
                ax.imshow(composite)
                ax.set_title("Composite image ({} layers)".format(n_channels))
                obj.order = orig_order
                fig.canvas.draw()
                plt.show()
                return
            else:
                composite = False
        else:
            composite = False

    if not composite:
        orig_order = obj.order
        obj.order = ts.TENSOR_MAJOR
        n_channels = reduce(mul, obj.tshape)
        input_buffer = obj.data.reshape((-1, *obj.vshape))
        for i in range(n_channels):
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.imshow(input_buffer[i], cmap="gray")
            ax.set_aspect("equal")
            multiindex = np.unravel_index(i, obj.tshape)
            ax.set_title("T={}".format(multiindex))
            fig.canvas.draw()
            fig.show()
        obj.order = orig_order
        plt.show()
        return


def preview3d(obj, centre=None, normal=(0, 0, 1), resolution=1, **kwargs):
    """
    Planar section preview of a 3D TImage.

    :param obj:
        TImage instance (the caller)
    :type obj: TImage
    :param centre:
        Centre of view (3 coordinates). If None, the centre of the image is
        calculated. Use complex coordinate values to indicate "distance from
        the centre".
    :type centre: Union[tuple, list, np.ndarray]
    :param normal:
        Normal vector of the viewing plane.
    :type normal: Union[tuple, list, np.ndarray]
    :param resolution:
        Canvas resolution. Can be specified either globally or separately for
        the vertical and horizontal axes. It is important to specify this value
        because the TImage is sliced in physical space. Usually the "millimeter
        scale" of TImages is set early in their transformation chain. For
        optimised display performance, set the resolution of the canvas
        isotropically, in accordance with the resolution of the TImage.
    :type resolution:
        Union[int, float, np.integer, np.floating, tuple, list, np.ndarray]
    :param kwargs:
        Additional keyword arguments for the preview method.
    :type kwargs: Any

    """
    # Calculate the size of the canvas
    pc = obj.domain.get_physical_coordinates()
    extent = np.max(pc, axis=0) - np.min(pc, axis=0)
    num_types = (int, float, np.integer, np.floating)
    if isinstance(resolution, num_types):
        resolution = (resolution,) * 2
    elif not all(res > 0 for res in resolution):
        raise ValueError(
            "Canvas resolution must be positive in both directions.")
    canvas_size = np.max(extent) / np.asarray(resolution)
    canvas_size = tuple(ceil(dim) for dim in canvas_size)
    # print("Canvas size:", canvas_size)
    # print("Canvas resolution:", resolution)

    # Calculate the 3D rotation of the preview plane
    normal = np.asarray(normal) / np.linalg.norm(normal)
    axis = np.cross((0, 0, 1), normal)
    sine = np.linalg.norm(axis)
    if np.abs(sine) > np.finfo(ts.DEFAULT_FLOAT_TYPE).eps:
        axis /= sine
    cosine = np.dot((0, 0, 1), normal)
    axis = np.asarray(normal) if np.allclose(axis, 0) else axis
    angle = np.arctan2(sine, cosine)

    # axis = normal
    # s = np.linalg.norm(axis) / np.linalg.norm(normal)
    # c = normal[-1] / np.linalg.norm(normal)
    # angle = np.arctan2(s, c)
    # angle = 0

    # Import 3D centre of the canvas
    if centre is None:
        centre = obj.centre(weighted=False)
    elif any(np.iscomplex(x) for x in centre):
        origo = obj.centre(weighted=False)
        coords = []
        for i, x in enumerate(centre):
            if np.iscomplex(x):
                coords.append(origo[i] + np.abs(x) * np.real(np.sign(x)))
            else:
                coords.append(x)
        centre = tuple(coords)
        # print(centre)
    else:
        num_types = (int, float, np.integer, np.floating)
        if not all(isinstance(x, num_types) for x in centre):
            raise TypeError("Canvas centre must be specified with numeric "
                            "coordinates.")

    # Define transformation sequence for canvas
    canvas_scale = TxScale(*resolution)
    emb = TxEmbed(gap=1, homogeneous=False)
    cent_trans = TxTranslation(*tuple(-dim / 2 for dim in canvas_size))
    rot = TxAxisAngle(angle, axis, mode="rad")
    pos_trans = TxTranslation(*centre[:3])
    tx = [cent_trans, canvas_scale, emb, rot, pos_trans]
    canvas = Domain(canvas_size, *tx)

    # Evaluate image on the canvas and display it
    disp = obj.evaluate(canvas)
    disp.name = "{}-display".format(disp.name)
    disp.preview()
