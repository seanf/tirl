#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _       __      __  _         _
#  |__   __| |_   _| |  __ \  | |      \ \    / / (_)       (_)
#     | |      | |   | |__) | | |       \ \  / /   _   ___   _    ___    _ __
#     | |      | |   |  _  /  | |        \ \/ /   | | / __| | |  / _ \  | '_ \
#     | |     _| |_  | | \ \  | |____     \  /    | | \__ \ | | | (_) | | | | |
#     |_|    |_____| |_|  \_\ |______|     \/     |_| |___/ |_|  \___/  |_| |_|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# PACKAGE IMPORTS