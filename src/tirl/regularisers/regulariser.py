#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import logging
import numpy as np
from numbers import Real, Number

# TIRL IMPORTS

from tirl import settings as ts
from tirl.tirlobject import TIRLObject


# IMPLEMENTATION

class Regulariser(TIRLObject):
    """
    Regulariser class - generic class for calculating regularisation terms.

    """
    RESERVED_KWARGS = ("parameters", "weight", "logger", "metaparameters")

    def __init__(self, parameters, weight=1.0, logger=None, **metaparameters):
        """
        Initialisation of Regulariser.

        :param parameters: Input parameters
        :type parameters: np.ndarray or tuple or list
        :param weight: Regularisation weight.
        :type weight: Real
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger
        :param kwargs:
            Additional keyword arguments required to compute the
            regularisation term.
        :type kwargs: Any

        """
        # Call superclass initialisation
        super(Regulariser, self).__init__()

        # Set parameters
        self.parameters = parameters

        # Set metaparameters
        metaparameters.update(weight=weight)
        self.metaparameters = metaparameters
        self.logger = logger

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, params):
        if isinstance(params, Number):
            params = (params,)
        params = np.asarray(params)

        if np.issubdtype(params.dtype, np.number):
            self._parameters = params
        else:
            raise TypeError(f"Invalid numeric type for regularisation "
                            f"parameters: {params.__class__.__name__}")

    @property
    def metaparameters(self):
        return self._metaparameters

    @metaparameters.setter
    def metaparameters(self, meta):
        if isinstance(meta, dict):
            self._metaparameters = meta
        else:
            raise TypeError(
                "Regulariser metaparameters msut be set with a dict.")

    @property
    def logger(self):
        return self.metaparameters.get("logger")

    @logger.setter
    def logger(self, lgr):
        if lgr is None:
            self.metaparameters.update(logger=logging.getLogger())
        elif isinstance(lgr, str):
            self.metaparameters.update(logger=logging.getLogger(lgr))
        elif isinstance(lgr, logging.Logger):
            self.metaparameters.update(logger=lgr)
        else:
            raise TypeError(f"Expected logger name of logger instance, "
                            f"got {lgr} instead.")

    def log(self, msg, *args, level=ts.COST_VALUE_LOG_LEVEL, **kwargs):
        """
        This method wraps Logger.log().

        Logs an event to the Logger that has been configured for the optimiser
        object. Uses the level that is specified in COST_VALUE_LOG_LEVEL as the
        default level for the logs.

        """
        self.logger.log(level, msg, *args, **kwargs)

    @property
    def weight(self):
        return self.metaparameters.get("weight")

    @weight.setter
    def weight(self, w):
        if isinstance(w, Real):
            # Allow negative numbers!
            self.metaparameters.update(weight=float(w))
        else:
            raise TypeError("Regularisation weight must be real.")

    def __call__(self):
        """
        Calls the function() method to calculate the regularisation term, which
        is returned after being multiplied by the regularisation weight.

        :returns: regularisation term (multiplied by the regularisation weight)
        :type: float

        """
        regcost = float(self.weight * self.function())
        self.log(f"Regularisation ({self.name}): {regcost}")
        return regcost

    def function(self):
        """
        Implementation of the regularisation algorithm. The base class computes
        the regularisation cost as the L2-norm of the parameter values. This
        method SHOULD be overloaded in subclasses to provide specialised
        class-specific regularisation methods.

        :returns: unscaled regularisation term
        :rtype: float

        """
        # L2-norm
        return np.linalg.norm(self.parameters, ord=2)

    def D(self, order=1):
        """
        Returns the nth-order derivative of the regularisation term with
        respect to the input parameters. Order=1 corresponds to the Jacobian,
        order=2 corresponds to the Hessian.

        :param order: derivative order (default: 1)
        :type order: int

        """
        if order < 1:
            raise ValueError("Invalid derivative order.")
        else:
            return getattr(self, f"grad{order}")() * self.weight

    def grad1(self):
        """
        Returns the Jacobian (1st-order derivatives) of the regularisation term
        with respect to the input parameters. Subclasses MUST override this
        method if they calculate the regularisation term differently than the
        base class.

        """
        # This is the analytical Jacobian of the L2-norm
        return self.parameters / self.function()

    def grad2(self):
        """
        Returns the Hessian (2nd-order derivatives) of the regularisation term
        with respect to the input parameters. Subclasses MUST overrid this
        method if they calculate the regularisation term differently than the
        base class.

        """
        # This is the analytical Hessian of the L2-norm
        x = self.parameters.reshape((-1, 1))
        E = np.eye(x.size)
        norm = self.function()
        hess = E / norm - x @ x.T / (norm ** 3)
        return hess


if __name__ == "__main__":
    print("""This module is not intended for execution.""")
