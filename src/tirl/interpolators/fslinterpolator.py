#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
This interpolator does not handle the fill_value parameter. A fill_value of
0 is hard-coded in the C++ implementation.

"""


# DEPENDENCIES

import numpy as np


# TIRL IMPORT

from tirl.interpolators.interpolator import Interpolator
from tirl.cmodules import fslinterpolator


# IMPLEMENTATION

class FSLInterpolator(Interpolator):
    """
    This class implements trilinear interpolation with constant (=0)
    extrapolation. The class demonstrates how custom interpolators can be
    used with TIRL.

    """
    RESERVED_KWARGS = ("source", "tensor_axes", "threads", "verbose",
                       "ipkwargs", "kwargs", "radius", "hold")

    def __init__(self, source, tensor_axes=(), threads=-1, verbose=False,
                 ipkwargs=None, **kwargs):
        """
        FSLInterpolator initialiser.

        """
        # Invoke the initialiser of the superclass
        kwargs = {k: v for k, v in kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        super(FSLInterpolator, self).__init__(
            source=source, tensor_axes=tensor_axes, radius=1, hold=False,
            threads=threads, verbose=verbose, ipkwargs=ipkwargs, **kwargs)

        # Set class-specific properties
        # self.radius = 1

    def interpolate(self, coordinates, input_array=None, **kwargs):
        """
        Implementation of the interpolation algorithm.

        :param coordinates:
            (n_points, n_dimensions) table of coordinates where the input array
            should be evaluated by interpolation
        :type coordinates: np.ndarray
        :param input_array:
            Array of known values. If the source data is bigger than memory,
            input_array will correspond to a chunk of the original array.
            Chunks are created with overlap and the values that are
            interpolated at the chunk boundary will be automatically discarded.
            The extent of the overlap is defined by
        :type input_array: np.ndarray
        :param kwargs:

        :returns: (n_points,) interpolated values
        :rtype: np.ndarray

        """
        if isinstance(coordinates, (tuple, list)):
            coordinates = np.asarray(coordinates).reshape((1, -1))
        try:
            res = fslinterpolator(input_array, coordinates)
        except:
            print("Interpolation failed with the following input: ",
                  input_array.shape, coordinates.shape, kwargs)
            raise
        else:
            return res
