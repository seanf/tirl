#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from scipy.interpolate import Rbf


# TIRL IMPORTS

from tirl.interpolators.interpolator import Interpolator


# DEFINITIONS


# IMPLEMENTATION

class RbfInterpolator(Interpolator):
    """
    The radial basis function interpolator is designed for tensor fields
    defined on a non-compact domain (a set of points instead of a regular grid).

    """
    RESERVED_KWARGS = ("source", "tensor_axes", "basis", "model", "epsilon",
                       "threads", "verbose", "radius", "hold", "kwargs",
                       "ipkwargs")

    MODELS = ("multiquadric", "inverse", "gaussian", "linear", "cubic",
              "quintic", "thin_plate")

    def __init__(self, source, tensor_axes=(), basis=None,
                 model="multiquadric", epsilon=None, threads=-1,
                 verbose=False, ipkwargs=None, **kwargs):
        """
        Initialisation of RbfInterpolator. This type of interpolator may only
        be initialised by a TField object, because the location of scattered
        points cannot be inferred from a list of values.

        :param source:
            Tensor values at known locations (a non-compact tensor field).
        :type source: Union[TField]
        :param threads: Number of computing threads.
        :type threads: Union[int, np.integer]
        :param verbose:
            If True, status messages will be displayed. If False, status
            messages will remain hidden.
        :type verbose: bool
        :param kwargs:
            Additional keyword arguments for SciPy's radial basis function
            interpolator (Rbf).
        :type kwargs: Any

        """
        # Construction by TField: provides coordinates + values
        if hasattr(source, "__tfield__"):

            values = source.data
            tensor_axes = source.taxes

            # The "non-compact domain trick"
            # (implemented in domain.py from v2.1):
            # The voxel coordinates of a non-compact domain are defined by a
            # quasi-1D voxel array and a TxDirect offset transformation that
            # directly assigns points to specific locations.
            # The RbfInterpolator assumes that it is this offset-mapped space
            # where the values of the field are really defined, not the
            # 1-D voxel array. Hence the coordinates are imported from the
            # merger of the offset and chain transformations.
            basis = source.domain.get_voxel_coordinates().T

        # Construction by array-like:
        # provides values, but need basis and tensor axes!
        elif hasattr(source, "__array__"):
            if (basis is None) or tensor_axes is None:
                raise AttributeError(
                    f"Missing specification for tensor axes and basis points.")
            else:
                values = np.asanyarray(source)

            if hasattr(basis, "__domain__"):
                basis = basis.get_voxel_coordinates().T

            elif hasattr(basis, "__array__"):
                basis = np.asanyarray(basis)

        # Allow empty construction with the promise that the interpolator will
        # be properly initialised once the source data is available.
        elif source is None:
            values = None

        # Construction by other types: not allowed!
        else:
            raise TypeError(
                f"Expected TField-like or array-like object to construct "
                f"{self.__class__.__name__}, got "
                f"{source.__class__.__name__} instead.")

        # Make sure that the basis array has at least two dimensions
        if basis.ndim == 1:
            basis = basis.reshape(1, -1)

        # Clean metaparameter definitions to avoid argument collisions.
        # The class-specific metaparameters of RbfInterpolator are basis,
        # model, and epsilon.
        kwargs = {k: v for k, v in kwargs.items()
                  if k not in self.RESERVED_KWARGS}

        # Set hold=True: this informs the Interpolator base class that ALL
        # source values must be kept in memory for any interpolation event
        # that is possibly carried out on a chunk of a larger array/field.
        # Where the interpolation is locally determined, this is not necessary,
        # but interpolation by radial basis functions is global,
        # hence hold=True.
        super(RbfInterpolator, self).__init__(
            source=values, tensor_axes=tensor_axes, radius=1, hold=True,
            threads=threads, verbose=verbose, ipkwargs=ipkwargs, **kwargs)

        # Set class-specific metaparameters
        self.basis = basis
        self.model = model
        self.epsilon = epsilon

    def interpolate(self, coordinates, input_array=None, **ipkwargs):
        if isinstance(coordinates, (tuple, list)):
            coordinates = np.asarray(coordinates).reshape((1, -1))
        # Load default keyword arguments for Rbf
        kw = {k: v for k, v in self.kwargs.get("ipkwargs", dict()).items()}
        # Override the ones that were specified for the call
        kw.update(ipkwargs)
        if input_array is None:
            values = self.values
        else:
            values = input_array
        ipol = Rbf(*self.basis, values, **kw)
        return ipol(*coordinates.T)

    def copy(self):
        kwargs = {k: v for k, v in self.kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        obj = type(self)(
            source=self.values, tensor_axes=self.tensor_axes, basis=self.basis,
            model=self.model, epsilon=self.epsilon, threads=self.threads,
            verbose=self.verbose, ipkwargs=self.ipkwargs, **kwargs)
        return obj

    @property
    def basis(self):
        return self.kwargs.get("basis")

    @basis.setter
    def basis(self, b):
        b = np.asanyarray(b)
        self.kwargs.update(basis=b)

    @property
    def model(self):
        return self.kwargs.get("model")

    @model.setter
    def model(self, m):
        m = str(m).lower()
        if m in self.MODELS:
            self.kwargs.update(model=m)
        else:
            raise ValueError(f"Unrecognised interpolation model: {m}. Choose "
                             f"from the following options: {self.MODELS}")

    @property
    def epsilon(self):
        return self.kwargs.get("epsilon", None)

    @epsilon.setter
    def epsilon(self, eps):
        self.kwargs.update(epsilon=eps)

    @classmethod
    def _load(cls, dump):
        values = dump.pop("values")
        kwargs = dump.pop("kwargs")

        kwargs.pop("radius")  # 1 by definition
        kwargs.pop("hold")    # True by definition
        ta = kwargs.pop("tensor_axes")
        threads = kwargs.pop("threads")
        verbose = kwargs.pop("verbose")
        ipkwargs = kwargs.pop("ipkwargs")
        basis = kwargs.pop("basis")
        model = kwargs.pop("model")
        epsilon = kwargs.pop("epsilon")

        # Recreate interpolator
        return cls(values, tensor_axes=ta, basis=basis, model=model,
                   epsilon=epsilon, threads=threads, verbose=verbose,
                   ipkwargs=ipkwargs, hold=True, radius=1, **kwargs)
