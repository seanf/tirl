#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

# Subclassing Interpolator:

# Every subclass must define the interpolate() method which must implement
# interpolation on regular grid of scalars, as the chunker method will
# distribute the interpolation of tensor components between multiple
# interpolator instances. If this is an undesired behavior, the
# _default_chunker() method should be redirected to the _no_chunker method,
# or - if the large-array compatibility is desirable to keep - the
# _default_chunker() method should be reimplemented with suitable modifications.


# DEPENDENCIES

import psutil
import warnings
import tempfile
import numpy as np
from operator import mul
from numbers import Integral
from scipy.optimize import fmin
import multiprocessing.dummy as mt
from functools import reduce, partial


# TIRL IMPORTS

from tirl.tirlobject import TIRLObject
from tirl import exceptions as te, settings as ts

# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

class Interpolator(TIRLObject):
    """
    The Interpolator base class provides an optimised compatibility framework
    for using custom interpolation routines with tensor field data. The base
    class itself is non-functional. Subclasses MUST implement the 'interpolate'
    method to provide specialised interpolation routines.

    """
    RESERVED_KWARGS = ("source", "tensor_axes", "radius", "hold", "threads",
                       "verbose", "ipkwargs", "kwargs")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, source, tensor_axes=(), radius=1, hold=False,
                 threads=-1, verbose=False, ipkwargs=None, fill_value=None,
                 **kwargs):
        """
        Initialisation of Interpolator.

        :param source:
            Source of known values. If source is a TField, the "tensor_axes"
            property is set accordingly, and the "threads" property is set to
            match the number of tensor elements. These allow interpolation to
            be carried out in parallel for each tensor element. If ndarray, the
            "tensor_axes" property is set to empty by default, and only a single
            thread is used to carry out the interpolation over the entire array.
            To enable parallel interpolation for ndarrays or disable it for
            TFields, update the "tensor_axes" property manually. Changing the
            tensor axes will auto-update the number of threads used, but this
            can also be further adjusted manually via the "threads" property.
        :type source: Union[TField, np.ndarray]
        :param threads:
            Number of parallel threads used for the interpolation. If -1,
            the number of threads will be determined automatically based on the
            input tensor shape. This may be modified after construction via the
            "threads" property or by changing the tensor axes. Note that when
            "tensor_axes" is empty, only one thread will be used regardless of
            this setting.
        :type threads: Union[int, np.integer]
        :param verbose:
            If True, status messages will be displayed during interpolation.
            If False, no status messages will appear.
        :type verbose: bool
        :param kwargs:
            Additional keyword arguments that will be passed on to
            'interpolate' that implements the interpolation algorithm.

            "hold": If True, the interpolator attempts to hold all known points
                    in memory to carry out interpolation on independent chunks
                    of the target points.
            "radius": Size of the voxel neighburhood that is required for
                      precise interpolation. This parameter is used for chunked
                      interpolation to discard invalid interpolants at the
                      chunk boundaries. (Default: 1)

        :type kwargs: Any

        """
        # Call parent-class initialisation
        super(Interpolator, self).__init__()

        # Initialise metaparameters
        self.kwargs = kwargs
        if fill_value is None:
            self.kwargs.update(fill_value=ts.FILL_VALUE)
        else:
            self.kwargs.update(fill_value=fill_value)

        # Set interpolation source values

        if hasattr(source, "__tfield__"):
            self.values = source.data
            self.tensor_axes = source.taxes

        elif hasattr(source, "__array__"):
            self.values = np.asanyarray(source)
            self.tensor_axes = tensor_axes

        # Permit empty construction
        elif source is None:
            self.values = None

        else:
            raise TypeError(f"Expected TField-like or array-like object as "
                            f"interpolation source, got "
                            f"{source.__class__.__name__} instead.")

        # Radius (number of voxels): specifies the size of the neighbourhood
        # (default=1) that the interpolation at each voxel is dependent on.
        # Chunked interpolation is performed with an overlap of "radius" voxels
        # to avoid round-off errors at tile edges.
        self.radius = radius

        # If hold=True, all values that the interpolator uses as "source" will
        # be kept in memory for every single interpolation operation. Setting
        # this to False (default) aims to maximise interpolation chunk size by
        # selectively loading only the relevant support points in memory at
        # once. Local interpolation methods such as linear or spline
        # interpolation only depend on neighbouring source values, hence it is
        # appropriate to load a chunk with its immediate neighbours (as
        # defined by radius). On the contrary, global interpolation methods
        # such as interpolation by radial basis functions depend on the entire
        # set of source values, all of which must therefore be kept in memory
        # for every single interpolation operation.
        self.hold = hold

        # Number of simultaneous interpolation threads (or processes). Must be
        # a positive integer or -1 to represent "all". Default=-1.
        if threads == -1:
            shape = self.values.shape
            tshape = tuple(shape[ax] for ax in self.tensor_axes)
            tsize = np.prod(tshape)
            threads = min(ts.CPU_CORES, tsize)

        # Verbosity
        self.verbose = verbose
        # Multi-threading
        self.threads = threads
        # Keyword arguments for external interpolator function
        self.ipkwargs = ipkwargs

        # If the data is available to the object and multiprocessing has been
        # requested, create a process pool with shared memory.
        if self.threads > 1:
            self._pool = mt.Pool(self.threads)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def hold(self):
        return self.kwargs.get("hold")

    @hold.setter
    def hold(self, h):
        if isinstance(h, (bool, np.bool, np.bool_)):
            self.kwargs.update(hold=bool(h))
        else:
            raise TypeError(f"Expected boolean value for hold option, "
                            f"got {h.__class__.__name__} instead.")

    @property
    def ipkwargs(self):
        return self.kwargs.get("ipkwargs")

    @ipkwargs.setter
    def ipkwargs(self, kw):
        kw = dict() if kw is None else kw
        if isinstance(kw, dict):
            self.kwargs.update(ipkwargs=kw)
        else:
            raise TypeError(f"Expected dict for external interpolator keyword "
                            f"arguments, got {kw.__class__.__name__} instead.")

    @property
    def kwargs(self):
        return self._kwargs

    @kwargs.setter
    def kwargs(self, kw):
        if isinstance(kw, dict):
            self._kwargs = kw
        else:
            raise TypeError(f"Expected dict for Interpolator kwargs, "
                            f"got {kw.__class__.__name__} instead.")

    @property
    def radius(self):
        return self.kwargs.get("radius")

    @radius.setter
    def radius(self, r):
        if isinstance(r, Integral) and (r >= 0):
            self.kwargs.update(radius=int(r))
        else:
            raise TypeError("Interpolation overlap size (radius) must be a "
                            "positive integer.")

    @property
    def tensor_axes(self):
        return self.kwargs.get("tensor_axes")

    @tensor_axes.setter
    def tensor_axes(self, ta):
        """
        Tensor axes of the input array. This property tells the Interpolator
        which subdivisions of the input array are independent from each other,
        allowing interpolations be carried out in parallel for faster
        performance. This property gets updated whenever the order of the
        container TField object is changed. Changing this property auto-updates
        the number of threads used by the interpolator to match the number of
        independent subarrays (i.e. the number of tensor elements).

        :param ta: tensor axis indices
        :type ta: tuple[int]

        """
        ta = () if ta is None else ta
        ta = (ta,) if not hasattr(ta, "__iter__") else tuple(ta)
        if all(isinstance(ax, Integral) and (ax >= 0) for ax in ta):
            self.kwargs.update(tensor_axes=ta)
        else:
            raise TypeError("Only non-negative integers may be specified "
                            "for tensor axes.")

    @property
    def threads(self):
        return self.kwargs.get("threads")

    @threads.setter
    def threads(self, n_threads):
        if n_threads == -1:
            n_threads = ts.CPU_CORES
        if isinstance(n_threads, Integral) and (n_threads > 0):
            # if n_threads > ts.CPU_CORES:
            #     warnings.warn(
            #         f"Maximum number of parallel processes: {ts.CPU_CORES}. "
            #         f"Adjust the CPU_CORES value in tirlconfig to "
            #         f"allow more processes.")
            n_threads = min(n_threads, ts.CPU_CORES)
            self.kwargs.update(threads=int(n_threads))
        else:
            raise TypeError("The number of simultaneous interpolation "
                            "threads must be a positive integer.")

    @property
    def values(self):
        """ Read-only property. This is to prevent the input array from
        accidental dereferencing and automatic garbage collection. """
        return self._values

    @values.setter
    def values(self, data):
        """Setter method for known values. Note that this method does not
        update the tensor_axes and threads attributes. Extra care must be taken
        to ensure that the tensor_axes attribute is valid for the new array."""

        if data is None:
            self._values = None
            return

        data = np.asanyarray(data)
        if np.issubdtype(data.dtype, np.number):
            self._values = data
        else:
            raise TypeError("Only numerical values are allowed as "
                            "interpolation values.")

    @property
    def verbose(self):
        return self.kwargs.get("verbose")

    @verbose.setter
    def verbose(self, v):
        if isinstance(v, (bool, np.bool, np.bool_)) or (v is None):
            self.kwargs.update(verbose=bool(v))
        else:
            raise TypeError(f"Expected boolean value for verbose option, "
                            f"got {v.__class__.__name__} instead.")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MAGIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __interpolator__(self):
        """ TIRL Interpolator interface. """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def _call_without_hold(self, coordinates, n_channels, tensor_shape,
                           input_array, output, isleading, istrailing,
                           issimple):
        # # TODO: work out the formulaic solution here
        # # TODO: Thread lock
        # self.threads = 1
        memlimit = psutil.virtual_memory().available
        if ts.IPOL_INSTANCE_MEMORY_LIMIT > 0:
            memlimit = min(memlimit, ts.IPOL_INSTANCE_MEMORY_LIMIT * 1024 ** 2)
        jobs = self._partition(
            coordinates, input_array, memlimit // (2 * self.threads))

        # Iterate over all partitions and perform interpolation on each of them
        if self.verbose:
            print("Starting interpolation...")
        counter = 0

        # large_enough = coordinates.shape[0] > ts.CHUNK_INTERPOLATION_IF_LARGER
        options = (coordinates, tensor_shape, issimple, isleading, istrailing,
                   n_channels, output, counter)
        worker_func = partial(self._interpolate_partition, *options)
        if self.threads > 1:  # and large_enough:
            self._pool.map(worker_func, jobs)
        else:
            for job in jobs:
                worker_func(job)

        return output

    def _interpolate_partition(self, coordinates, tensor_shape, issimple,
                               isleading, istrailing, n_channels, output,
                               counter, job):

        subfield, offset, points = job
        if self.verbose:
            print("{} interpolating at {}.".format(
                mt.current_process().name, offset))

        # Split point coordinates and indices
        indices = points[:, -1].astype(np.intp)
        points = points[:, :-1]

        # Create output with all the voxel axes flattened
        start, stop = np.min(indices), np.max(indices) + 1
        if issimple:
            out = output.reshape((-1,))[start:stop]
        elif isleading:
            out = output.reshape((*tensor_shape, -1))[..., start:stop]
        elif istrailing:
            out = output.reshape((-1, *tensor_shape))[start:stop, ...]

        # Adjust indices and points by slicing offsets
        indices -= np.min(indices)
        points = points - offset  # note: dtype mismatch if -= is used

        # Perform interpolation
        threads = reduce(mul, tensor_shape) if tensor_shape else 1
        threads = min(self.threads, threads)
        # # TODO: Thread lock
        # threads = 1
        if threads > 1:
            args = (points, subfield, indices, out, tensor_shape)
            job_func = partial(self._finish, *args)
            self._pool.map(job_func, range(n_channels))
        else:
            for i in range(n_channels):
                self._finish(points, subfield, indices, out, tensor_shape, i)

        # for i in range(n_channels):
        #     args = (points, subfield, indices, out, tensor_shape)
        #     self._finish(*args, i)
        counter += points.shape[0]
        if self.verbose:
            print("Interpolation is complete for {}/{} points."
                  .format(counter, coordinates.shape[0]))

    def _call_with_hold(self, coordinates, input_array, n_channels,
                        tensor_shape, issimple, isleading, istrailing, output):

        # Find optimum chunk size for target points
        memlimit = psutil.virtual_memory().available
        n_points, n_coordinates = coordinates.shape
        c_itemsize = np.dtype(coordinates.dtype).itemsize
        out_itemsize = np.dtype(input_array.dtype).itemsize
        chunklen = (memlimit - input_array.nbytes) \
                   // (n_coordinates * c_itemsize + out_itemsize * n_channels)

        if self.verbose:
            print("Starting interpolation...")
        counter = 0
        for start in range(0, n_points, chunklen):
            # Create input chunk
            stop = min(start + chunklen, n_points)
            points = coordinates[start:stop, :]
            # Create output chunk
            if issimple:
                out = output.reshape((-1,))[start:stop]
            elif isleading:
                out = output.reshape((*tensor_shape, -1))[..., start:stop]
            elif istrailing:
                out = output.reshape((-1, *tensor_shape))[start:stop, ...]
            # Perform interpolation
            indices = np.arange(start, stop)
            if self.threads > 1:
                args = (points, input_array, indices, out, tensor_shape)
                job_func = partial(self._finish, *args)
                self._pool.map(job_func, range(n_channels))
            else:
                for i in range(n_channels):
                    args = (points, input_array, indices, out, tensor_shape)
                    self._finish(*args, i)
            counter += points.shape[0]
            if self.verbose:
                print("Interpolation is complete for {}/{} points."
                      .format(counter, coordinates.shape[0]))

        return output

    def _finish(self, points, subfield, indices, out, t_shape, i=0):
        # Report status
        if self.verbose:
            print("{} is working...".format(mt.current_process().name))

        # Create channel selector
        if not t_shape:
            selector = (Ellipsis,)
        else:
            t_slicer = \
                tuple(slice(t, t+1) for t in np.unravel_index(i, t_shape))
            if 0 in self.tensor_axes:
                selector = t_slicer + (Ellipsis,)
            else:
                selector = (Ellipsis,) + t_slicer

        # Create input array for interpolator
        # Apply the selector and make sure that tensors are reduced, but
        # voxel axes are kept. Note: np.squeeze() would not differentiate
        # between the two. (this was a bugfix)
        input_array = subfield[selector]
        for ax in sorted(self.tensor_axes)[::-1]:
            if input_array.shape[ax] == 1:
                input_array = np.take(input_array, 0, ax)

        # Export channel-specific interpolation results
        out[selector].flat[indices] = self.interpolate(
            points, input_array, **self.ipkwargs)

        # Write data to disk, if the output is memory-mapped
        if isinstance(out, np.memmap):
            out.flush()
            
    def __del__(self):
        """
        Interpolator destructor method.
     
        """
        # Close the multiprocessing pool
        if hasattr(self, "_pool"):
            self._pool.close()
        # Call parent-class deletion routine
        super(Interpolator, self).__del__()

    def __eq__(self, other):
        try:
            assert self.__class__ is other.__class__
            assert np.all(self.values == other.values)
            assert self.kwargs == other.kwargs
        except Exception:
            return False
        else:
            return True

    def copy(self, **newparams):
        """
        Copy constructor. Subclasses that define their constructor signature
        differently MAY find it necessary to overload this constructor as well.

        :returns: new Interpolator instance
        :rtype: Interpolator

        """
        kwargs = {k: v for k, v in self.kwargs.items()
                  if k not in self.RESERVED_KWARGS}
        kw = dict(source=self.values, tensor_axes=self.tensor_axes,
            radius=self.radius, hold=self.hold, threads=self.threads,
            verbose=self.verbose, ipkwargs=self.ipkwargs, **kwargs)
        kw.update(newparams)
        obj = type(self)(**kw)
        return obj

    @classmethod
    def _load(cls, dump):
        # Note: Interpolator subclasses will call this parent-class method when
        # their type is identified by locate() in tirl.__init__.py, and this is
        # why this is a class method: it can construct interpolators of any
        # specific type, as long as the syntax is the same, and it is very much
        # the case for interpolator classes. If the construction signature is
        # different, the subclass must override this classmethod!

        # Load object properties from object dump
        values = dump.get("values")
        kwargs = dump.get("kwargs")

        ta = kwargs.get("tensor_axes")
        radius = kwargs.pop("radius")
        hold = kwargs.pop("hold")
        threads = kwargs.pop("threads")
        verbose = kwargs.pop("verbose")
        ipkwargs = kwargs.pop("ipkwargs")

        # Resurrect the specific type of interpolator instance
        kwargs = {k: v for k, v in kwargs.items()
                  if k not in cls.RESERVED_KWARGS}
        obj = cls(source=values, tensor_axes=ta, radius=radius, hold=hold,
                  threads=threads, verbose=verbose, ipkwargs=ipkwargs, **kwargs)
        return obj

    def _dump(self):
        # Note: the interpolation array is not saved when the interpolator is
        # dumped. This is because both TField and TImage permit construction
        # with an empty interpolator (and so does Interpolator), assuming that
        # the interpolation array is the same as the data array. Actually,
        # TImage copies the interpolator to create a separate one for the mask.
        from tirl.utils import rcopy
        objdump = super(Interpolator, self)._dump()
        objdump.update({
            "values": None,
            "tensor_axes": self.tensor_axes,
            "kwargs": rcopy(self.kwargs)})
        return objdump

    def _partition(self, coordinates, input_array, memlimit):
        """
        Create corresponding partitions of interpolation points and the input
        array. Each of these partitions are independent from the rest, but
        array partitions may be overlapping.

        Separate partitions may be loaded into memory sequentially to perform
        interpolation on domains that are too large to fit into memory at once,
        or in parallel to achieve faster interpolation performance.

        By default, spatial partitioning is used only for domains that are too
        large, and parallelism is used only for the separate channels
        (tensor components) of the input array/field.

        :param coordinates:
            (n_points, n_coordinates) table of interpolation points.
        :type coordinates: Union[np.ndarray, np.memmap]
        :param input_array:
            Array of known values that are used in the interpolation.
        :type input_array: Union[np.ndarray, np.memmap]
        :param memlimit:
            Total amount of memory in bytes that can be used by the
            Interpolator instance.
        :type memlimit: int

        :returns:
            A generator for independent partitions. Each element returned by
            the generator consist of the following:

            subfield:
                A convex partition of the input array, that is large enough to
                contain all interpolation points pertianing to the current
                partition.
            offset:
                Voxel offset of the array partition that must be subtracted
                from the coordinates of the interpolation points.
            points:
                A subset of interpolation points that fall within the bounds of
                'subfield'. Note that the list of interpolation points in this
                range is not exclusive: there might be further interpolation
                points that fall within 'subfield', but these may be part of
                a different partition, as subfields may overlap. The last
                column of the 'points' table is the linear index of the each
                point within the current partition. This must be used to
                restore the order of interpolation results to match the
                originally specified order of interpolation points.
        :rtype: generator

        """
        # Acquire basic shape information
        n_points, ndim = coordinates.shape
        t_shape = [input_array.shape[ax] for ax in self.tensor_axes]
        n_channels = reduce(mul, t_shape) if t_shape else 1
        p_shape = tuple(input_array.shape[ax] for ax in range(input_array.ndim)
                        if ax not in self.tensor_axes)
        sizeof_coordinate = np.dtype(coordinates.dtype).itemsize
        sizeof_fieldvalue = np.dtype(input_array.dtype).itemsize
        N = self._get_optimal_chunksize(coordinates, input_array, memlimit)

        # Create chunks of interpolation points based on the optimal chunksize
        coordinate_chunk_indices = tuple(range(0, n_points, N))
        for start in coordinate_chunk_indices:
            stop = min(start + N, n_points)
            # Append interpolation points with their linear indices
            current_coords = np.hstack(
                (np.asarray(coordinates[start:stop, :]),
                 np.arange(start, stop).reshape((-1, 1))))
            field_limit = \
                memlimit - N * ((ndim + 1) * sizeof_coordinate +
                                n_channels * sizeof_fieldvalue)
            # Divide the current chunk of interpolation points further via
            # recursive bisection if the array of input values turns out to be
            # too large to perform interpolation in memory. Note: this is
            # necessary, because the actual coordinates of the interpolation
            # points in the selected chunk are unknown when the chunk is
            # created, hence the size of the corresponding array partition.
            chunker = self._subdivide(
                current_coords, p_shape, sizeof_fieldvalue, field_limit)

            # Create a generator that returns actual in-memory partitions of
            # the interpolation points and the input array, each of which can
            # be an independent input of an interpolation algorithm
            # (Interpolator.interpolate).
            for points in chunker:
                # Split indices from coordinates
                lows = np.floor(np.min(points[:, :-1], axis=0)) - self.radius
                lows[lows < 0] = 0
                highs = np.ceil(np.max(points[:, :-1], axis=0)) + self.radius
                extent = np.asarray(p_shape)
                highs = np.where(highs >= extent, extent, highs)
                # Raise error if highs are less than lows, as this indicates
                # that interpolation coordinates are completely missing their
                # target.
                if any(l > h for l, h in zip(lows, highs)):
                    warnings.warn(
                        "Interpolation coordinates are outside the source.")
                # Create appropriate slicer that isolates a partition of the
                # input array.
                slicer = tuple(slice(int(l), int(max(l + 1, h)))
                               for l, h in zip(lows, highs))
                t_slicer = (slice(None),) * len(self.tensor_axes)
                if not self.tensor_axes:
                    pass
                elif 0 in self.tensor_axes:
                    slicer = t_slicer + slicer
                else:
                    slicer = slicer + t_slicer
                subfield = input_array[slicer]

                # if self.threads > 1:
                #     # Transfer subfield data to shared memory
                #     ct = np.ctypeslib.as_ctypes_type(subfield.dtype)
                #     shared = mp.Array(ct, subfield.shape)
                # else:
                #     shared = None

                # Return a partition of the input array with the corresponding
                # voxel offset and subset of interpolation points.
                # print("subfield:", subfield.shape)
                yield subfield, lows, points

    def _get_optimal_chunksize(self, coordinates, input_array, memlimit):
        """
        This method optimises the interpolators cost_function method to promote
        a beneficial choice of N, i.e. the number of interpolation points that
        are loaded into memory at once.

        :param coordinates:
            (n_points, n_coordinates) table of interpolation points.
        :type coordinates: Union[np.ndarray, np.memmap]
        :param input_array:
            Array of known values that are used in the interpolation.
        :type input_array: Union[np.ndarray, np.memmap]
        :param memlimit:
            Total amount of memory in bytes that is available for the
            Interpolator object.

        :returns: optimal chunksize
        :rtype: int

        """
        # Obtain basic information
        n_points, ndim = coordinates.shape
        t_shape = [input_array.shape[ax] for ax in self.tensor_axes]
        n_channels = reduce(mul, t_shape) if t_shape else 1
        p_shape = tuple(input_array.shape[ax] for ax in range(input_array.ndim)
                        if ax not in self.tensor_axes)
        n_nodes = reduce(mul, p_shape)

        sizeof_coordinate = np.dtype(coordinates.dtype).itemsize
        # Calculate interpolant for the first coordinate to determine the
        # data types accurately, as this depends on the implementation of the
        # actual interpolator instance.
        sizeof_fieldvalue = np.dtype(input_array.dtype).itemsize
        # out_single = self.interpolate(, )
        # sizeof_output = np.dtype(out_single.dtype).itemsize
        sizeof_output = 8
        # The input is often converted to the same type as the final output,
        # so this must be taken into account.
        sizeof_fieldvalue = max(sizeof_fieldvalue, sizeof_output)

        # Initial guess
        N = int(memlimit //
                ((ndim + 1) * sizeof_coordinate +
                 (n_nodes / n_points) * n_channels * sizeof_fieldvalue +
                 n_channels * sizeof_output))

        if N <= 1:
            raise MemoryError(
                "Not enough memory to interpolate field at a single point.")
        elif N >= n_points:
            # Do not waste time on optimisation if all points can be loaded
            # into memory based on the initial guess.
            return n_points
        else:
            # Attempt to enforce the choice of N such that the corresponding
            # partition of the input array fits in memory. Otherwise a recursive
            # algorithm is called that creates smaller subdivisions of the input
            # array, which takes longer to execute.
            N = fmin(self._cost_function, N, maxfun=10,
                     args=(0.5, coordinates, input_array, memlimit, n_channels,
                           ndim, sizeof_coordinate, sizeof_fieldvalue,
                           sizeof_output, p_shape))
        return int(N)

    def _cost_function(self, N, k, coordinates, input_array, memlimit,
                       n_channels, ndim, sizeof_coordinate, sizeof_fieldvalue,
                       sizeof_output, p_shape):
        """
        Cost function that is minimised to choose the number of interpolation
        points that are loaded into memory at once.

        The aim is to facilitate an optimal choice of N, such that the
        corresponding convex partition of the input array fits into memory, and
        the recursion to divide the input further can be avoided.

        :param N:
            The number of interpolation points to load into memory at once.
        :type N: int
        :param k:
            Ratio of the required and the available memory to load a convex
            partition of the input array that encapsulates all N interpolation
            points.
        :type k: int
        :param coordinates:
            (n_points, n_coordinates) table of interpolation points.
        :type coordinates: Union[np.ndarray, np.memmap]
        :param input_array:
            Input array of known values. Usually some form of a tensor field.
        :type input_array: Union[np.ndarray, np.memmap, domain.Domain]
        :param memlimit:
            Total available memory in bytes that can be used by the
            Interpolator.
        :type memlimit: int
        :param n_channels:
            Total number of channels (independent tensor components) in the
            input_array.
        :type n_channels: int
        :param ndim:
            Number of spatial coordinates in the specification of the
            interpolation points.
        :type ndim: int
        :param sizeof_coordinate:
            Number of bytes that are necessary to store a single coordinate.
        :type sizeof_coordinate: int
        :param sizeof_fieldvalue:
            Number of bytes that are necessary to store a single value in the
            input_array.
        :type sizeof_fieldvalue: int
        :param p_shape:
            Spatial extent of the input array.
        :type p_shape: tuple[int]

        :returns: cost value
        :rtype: float

        """
        N = int(N)
        # Determine the amount of memory left to hold the corresponding convex
        # partition of the input array.
        field_limit = \
            memlimit - N * ((ndim + 1) * sizeof_coordinate +
                            n_channels * sizeof_output)
        # Determine the extent of the corresponding convex partition of the
        # input array.
        box, required = self._get_subfield(
            coordinates[:N], p_shape, sizeof_fieldvalue)
        return (field_limit * k - required) ** 2

    def _get_subfield(self, points, domain_shape, sizeof_fieldvalue):
        """
        Returns the spatial extent and the memory requirement of the minimal
        convex partition of the input array that contains the specified
        interpolation points.

        Note that the minimal partition includes a neighbourhood in all
        directions, the extent of which is defined by the instance-specific
        variable 'self.radius'.

        :param points:
            (n_points, n_coordinates) table of interpolation points.
        :type points: Union[np.ndarray, np.memmap]
        :param domain_shape:
            Spatial extent of the input array. This is necessary to identify
            truncations at the edge of the input array.
        :type domain_shape: tuple
        :param sizeof_fieldvalue:
            Number of bytes that are used to represent the values of the input
            array.
        :type sizeof_fieldvalue: int

        :returns:
            box: the spatial extent of the corresponding input array partion
            required: the memory footprint of the corresponding input array
                      partition
        :rtype: tuple[tuple, int]

        """
        # Is there enough memory to load the corresponding convex section of the
        # field, and interpolate at the current set of points?
        highs = np.ceil(np.max(points, axis=0)) + self.radius
        shape = np.asarray(domain_shape)
        highs = np.where(highs < shape - 1, highs, shape - 1)
        lows = np.floor(np.min(points, axis=0)) - self.radius
        lows = np.where(lows > 0, lows, 0)
        box = highs + 1 - lows
        required = reduce(mul, box) * sizeof_fieldvalue
        return box, required

    def _subdivide(self, coordinates, domain_shape, sizeof_fieldvalue,
                   field_limit, sortaxis=-1, axis=0):
        """
        This is a recursive method that divides a subset of interpolation
        points until a smaller subset of points can finally be loaded into
        memory with the corresponding convex partition of the input array, and
        the result of the interpolation can also be stored in memory.

        Points are sorted along the first axis, and bisected recursively until
        the resultant subset of points is small enough to allow in-memory
        interpolation. If the stopping criteria are not met, the remaining
        points are sorted along the second axis, and so on.

        :param coordinates:
            (n_points, n_coordinates) table of interpolation points.
        :type coordinates: np.ndarray
        :param domain_shape:
            Spatial extent of the input array. This information is necessary to
            identify truncations at the edge of the array, should any of the
            input coordinates go beyond these.
        :type domain_shape: tuple[int]
        :param sizeof_fieldvalue:
            Number of bytes used to represent a single value of the input array.
        :type sizeof_fieldvalue: int
        :param field_limit:
            The amount of memory available to hold the corresponding partition
            of the input field (= total - indexed coordinates - results).
        :param sortaxis:
            The coordinate axis along which the points have been sorted. -1 if
            the input is not sorted. This is necessary to avoid repeated sorts
            along the same axis.
        :type sortaxis: int
        :param axis:
            The axis to be bisected recursively. If the criteria are not met
            after a series of bisection along one axis, this parameter is
            incremented.
        :type axis: int

        :returns:
            A generator that returns independent partitions of the specified
            inpterpolation points.
        :rtype: generator

        """
        # Restore tabular shape of interpolation points if a reduction
        # operation previously created a flat array from the coordinates of a
        # single point.
        if coordinates.ndim == 1:
            coordinates = coordinates.reshape((1, -1))

        # Obtain basic information
        assert coordinates.ndim == 2
        n_points, ndim = coordinates.shape
        n_coordinates = ndim - 1   # compensate for the last column of indices

        # Calculate the corresponding convex subfield based on the current set
        # of points, and determine whether it fits into the memory. The column
        # of indices is discarded.
        box, required = self._get_subfield(
            coordinates[:, :-1], domain_shape, sizeof_fieldvalue)

        # Proceed if the current subset of interpolation points is too large
        if required > field_limit:

            # Sort the points by one of their coordinates
            if sortaxis != axis:
                typecode = np.dtype(coordinates.dtype).descr[0][1][1:]
                viewtype = ",".join((typecode,) * ndim)
                fieldcode = "f%d" % axis
                coordinates.view(viewtype).sort(axis=0, order=[fieldcode])
                sortaxis = axis

            # Proceed as long as the subset of points can be bisected further
            # along the same axis.
            if box[axis] > 1 + 2 * self.radius:
                half = n_points // 2
                step = 0

            # Switch to the next axis otherwise
            else:
                half = n_points
                step = 1
                assert axis + step < n_coordinates

            # If not empty, pass lesser half to the next level of recursion
            if half > 0:
                yield from self._subdivide(
                    coordinates[:half, :], domain_shape, sizeof_fieldvalue,
                    field_limit, sortaxis, axis + step)

            # If not empty, pass greater half to the next level of recursion
            if n_points > half:
                yield from self._subdivide(
                    coordinates[half:, :], domain_shape, sizeof_fieldvalue,
                    field_limit, sortaxis, axis + step)

        else:
            yield coordinates

    def _presort(self, input_array, target):
        """
        Pre-sort the array of target coordinates before interpolation. Implement
        this method to present the target coordinates to the interpolation
        algorithm in a more efficient order.

        This is useful if the interpolant field is larger than memory, and
        interpolation must be carried out in rectangular chunks. If the target
        points follow a specific spatial pattern, convex subsets of points
        might be clustered (sorted) together. In this case fewer chunks and
        fewer I/O operations are required to carry out the interpolation, which
        results in faster performance.

        The base class does not implement this method, but it MAY be overloaded
        by subclasses for a specific application where high performance is
        necessary.

        :param input_array:
            Array of known values. This is passed on by the __call__ method,
            should it be required to define the sorting algorithm.
        :type input_array: np.ndarray
        :param target:
            (m_points, n_coordinates) array of target coordinates
        :type target: np.ndarray

        :returns:
            (m_points, n_coordinates) array of target coordinates in optimal
            order. Although discouraged, sorting might be in-place, but in this
            case the method must also return the sorted array.
        :rtype: np.ndarray

        """
        return target

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __call__(self, target, output=None, input_array=None, mode=MEM):
        """
        Uniform call interface of Interpolators that takes care of
        parallelisation and memory-mapped input/output.

        :param target:
            (m_points, n_coordinates) table of coordinates or Domain instance.
            WARNING: If target is a Domain, the interpolation will be performed
            at the voxel coordinates of the Domain, not the physical
            coordinates! (That functionality is provided by TField.evaluate.)
        :type target: Union[np.ndarray, domain.Domain]
        :param output:
            (m_points, t1, t2, ..., tk) flattened array of tensors with k
            tensor dimensions.
        :type output: np.ndarray
        :param input_array:
            Array of known values. The interpolation domain is defined
            implicitly by the shape of this array. If None, the values are
            taken from the input_array specified at initialisation.
        :type input_array: Union[np.ndarray, None]
        :param mode:
            If MEM, the result of the interpolation is returned as an array
            in memory. If HDD, a new file is created in the temporary working
            directory (see settings.TWD), and returned as a memory-mapped array.
        :type mode: HDD or MEM

        :returns:
            (m_points, t1, t2, ..., tk) result of interpolation (only if
            output=None)
        :rtype: Union[None, np.ndarray]

        """
        # As interpolations are usually called several times, care must be
        # taken to avoid any unnecessary type validations etc, as these can be
        # ruled out once and for all earlier in the process to save time on
        # repeated calls.
        target = np.atleast_2d(target)

        mode = str(mode).lower()

        # Set default input array
        input_array = self.values if input_array is None else input_array
        itemsize = np.dtype(input_array.dtype).itemsize

        # Retrieve target coordinates and determine the voxel shape of
        # the output.
        if hasattr(target, "__domain__"):
            coordinates = target.get_voxel_coordinates()
            coordinates = self._presort(input_array, coordinates)
            voxel_shape = target.shape   # either compact or scattered
        elif isinstance(target, np.ndarray):
            coordinates = target
            if coordinates.ndim != 2:
                raise te.ArgumentError("Invalid shape for coordinate array.")
            n_points, n_coordinates = coordinates.shape
            voxel_shape = (n_points,)
        else:
            raise TypeError("Invalid target specification.")

        # Assumption: the tensor axes are either leading or trailing axes of
        # the input array, but never inner axes.
        tdim = len(self.tensor_axes)
        if tdim > 0:
            leading = tuple(range(input_array.ndim))[:tdim]
            trailing = tuple(range(input_array.ndim))[-tdim:]

            # Determine the tensor shape and the full shape of the output
            tensor_shape = \
                tuple(input_array.shape[ax] for ax in self.tensor_axes)
            n_channels = reduce(mul, tensor_shape)
            if self.tensor_axes == leading:
                result_shape = tensor_shape + voxel_shape
                isleading, istrailing, issimple = True, False, False
            elif self.tensor_axes == trailing:
                result_shape = voxel_shape + tensor_shape
                isleading, istrailing, issimple = False, True, False
            else:
                raise te.ArgumentError(
                    "The interpolator's tensor axes must specify either "
                    "leading or trailing axes of the input array.")
        else:
            tensor_shape = ()
            n_channels = 1
            isleading, istrailing, issimple = False, False, True
            result_shape = voxel_shape

        # Prepare output
        if output is None:
            if mode == MEM:
                output = np.zeros(result_shape, dtype=input_array.dtype,
                                  order="C")
            elif mode == HDD:
                # Pre-allocate space for interpolation result
                fd, fname = tempfile.mkstemp(dir=ts.TWD, prefix="ip_")
                with open(fname, mode="r+") as f:
                    output_size = reduce(mul, result_shape) * itemsize
                    f.seek(output_size - 1)
                    f.write("\n")
                output = np.memmap(fname, mode="r+", dtype=input_array.dtype,
                                   offset=0, shape=result_shape, order="C")
            else:
                raise te.ArgumentError(
                    "Unrecognised input for mode. Please choose from 'mem' "
                    "and 'hdd'.")
        else:
            if output.shape != result_shape:
                raise ValueError(
                    "The shape of interpolation results {} does not match the "
                    "output shape {}.".format(result_shape, output.shape))

        # Break down the interpolation problem into independent in-memory
        # tasks, while respecting the "hold" instruction.
        if self.hold:
            return self._call_with_hold(
                coordinates, input_array, n_channels, tensor_shape, issimple,
                isleading, istrailing, output)
        else:
            return self._call_without_hold(
                coordinates, n_channels, tensor_shape, input_array, output,
                isleading, istrailing, issimple)

    def interpolate(self, coordinates, input_array=None, **ipkwargs):
        """
        Interpolation function that returns the results of interpolating
        a scalar field. The implementation should regard all inputs and
        outputs as if they were in memory. The base class implementation is
        responsible for handling memory-mapped input and output upstream to
        this method.

        :param coordinates:
            (n_points, n_coordinates) array of interpolation points
        :type coordinates: np.ndarray
        :param input_array:
            Array of known values. The interpolation domain is defined
            implicitly by the shape of this array. If None, the values are
            taken from the input_array specified at initialisation.
        :type input_array: Union[np.ndarray, None]

        """
        raise NotImplementedError()


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")
