#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import warnings
import numpy as np


# TIRL IMPORTS

from tirl import settings as ts
from tirl.interpolators.interpolator import Interpolator


# IMPLEMENTATION

class ScipyInterpolator(Interpolator):
    """
    ScipyInterpolator class - wrapper around Scipy.ndimage.map_coordinates

    """

    def interpolate(self, coordinates, input_array=None, **ipkwargs):

        from scipy.ndimage.interpolation import map_coordinates

        kw = {k: v for k, v in self.kwargs.get("ipkwargs", dict()).items()}
        # Override the ones that were specified for the call
        kw.update(ipkwargs)
        mode = kw.get("mode", None)

        # Set default fill value unless the mode parameter was given
        if mode is None:
            mode = "constant"
            cval = self.kwargs.get("fill_value")
            kw.update(mode=mode, cval=cval)

        if isinstance(coordinates, (tuple, list)):
            coordinates = np.asarray(coordinates).reshape((1, -1))
        try:
            res = map_coordinates(input_array, coordinates.T, **kw)
        except Exception:
            print("Interpolation failed with the following input: ",
                  input_array.shape, coordinates.shape, kw)
            raise
        return res


class ScipyRegularGridInterpolator(Interpolator):

    def __init__(self, source, tensor_axes=(), radius=1, hold=False,
                 threads=-1, verbose=False, ipkwargs=None, fill_value=None,
                 **kwargs):
        """
        Initialisation of ScipyRegularGridInterpolator

        """
        # Set default arguments for the RegularGridInterpolator.
        # These parameters will repress out-of-bounds errors when values need
        # to be interpolated outside the domain of known values.
        default_ipkwargs = dict(bounds_error=False, fill_value=fill_value)
        if isinstance(ipkwargs, dict):
            default_ipkwargs.update(ipkwargs)
        ipkwargs = default_ipkwargs

        # Call parent-class initialisation
        super(ScipyRegularGridInterpolator, self).__init__(
            source, tensor_axes=tensor_axes, radius=radius, hold=hold,
            threads=threads, verbose=verbose, ipkwargs=ipkwargs,
            fill_value=fill_value, **kwargs)

        if self.ipkwargs.get("fill_value", None) is None:
            self.ipkwargs.update(fill_value=ts.FILL_VALUE)

    def interpolate(self, coordinates, input_array=None, **ipkwargs):
        """
        Interpolation routine.

        """
        from scipy.interpolate import RegularGridInterpolator

        kw = {k: v for k, v in self.kwargs.get("ipkwargs", dict()).items()}
        # Override the ones that were specified for the call
        kw.update(ipkwargs)

        if isinstance(coordinates, (tuple, list)):
            coordinates = np.asarray(coordinates).reshape((1, -1))
        try:
            grid = tuple(np.arange(dim) for dim in input_array.shape)
            ip = RegularGridInterpolator(grid, input_array, **kw)
            res = ip(coordinates)
        except:
            print("Interpolation failed with the following input: ",
                  input_array.shape, coordinates.shape, kw)
            raise
        else:
            return res


class ScipyNearestNeighbours(Interpolator):

    def __init__(self, source, tensor_axes=(), radius=1, hold=False,
                 threads=-1, verbose=False, ipkwargs=None, fill_value=None,
                 **kwargs):
        """
        Initialisation of ScipyNearestNeighboursr

        """
        # Set default arguments for the RegularGridInterpolator.
        # These parameters will repress out-of-bounds errors when values need
        # to be interpolated outside the domain of known values.
        default_ipkwargs = dict(rescale=False)
        if isinstance(ipkwargs, dict):
            default_ipkwargs.update(ipkwargs)
        ipkwargs = default_ipkwargs

        # Note: the fill_value parameter lands in the kwargs, as it is not used
        # by this interpolator.
        if fill_value is not None:
            warnings.warn(f"The fill_value parameter was ignored by "
                          f"{self.__class__.__name__}")

        # Call parent-class initialisation
        super(ScipyNearestNeighbours, self).__init__(
            source, tensor_axes=tensor_axes, radius=radius, hold=hold,
            threads=threads, verbose=verbose, ipkwargs=ipkwargs, **kwargs)


    def interpolate(self, coordinates, input_array=None, **ipkwargs):

        from scipy.interpolate import NearestNDInterpolator

        kw = {k: v for k, v in self.kwargs.get("ipkwargs", dict()).items()}
        # Override the ones that were specified for the call
        kw.update(ipkwargs)

        if isinstance(coordinates, (tuple, list)):
            coordinates = np.asarray(coordinates).reshape((1, -1))
        try:
            vcoords = np.stack(
                np.meshgrid(*tuple(range(dim) for dim in input_array.shape),
                            indexing="ij"), axis=-1)
            ip = NearestNDInterpolator(vcoords.reshape((-1, input_array.ndim)),
                input_array.ravel(), **kw)
            res = ip(coordinates)
        except:
            print("Interpolation failed with the following input: ",
                  input_array.shape, coordinates.shape, kw)
            raise
        else:
            return res
