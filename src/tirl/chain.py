#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import dill
import hashlib
import numpy as np
from operator import add
from functools import reduce


# TIRL IMPORTS

from tirl import settings as ts
from tirl.signature import Signature


# DEFINITIONS

from tirl.constants import TX_LINEAR
from tirl.constants import RULE_SSR


# IMPLEMENTATION

class Chain(list):
    """
    Chain is a list of Transformation objects that implements convenience
    features such as reduction, advanced indexing by both position and name,
    as well as methods to test equality of Chains, obtain parameters and
    transformation derivatives. Chain is mutable: its elements can be
    modified.

    """

    def __init__(self, *transformations):
        """
        Initialisation of Chain.

        :param transformations: transformations
        :type transformations: Transformation

        """
        # Construction with iterable
        if (len(transformations) == 1) and \
                hasattr(transformations[0], "__iter__"):
            super(Chain, self).__init__(transformations[0])
            return

        # Construction by explicitly listing transformations
        for tx_no, tx in enumerate(transformations):
            if not hasattr(tx, "__tx_priority__"):
                raise TypeError(
                    f"Expected Transformation object at chain position {tx_no} "
                    f"(zero-indexed), got {tx.__class__.__name__} instead.")
        else:
            super(Chain, self).__init__(transformations)

    def copy(self):
        """
        Returns new Chain instance with copies of the transformations.

        """
        return Chain(tx.copy() for tx in self)

    def __chain__(self):
        """ Type attribute. """
        return self

    def __repr__(self):
        # listrepr = super(Chain, self).__repr__()
        # return f"Chain{listrepr}"
        if len(self) == 0:
            return f"Chain[]"
        elif len(self) == 1:
            return f"Chain[{self[0].__repr__()}]"
        else:
            listrep = "\n".join([tx.__repr__() for tx in self])
            return f"Chain[\n{listrep}\n]"

    def __add__(self, other):
        other = [other] if hasattr(other, "__tx_priority__") else other
        result = super(Chain, self).__add__(other)
        return Chain(*result)

    def __radd__(self, other):
        if hasattr(other, "__tx_priority__"):
            return Chain(other, *self)
        elif hasattr(other, "__iter__"):
            return Chain(*other, *self)
        else:
            return NotImplemented

    def __bool__(self):
        return len(self) != 0

    def __eq__(self, other):
        """
        Two Chains are equal if they map all points to the same points globally.

        The equality operator first checks trivial indicators of equality and
        inequality, then it performs more computationally demanding tests to
        determine equality. A True result is always trustworthy, but a False
        result may indicate differences in the metaparameters or the extent of
        the transformation (domain), which may or may not be relevant for
        mapping certain sets of coordinates.

        E.g. comparing a linear Chain with a non-linear Chain that produces
        equivalent mapping in a confined domain returns False, because the
        linear chain reduces to a linear transformation, whereas the non-linear
        chain reduces to a defomration field, and these transformation types
        are different. For a conceptual justification one might say that
        they are not globally equivalent, because outside the local domain of
        the non-linear transformation their mapping is different.

        """
        # Equality is granted if the Chain IDs are identical
        if self is other:
            return True

        # Equality is impossible if the other object is not a Chain-like object
        if not isinstance(other, type(self)):
            return False

        # Let's see if we can verify equality by comparing signatures
        selfsig = self.signature()
        othersig = other.signature()

        # Equality granted by all transformations being pairwise identical
        if selfsig(0) == othersig(0):
            return True

        # If the transformations are maximally reduced ...
        if (len(self) == 1) and (len(other) == 1):

            # If they are linear transformations with an identical matrix ...
            if self[0].kind == other[0].kind == TX_LINEAR:
                try:
                    atol = np.finfo(ts.DEFAULT_FLOAT_TYPE).eps
                    return np.allclose(
                        self[0].matrix - other[0].matrix, 0, atol=atol)
                except:
                    return False

            # For other kinds of transformations we must check the identity
            # of all parameters and metaparameters
            else:
                if selfsig(1) != othersig(1):  # type (controls behaviour)
                    return False
                if selfsig(2) != othersig(2):  # parameters
                    return False
                # if selfsig(3) != othersig(3):  # metaparameters
                #     return False
                return True

        # If further reduction of the transformations is possible ...
        else:

            # If the transformation classes are pairwise identical, we can
            # directly check parameter and metaparameter equivalence in these
            if selfsig(1) == othersig(1):

                if selfsig(2) != othersig(2):  # parameters
                    return False
                # if selfsig(3) != othersig(3):  # metaparameters
                #     return False
                return True

            # Otherwise we have to reduce the chains and re-test equality
            else:
                self_reduced = self.reduce(merge="all")
                other_reduced = other.reduce(merge="all")
                return self_reduced == other_reduced

    def __getitem__(self, item):
        """
        Returns an element from the transformation Chain either by its index
        or by its name.

        :returns:
            Depending on the number of indexing elements, slicing either
            returns a Chain with multiple transformations or a Transformation
            object.
        :rtype: Chain or Transformation

        """
        # Convert single index term to multi-index term
        if isinstance(item, (int, str)):
            item = (item,)
            return_tx = True
        else:
            return_tx = False

        # Chain supports multi-indexing by a sequence of integers/strings
        if hasattr(item, "__iter__"):
            chain = []
            for ix in item:
                if isinstance(ix, str):
                    try:
                        ix = [tx.name for tx in self].index(ix)
                    except ValueError:
                        raise IndexError(f"'{ix}' is not in chain.")
                elif isinstance(ix, int):
                    ix = ix
                else:
                    raise TypeError(f"Invalid Chain index: {ix}")
                chain.append(super(Chain, self).__getitem__(ix))
            else:
                if return_tx:
                    return chain[0]
                else:
                    return Chain(chain)

        # Resort to list indexing
        else:
            result = super(Chain, self).__getitem__(item)
            return Chain(*result) if isinstance(result, list) else result

    def __setitem__(self, key, value):
        """
        :returns:
            Depending on the number of indexing elements, slicing either
            returns a Chain with multiple transformations or a Transformation
            object.
        :rtype: Chain or Transformation

        """
        # Multi-element setting by a sequence of integer indices
        if hasattr(key, "__iter__"):
            if all(isinstance(ix, int) for ix in key):
                if hasattr(value, "__iter__"):
                    for ix, val in zip(key, value):
                        if hasattr(val, "__tx_priority__"):
                            super(Chain, self).__setitem__(ix, val)
                        else:
                            raise TypeError(f"{val} is not a Transformation.")
                else:
                    for ix in key:
                        if hasattr(value, "__tx_priority__"):
                            super(Chain, self).__setitem__(ix, value)
                        else:
                            raise TypeError(f"{value} is not a Transformation.")
            else:
                raise TypeError("Chain multi-indexing expects integer indices.")

        # Multi-element setting by slice indexing
        elif isinstance(key, slice):
            indices = range(*key.indices(len(self)))
            if len(indices) > 1:
                for i, ix in enumerate(indices):
                    if hasattr(value, "__iter__"):
                        if hasattr(value[i], "__tx_priority__"):
                            super(Chain, self).__setitem__(ix, value[i])
                        else:
                            raise TypeError(
                                f"{value[i]} is not a Transformation.")
                    else:
                        if hasattr(value, "__tx_priority__"):
                            super(Chain, self).__setitem__(ix, value)
                        else:
                            raise TypeError(f"{value} is not a Transformation.")
            else:
                if hasattr(value, "__tx_priority__"):
                    return super(Chain, self).__setitem__(key, value)
                else:
                    raise TypeError(f"{value} is not a Transformation.")

        # Single-element setting by slice or integer indexing
        else:
            if hasattr(value, "__tx_priority__"):
                return super(Chain, self).__setitem__(key, value)
            else:
                raise TypeError(f"{value} is not a Transformation.")

    def signature(self):
        """
        The signature of two transformation chains are equal if the represented
        transformations are globally equivalent. The returned value
        is a Signature object.

        """
        # Level 0: transformation object IDs
        def level0():
            hasher = hashlib.sha1()
            for tx in self:
                hasher.update(str(id(tx)).encode("utf-8"))
            else:
                return hasher.hexdigest()

        # Level 1: transformation classes
        def level1():
            hasher = hashlib.sha1()
            for tx in self:
                module = tx.__class__.__module__
                if module is None:
                    cname = tx.__class__.__name__
                else:
                    cname = module + "." + tx.__class__.__name__
                hasher.update(str(cname).encode("utf-8"))
            else:
                return hasher.hexdigest()

        # Level 2: transformation parameters
        def level2():
            hasher = hashlib.sha1()
            for tx in self:
                # Only compare parameter values. Bounds and lock states are
                # related to optimisation; any difference in these will not
                # affect the mapping, hence chains can be equal.
                hasher.update(dill.dumps(tx.parameters[:]))
            else:
                return hasher.hexdigest()

        # Level 3: transformation metaparameters
        def level3():
            hasher = hashlib.sha1()
            for tx in self:
                hasher.update(dill.dumps(tx.metaparameters))
            else:
                return hasher.hexdigest()

        return Signature(level0, level1, level2, level3)

    def reduce(self, merge="linear"):
        """
        Returns a new Chain with the minimum number of transformations that
        provide identical mapping to the original Chain. The parameter 'merge'
        controls the type of transformations that will be merged during
        reduction. Note that the result of chain reduction depends on the
        implementation of the 'add' operator in Transformation object and their
        relative priority values.

        :param merge:
            "linear" (default): linear subchains are replaced by a single
                                linear transformation
            "nonlinear": non-linear subchains are replaced by a single
                         non-linear transformation
            "all": linear and non-linear transformations are replaced by a
                   single linear transformation if all transformations are
                   linear, otherwise replaced by a single non-linear
                   transformation
        :type merge: str

        :returns:
            Minimal equivalent transformation chain.
        :rtype: Chain

        """
        def replace(subchain, kind):
            reduced = Chain()
            selected = []
            while len(subchain) > 0:
                tx = subchain.pop(0)
                if selected:
                    if (tx.kind != selected[0].kind) or (tx.kind != kind):
                        reduced.append(reduce(add, selected))
                        selected = [tx]
                    else:
                        selected.append(tx)
                else:
                    selected.append(tx)
            else:
                if selected:
                    reduced.append(reduce(add, selected))
            return reduced

        newchain = Chain(*self)
        if merge == "linear":
            newchain = replace(newchain, "linear")
        elif merge == "nonlinear":
            newchain = replace(newchain, "nonlinear")
        elif merge == "all":
            txs = replace(replace(newchain, "linear"), "nonlinear")
            newchain = Chain()
            acc = None
            for tx in txs:
                if tx.kind in ("linear", "nonlinear"):
                    acc += tx
                else:
                    if acc is not None:
                        newchain.append(acc)
                        acc = None
                    newchain.append(tx)
            else:
                if acc is not None:
                    newchain.append(acc)
        else:
            raise ValueError(f"Unrecognised merge option: '{merge}'")

        return newchain

    def inverse(self):
        return Chain(*tuple(tx.inverse() for tx in self[::-1]))

    def map(self, coords, merge="linear", signature=None):
        if (merge is None) or (merge is False):
            minimal_chain = self
        else:
            minimal_chain = self.reduce(merge=merge)
        for tx in minimal_chain:
            coords = tx.map(coords, signature=signature)
        else:
            return np.asanyarray(coords)

    def map_with_signature(self, coords, merge="linear"):
        return self.map(coords, merge=merge), self.signature()

    # Note that chains must update the locations for the vectors/tensors before
    # applying the next transformation on them, so its not possible to fully
    # isolate the transformation of vectors/tensors from the transformation of
    # coordinates to achieve higher performance. This can only be skipped for
    # the final transformation, or the only transformation in the chain. As
    # in many cases TIRL Chains will have a single transformation, this
    # optimisation was baked into the map_vector and map_tensor methods, which
    # should be used whenever the transformed coordinates are not needed by
    # the caller.

    def map_vector(self, vects, coords=None, rule=RULE_SSR, merge="linear"):
        if (merge is None) or (merge is False):
            minimal_chain = self
        else:
            minimal_chain = self.reduce(merge=merge)
        # Update vectors and coordinates through the largest part of the chain
        for tx in minimal_chain[:-1]:
            vects, coords = tx.map_vector_and_coordinates(vects, coords, rule)

        # Only apply the final transformation to the vectors
        if minimal_chain[-2:]:
            last_tx = minimal_chain[-1]
            vects = last_tx.map_vector(vects, coords, rule)

        # Need to cast as the input will just pass through for an empty chain
        return np.asanyarray(vects)

    def map_tensor(self, tensors, coords=None, rule=RULE_SSR, merge="linear"):
        if (merge is None) or (merge is False):
            minimal_chain = self
        else:
            minimal_chain = self.reduce(merge=merge)
        for tx in minimal_chain[:-1]:
            tensors, coords = tx.map_tensor_and_coordinates(
                tensors, coords, rule)

        if minimal_chain[-2:]:
            last_tx = minimal_chain[-1]
            tensors = last_tx.map_tensor(tensors, coords, rule)

        # Need to cast as the input will just pass through for an empty chain
        return np.asanyarray(tensors)

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=RULE_SSR, merge="linear"):
        if (merge is None) or (merge is False):
            minimal_chain = self
        else:
            minimal_chain = self.reduce(merge=merge)
        for tx in minimal_chain:
            vects, coords = tx.map_vector_and_coordinates(vects, coords, rule)

        # Need to cast as the input will just pass through for an empty chain
        return np.asanyarray(vects), np.asanyarray(coords)

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=RULE_SSR, merge="linear"):
        if (merge is None) or (merge is False):
            minimal_chain = self
        else:
            minimal_chain = self.reduce(merge=merge)
        for tx in minimal_chain:
            tensors, coords = tx.map_tensor_and_coordinates(
                tensors, coords, rule)

        # Need to cast as the input will just pass through for an empty chain
        return np.asanyarray(tensors), np.asanyarray(coords)

    def apply(self, tf):
        ntx = len(self)
        tf.domain.chain.extend(self)
        result = tf.evaluate(tf.domain[:-ntx])
        tf.domain = tf.domain[:-ntx]
        return result

    def prepend(self, item):
        """ This operation is obviously a bit costly. """
        if hasattr(item, "__tx_priority__"):
            if len(self) == 0:
                self.append(item)
                return
            else:
                self.append(self[-1])
                for i in range(len(self) - 2, 0, -1):
                    self[i] = self[i - 1]
                else:
                    self[0] = item
        else:
            raise TypeError("The chain can be prepended by "
                            "a single transformation.")
