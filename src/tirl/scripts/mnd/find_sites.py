#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 9 June 2020


# SHBASECOPYRIGHT


from tirl.scripts.mnd import __version__
__tirlscript__ = True


# DEPENDENCIES

import os
import sys
import logging
import argparse
import numpy as np
from operator import add
from functools import reduce
from functools import partial
import multiprocessing as mp
from attrdict import AttrMap
from math import radians, degrees
from skimage.measure import label
from skimage.filters import median
from scipy.ndimage.morphology import distance_transform_edt as edt
from skimage.measure import regionprops


# TIRL IMPORTS

import tirl
from tirl.chain import Chain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl import settings as ts
from tirl.costs.mi import CostMI
from tirl.costs.mind import CostMIND
from tirl.transformations.linear.scale import TxScale
from tirl.transformations.linear.rotation import TxRotation2D
from tirl.transformations.linear.translation import TxTranslation
from tirl.transformations.nonlinear.displacement import TxDisplacementField
from tirl.transformations.transformation import TransformationGroup
from tirl.regularisers.diffusion import DiffusionRegulariser
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion


# CONDITIONAL IMPORT

if not os.getenv("DISPLAY") or not ts.ENABLE_VISUALISATION:
    import matplotlib
    matplotlib.use("agg")
else:
    import matplotlib
    matplotlib.use(ts.MPL_BACKEND)
import matplotlib.pyplot as plt


# DEFINITIONS

from tirl.constants import *

OPTIONS_RIGID2D = {
    "scaling": [],
    "smoothing": []
}

OPTIONS_ROTSEARCH = {
    "coarse": 30,
    "scale": 0.05,
    "n_best": 3,
    "visualise": False,
    "xtol_rel": 0.01,
    "xtol_abs": [0.01, 0.001, 0.001, 0.1, 0.1],
    "opt_step": 0.1
}

OPTIONS_NONLINEAR = {
    "scaling": [30, 20],
    "smoothing": [0, 0],
    "sigma": 1,
    "truncate": 1.5,
    "regweight": 0.6,
    "maxiter": [20, 20],
    "xtol_abs": 0.1,
    "xtol_rel": 0.01,
    "visualise": False
}

np.set_printoptions(precision=4)


# IMPLEMENTATION

def create_logger(logfile, paramlogfile=None, verbose=False, mode="a"):
    """
    Creates logger instance.

    """
    logger = logging.getLogger("tirl.scripts.mnd.find_sites")
    logger.handlers = []
    fh = logging.FileHandler(logfile, mode=mode, delay=False)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        fmt="%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) "
            "- %(message)s",
        datefmt="[%Y-%m-%d %H:%M:%S]")
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Also print messages to the command line if required
    if verbose:
        sh = logging.StreamHandler()
        sh.setLevel(1)
        logger.addHandler(sh)

    # Redirect parameter updates to the root logger
    if paramlogfile is None:
        return logger

    # Redirect parameter update logs to the standard output (terminal)
    elif paramlogfile == "stdout":
        ph = logging.StreamHandler()
        ph.setFormatter(logging.Formatter(fmt="%(message)s"))
        ph.setLevel(1)
        pf = logging.Filter()
        pf.filter = lambda \
                record: record.levelno == ts.PARAMETER_UPDATE_LOG_LEVEL
        ph.addFilter(pf)
        logger.addHandler(ph)

    # Redirect parameter update logs to the parameter log file
    else:
        ph = logging.FileHandler(paramlogfile, mode=mode, delay=False)
        ph.setFormatter(logging.Formatter(fmt="%(message)s"))
        ph.setLevel(1)
        pf = logging.Filter()
        pf.filter = \
            lambda record: record.levelno == ts.PARAMETER_UPDATE_LOG_LEVEL
        ph.addFilter(pf)
        logger.addHandler(ph)

    return logger


def sort_images(*images):
    """
    Assuming foreground-segmented images, sort the input brain slice
    photographs in descending order based on the number of foreground pixels.
    The image with the largest number of foreground pixels should be the
    "intact" image that is used for stage-3 (slice-to-volume) registration.

    """
    if len(images) == 1:
        return images

    counts = []
    for img in images:
        img = TImage(img).reduce_tensors(copy=True)
        counts.append(np.count_nonzero(img.data > 0))
    else:
        order = np.argsort(counts)[::-1]

    return tuple(images[ix] for ix in order)


def pairwise_registrations(p, *images, logger=None):
    """
    Assuming that the input images are ordered from the most intact to the most
    sampled, determine the insertion sites. Rigid-body registration takes place
    between each consecutive pair of images, including a more inteact (i) and a
    more sampled (s) image. Insertion sites are reported in voxel coordinates
    of the more intact half of the pair (image i).

    """
    p = AttrMap(p)

    if len(images) == 1:
        # Note: detecting potential insertion sites is certainly possible with
        # some degree of accuracy, but this would require recognising features
        # from a highly variable set of photos, so it is not supported by the
        # current version of the script.
        raise AssertionError(
            "Need at least two images to determine insertion sites. Detection "
            "from a single slice is not currently implemented by this script.")

    # Save the sampling order of the input images to a separate text file. This
    # could be used by stage 3 to identify the photo that will be registered to
    # the volume.
    imfiles = "\n".join(images)
    logger.info(f"Brain slice images in sampling order "
                f"(starting from the most intact): \n{imfiles}")
    with open(p.order, "w") as fp:
        fp.writelines(imfiles)

    # <--- Parallel computations --->
    logger.info("Starting pairwise registrations...")

    # Perform pairwise registrations using multiple processes with logging
    outputdir = os.path.dirname(p.sites)
    jobs = enumerate(zip(images[:-1], images[1:]))
    worker = partial(_worker_func, outputdir=outputdir, p=p)
    n_cpu = max(min(int(p.cpu) % mp.cpu_count(), len(images) - 1), 1)
    n_cpu = 1 if p.debug else n_cpu
    if n_cpu > 1:
        queue = mp.Queue(-1)
        listener = mp.Process(target=listener_process, args=(queue, p))
        listener.start()
        pool = mp.Pool(n_cpu, initializer=worker_initialiser,
                       initargs=(queue,), maxtasksperchild=1)
        import threadpoolctl
        # Force single-threading in multi-CPU setting
        with threadpoolctl.threadpool_limits(1, user_api="blas"):
            sites = pool.map(worker, jobs)

    else:
        import threading, queue
        queue = queue.Queue(-1)
        listener = threading.Thread(target=listener_process, args=(queue, p))
        listener.start()
        worker_initialiser(queue)
        sites = map(worker, jobs)
    sites = [site for site in sites if site is not None]

    # Close any open workers
    if n_cpu > 1:
        pool.terminate()
        pool.join()

    # Close listener process or thread
    queue.put_nowait(None)
    listener.join()

    logger.info("Finished pairwise registrations.")
    # <--- End of parallel computations --->

    # Load inverse transformations
    invtgs = [tirl.load(os.path.join(outputdir, f"chain{i}.tg"))
              for i in range(len(images) - 1)]

    # Map site coordinates to the intact image (first one in the list)
    mapped_sites = [sites[0]]  # the first set of sites need no mapping
    logger.info(f"Pairwise registration No. 1 identified the "
                f"following insertion sites: \n{sites[0]}")
    for index in range(1, len(images) - 1):
        if index >= len(sites):
            break
        site = reduce(add, invtgs[:index][::-1], None).map(sites[index])
        logger.info(f"Pairwise registration No. {index + 1} identified the "
                    f"following insertion sites: \n{site}")
        mapped_sites.append(site)

    mapped_sites = np.vstack(mapped_sites)

    # Create figure with dots on the intact image highlighting insertion sites
    fig, ax = plt.subplots(1, 1)
    ax.imshow(TImage(images[0], order=VOXEL_MAJOR).data)
    y, x = mapped_sites.T
    ax.scatter(x, y, marker="x", s=40, color="r")
    plt.savefig(os.path.join(outputdir, "insertion_sites.png"))
    plt.close(fig)
    logger.info("Created insertion site figure.")
    return mapped_sites


def listener_process(queue, args):
    args = AttrMap(args)

    # Create logger
    logger = create_logger(args.logfile, paramlogfile=args.paramlogfile,
                           verbose=args.verbose, mode="a")

    # Forward logs from workers to the main logger
    while True:
        try:
            record = queue.get()
            if record is None:  # tells the listener to quit
                break
            logger.handle(record)
        except Exception:
            import sys, traceback
            print("Error while logging from worker process:", file=sys.stderr)
            traceback.print_exc(file=sys.stderr)


def worker_initialiser(queue):
    """
    Initialises worker process to allow safe logging from multiple simultaneous
    workers.

    """
    import logging.handlers
    h = logging.handlers.QueueHandler(queue)
    h.setLevel(1)
    name = f"Process-{mp.current_process().pid}"
    root = logging.getLogger(f"find_sites.{name}")
    # Avoid duplicate logs on repeated initialisation of the same process
    root.handlers = []
    root.addHandler(h)
    root.setLevel(1)


def _worker_func(job, outputdir, p):
    index, (intact, sampled) = job
    p = AttrMap(p)
    name = f"Process-{mp.current_process().pid}"
    # Load the process-specific logger instance
    logger = logging.getLogger(f"find_sites.{name}")
    logger.debug(f"Worker started: {name}")

    # Initialise the images
    i = TImage(intact, dtype=ts.DEFAULT_FLOAT_TYPE).reduce_tensors(copy=True)
    i.resolution = p.resolution
    i.centralise()
    s = TImage(sampled, dtype=ts.DEFAULT_FLOAT_TYPE).reduce_tensors(copy=True)
    s.resolution = p.resolution
    s.centralise()

    # Perform rotation search, followed by rigid + scale optimisation
    tx_rotation = TxRotation2D(0.0, mode="deg", name="rotation")
    tx_scale = TxScale(1.0, 1.0, name="scale")
    tx_translation = TxTranslation(0.0, 0.0, name="translation")
    i.domain.chain += Chain(tx_rotation, tx_scale, tx_translation)
    if not p.norotsearch:
        logger.info("Starting linear registration...")
        rapid_alignment(fixed=i, moving=s, logger=logger, **OPTIONS_ROTSEARCH)

    if not p.linear:
        # Perform non-linear registration
        field = TField(i.domain[:])
        i.domain.chain.append(TxDisplacementField(field, mode=NL_ABS))
        logger.info("Starting non-linear registration...")
        diffreg2d(i, s, logger=logger, **OPTIONS_NONLINEAR)

        # Reduce the density of the non-linear transformation
        warp = i.domain.chain[-1]
        i.domain.chain[-1] = warp.resample(warp.domain.resize(0.25))

    # Identify insertion sites
    sites = identify_sites(i, s, p, index)

    # Save voxel-to-voxel transformation chains
    # (they are saved because they are hard to pickle at the moment)
    chainfile = os.path.join(outputdir, f"chain{index}.tg")
    tx = TransformationGroup(*s.domain.chain, *i.domain.chain.inverse())
    tx.save(chainfile, overwrite=True)

    return sites


def rapid_alignment(fixed, moving, logger=None, **options):

    q = AttrMap(options)
    # Part 1: coarse search for N best orientations

    # Coarse search at a predefined scale
    fixed.resample(float(q.scale), copy=False)
    moving.resample(float(q.scale), copy=False)
    step = radians(q.coarse)
    tx_rotation = fixed.domain.chain["rotation"]
    lb, ub = tx_rotation.parameters.get_bounds()[0]
    rotations = np.arange(lb, ub, step)
    costvals = []
    for i, angle in enumerate(rotations):
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_bounds((angle - step / 2, angle + step / 2))
        tx_rotation.parameters.unlock()
        cost = CostMI(moving, fixed, normalise=True, bins=32)()
        logger.debug(f"{degrees(angle)} deg: {cost}")
        costvals.append([cost, angle])
    else:
        costvals = np.asarray(costvals)

    # Test the best N initial rotations
    n_best = max(min(q.n_best, len(rotations)), 1)
    best_angles = costvals[np.argsort(costvals[:, 0]), 1][:n_best].ravel()
    logger.info(f"The {n_best} best initialisation angles: "
                f"{np.rad2deg(best_angles)} deg")

    # Part 2: fine-tune the rotation parameter by simultaneous additional
    # optimisation of translation and scaling, starting from the best three
    # orientations.
    tx_scale = fixed.domain.chain["scale"]
    tx_translation = fixed.domain.chain["translation"]
    og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
    scale_orig = tx_scale.parameters.parameters.copy()
    translation_orig = tx_scale.parameters.parameters.copy()
    costvals = []
    for angle in best_angles:
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_bounds((angle - step / 2, angle + step / 2))
        tx_rotation.parameters.unlock()
        tx_scale.parameters.parameters[:] = scale_orig
        tx_scale.parameters.set_bounds(scale_orig - 0.2, scale_orig + 0.2)
        tx_scale.parameters.unlock()
        tx_translation.parameters.parameters[:] = translation_orig
        tx_translation.parameters.set_bounds(
            translation_orig - 10.0, translation_orig + 10.0)
        tx_translation.parameters.unlock()
        # Rescale to the same predefined resolution as above
        fixed.resample(float(q.scale), copy=False)
        moving.resample(float(q.scale), copy=False)
        # Set cost function
        # cost = CostMIND(moving, fixed, maskmode="and", normalise=False)
        cost = CostMI(moving, fixed, maskmode="and", normalise=True)
        # Start optimisation
        logger.info(f"Co-optimising scale and translation at "
                    f"{degrees(angle)} deg...")
        OptNL(og, cost, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              normalised=True, logger=logger)()
        costvals.append((cost(), og.parameters[:]))

    # Find best initialisation based on cost
    best_params = min(costvals, key=lambda r: r[0])[1]
    logger.info(f"Best parameters after rotation search: {best_params}")
    og.set(best_params)
    tx_rotation.parameters.set_bounds(
        (best_params[0] - step / 2, best_params[0] + step / 2))
    tx_rotation.parameters.unlock()

    # Return to full resolution after the rotation search
    fixed.resample(1, copy=False)
    moving.resample(1, copy=False)


def diffreg2d(fixed, moving, logger=None, **options):
    """
    Performs a non-linear registration. The transformation is parameterised as
    a dense displacement field. The cost is MIND, and diffusion regularisation
    is used to create smoothness in the deformation field.

    """
    q = AttrMap(options)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.resample(1 / sc, copy=False)
        moving.resample(1 / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_nonlinear = fixed_smooth.domain.chain[-1]
        # Set cost and regulariser
        cost = CostMIND(moving_smooth, fixed_smooth, sigma=float(q.sigma),
                        truncate=float(q.truncate), kernel=MK_FULL)
        regularisation = DiffusionRegulariser(
            tx_nonlinear, weight=float(q.regweight))
        # Optimise the non-linear transformation
        GNOptimiserDiffusion(
            tx_nonlinear, cost, regularisation, maxiter=int(q.maxiter[i]),
            xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs, visualise=q.visualise,
            logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore the original resolution of the images
        fixed.resample(1, copy=False)
        moving.resample(1, copy=False)


def identify_sites(intact, sampled, p, index):
    """
    Finds potential insertion sites given a registered pair of photographs
    taken before and after the tissue block sampling. At any one stage, there
    might have been more than blocks removed from the 'intact' brain slice.

    """
    p = AttrMap(p)

    # XOR and median filtering of binary images
    sampled = sampled.evaluate(intact.domain)
    intact.reduce_tensors()
    intact.normalise()
    sampled.reduce_tensors()
    sampled.normalise()
    difference = np.bitwise_xor(intact.data > p.threshold,
                                sampled.data > p.threshold)
    if p.debug:
        plt.imshow(difference)
        plt.show()

    # Area filter
    labelimg = label(difference)
    binary = np.zeros_like(difference)
    regions = regionprops(labelimg, difference)
    res = np.max(intact.resolution)
    for region in regions:
        if region.area >= (p.area / (res ** 2)):
            binary[labelimg == region.label] = 1

    if p.debug:
        plt.imshow(binary)
        plt.show()

    # Generate distance map
    difference = median(difference, np.ones((10, 10)))
    distmap = edt(difference)

    # How many blocks are expected? (0: data driven)
    if p.n:
        n = p.n[index] if len(p.n) > 1 else p.n[0]
    else:
        n = 0

    # Use k-Means clustering if a certain number of blocks are expected
    if n > 0:
        # Streak filtering
        distmap[distmap < p.width / res] = 0  # streak filter on distance map
        if p.debug:
            plt.imshow(distmap)
            plt.show()

        # Fit a fixed number of region centroids
        from sklearn.cluster import KMeans
        coords = np.stack(np.meshgrid(
            *tuple(np.arange(dim) for dim in distmap.shape),
            indexing="ij"), axis=-1).reshape((-1, 2))
        km = KMeans(n_clusters=n, random_state=0)
        km.fit(X=coords[distmap.ravel() > 0, :])
        sites = km.cluster_centers_.tolist()

    # Get sites from binary segmentation
    else:
        # Streak filtering
        binary = np.ones_like(distmap, dtype=np.int64)
        binary[distmap < p.width / res] = 0
        if p.debug:
            plt.imshow(binary)
            plt.show()
        # Extract region centroids
        sites = [region.centroid for region in regionprops(label(binary))]

    if sites:
        return np.atleast_2d(sites)
    else:
        return None


def find_sites(args):
    """
    Main program code.

    """
    outputdir = os.path.join(os.path.dirname(args.images[0]), "sites")
    if not os.path.isdir(outputdir):
        os.makedirs(outputdir)
    args.sites = os.path.join(outputdir, "sites.txt")
    args.order = os.path.join(outputdir, "sampling_order.txt")

    p = AttrMap(args.__dict__)
    # p = AttrMap(dict(
    #     resolution=args.resolution, linear=args.linear, width=args.width,
    #     area=args.area, threshold=args.threshold, debug=args.debug,
    #     norotsearch=args.norotsearch, order=args.order, sites=args.sites))

    # Create logger
    args.logfile = os.path.join(outputdir, "find_sites.log")
    args.paramlogfile = os.path.join(outputdir, "paramlog.log")
    if os.path.isfile(args.logfile):
        try:
            os.remove(args.logfile)
        except:
            print("Could not remove previous log file.")
    logger = create_logger(args.logfile, paramlogfile=args.paramlogfile,
                           verbose=p.verbose, mode="a")

    # Record the start of the program
    logger.critical(f"The program started with the command: "
                    f"{' '.join(sys.argv)}")

    # Sort images based on foreground area
    if p.nosort:
        sorted_images = args.images
    else:
        sorted_images = sort_images(*args.images)

    # Perform parallel pairwise rigid registrations to find sites
    # and conclude run
    try:
        sites = pairwise_registrations(p, *sorted_images, logger=logger)
    except Exception as exc:
        logger.error(exc.args)
        raise

    if sites is not None:
        np.savetxt(args.sites, sites, fmt='%1.3f')
        logger.fatal(f"Insertion site search complete. "
                     f"Found {sites.shape[0]} site(s).")
    else:
        logger.fatal(f"Insertion site search complete. No site was found.")


def create_cli():
    """
    Creates command-line interface.

    """
    usage = "find_sites [--cpu=-1] <image1> <image2> [images ...] "
    descr = "Finds tissue block insertion sites in a brain section photograph."
    parser = argparse.ArgumentParser(
        prog="find_sites", usage=usage, description=descr)

    parser.add_argument("images", nargs="+", type=str, metavar="images",
                        default=None, help="Sampled brain slice photographs.")
    parser.add_argument("--norotsearch", default=False,
                        action="store_true", required=False,
                        help="Do not use rotation search in the registration "
                             "between consecutive slices. Default: False "
                             "(use it)")
    parser.add_argument("--linear", default=False,
                        action="store_true", required=False,
                        help="Do not use non-linear registration between "
                             "consecutive slices. Default: False (use it)")
    parser.add_argument("--debug", default=False,
                        action="store_true", required=False,
                        help="Show intermediate results step by step.")
    parser.add_argument("--width", metavar="x", default=4, type=float,
                        required=False,
                        help="Remove streaks that are narrower than x mm. "
                             "Default: 6 mm")
    parser.add_argument("--resolution", metavar="x", type=float, default=0.050,
                        required=False, help="Resolution of the input images: "
                                             "x mm/px. Default: 0.05 mm/px")
    parser.add_argument("--area", metavar="x", type=float, default=100,
                        required=False,
                        help="Only detect blocks that are larger than x mm2. "
                             "Default: 100 mm2")
    parser.add_argument("-n", metavar="N", type=int, default=0, nargs="+",
                        required=False,
                        help="Look for N0, N1, ... number of sites between "
                             "consecutive image pairs. Default: 0 (not fixed)")
    parser.add_argument("--nosort", default=False, action="store_true",
                        required=False,
                        help="Do not sort images. Handle them in the order as "
                             "they are specified.")

    parser.add_argument("--threshold", type=float, default=0.1, metavar="x",
                        required=False, help="Fractional lower intensity "
                                             "for image thresholding. "
                                             "Default: 0.1")
    parser.add_argument("--cpu", type=int, default=-1, metavar="N",
                        required=False,
                        help="Parallelise across this number of CPU cores. "
                             "Default: -1 (all cores).")
    parser.add_argument("-v", "--verbose", default=False,
                        action="store_true", required=False,
                        help="Print status messages to the command line.")
    return parser


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        find_sites(parser.parse_args(args))
    else:
        parser.print_help()


# Program execution starts here
if __name__ == "__main__":
    main(*sys.argv[1:])
