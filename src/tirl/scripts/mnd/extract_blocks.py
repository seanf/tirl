#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 9 June 2020


# SHBASECOPYRIGHT


from tirl.scripts.mnd import __version__
__tirlscript__ = True


# DEPENDENCIES

import os
import sys
import argparse
import numpy as np
from tirl.timage import TImage
from skimage.color import rgb2lab
from sklearn.cluster import KMeans
from skimage.measure import label, regionprops


# DEFINITIONS

OPTIONS = {
    "distance": 0.25,
    "channel": 2
}


# IMPLEMENTATION

def extract_blocks(args):
    """ Main program code. """

    # Resample image to 1/16 resolution
    img = TImage(args.image).tensors[:3]
    img.resample(args.scale, copy=False)

    # Find tissue-like pixels by clustering in b channel (Lab color space)
    lab_b = rgb2lab(img.data)[..., OPTIONS["channel"]]
    X = (lab_b - np.mean(lab_b)) / np.std(lab_b)
    km = KMeans(n_clusters=args.k, random_state=0)
    segmentation = km.fit_predict(X.reshape(-1, 1))
    block_cluster = np.argmax(km.cluster_centers_)
    segmentation[segmentation != block_cluster] = 0
    segmentation[segmentation > 0] = 1
    segmentation = segmentation.reshape(lab_b.shape)

    # Label blocks
    labelimg = label(segmentation, background=0)
    regions = regionprops(labelimg, lab_b)

    # Extract regions from full-resolution image
    img.resample(1, copy=False)
    h, w = img.vshape
    block_no = 0
    for region in regions:
        if region.label == 0:
            continue
        if region.area < args.area:
            continue
        radius = np.multiply(OPTIONS["distance"], img.vshape)
        centre = np.divide(img.vshape, 2)
        pos = np.divide(region.centroid, args.scale)
        dist = np.abs(np.subtract(centre, pos))
        if np.all(dist <= radius):

            block_no += 1

            mask = np.zeros_like(segmentation)
            mask[labelimg == region.label] = 1
            mask = TImage.fromarray(mask)
            mask.resample(img.domain, copy=False)
            mask.data[mask.data > 0.5] = 1
            mask.data[mask.data <= 0.5] = 0

            minrow, mincol, maxrow, maxcol = region.bbox
            minrow = int(minrow / args.scale)
            mincol = int(mincol / args.scale)
            maxrow = int(maxrow / args.scale)
            maxcol = int(maxcol / args.scale)
            height = maxrow - minrow
            width = maxcol - mincol
            left, right, top, bottom = args.margin

            blockimg = np.zeros(
                (int(height * (1 + top + bottom)),
                 int(width * (1 + left + right)), 3), dtype=img.dtype)
            top = int(height * top)
            left = int(width * left)
            blockimg[top:top+height, left:left+width] = \
                img.data[minrow:maxrow, mincol:maxcol] \
                * mask[minrow:maxrow, mincol:maxcol, np.newaxis]

            # Generate file name
            fname, ext = os.path.splitext(args.image)
            tag = f"_c{block_no:02d}"
            fname = fname.rstrip(".") + tag + f".{ext}"

            # Flip image if needed
            if args.flip:
                fname = fname.replace(tag, tag + "_flip")
                blockimg.data[...] = blockimg.data[:, ::-1]

            # Save file
            blockimg = TImage.fromarray(blockimg, tensor_axes=(2,))
            blockimg.snapshot(fname, overwrite=True)


def create_cli():
    """
    Creates command-line interface.

    """
    descr = "Segments brain slice from dissection photograph."
    parser = argparse.ArgumentParser(prog="extract_blocks", description=descr)

    parser.add_argument("-i", "--image", metavar="file", type=str, default="",
                        required=True, help="Input image.")
    parser.add_argument("-s", "--scale", metavar="factor", type=float,
                        default=0.25, required=False, help="Scaling factor.")
    parser.add_argument("-a", "--area", metavar="px2", type=float,
                        default=1000, required=False, help="Area threshold.")
    parser.add_argument("-m", "--margin", metavar="ratio",
                        type=float, nargs=4, default=[0.25, 0.25, 0.25, 0.25],
                        required=False,
                        help="Relative margins (left, right, top, bottom).")
    parser.add_argument("-k", metavar="clusters", type=int,
                        default=3, required=False, help="Number of clusters.")
    parser.add_argument("--flip", action="store_true", default=False,
                        required=False, help="Flip input horizontally.")

    return parser


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        extract_blocks(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
