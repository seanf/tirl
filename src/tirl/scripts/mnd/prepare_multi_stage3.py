#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 21 June 2020


# SHBASECOPYRIGHT


from tirl.scripts.mnd import __version__
__tirlscript__ = True


# DEPENDENCIES

import os
import sys
import json
import shutil
import numpy as np
from attrdict import AttrMap
from collections import namedtuple

import tirl
from tirl import settings as ts

# DEFINITIONS

config = namedtuple("config", ["cnf", "mri", "origin", "direction", "slices"])


# IMPLEMENTATION

def prepare_multi_stage3(confile, yestoall, tag=""):
    """ Main program code. """

    opts = parse_configuration_file(confile)
    if tag:
        tag = f".{tag}" if not tag.startswith(".") else tag

    for current_slice in opts.slices:
        # Create output directory
        path, fname = os.path.split(current_slice[0])
        fname, ext = os.path.splitext(fname)
        outputdir = os.path.join(path, fname + ".stage3" + tag)

        if os.path.isdir(outputdir) and not yestoall:
            msg = f"{outputdir} already exists. " \
                  f"Would you like to overwrite it? [y/n] "
            if not confirm(msg):
                print(f"{outputdir} was not updated.")
                continue
            else:
                print(f"{outputdir} is set for overwrite.")
                shutil.rmtree(outputdir)

        elif yestoall:
            try:
                shutil.rmtree(outputdir)
            except:
                pass

        try:
            os.makedirs(outputdir)
        except:
            pass

        # Load configuration file prototype
        with open(opts.cnf, "r") as fp:
            cnf = json.load(fp)

        # Adapt the configurations
        cnf = AttrMap(cnf)
        cnf.general.outputdir = outputdir
        cnf.general.paramlogfile = os.path.join(outputdir, "paramlog.log")

        # Slice
        cnf.slice.file = current_slice[0]
        if current_slice[2] is not None:
            cnf.slice.resolution = current_slice[2]
        cnf.slice.mask.file = current_slice[3]

        # MRI volume
        cnf.volume.file = opts.mri[0]
        if opts.mri[1] is not None:
            cnf.volume.resolution = opts.mri[1]
            cnf.volume.usesqform = False
        cnf.volume.mask.file = opts.mri[2]

        # Set up the search slab
        slab = cnf.regparams.stage_1.slab
        slab.centre = opts.origin.tolist()
        slab.normal = opts.direction.tolist()
        slab.offset = current_slice[1]

        # Save configurations to the output directory
        configfile = os.path.join(outputdir, fname + "_config.json")
        with open(configfile, "w") as fp:
            json.dump(dict(cnf), fp, indent=4, sort_keys=True)

        # Create launcher script for convenience
        executable = tirl.home("scripts", "mnd", "slice_to_volume.py")
        # executable = "tirl slice_to_volume"
        script = os.path.join(outputdir, "register.sh")
        with open(script, "w") as fp:
            fp.write("#!" + ts.DEFAULT_SHELL + "\n")
            fp.write(f"python {executable} --config {configfile}\n")
            # fp.write(f"{executable} --config {configfile}\n")
            fp.write("\n")
        os.chmod(script, 0o755)


def confirm(msg):
    while True:
        ans = str(input(msg)).lower()
        if ans in ["yes", "ye", "y"]:
            return True
        elif ans in ["no", "n"]:
            return False


def parse_configuration_file(cf):
    """
    Reads stage-3 configuration file. Returns a dictionary.

    """
    with open(cf, "r") as fp:
        lines = fp.readlines()

    mri = ()
    origin = np.array([0, 0, 0])     # default origin: (0, 0, 0)
    direction = np.array([0, 1, 0])  # default coronal orientation
    cnf = ""
    slices = []

    for line_no, line in enumerate(lines):
        # Remove trailing newline character
        line = line.rstrip("\n")

        # Remove comments and empty lines
        if not line:
            continue
        if line.startswith("#"):
            continue
        if "#" in line:
            line = line[:line.index("#")]

        # Read MRI
        if line.lower().startswith("mri"):
            mri, *ignored = line.split(",")[1:]
            if ignored:
                try:
                    resolution = float(ignored.pop(0))
                except:
                    resolution = None
            else:
                resolution = None
            if ignored:
                mask = ignored.pop(0)
            else:
                mask = None
            assert os.path.isfile(mri), f"Slice image does not exist: {mri}"
            if mask is not None:
                assert os.path.isfile(mask), f"Mask file does not exist: {mask}"
            if resolution is not None:
                assert resolution > 0, \
                    f"Invalid resolution: {resolution}"
            if ignored:
                print(f"Ignored information on line {line_no}: {ignored}")
            mri = (mri, resolution, mask)

        # Read slice
        elif line.lower().startswith("slice"):
            sl, pos, *ignored = line.split(",")[1:]
            if ignored:
                resolution = float(ignored.pop(0))
            else:
                resolution = None
            if ignored:
                mask = ignored.pop(0)
            else:
                mask = None
            assert os.path.isfile(sl), f"Slice image does not exist: {sl}"
            if mask is not None:
                assert os.path.isfile(mask), f"Mask file does not exist: {mask}"
            if resolution is not None:
                assert resolution > 0, \
                    f"Invalid resolution: {resolution}"
            if ignored:
                print(f"Ignored information on line {line_no}: {ignored}")
            slices.append((sl, float(pos), resolution, mask))

        # Read origin (centre point of the reference slice in MRI space)
        elif line.lower().startswith("origin"):
            origin = np.asarray(line.split(",")[1:4], dtype=np.float64)
            ignored = line.split(",")[4:]
            if ignored:
                print(f"Ignored information on line {line_no}: {ignored}")
            origin = np.asarray(origin)
            assert origin.size == 3, "need 3 coordinates for the origin."

        # # Read reference position
        # elif line.lower().startswith("reference"):
        #     ref, *ignored = line.split(",")[1:]
        #     if ignored:
        #         print(f"Ignored information on line {line_no}: {ignored}")
        #     ref = float(ref)

        # Read slicing direction
        elif line.lower().startswith("direction"):
            direction = np.asarray(line.split(",")[1:4], dtype=np.float64)
            ignored = line.split(",")[4:]
            if ignored:
                print(f"Ignored information on line {line_no}: {ignored}")
            direction = np.asarray(direction)
            assert direction.size == 3, "need 3 coordinates for direction."

        # Read configuration file prototype
        elif line.lower().startswith("configuration"):
            cnf, *ignored = line.split(",")[1:]
            if ignored:
                print(f"Ignored information on line {line_no}: {ignored}")
            assert os.path.isfile(cnf), "Configuration file does not exist."

        else:
            print(f"Ignored information on line {line_no}: {line}")

    c = config(cnf=cnf, mri=mri, origin=origin, direction=direction,
               slices=slices)
    return c


def main(*args):
    """ Main program code. """

    usage = "prepare_multi_stage3 stage3.con [-y] [-tag <name>]"
    if args:
        y = "-y" in args
        try:
            ix = sys.argv.index("-tag")
            name = str(sys.argv[ix + 1])
        except:
            name = ""
        finally:
            prepare_multi_stage3(confile=args[0], yestoall=y, tag=name)
    else:
        print(usage)


if __name__ == "__main__":
    main(*sys.argv[1:])
