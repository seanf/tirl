#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 21 June 2020


# SHBASECOPYRIGHT


"""
This package contains common routines that are used in the MND
scripts/pipelines based on TIRL.

The package includes the following modules:

    general:    housekeeping, running stages
    io:         loading and saving files
    image:      TImage manipulations

"""

__version__ = "2020.7.1"

from tirl.scripts.mnd import general