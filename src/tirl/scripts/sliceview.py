#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 23 June 2020


# SHBASECOPYRIGHT


__version__ = "2020.7.1"
__tirlscript__ = True


# DEPENDENCIES

import os
import sys
import argparse


# TIRL IMPORTS

import tirl


# IMPLEMENTATION

def sliceview(args):
    """
    Main program code.

    """
    from mayavi import mlab
    from scipy.spatial import Delaunay

    images = []
    for imfile in args.slice:
        if os.path.isfile(imfile):
            images.append(tirl.load(imfile))
        else:
            raise FileNotFoundError(f"Image file not found: {imfile}")

    fig = mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0.5, 0.5, 0.5))

    # Volume
    if os.path.isfile(args.volume):
        mri = tirl.load(args.volume)
        points = mri.domain.get_voxel_coordinates()
        x, y, z = points.T
        x = x.reshape(mri.data.shape)
        y = y.reshape(mri.data.shape)
        z = z.reshape(mri.data.shape)
        mlab.volume_slice(x, y, z, mri.data, colormap="gray")
    else:
        mri = None

    # Slices
    for img in images:
        img = img.tensors[0]
        tri = Delaunay(img.domain.get_voxel_coordinates())
        points = img.domain.get_physical_coordinates()
        if mri is not None:
            points = mri.domain.map_physical_coordinates(points)
        tmesh = mlab.triangular_mesh(*points.T, tri.simplices, colormap="gray")
        tmesh.mlab_source.scalars = img.data.reshape(-1)
        tmesh.actor.mapper.scalar_visibility = True

    mlab.show()


def create_cli():
    """
    Creates command-line interface.

    """
    usage = """./sliceview --slice <slice> [slices ...] --volume <volume>"""
    descr = "View registered slices in a 3D window with the volume."

    parser = argparse.ArgumentParser(
        prog="sliceview", usage=usage, description=descr)
    parser.add_argument("--slice", metavar="timg", help="Registered 2D TImage.",
                        nargs="+", required=False, type=str)
    parser.add_argument("--volume", metavar="timg", help="3D TImage.",
                        required=False, type=str, default="")

    return parser


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        sliceview(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
