#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# PACKAGE IMPORTS

from tirl.transformations.linear.linear import TxLinear
# from tirl import expose_package_contents

# The default libraries must be imported first.
# from tirl.transformations.linear.translation import TxTranslation
# from tirl.transformations.linear.scale import TxScale
# from tirl.transformations.linear.shear import TxShear3D
# from tirl.transformations.linear.rotation import TxRotation
# from tirl.transformations.linear.rotation import TxRotation2D
# from tirl.transformations.linear.rotation import TxRotation3D
# from tirl.transformations.linear.rigid import TxRigid
# from tirl.transformations.linear.affine import TxAffine

# Expose all other linear Transformation objects at the module level
# Note: this routine imports the Transformation base class, and every
# subclass thereof from all modules within the "transformations.linear" package.
# expose_package_contents(
#     baseclass=TxLinear, pkg="tirl.transformations.linear",
#     path=__path__, globals=globals())
