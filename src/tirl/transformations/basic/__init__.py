#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# PACKAGE IMPORTS

# from tirl import expose_package_contents
# from tirl.transformations.transformation import Transformation

# The default libraries must be loaded first.
# from tirl.transformations.basic.identity import TxIdentity
# from tirl.transformations.basic.embed import TxEmbed, TxReduce

# Expose all other Transformation objects at the module level
# Note: this routine imports the Transformation base class, and every
# subclass thereof from all modules within the "transformations.basic" package.
# expose_package_contents(
#     baseclass=Transformation, pkg="tirl.transformations.basic", path=__path__,
#     globals=globals())
